	double precision pi
	double precision k
	double precision h
	double precision c
	double precision sigma

	parameter(pi=3.141592653D+0)
        parameter(k=1.380649D-23)    ! J/K
	parameter(h=6.62607015D-34)  ! J.s
	parameter(c=299792458D+0)    ! m/s
	parameter(sigma=2.0D+0*pi**5.0D+0*k**4.0D+0
     &	            /(15.0D+0*h**3.0D+0*c**2.0D+0))      ! W/m^2/K^4