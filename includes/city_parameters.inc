	double precision bridge_side_width
	double precision bridge_deck_height_above_water
	double precision bridge_side_height_above_water
	double precision bridge_overlapping_distance
	double precision river_road_distance
	double precision river_dirtroad_distance
	double precision major_road_width
	double precision normal_road_width
	double precision minor_road_width
	double precision dirt_road_width
	double precision small_street_width
	double precision pavement_width
	double precision pavement_thickness
	double precision lawn_thickness
	integer Nsector_min,Nsector_max
	double precision sp_waterzone_length
	double precision sp_waterzone_width
	double precision sp_edge_width
	double precision sp_thickness
	double precision roof_thickness
	double precision mu_lv1rr_radius
	double precision sigma_lv1rr_radius
	double precision mu_lv2rr_radius
	double precision sigma_lv2rr_radius
	double precision mu_csquare_dim
	double precision sigma_csquare_dim
	double precision minimum_segment_length
	double precision maximum_aspect_ratio
	
	character*(Nchar_mx) default_map_material
	character*(Nchar_mx) central_plazza_material
	character*(Nchar_mx) bridge_deck_material
	character*(Nchar_mx) bridge_side_material
	character*(Nchar_mx) white_bands_material
	character*(Nchar_mx) water_zones_material
	character*(Nchar_mx) roads_material
	character*(Nchar_mx) tracks_material
	character*(Nchar_mx) streets_material
	character*(Nchar_mx) lawns_material
	character*(Nchar_mx) swimmingpools_border_material
	character*(Nchar_mx) park_hedges_material
	character*(Nchar_mx) unused_zones_material

	parameter(bridge_side_width=1.0D+0)
	parameter(bridge_deck_height_above_water=2.00D+0)
	parameter(bridge_side_height_above_water=2.50D+0)
	parameter(bridge_overlapping_distance=15.0D+0)
	parameter(river_road_distance=30.0D+0)
	parameter(river_dirtroad_distance=2.0D+0)
	parameter(major_road_width=20.0D+0)
	parameter(normal_road_width=10.0D+0)
	parameter(minor_road_width=6.0D+0)
	parameter(dirt_road_width=2.0D+0)
	parameter(small_street_width=3.0D+0)
	parameter(pavement_width=1.20D+0)
	parameter(pavement_thickness=5.0D-2)
	parameter(lawn_thickness=1.0D-1)
	parameter(Nsector_min=8)
	parameter(Nsector_max=12)
	parameter(sp_waterzone_length=10.0D+0)
	parameter(sp_waterzone_width=5.0D+0)
	parameter(sp_edge_width=3.0D-1)
	parameter(sp_thickness=1.50D-1)
	parameter(roof_thickness=2.0D-1)
	parameter(mu_lv1rr_radius=200.0D+0)
	parameter(sigma_lv1rr_radius=20.0D+0)
	parameter(mu_lv2rr_radius=100.0D+0)
	parameter(sigma_lv2rr_radius=10.0D+0)
	parameter(mu_csquare_dim=50.0D+0)
	parameter(sigma_csquare_dim=5.0D+0)
	parameter(minimum_segment_length=1.0D+0)
	parameter(maximum_aspect_ratio=15.0D+0)
	
	parameter(default_map_material='grass')
	parameter(central_plazza_material='fire_brick')
	parameter(bridge_deck_material='bitumen')
	parameter(bridge_side_material='sandstone')
	parameter(white_bands_material='white_paint')
	parameter(water_zones_material='blue_water')
	parameter(roads_material='bitumen')
	parameter(tracks_material='sand')
	parameter(streets_material='concrete')
	parameter(lawns_material='grass')
	parameter(swimmingpools_border_material='sandstone')
	parameter(park_hedges_material='dark_green')
	parameter(unused_zones_material='medium_dirt')