	integer Ndim_mx
	integer Ndist_mx
	integer Nproba_mx
	integer Niter_mx
	integer Nppt_mx
	integer Ncontour_mx
	integer Nppc_mx
	integer Nbridge_mx
	integer Nroad_mx
	integer Nmat_mx
	integer Ntree_mx
	integer Nv_so_mx
	integer Nf_so_mx
	integer Nchar_mx

	parameter(Ndim_mx=3)
	parameter(Ndist_mx=1000)
	parameter(Nproba_mx=100)
	parameter(Niter_mx=100)
        parameter(Nppt_mx=100)
        parameter(Ncontour_mx=100)
        parameter(Nppc_mx=100)
        parameter(Nbridge_mx=10)
        parameter(Nroad_mx=100)
        parameter(Nmat_mx=100)
        parameter(Ntree_mx=1000)
        parameter(Nv_so_mx=1000000)
        parameter(Nf_so_mx=1000000)
	parameter(Nchar_mx=1000)
