c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine populate_sector(dim,
     &     mapXsize,mapYsize,
     &     Nzone,Nbuilding,Nsp,Nsector,alpha1,xcenter,
     &     contour_file,
     &     ilevel,isector,
     &     Ncontour,Nppc,contour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type,enable_t4b,
     &     draw_wah,
     &     Adistrict,Aterrain,Astreet,Abuilding,
     &     Acourtyard,Apark,Awah)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'city_parameters.inc'
      include 'distribution_parameters.inc'
      include 'constants.inc'
c
c     Purpose: to generate 3D data for a given sector
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nzone: number of zones
c       + Nbuilding: number of buildings
c       + Nsp: number of swimming pools
c       + Nsector: number of angular sectors
c       + alpha1: angle for generating level1 ring road contour
c       + xcenter: position of the ring center
c       + contour_file: file holding information about all contours (needed for later placing trees)
c       + ilevel: level index [1,2]
c       + isector: index of the sector
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c       + contour_type: type of contour
c         - 0: surrounding contour (contour index 1)
c         - 1: building only
c         - 2: terrain + building
c         - 3: terrain only (parks)
c         - 4: no man's land
c       + enable_t4b: set to true if type 4 buildings have to be enabled
c       + draw_wah: set to true if wall and hedges have to be drawn
c       + Adistrict: total area of districts [m²]
c       + Aterrain: total area of terrains [m²]
c       + Astreet: total area of streets [m²]
c       + Abuilding: total area of buildings [m²]
c       + Acourtyard: total area of courtyards [m²]
c       + Apark: total area of parks [m²]
c       + Awah: total area of walls and hedges [m²]
c     
c     Output:
c       + updated values for: Nzones, Nbuildings, areas...
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nzone
      integer Nbuilding
      integer Nsp
      integer Nsector
      double precision alpha1
      double precision xcenter(1:Ndim_mx)
      character*(Nchar_mx) contour_file
      integer ilevel
      integer isector
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer contour_type(1:Ncontour_mx)
      logical enable_t4b
      logical draw_wah
      double precision Adistrict
      double precision Aterrain
      double precision Astreet
      double precision Abuilding
      double precision Acourtyard
      double precision Apark
      double precision Awah
c     temp
      character*(Nchar_mx) filename
      integer Nmat_in
      character*(Nchar_mx) mat_in(1:Nmat_mx)
      double precision cdf_in(0:Nproba_mx)
      integer Nmat_ext
      character*(Nchar_mx) mat_ext(1:Nmat_mx)
      double precision cdf_ext(0:Nproba_mx)
      integer Nmat_roof
      character*(Nchar_mx) mat_roof(1:Nmat_mx)
      double precision cdf_roof(0:Nproba_mx)
      integer Nmat_cy
      character*(Nchar_mx) mat_cy(1:Nmat_mx)
      double precision cdf_cy(0:Nproba_mx)
      integer Nmat_zt
      character*(Nchar_mx) mat_zt(1:Nmat_mx)
      double precision cdf_zt(0:Nproba_mx)
c     temp
      logical debug
      integer icontour,ic,i,j,iseg,i1,i2,izone,imat,zone_type
      double precision thickness,tafubb
      character*(Nchar_mx) zone_mat
      integer Ncontour_tmp
      integer Nppc_tmp(1:Ncontour_mx)
      double precision contour_tmp(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer Ncontour_zone
      integer Nppc_zone(1:Ncontour_mx)
      double precision contour_zone(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer Ncontourz
      integer Nppcz(1:Ncontour_mx)
      double precision contourz(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
      integer Npb
      double precision ctrb(1:Nppc_mx,1:Ndim_mx-1)
      integer Npb2
      double precision ctrb2(1:Nppc_mx,1:Ndim_mx-1)
      double precision x(1:4,1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision tmp2(1:Ndim_mx)
      double precision x0(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P(1:Ndim_mx)
      double precision xsp1(1:Ndim_mx)
      double precision xsp2(1:Ndim_mx)
      double precision xsp3(1:Ndim_mx)
      double precision xsp4(1:Ndim_mx)
      double precision Plast(1:Ndim_mx)
      double precision bl(1:Ndim_mx-1)
      double precision u12(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision lseg(1:4),d,d1,d2,l1,l2,l3,l4
      double precision d12,d23,d34,d41,dmin
      double precision zone_height,zone_width
      double precision xmin,xmax,ymin,ymax
      double precision pbuilding_lv1,pbuilding_lv2
      integer building_type,main_dir,idx
      integer Nbt_lv1,Nbt_lv2
      double precision proba_bt_lv1(1:Nproba_mx)
      double precision cdf_bt_lv1(0:Nproba_mx)
      character*(Nchar_mx) mat_bt_lv1(1:Nmat_mx)
      double precision proba_bt_lv2(1:Nproba_mx)
      double precision cdf_bt_lv2(0:Nproba_mx)
      character*(Nchar_mx) mat_bt_lv2(1:Nmat_mx)
      double precision pbuilding
      integer Nppt,Nppt2
      double precision track(1:Nppt_mx,1:2)
      double precision track2(1:Nppt_mx,1:2)
      logical position_is_clear
      double precision scale
      logical keep_going,keep_searching,inside,inside1,file_exist
      integer Niter,variant
c     temp
      integer Nfloor
      double precision Hfloor
      integer Ncubes(1:Ndim_mx)
      double precision length(1:Ndim_mx)
      double precision e_in
      double precision e_ext
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision fwall
      double precision ffloor
      double precision e_glass
      double precision roof_height
      character*(Nchar_mx) internal_walls_mat
      character*(Nchar_mx) external_walls_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      double precision r
      logical possible_sp,swimming_pool,success
      double precision sp_deltax,sp_deltay,edge_width
      double precision width_min,width_max
      logical intersection,intersection_found,self_intersection
      character*3 str3
      double precision Ln1,Ln2
      integer Ncx,Ncy
      double precision area
      double precision main_area
      double precision secondary_area
      double precision total_area
      double precision Awt,Awi,Awc,Hbuilding
      double precision fwall_max,wwf
c     functions
      double precision arctan
c     label
      character*(Nchar_mx) label
      label='subroutine populate_sector'

      debug=.false.
c     -----------------------------------------------------
c     Reading probability sets for materials
c     -----------------------------------------------------
      glass_panes_mat='standard_glass'
      if (ilevel.eq.1) then
c     Materials on external side of walls (in contact with the atmosphere)
         filename='./data/internal_materials_level1.in'
         call read_materials_file(filename,
     &        Nmat_in,mat_in,cdf_in)
c     Materials on internal side of walls (inside rooms)
         filename='./data/external_materials_level1.in'
         call read_materials_file(filename,
     &        Nmat_ext,mat_ext,cdf_ext)
c     Materials for roofs
         filename='./data/roof_materials_level1.in'
         call read_materials_file(filename,
     &        Nmat_roof,mat_roof,cdf_roof)
c     Materials for courtyards
         filename='./data/courtyard_materials_level1.in'
         call read_materials_file(filename,
     &        Nmat_cy,mat_cy,cdf_cy)
c     Materials for contour delimiters (walls and hedges)
         filename='./data/hedge_and_wall_materials_level1.in'
         call read_materials_file(filename,
     &        Nmat_zt,mat_zt,cdf_zt)
      else if (ilevel.eq.2) then
c     Materials on external side of walls (in contact with the atmosphere)
         filename='./data/internal_materials_level2.in'
         call read_materials_file(filename,
     &        Nmat_in,mat_in,cdf_in)
c     Materials on internal side of walls (inside rooms)
         filename='./data/external_materials_level2.in'
         call read_materials_file(filename,
     &        Nmat_ext,mat_ext,cdf_ext)
c     Materials for roofs
         filename='./data/roof_materials_level2.in'
         call read_materials_file(filename,
     &        Nmat_roof,mat_roof,cdf_roof)
c     Materials for courtyards
         filename='./data/courtyard_materials_level2.in'
         call read_materials_file(filename,
     &        Nmat_cy,mat_cy,cdf_cy)
c     Materials for contour delimiters (walls and hedges)
         filename='./data/hedge_and_wall_materials_level2.in'
         call read_materials_file(filename,
     &        Nmat_zt,mat_zt,cdf_zt)
      else
         call error(label)
         write(*,*) 'ilevel=',ilevel
         stop
      endif
c
c     -----------------------------------------------------
c     Reading from file:
c     + the global probability to draw a building
c     + the probability distribution for building types 0-4 for type 2-3 sector
c     The probability to draw a type 4 building on a type 1 sector
c     is just the global probability to draw a building (on any kind of sector)
c     because only type 4 buildings can be placed on type 1 sectors
c     -----------------------------------------------------
c     Level 1 sectors
      filename='./data/buildings_probabilities_level1.in'
      call read_building_probabilities(filename,enable_t4b,
     &     pbuilding_lv1,Nbt_lv1,mat_bt_lv1,cdf_bt_lv1)
      if (Nbt_lv1.ne.5) then
         call error(label)
         write(*,*) 'Number of elements in ',trim(filename),
     &        ' is:',Nbt_lv1
         write(*,*) 'is supposed to be 5 (building type 0-4)'
         stop
      endif
c     Level 2 sectors
      filename='./data/buildings_probabilities_level2.in'
      call read_building_probabilities(filename,enable_t4b,
     &     pbuilding_lv2,Nbt_lv2,mat_bt_lv2,cdf_bt_lv2)
      if (Nbt_lv2.ne.5) then
         call error(label)
         write(*,*) 'Number of elements in ',trim(filename),
     &        ' is:',Nbt_lv2
         write(*,*) 'is supposed to be 5 (building type 0-4)'
         stop
      endif
c
      do icontour=1,Ncontour               
         if (contour_type(icontour).eq.0) then ! contour_type=0: pavement
            thickness=pavement_thickness
            Ncontour_tmp=1
            do ic=1,1
               Nppc_tmp(ic)=Nppc(ic)
               do i=1,Nppc(ic)
                  do j=1,dim-1
                     contour_tmp(ic,i,j)=contour(ic,i,j)
                  enddo         ! j
               enddo            ! i
            enddo               ! ic
            zone_mat=trim(streets_material)
            Nzone=Nzone+1
            call record_zone_definition_file(dim,Nzone,
     &           thickness,Ncontour_tmp,Nppc_tmp,contour_tmp,zone_mat)
         endif                  ! contour_type=0
c-----------------------------------------------------------------------------
         if (contour_type(icontour).eq.1) then ! contour_type=1: buildings occupy the whole area (building_type=4 only)
            if (enable_t4b) then ! enable_t4b=T
               if (ilevel.eq.1) then
                  pbuilding=pbuilding_lv1
               else if (ilevel.eq.2) then
                  pbuilding=pbuilding_lv2
               endif
            else                ! enable_t4b=F
               pbuilding=0.0D+0
            endif               ! enable_t4b = T or F
            call random_gen(r)
            if (r.gt.pbuilding) then
               contour_type(icontour)=3 ! the contour has been changed to a park
               goto 333
            endif
c     otherwise a (type 4) building has to be generated
            thickness=lawn_thickness
            Ncontour_tmp=1
            Nppc_tmp(1)=Nppc(icontour)
            do i=1,Nppc(icontour)
               do j=1,dim-1
                  contour_tmp(1,i,j)=contour(icontour,i,j)
               enddo            ! j
            enddo               ! i
            call choose_integer(Nmat_cy,cdf_cy,i)
            zone_mat=trim(mat_cy(i))
            Nzone=Nzone+1
            call record_zone_definition_file(dim,Nzone,
     &           thickness,Ncontour_tmp,Nppc_tmp,contour_tmp,zone_mat)
c     
            Nbuilding=Nbuilding+1
c     ----------------------------------------------------
c     Number of floors
c     ----------------------------------------------------
            if (ilevel.eq.1) then
               if (building4_level1_Nfloor_dist.eq.1) then ! fixed value
                  Nfloor=building4_level1_Nfloor_N
               else if (building4_level1_Nfloor_dist.eq.2) then ! uniform distribution
                  call choose_uniform_integer(
     &                 building4_level1_Nfloor_Nmin,
     &                 building4_level1_Nfloor_Nmax,
     &                 Nfloor)
               else if (building4_level1_Nfloor_dist.eq.3) then ! normal distribution
                  call choose_normal_integer(
     &                 building4_level1_Nfloor_mu,
     &                 building4_level1_Nfloor_sigma,
     &                 Nfloor)
               endif            ! value of building4_level1_Nfloor_dist
            else if (ilevel.eq.2) then
               if (building4_level2_Nfloor_dist.eq.1) then ! fixed value
                  Nfloor=building4_level2_Nfloor_N
               else if (building4_level2_Nfloor_dist.eq.2) then ! uniform distribution
                  call choose_uniform_integer(
     &                 building4_level2_Nfloor_Nmin,
     &                 building4_level2_Nfloor_Nmax,
     &                 Nfloor)
               else if (building4_level2_Nfloor_dist.eq.3) then ! normal distribution
                  call choose_normal_integer(
     &                 building4_level2_Nfloor_mu,
     &                 building4_level2_Nfloor_sigma,
     &                 Nfloor)
               endif            ! value of building4_level2_Nfloor_dist
            endif
c     ----------------------------------------------------
c     Height of floors
c     ----------------------------------------------------
            if (ilevel.eq.1) then
               if (building4_level1_Hfloor_dist.eq.1) then ! fixed value
                  Hfloor=building4_level1_Hfloor_H
               else if (building4_level1_Hfloor_dist.eq.2) then ! uniform distribution
                  call uniform(
     &                 building4_level1_Hfloor_Hmin,
     &                 building4_level1_Hfloor_Hmax,
     &                 Hfloor)
               else if (building4_level1_Hfloor_dist.eq.3) then ! normal distribution
                  call sample_normal(
     &                 building4_level1_Hfloor_mu,
     &                 building4_level1_Hfloor_sigma,
     &                 Hfloor)
               endif            ! value of building4_level1_Hfloor_dist
            else if (ilevel.eq.2) then
               if (building4_level2_Hfloor_dist.eq.1) then ! fixed value
                  Hfloor=building4_level2_Hfloor_H
               else if (building4_level2_Hfloor_dist.eq.2) then ! uniform distribution
                  call uniform(
     &                 building4_level2_Hfloor_Hmin,
     &                 building4_level2_Hfloor_Hmax,
     &                 Hfloor)
               else if (building4_level2_Hfloor_dist.eq.3) then ! normal distribution
                  call sample_normal(
     &                 building4_level2_Hfloor_mu,
     &                 building4_level2_Hfloor_sigma,
     &                 Hfloor)
               endif            ! value of building4_level2_Hfloor_dist
            endif
c
            width_min=5.0D+0
            width_max=15.0D+0
            call get_contour(dim,Ncontour_tmp,Nppc_tmp,contour_tmp,1,
     &           Np_tmp,ctr_tmp)
            call generate_close_contour2(dim,.false.,Np_tmp,ctr_tmp,
     &           width_min,width_max,.true.,.true.,success,
     &           Np2,ctr2)
            if (success) then
               call add_ctr_to_contour(dim,
     &              Ncontour_tmp,Nppc_tmp,contour_tmp,
     &              Np2,ctr2)
            endif               ! success            
c     check contour for self_intersection
            call contour_self_intersects(dim,
     &           0.0D+0,0.0D+0,
     &           Ncontour_tmp,Nppc_tmp,contour_tmp,Ncontour_tmp,
     &           self_intersection)
c     check for intersection with outer contour
            if (Ncontour_tmp.eq.2) then
               call contours_intersect(dim,
     &              0.0D+0,0.0D+0,
     &              Ncontour_tmp,Nppc_tmp,contour_tmp,2,1,
     &              intersection_found)
            else
               intersection_found=.false.
            endif
            if ((self_intersection).or.(intersection_found)) then
c     remove inner contour
               Ncontour_tmp=1
            endif
c     remove courtyard based on size aspect ratio
            if (Ncontour_tmp.gt.1) then
               call check_courtyard_contour(dim,
     &              Ncontour_tmp,Nppc_tmp,contour_tmp)
            endif
c     ----------- Sampling of e_in and e_ext -----------
            if (ilevel.eq.1) then
               if (ein_level1_dist.eq.1) then
                  e_in=ein_level1_L
               else if (ein_level1_dist.eq.2) then
                  call uniform(ein_level1_Lmin,
     &                 ein_level1_Lmax,e_in)
               else if (ein_level1_dist.eq.3) then
                  call sample_normal(ein_level1_mu,
     &                 ein_level1_sigma,e_in)
               endif
               if (eext_level1_dist.eq.1) then
                  e_ext=eext_level1_L
               else if (eext_level1_dist.eq.2) then
                  call uniform(eext_level1_Lmin,
     &                 eext_level1_Lmax,e_ext)
               else if (eext_level1_dist.eq.3) then
                  call sample_normal(eext_level1_mu,
     &                 eext_level1_sigma,e_ext)
               endif
            else if (ilevel.eq.2) then
               if (ein_level2_dist.eq.1) then
                  e_in=ein_level2_L
               else if (ein_level2_dist.eq.2) then
                  call uniform(ein_level2_Lmin,
     &                 ein_level2_Lmax,e_in)
               else if (ein_level2_dist.eq.3) then
                  call sample_normal(ein_level2_mu,
     &                 ein_level2_sigma,e_in)
               endif
               if (eext_level2_dist.eq.1) then
                  e_ext=eext_level2_L
               else if (eext_level2_dist.eq.2) then
                  call uniform(eext_level2_Lmin,
     &                 eext_level2_Lmax,e_ext)
               else if (eext_level2_dist.eq.3) then
                  call sample_normal(eext_level2_mu,
     &                 eext_level2_sigma,e_ext)
               endif
            else
               call error(label)
               write(*,*) 'ilevel=',ilevel
               stop
            endif               ! ilevel
c     ----------- Sampling of e_in and e_ext -----------
c     
            do j=1,dim-1
               position(j)=0.0D+0
            enddo               ! j
            angle=0.0D+0
c     
c     ----------- Sampling of fwall and ffloor -----------
            if (ilevel.eq.1) then
               if (fwall_level1_dist.eq.1) then
                  fwall=fwall_level1_val
               else if (fwall_level1_dist.eq.2) then
                  call uniform(fwall_level1_valmin,
     &                 fwall_level2_valmax,fwall)
               else if (fwall_level1_dist.eq.3) then
                  call sample_normal(fwall_level1_mu,
     &                 fwall_level2_sigma,fwall)
               endif
               if (ffloor_level1_dist.eq.1) then
                  ffloor=ffloor_level1_val
               else if (ffloor_level1_dist.eq.2) then
                  call uniform(ffloor_level1_valmin,
     &                 ffloor_level2_valmax,ffloor)
               else if (ffloor_level1_dist.eq.3) then
                  call sample_normal(ffloor_level1_mu,
     &                 ffloor_level2_sigma,ffloor)
               endif
            else if (ilevel.eq.2) then
               if (fwall_level2_dist.eq.1) then
                  fwall=fwall_level2_val
               else if (fwall_level2_dist.eq.2) then
                  call uniform(fwall_level2_valmin,
     &                 fwall_level2_valmax,fwall)
               else if (fwall_level2_dist.eq.3) then
                  call sample_normal(fwall_level2_mu,
     &                 fwall_level2_sigma,fwall)
               endif
               if (ffloor_level2_dist.eq.1) then
                  ffloor=ffloor_level2_val
               else if (ffloor_level2_dist.eq.2) then
                  call uniform(ffloor_level2_valmin,
     &                 ffloor_level2_valmax,ffloor)
               else if (ffloor_level2_dist.eq.3) then
                  call sample_normal(ffloor_level2_mu,
     &                 ffloor_level2_sigma,ffloor)
               endif
            else
               call error(label)
               write(*,*) 'ilevel=',ilevel
               stop
            endif               ! ilevel
            if (fwall.lt.0.0D+0) then
               call error(label)
               write(*,*) 'fwall=',fwall
               write(*,*) 'should be > 0'
               stop
            endif
            if (ffloor.lt.0.0D+0) then
               call error(label)
               write(*,*) 'ffloor=',ffloor
               write(*,*) 'should be > 0'
               stop
            endif
            if (fwall.gt.1.0D+0) then
               call error(label)
               write(*,*) 'fwall=',fwall
               write(*,*) 'should be < 1'
               stop
            endif
            if (ffloor.gt.1.0D+0) then
               call error(label)
               write(*,*) 'ffloor=',ffloor
               write(*,*) 'should be < 1'
               stop
            endif
c     ----------- Sampling of fwall and ffloor -----------
            
c     ----------- Sampling of e_glass -----------
            if (ilevel.eq.1) then
               if (eglass_level1_dist.eq.1) then
                  e_glass=eglass_level1_L
               else if (eglass_level1_dist.eq.2) then
                  call uniform(eglass_level1_Lmin,
     &                 eglass_level1_Lmax,e_glass)
               else if (eglass_level1_dist.eq.3) then
                  call sample_normal(eglass_level1_mu,
     &                 eglass_level1_sigma,e_glass)
               endif
            else if (ilevel.eq.2) then
               if (eglass_level2_dist.eq.1) then
                  e_glass=eglass_level2_L
               else if (eglass_level2_dist.eq.2) then
                  call uniform(eglass_level2_Lmin,
     &                 eglass_level2_Lmax,e_glass)
               else if (eglass_level2_dist.eq.3) then
                  call sample_normal(eglass_level2_mu,
     &                 eglass_level2_sigma,e_glass)
               endif
            else
               call error(label)
               write(*,*) 'ilevel=',ilevel
               stop
            endif               ! ilevel
c     ----------- Sampling of e_glass -----------
c                  
            call uniform(1.0D+0,2.0D+0,roof_height)
            call choose_integer(Nmat_in,cdf_in,i)
            internal_walls_mat=trim(mat_in(i))
            call choose_integer(Nmat_ext,cdf_ext,i)
            external_walls_mat=trim(mat_ext(i))
            call choose_integer(Nmat_roof,cdf_roof,i)
            roof_mat=trim(mat_roof(i))
            call record_type4_building_definition_file(dim,
     &           Nfloor,Hfloor,e_in,e_ext,position,
     &           angle,fwall,ffloor,e_glass,roof_height,
     &           internal_walls_mat,
     &           external_walls_mat,
     &           glass_panes_mat,
     &           roof_mat,
     &           Ncontour_tmp,Nppc_tmp,contour_tmp,Nbuilding)
c     compute the area of the building
            call contour_area(dim,Ncontour_tmp,Nppc_tmp,contour_tmp,
     &           main_area,secondary_area,total_area)
            Abuilding=Abuilding+total_area
            Acourtyard=Acourtyard+secondary_area
         endif                  ! contour_type=1
c-----------------------------------------------------------------------------
         if (contour_type(icontour).eq.2) then ! contour_type=2: buildings type 0-3 occupy a fraction of the area
c     Gobal probability to generate a building on the current contour
            if (ilevel.eq.1) then
               pbuilding=pbuilding_lv1
            else if (ilevel.eq.2) then
               pbuilding=pbuilding_lv2
            endif
            call random_gen(r)
c
            if (r.gt.pbuilding) then
               contour_type(icontour)=3 ! the contour has been changed to a park
               goto 333
            endif
c     otherwise a (type 0-4) building has to be generated
            thickness=lawn_thickness
            Ncontour_zone=1
            Nppc_zone=Nppc(icontour)
            do i=1,Nppc(icontour)
               do j=1,dim-1
                  contour_zone(1,i,j)=contour(icontour,i,j)
               enddo            ! j
            enddo               ! i
            zone_mat=trim(lawns_material)
            Nzone=Nzone+1
            call record_zone_definition_file(dim,Nzone,
     &        thickness,Ncontour_zone,Nppc_zone,contour_zone,zone_mat)
c     
            if (Nppc(icontour).eq.4) then
c     Sample building type according to the "cdf_bt" cumulative distribution
               if (ilevel.eq.1) then
                  call choose_integer(Nbt_lv1,cdf_bt_lv1,idx) ! idx=[1,5]
               else if (ilevel.eq.2) then
                  call choose_integer(Nbt_lv2,cdf_bt_lv2,idx) ! idx=[1,5]
               endif
               building_type=idx-1 ! building_type=[0,4]
c     ----------------------------------------------------
c     Number of floors
c     ----------------------------------------------------
               if (ilevel.eq.1) then
                  if (building_type.eq.0) then
                     if (building0_level1_Nfloor_dist.eq.1) then
                        Ncubes(3)=building0_level1_Nfloor_N
                     else if (building0_level1_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building0_level1_Nfloor_Nmin,
     &                       building0_level1_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building0_level1_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building0_level1_Nfloor_mu,
     &                       building0_level1_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.1) then
                     if (building1_level1_Nfloor_dist.eq.1) then
                        Ncubes(3)=building1_level1_Nfloor_N
                     else if (building1_level1_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building1_level1_Nfloor_Nmin,
     &                       building1_level1_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building1_level1_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building1_level1_Nfloor_mu,
     &                       building1_level1_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.2) then
                     if (building2_level1_Nfloor_dist.eq.1) then
                        Ncubes(3)=building2_level1_Nfloor_N
                     else if (building2_level1_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building2_level1_Nfloor_Nmin,
     &                       building2_level1_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building2_level1_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building2_level1_Nfloor_mu,
     &                       building2_level1_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.3) then
                     if (building3_level1_Nfloor_dist.eq.1) then
                        Ncubes(3)=building3_level1_Nfloor_N
                     else if (building3_level1_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building3_level1_Nfloor_Nmin,
     &                       building3_level1_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building3_level1_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building3_level1_Nfloor_mu,
     &                       building3_level1_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.4) then
                     if (building4_level1_Nfloor_dist.eq.1) then
                        Ncubes(3)=building4_level1_Nfloor_N
                     else if (building4_level1_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building4_level1_Nfloor_Nmin,
     &                       building4_level1_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building4_level1_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building4_level1_Nfloor_mu,
     &                       building4_level1_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else
                     call error(label)
                     write(*,*) 'building_type=',building_type
                  endif         ! building_type
               else if (ilevel.eq.2) then
                  if (building_type.eq.0) then
                     if (building0_level2_Nfloor_dist.eq.1) then
                        Ncubes(3)=building0_level2_Nfloor_N
                     else if (building0_level2_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building0_level2_Nfloor_Nmin,
     &                       building0_level2_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building0_level2_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building0_level2_Nfloor_mu,
     &                       building0_level2_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.1) then
                     if (building1_level2_Nfloor_dist.eq.1) then
                        Ncubes(3)=building1_level2_Nfloor_N
                     else if (building1_level2_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building1_level2_Nfloor_Nmin,
     &                       building1_level2_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building1_level2_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building1_level2_Nfloor_mu,
     &                       building1_level2_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.2) then
                     if (building2_level2_Nfloor_dist.eq.1) then
                        Ncubes(3)=building2_level2_Nfloor_N
                     else if (building2_level2_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building2_level2_Nfloor_Nmin,
     &                       building2_level2_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building2_level2_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building2_level2_Nfloor_mu,
     &                       building2_level2_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.3) then
                     if (building3_level2_Nfloor_dist.eq.1) then
                        Ncubes(3)=building3_level2_Nfloor_N
                     else if (building3_level2_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building3_level2_Nfloor_Nmin,
     &                       building3_level2_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building3_level2_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building3_level2_Nfloor_mu,
     &                       building3_level2_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else if (building_type.eq.4) then
                     if (building4_level2_Nfloor_dist.eq.1) then
                        Ncubes(3)=building4_level2_Nfloor_N
                     else if (building4_level2_Nfloor_dist.eq.2) then
                        call choose_uniform_integer(
     &                       building4_level2_Nfloor_Nmin,
     &                       building4_level2_Nfloor_Nmax,
     &                       Ncubes(3))
                     else if (building4_level2_Nfloor_dist.eq.3) then
                        call choose_normal_integer(
     &                       building4_level2_Nfloor_mu,
     &                       building4_level2_Nfloor_sigma,
     &                       Ncubes(3))
                     endif
                  else
                     call error(label)
                     write(*,*) 'building_type=',building_type
                  endif         ! building_type
               endif ! level
c     ----------------------------------------------------
c     Height of floors
c     ----------------------------------------------------
               if (ilevel.eq.1) then
                  if (building_type.eq.0) then
                     if (building0_level1_Hfloor_dist.eq.1) then
                        length(3)=building0_level1_Hfloor_H
                     else if (building0_level1_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building0_level1_Hfloor_Hmin,
     &                       building0_level1_Hfloor_Hmax,
     &                       length(3))
                     else if (building0_level1_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building0_level1_Hfloor_mu,
     &                       building0_level1_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.1) then
                     if (building1_level1_Hfloor_dist.eq.1) then
                        length(3)=building1_level1_Hfloor_H
                     else if (building1_level1_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building1_level1_Hfloor_Hmin,
     &                       building1_level1_Hfloor_Hmax,
     &                       length(3))
                     else if (building1_level1_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building1_level1_Hfloor_mu,
     &                       building1_level1_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.2) then
                     if (building2_level1_Hfloor_dist.eq.1) then
                        length(3)=building2_level1_Hfloor_H
                     else if (building2_level1_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building2_level1_Hfloor_Hmin,
     &                       building2_level1_Hfloor_Hmax,
     &                       length(3))
                     else if (building2_level1_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building2_level1_Hfloor_mu,
     &                       building2_level1_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.3) then
                     if (building3_level1_Hfloor_dist.eq.1) then
                        length(3)=building3_level1_Hfloor_H
                     else if (building3_level1_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building3_level1_Hfloor_Hmin,
     &                       building3_level1_Hfloor_Hmax,
     &                       length(3))
                     else if (building3_level1_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building3_level1_Hfloor_mu,
     &                       building3_level1_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.4) then
                     if (building4_level1_Hfloor_dist.eq.1) then
                        length(3)=building4_level1_Hfloor_H
                     else if (building4_level1_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building4_level1_Hfloor_Hmin,
     &                       building4_level1_Hfloor_Hmax,
     &                       length(3))
                     else if (building4_level1_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building4_level1_Hfloor_mu,
     &                       building4_level1_Hfloor_sigma,
     &                       length(3))
                     endif
                  else
                     call error(label)
                     write(*,*) 'building_type=',building_type
                  endif         ! building_type
               else if (ilevel.eq.2) then
                  if (building_type.eq.0) then
                     if (building0_level2_Hfloor_dist.eq.1) then
                        length(3)=building0_level2_Hfloor_H
                     else if (building0_level2_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building0_level2_Hfloor_Hmin,
     &                       building0_level2_Hfloor_Hmax,
     &                       length(3))
                     else if (building0_level2_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building0_level2_Hfloor_mu,
     &                       building0_level2_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.1) then
                     if (building1_level2_Hfloor_dist.eq.1) then
                        length(3)=building1_level2_Hfloor_H
                     else if (building1_level2_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building1_level2_Hfloor_Hmin,
     &                       building1_level2_Hfloor_Hmax,
     &                       length(3))
                     else if (building1_level2_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building1_level2_Hfloor_mu,
     &                       building1_level2_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.2) then
                     if (building2_level2_Hfloor_dist.eq.1) then
                        length(3)=building2_level2_Hfloor_H
                     else if (building2_level2_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building2_level2_Hfloor_Hmin,
     &                       building2_level2_Hfloor_Hmax,
     &                       length(3))
                     else if (building2_level2_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building2_level2_Hfloor_mu,
     &                       building2_level2_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.3) then
                     if (building3_level2_Hfloor_dist.eq.1) then
                        length(3)=building3_level2_Hfloor_H
                     else if (building3_level2_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building3_level2_Hfloor_Hmin,
     &                       building3_level2_Hfloor_Hmax,
     &                       length(3))
                     else if (building3_level2_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building3_level2_Hfloor_mu,
     &                       building3_level2_Hfloor_sigma,
     &                       length(3))
                     endif
                  else if (building_type.eq.4) then
                     if (building4_level2_Hfloor_dist.eq.1) then
                        length(3)=building4_level2_Hfloor_H
                     else if (building4_level2_Hfloor_dist.eq.2) then
                        call uniform(
     &                       building4_level2_Hfloor_Hmin,
     &                       building4_level2_Hfloor_Hmax,
     &                       length(3))
                     else if (building4_level2_Hfloor_dist.eq.3) then
                        call sample_normal(
     &                       building4_level2_Hfloor_mu,
     &                       building4_level2_Hfloor_sigma,
     &                       length(3))
                     endif
                  else
                     call error(label)
                     write(*,*) 'building_type=',building_type
                  endif         ! building_type
               endif            ! ilevel
c     sampling of cube length, in X and Y directions
c     ----------- Sampling of room length and width  -----------
               if (ilevel.eq.1) then
                  if (room_level1_length_dist.eq.1) then
                     length(1)=room_level1_length_L
                  else if (room_level1_length_dist.eq.2) then
                     call uniform(room_level1_length_Lmin,
     &                    room_level1_length_Lmax,length(1))
                  else if (room_level1_length_dist.eq.3) then
                     call sample_normal(room_level1_length_mu,
     &                    room_level1_length_sigma,length(1))
                  endif
                  if (room_level1_width_dist.eq.1) then
                     length(2)=room_level1_width_L
                  else if (room_level1_width_dist.eq.2) then
                     call uniform(room_level1_width_Lmin,
     &                    room_level1_width_Lmax,length(2))
                  else if (room_level1_width_dist.eq.3) then
                     call sample_normal(room_level1_width_mu,
     &                    room_level1_width_sigma,length(2))
                  endif
               else if (ilevel.eq.2) then
                  if (room_level2_length_dist.eq.1) then
                     length(1)=room_level2_length_L
                  else if (room_level2_length_dist.eq.2) then
                     call uniform(room_level2_length_Lmin,
     &                    room_level2_length_Lmax,length(1))
                  else if (room_level2_length_dist.eq.3) then
                     call sample_normal(room_level2_length_mu,
     &                    room_level2_length_sigma,length(1))
                  endif
                  if (room_level2_width_dist.eq.1) then
                     length(2)=room_level2_width_L
                  else if (room_level2_width_dist.eq.2) then
                     call uniform(room_level2_width_Lmin,
     &                    room_level2_width_Lmax,length(2))
                  else if (room_level2_width_dist.eq.3) then
                     call sample_normal(room_level2_width_mu,
     &                    room_level2_width_sigma,length(2))
                  endif
               else
                  call error(label)
                  write(*,*) 'ilevel=',ilevel
                  stop
               endif            ! ilevel
c     ----------- Sampling of room length and width  -----------
c               
c     ---------------------------------------------------------------------
c     computation of d1 and d2, the total length of the building
c     along X and Y directions, from the geometry of the contour
c     ----------- Sampling of e_in and e_ext -----------
               if (ilevel.eq.1) then
                  if (ein_level1_dist.eq.1) then
                     e_in=ein_level1_L
                  else if (ein_level1_dist.eq.2) then
                     call uniform(ein_level1_Lmin,
     &                    ein_level1_Lmax,e_in)
                  else if (ein_level1_dist.eq.3) then
                     call sample_normal(ein_level1_mu,
     &                    ein_level1_sigma,e_in)
                  endif
                  if (eext_level1_dist.eq.1) then
                     e_ext=eext_level1_L
                  else if (eext_level1_dist.eq.2) then
                     call uniform(eext_level1_Lmin,
     &                    eext_level1_Lmax,e_ext)
                  else if (eext_level1_dist.eq.3) then
                     call sample_normal(eext_level1_mu,
     &                    eext_level1_sigma,e_ext)
                  endif
               else if (ilevel.eq.2) then
                  if (ein_level2_dist.eq.1) then
                     e_in=ein_level2_L
                  else if (ein_level2_dist.eq.2) then
                     call uniform(ein_level2_Lmin,
     &                    ein_level2_Lmax,e_in)
                  else if (ein_level2_dist.eq.3) then
                     call sample_normal(ein_level2_mu,
     &                    ein_level2_sigma,e_in)
                  endif
                  if (eext_level2_dist.eq.1) then
                     e_ext=eext_level2_L
                  else if (eext_level2_dist.eq.2) then
                     call uniform(eext_level2_Lmin,
     &                    eext_level2_Lmax,e_ext)
                  else if (eext_level2_dist.eq.3) then
                     call sample_normal(eext_level2_mu,
     &                    eext_level2_sigma,e_ext)
                  endif
               else
                  call error(label)
                  write(*,*) 'ilevel=',ilevel
                  stop
               endif            ! ilevel
c     ----------- Sampling of e_in and e_ext -----------
c     
               do i=1,4
                  do j=1,dim-1
                     x(i,j)=contour(icontour,i,j)
                  enddo         ! j
                  x(i,dim)=0.0D+0
               enddo            ! i
               do iseg=1,4
                  i1=iseg
                  if (iseg.eq.4) then
                     i2=1
                  else
                     i2=i1+1
                  endif
                  do j=1,dim
                     x1(j)=x(i1,j)
                     x2(j)=x(i2,j)
                  enddo         ! j
                  call substract_vectors(dim,x2,x1,tmp)
                  call vector_length(dim,tmp,lseg(iseg))
               enddo            ! iseg
               if (lseg(1).gt.lseg(2)) then
                  main_dir=1
               else
                  main_dir=2
               endif
c     lseg(1) and lseg(2) are the dimensions of the (rectangular) terrain.
c     Now let us sample the fraction of the terrain area that will be used by the building
c     ----------- Sampling of terrain area fraction used by building   -----------
               if (ilevel.eq.1) then
                  if (tafubb_level1_dist.eq.1) then
                     tafubb=tafubb_level1_val
                  else if (tafubb_level1_dist.eq.2) then
                     call uniform(tafubb_level1_valmin,
     &                    tafubb_level1_valmax,tafubb)
                  else if (tafubb_level1_dist.eq.3) then
                     call sample_normal(tafubb_level1_mu,
     &                    tafubb_level1_sigma,tafubb)
                  endif
               else if (ilevel.eq.2) then
                  if (tafubb_level2_dist.eq.1) then
                     tafubb=tafubb_level2_val
                  else if (tafubb_level2_dist.eq.2) then
                     call uniform(tafubb_level2_valmin,
     &                    tafubb_level2_valmax,tafubb)
                  else if (tafubb_level2_dist.eq.3) then
                     call sample_normal(tafubb_level2_mu,
     &                    tafubb_level2_sigma,tafubb)
                  endif
               else
                  call error(label)
                  write(*,*) 'ilevel=',ilevel
                  stop
               endif            ! ilevel
c     ----------- Sampling of terrain area fraction used by building   -----------
c               
c     "tafubb" is the Terrain Area Fraction Used By Building
               if (tafubb.le.0.0D+0) then
                  call error(label)
                  write(*,*) 'terrain area fraction used by building=',
     &                 tafubb
                  write(*,*) 'should be > 0'
                  stop
               endif
               if (tafubb.ge.1.0D+0) then
                  call error(label)
                  write(*,*) 'terrain area fraction used by building=',
     &                 tafubb
                  write(*,*) 'should be < 1'
                  stop
               endif
               d1=dsqrt(tafubb)*lseg(1)
               d2=dsqrt(tafubb)*lseg(2)
c     ---------------------------------------------------------------------
c     
c     Number of rooms along X and Y directions (in local district referential)
c     are computed from the total length of the building and the number
c     of rooms in both directions
               Ncubes(1)=int((d1-2.0D+0*e_ext+e_in)/(length(1)+e_in))
               Ncubes(2)=int((d2-2.0D+0*e_ext+e_in)/(length(2)+e_in))
c     The total length of the building in each direction is now
c     re-computed from effective room numbers, because of the integer
c     in previous computation
               if ((Ncubes(1).ge.1).and.(Ncubes(2).ge.1)) then
                  do j=1,dim-1
c     bl(j): building length in direction j
                     bl(j)=Ncubes(j)*(length(j)+e_in)-e_in+2.0D+0*e_ext
                  enddo         ! j
                  Nbuilding=Nbuilding+1
                  do j=1,dim
                     x1(j)=x(1,j)
                     x2(j)=x(2,j)
                     x3(j)=x(3,j)
                     x4(j)=x(4,j)
                  enddo         ! j
                  l1=dmin1(3.0D+0,(lseg(1)-d1)/2.0D+0)
                  l2=dmin1(3.0D+0,(lseg(2)-d2)/2.0D+0)
                  call substract_vectors(dim,x2,x1,tmp)
                  call normalize_vector(dim,tmp,u12)
                  call scalar_vector(dim,-1.0D+0,u12,u21)
                  call substract_vectors(dim,x4,x1,tmp)
                  call normalize_vector(dim,tmp,u14)
                  call scalar_vector(dim,-1.0D+0,u14,u41)
                  call scalar_vector(dim,l1,u12,tmp)
                  call add_vectors(dim,x1,tmp,tmp2)
                  call scalar_vector(dim,l2,u14,tmp)
c     P1, P2, P3 & P4: position of the 4 points that define the contour of the building
                  call add_vectors(dim,tmp2,tmp,P1)
                  do j=1,dim-1
                     position(j)=P1(j)
                  enddo         ! j
                  call scalar_vector(dim,bl(1),u12,tmp)
                  call add_vectors(dim,P1,tmp,P2)
                  call scalar_vector(dim,bl(2),u14,tmp)
                  call add_vectors(dim,P2,tmp,P3)
                  call scalar_vector(dim,bl(1),u21,tmp)
                  call add_vectors(dim,P3,tmp,P4)
c
c                  fwall=0.20D+0
c                  ffloor=0.60D+0
c     ----------- Sampling of fwall and ffloor -----------
                  if (ilevel.eq.1) then
                     if (fwall_level1_dist.eq.1) then
                        fwall=fwall_level1_val
                     else if (fwall_level1_dist.eq.2) then
                        call uniform(fwall_level1_valmin,
     &                       fwall_level2_valmax,fwall)
                     else if (fwall_level1_dist.eq.3) then
                        call sample_normal(fwall_level1_mu,
     &                       fwall_level2_sigma,fwall)
                     endif
                     if (ffloor_level1_dist.eq.1) then
                        ffloor=ffloor_level1_val
                     else if (ffloor_level1_dist.eq.2) then
                        call uniform(ffloor_level1_valmin,
     &                       ffloor_level2_valmax,ffloor)
                     else if (ffloor_level1_dist.eq.3) then
                        call sample_normal(ffloor_level1_mu,
     &                       ffloor_level2_sigma,ffloor)
                     endif
                  else if (ilevel.eq.2) then
                     if (fwall_level2_dist.eq.1) then
                        fwall=fwall_level2_val
                     else if (fwall_level2_dist.eq.2) then
                        call uniform(fwall_level2_valmin,
     &                       fwall_level2_valmax,fwall)
                     else if (fwall_level2_dist.eq.3) then
                        call sample_normal(fwall_level2_mu,
     &                       fwall_level2_sigma,fwall)
                     endif
                     if (ffloor_level2_dist.eq.1) then
                        ffloor=ffloor_level2_val
                     else if (ffloor_level2_dist.eq.2) then
                        call uniform(ffloor_level2_valmin,
     &                       ffloor_level2_valmax,ffloor)
                     else if (ffloor_level2_dist.eq.3) then
                        call sample_normal(ffloor_level2_mu,
     &                       ffloor_level2_sigma,ffloor)
                     endif
                  else
                     call error(label)
                     write(*,*) 'ilevel=',ilevel
                     stop
                  endif         ! ilevel
                  if (fwall.lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'fwall=',fwall
                     write(*,*) 'should be > 0'
                     stop
                  endif
                  if (ffloor.lt.0.0D+0) then
                     call error(label)
                     write(*,*) 'ffloor=',ffloor
                     write(*,*) 'should be > 0'
                     stop
                  endif
                  if (fwall.gt.1.0D+0) then
                     call error(label)
                     write(*,*) 'fwall=',fwall
                     write(*,*) 'should be < 1'
                     stop
                  endif
                  if (ffloor.gt.1.0D+0) then
                     call error(label)
                     write(*,*) 'ffloor=',ffloor
                     write(*,*) 'should be < 1'
                     stop
                  endif
c     fwall_max and wwf
                  if ((building_type.ge.0).and.
     &                 (building_type.le.3)) then
                     Awt=2.0D+0*(Ncubes(1)*length(1)+Ncubes(2)*length(2)
     &                    +(Ncubes(1)+Ncubes(2)-2)*e_in
     &                    +4.0D+0*e_ext)
     &                    *(Ncubes(3)*length(3)
     &                    +(Ncubes(3)-1)*e_in+2.0D+0*e_ext)
                     Awc=2.0D+0*(Ncubes(1)*length(1)
     &                    +Ncubes(2)*length(2))*Ncubes(3)*length(3)
                     if (Awc.ge.Awt) then
                        call error(label)
                        write(*,*) 'Awc=',Awc
                        write(*,*) 'should be < Awt=',Awt
                        stop
                     endif
                     Awi=Awt-Awc
                     fwall_max=Awc/Awt
                     if (fwall.gt.fwall_max) then
                        fwall=fwall_max
                     endif
                     wwf=fwall/fwall_max
                     if (wwf.gt.1.0D+0) then
                        call error(label)
                        write(*,*) 'building_type=',building_type
                        write(*,*) 'wwf=',wwf
                        write(*,*) 'should be < 1'
                        stop
                     endif
c                  else          ! T4 buildings only
c                     Hbuilding=Ncubes(3)*length(3)
c     &                    +(Ncubes(3)-1)*e_in+2.0D+0*e_ext
c                     fwall_max=Ncubes(3)*length(3)/Hbuilding                   
                  endif         ! building_type
c                  

c     ----------- Sampling of fwall and ffloor -----------
c     
c                  e_glass=4.0D+0
c     ----------- Sampling of e_glass -----------
                  if (ilevel.eq.1) then
                     if (eglass_level1_dist.eq.1) then
                        e_glass=eglass_level1_L
                     else if (eglass_level1_dist.eq.2) then
                        call uniform(eglass_level1_Lmin,
     &                       eglass_level1_Lmax,e_glass)
                     else if (eglass_level1_dist.eq.3) then
                        call sample_normal(eglass_level1_mu,
     &                       eglass_level1_sigma,e_glass)
                     endif
                  else if (ilevel.eq.2) then
                     if (eglass_level2_dist.eq.1) then
                        e_glass=eglass_level2_L
                     else if (eglass_level2_dist.eq.2) then
                        call uniform(eglass_level2_Lmin,
     &                       eglass_level2_Lmax,e_glass)
                     else if (eglass_level2_dist.eq.3) then
                        call sample_normal(eglass_level2_mu,
     &                       eglass_level2_sigma,e_glass)
                     endif
                  else
                     call error(label)
                     write(*,*) 'ilevel=',ilevel
                     stop
                  endif         ! ilevel
c     ----------- Sampling of e_glass -----------
c                  
                  call uniform(1.0D+0,2.0D+0,roof_height)
                  call choose_integer(Nmat_in,cdf_in,i)
                  internal_walls_mat=trim(mat_in(i))
                  call choose_integer(Nmat_ext,cdf_ext,i)
                  external_walls_mat=trim(mat_ext(i))
                  call choose_integer(Nmat_roof,cdf_roof,i)
                  roof_mat=trim(mat_roof(i))
                  Npb=4
                  angle=arctan(x(2,1)-x(1,1),x(2,2)-x(1,2))*180.0D+0/pi
                  if (((building_type.eq.1).or.(building_type.eq.2))
     &                 .and.(bl(2).gt.bl(1))) then
                     do j=1,dim-1
                        ctrb(1,j)=P2(j)
                        ctrb(2,j)=P3(j)
                        ctrb(3,j)=P4(j)
                        ctrb(4,j)=P1(j)
                        position(j)=P2(j)
                     enddo      ! j
                     angle=angle+90.0D+0
                     Ln1=length(1)
                     Ln2=length(2)
c     Final re-assign the total X/Y lengths of the building to "length()"
                     length(1)=Ln2
                     length(2)=Ln1
                     Ncx=Ncubes(1)
                     Ncy=Ncubes(2)
                     Ncubes(1)=Ncy
                     Ncubes(2)=Ncx
                  else
                     do j=1,dim-1
                        ctrb(1,j)=P1(j)
                        ctrb(2,j)=P2(j)
                        ctrb(3,j)=P3(j)
                        ctrb(4,j)=P4(j)
                        position(j)=P1(j)
                     enddo      ! j
                  endif         ! position and angle of the building have to be redefined
                  call generate_close_contour2(dim,.false.,Npb,ctrb,
     &                 0.0D+0,3.0D+0,.false.,.true.,success,
     &                 Npb2,ctrb2)
                  if (.not.success) then
                     call error(label)
                     write(*,*) 'could not generate outward contour'
                     write(*,*) 'for the building at distance:',3.0D+0
                     stop
                  endif         ! success=F
c     Primary contour (required for area computation, and also for the definition of a
c     type 4 building)
                     Ncontour_tmp=1
                     Nppc_tmp(Ncontour_tmp)=4
                     do i=1,Nppc_tmp(Ncontour_tmp)
                        do j=1,dim-1
                           contour_tmp(Ncontour_tmp,i,j)=ctrb(i,j)
                        enddo   ! j
                     enddo      ! i
c     record definition file
                  if (building_type.eq.0) then
                     call record_type0_building_definition_file(dim,
     &                    Ncubes,length,e_in,e_ext,position,
     &                    angle,wwf,e_glass,
     &                    internal_walls_mat,
     &                    external_walls_mat,
     &                    glass_panes_mat,
     &                    Nbuilding)
                  else if (building_type.eq.1) then
                     call record_type1_building_definition_file(dim,
     &                    Ncubes,length,e_in,e_ext,position,
     &                    angle,wwf,e_glass,roof_height,
     &                    internal_walls_mat,
     &                    external_walls_mat,
     &                    glass_panes_mat,
     &                    roof_mat,
     &                    Nbuilding)
                  else if (building_type.eq.2) then
                     call record_type2_building_definition_file(dim,
     &                    Ncubes,length,e_in,e_ext,position,
     &                    angle,wwf,e_glass,roof_height,
     &                    internal_walls_mat,
     &                    external_walls_mat,
     &                    glass_panes_mat,
     &                    roof_mat,
     &                    Nbuilding)
                  else if (building_type.eq.3) then
                     call record_type3_building_definition_file(dim,
     &                    Ncubes,length,e_in,e_ext,position,
     &                    angle,wwf,e_glass,roof_height,
     &                    internal_walls_mat,
     &                    external_walls_mat,
     &                    glass_panes_mat,
     &                    roof_mat,
     &                    Nbuilding)
                  else if (building_type.eq.4) then
c     Generate inner contour at possible distance within the [width_min, width_max] range
                     width_min=5.0D+0
                     width_max=15.0D+0
                     call get_contour(dim,
     &                    Ncontour_tmp,Nppc_tmp,contour_tmp,1,
     &                    Np_tmp,ctr_tmp)
                     call generate_close_contour2(dim,.false.,
     &                    Np_tmp,ctr_tmp,
     &                    width_min,width_max,.true.,.true.,success,
     &                    Np2,ctr2)
                     if (success) then
                        call add_ctr_to_contour(dim,
     &                       Ncontour_tmp,Nppc_tmp,contour_tmp,
     &                       Np2,ctr2)
                     endif      ! success            
c     check contour for self_intersection
                     call contour_self_intersects(dim,
     &                    0.0D+0,0.0D+0,
     &                    Ncontour_tmp,Nppc_tmp,contour_tmp,
     &                    Ncontour_tmp,
     &                    self_intersection)
c     check for intersection with outer contour
                     if (Ncontour_tmp.eq.2) then
                        call contours_intersect(dim,
     &                       0.0D+0,0.0D+0,
     &                       Ncontour_tmp,Nppc_tmp,contour_tmp,2,1,
     &                       intersection_found)
                     else
                        intersection_found=.false.
                     endif
                     if ((self_intersection).or.
     &                    (intersection_found)) then
c     remove inner contour
                        Ncontour_tmp=1
                     endif
c     remove courtyard based on size aspect ratio
                     if (Ncontour_tmp.gt.1) then
                        call check_courtyard_contour(dim,
     &                       Ncontour_tmp,Nppc_tmp,contour_tmp)
                     endif
c     
                     do j=1,dim-1
                        position(j)=0.0D+0
                     enddo      ! j
                     angle=0.0D+0
                     call record_type4_building_definition_file(dim,
     &                    Ncubes(3),length(3),e_in,e_ext,position,
     &                    angle,fwall,ffloor,e_glass,roof_height,
     &                    internal_walls_mat,
     &                    external_walls_mat,
     &                    glass_panes_mat,
     &                    roof_mat,
     &                    Ncontour_tmp,Nppc_tmp,contour_tmp,Nbuilding)
                  else
                     call error(label)
                     write(*,*) 'building_type=',building_type
                     write(*,*) 'should not be possible'
                     stop
                  endif         ! building_type
c     compute area of the building
                  call contour_area(dim,
     &                 Ncontour_tmp,Nppc_tmp,contour_tmp,
     &                 main_area,secondary_area,total_area)
                  Abuilding=Abuilding+total_area
                  if (building_type.eq.4) then
                     Acourtyard=Acourtyard+secondary_area
                  endif
c     
c     create hedge or wall
                  call choose_integer(Nmat_zt,cdf_zt,zone_type) ! zone_type: 1 for hedge, 2 for wall
                  if (zone_type.eq.1) then
                     zone_width=0.60D+0
                     call uniform(1.40D+0,2.50D+0,zone_height)
                  else if (zone_type.eq.2) then
                     zone_width=0.30D+0
                     call uniform(0.80D+0,1.50D+0,zone_height)
                  else
                     call error(label)
                     write(*,*) 'zone_type=',zone_type
                     write(*,*) 'should not be possible'
                     write(*,*) 'Nmat_zt=',Nmat_zt
                     do i=0,Nmat_zt
                        write(*,*) 'cdf_zt(',i,')=',cdf_zt(i)
                     enddo      ! i
                     stop
                  endif         ! zone_type

                  call get_contour(dim,
     &                 Ncontour_zone,Nppc_zone,contour_zone,1,
     &                 Np,ctr)
                  call generate_close_contour2(dim,.false.,Np,ctr,
     &                 0.0D+0,zone_width+1.0D+0,.true.,.true.,
     &                 success,Np2,ctr2)
                  if (.not.success) then
                     call error(label)
                     write(*,*) 'could not generate inward contour'
                     write(*,*) 'for the zone at distance:'
                     write(*,*) 'zone_width+1=',zone_width+1.0D+0
                     stop
                  endif         ! success=F
c     Now contour index 2 of "contour_zone" is the hedge, or wall
                  call add_ctr_to_contour(dim,
     &                 Ncontour_zone,Nppc_zone,contour_zone,
     &                 Np2,ctr2)
                  if (draw_wah) then
c     produce definition files for the hedge or wall
                     call make_hedge_or_wall(dim,Nzone,
     &                    zone_width,zone_height,mat_zt(zone_type),bl,
     &                    l1,l2,u12,u14,u21,u41,x1,x2,x3,x4,P1,P2,area)
                     Awah=Awah+area
                  endif         ! draw_wah
c
c     Now contour index 3 of "contour_zone" is the contour of the building
                  call add_ctr_to_contour(dim,
     &                 Ncontour_zone,Nppc_zone,contour_zone,
     &                 Npb2,ctrb2)
c     create swimming pool or park
                  l3=lseg(1)-bl(1)-l1
                  l4=lseg(2)-bl(2)-l2
                  if (main_dir.eq.1) then
                     sp_deltax=sp_waterzone_length
                     sp_deltay=sp_waterzone_width
                  else
                     sp_deltax=sp_waterzone_width
                     sp_deltay=sp_waterzone_length
                  endif
                  possible_sp=.false.
                  if (((main_dir.eq.1).and.(l4.gt.sp_deltay)).or.
     &                 ((main_dir.eq.2).and.(l3.gt.sp_deltax))) then
                     possible_sp=.true.
                  endif
                  swimming_pool=.false.
                  if (possible_sp) then
                     call random_gen(r)
                     if (r.lt.0.50D+0) then
                        swimming_pool=.true.
                     endif
                  endif         ! possible_sp
                  if (swimming_pool) then
                     if (main_dir.eq.1) then
                        call scalar_vector(dim,
     &                       (lseg(1)-sp_deltax)/2.0D+0,u12,tmp)
                        call add_vectors(dim,x1,tmp,tmp2)
                        call scalar_vector(dim,l2+bl(2)+5.0D+0,u14,tmp)
                        call add_vectors(dim,tmp2,tmp,xsp1)
                     else
                        sp_deltax=sp_waterzone_width
                        sp_deltay=sp_waterzone_length
                        call scalar_vector(dim,l1+bl(1)+5.0D+0,u12,tmp)
                        call add_vectors(dim,x1,tmp,tmp2)
                        call scalar_vector(dim,
     &                       (lseg(2)-sp_deltay)/2.0D+0,u14,tmp)
                        call add_vectors(dim,tmp2,tmp,xsp1)
                     endif
                     call scalar_vector(dim,sp_deltax,u12,tmp)
                     call add_vectors(dim,xsp1,tmp,xsp2)
                     call scalar_vector(dim,sp_deltay,u14,tmp)
                     call add_vectors(dim,xsp2,tmp,xsp3)
                     call add_vectors(dim,xsp1,tmp,xsp4)
c     
                     edge_width=sp_edge_width
                     thickness=sp_thickness
                     zone_mat=trim(swimmingpools_border_material)
c     contour for the water zone
                     Np=4
                     do j=1,dim-1
                        ctr(1,j)=xsp1(j)
                        ctr(2,j)=xsp2(j)
                        ctr(3,j)=xsp3(j)
                        ctr(4,j)=xsp4(j)
                     enddo      ! j
c     generate outward contour at a distance "edge_width" from the water zone
c                     call generate_close_contour(dim,
c     &                    Np,ctr,edge_width,.false.,
c     &                    Np2,ctr2)
                     call generate_close_contour2(dim,.false.,Np,ctr,
     &                    0.0D+0,edge_width,.false.,.true.,success,
     &                    Np2,ctr2)
                     if (.not.success) then
                        call error(label)
                        write(*,*) 'could not generate outward contour'
                        write(*,*) 'for the swimmingpool at distance:'
                        write(*,*) 'edge_width=',edge_width
                        stop
                     endif      ! success=F
c     check all points of that contour are within the contour of the hedge, or wall
                     inside1=.true.
                     do i=1,Np2
                        do j=1,dim-1
                           tmp(j)=ctr2(i,j)
                        enddo   ! j
                        tmp(dim)=0.0D+0
                        call is_inside_contour(.false.,dim,
     &                       Ncontour_zone,Nppc_zone,contour_zone,
     &                       2,tmp,inside)
                        if (.not.inside) then
                           inside1=.false.
                        endif
                     enddo      ! i
c     then another 1m around, for trees
c                     call generate_close_contour(dim,
c     &                    Np2,ctr2,2.0D+0,.false.,
c     &                    Npb2,ctrb2)
                     call generate_close_contour2(dim,.false.,Np2,ctr2,
     &                    0.0D+0,2.0D+0,.false.,.true.,success,
     &                    Npb2,ctrb2)
                     if (.not.success) then
                        call error(label)
                        write(*,*) 'could not generate outward contour'
                        write(*,*) 'for the swimmingpool at distance:'
                        write(*,*) 2.0D+0
                        stop
                     endif      ! success=F
c     Now contour index 4 of "contour_zone" is the contour of the swimming pool
                     call add_ctr_to_contour(dim,
     &                    Ncontour_zone,Nppc_zone,contour_zone,
     &                    Np2,ctr2)
c     check contour for self_intersection
                     call contour_self_intersects(dim,
     &                    0.0D+0,0.0D+0,
     &                    Ncontour_zone,Nppc_zone,contour_zone,
     &                    Ncontour_zone,
     &                    self_intersection)
c     check for intersection with outer contour
                     intersection=.false.
                     do i=1,Ncontour_zone-1
                        call contours_intersect(dim,
     &                       0.0D+0,0.0D+0,
     &                       Ncontour_zone,Nppc_zone,contour_zone,
     &                       Ncontour_zone,i,
     &                       intersection_found)
                        if (intersection_found) then
                           intersection=.true.
                           goto 123
                        endif
                     enddo      ! i
 123                 continue
                     if ((.not.inside1).or.(self_intersection).or.
     &                    (intersection)) then
c     remove swimmingpool contour if it intersects anything
                        Ncontour_zone=Ncontour_zone-1
                     else
                        Ncontour_tmp=0
                        call add_ctr_to_contour(dim,
     &                       Ncontour_tmp,Nppc_tmp,contour_tmp,
     &                       Np,ctr)
                        Nsp=Nsp+1
                        call record_sp_definition_file(dim,Nsp,
     &                       edge_width,thickness,
     &                       Ncontour_tmp,Nppc_tmp,contour_tmp,
     &                       zone_mat)
                     endif
                  endif         ! swimming_pool
c     append contour to file, so that the final step of the code may be
c     to retrieve this information in order to generate trees on the terrain
                  call append_contour_file(dim,contour_file,
     &                 Ncontour_zone,Nppc_zone,contour_zone,ilevel)
               else
                  contour_type(icontour)=3
               endif            ! Ncubes(1)>=1 AND Ncubes(2)>=1
            endif               ! Nppc(icontour)=4
         endif                  ! contour_type=2
c-----------------------------------------------------------------------------
 333     continue
         if (contour_type(icontour).eq.3) then ! contour_type=3: parks (and recreations)
            thickness=lawn_thickness
            Ncontour_tmp=1
            Nppc_tmp=Nppc(icontour)
            do i=1,Nppc(icontour)
               do j=1,dim-1
                  contour_tmp(1,i,j)=contour(icontour,i,j)
               enddo            ! j
            enddo               ! i
            zone_mat=trim(lawns_material)
            Nzone=Nzone+1
            call record_zone_definition_file(dim,Nzone,
     &           thickness,Ncontour_tmp,Nppc_tmp,contour_tmp,zone_mat)
            Nppt=Nppc_tmp(1)
            do i=1,Nppc_tmp(1)
               do j=1,dim-1
                  track(i,j)=contour_tmp(1,i,j)
               enddo            ! j
            enddo               ! i
            zone_width=0.60D+0
            zone_mat=trim(park_hedges_material)
            call uniform(1.40D+0,2.50D+0,zone_height)
c            call generate_close_contour(dim,
c     &           Nppt,track,zone_width,.true.,
c     &           Nppt2,track2)
            call generate_close_contour2(dim,.false.,Nppt,track,
     &           0.0D+0,zone_width,.true.,.true.,success,
     &           Nppt2,track2)
            if (.not.success) then
               call error(label)
               write(*,*) 'could not generate inward contour'
               write(*,*) 'for the footprint at distance:'
               write(*,*) 'zone_width=',zone_width
               stop
            endif               ! success=F
            Ncontourz=2
            Nppcz(1)=Nppc_tmp(1)
            do i=1,Nppcz(1)
               do j=1,dim-1
                  contourz(1,i,j)=contour_tmp(1,i,j)
               enddo            ! j
            enddo               ! i
            Nppcz(2)=Nppt2
            do i=1,Nppcz(2)
               do j=1,dim-1
                  contourz(2,i,j)=track2(i,j)
               enddo            ! j
            enddo               ! i
c     check inner contour for self_intersection
            call contour_self_intersects(dim,
     &           0.0D+0,0.0D+0,
     &           Ncontourz,Nppcz,contourz,2,
     &           self_intersection)
c     check for intersection with inner contour
            call contours_intersect(dim,
     &           0.0D+0,0.0D+0,
     &           Ncontourz,Nppcz,contourz,2,1,
     &           intersection_found)
            if ((self_intersection).or.
     &           (intersection_found)) then
c     duplicate contour 1 as second contour (needed for tree filling)
               Nppcz(2)=Nppcz(1)
               do i=1,Nppcz(2)
                  do j=1,dim-1
                     contourz(2,i,j)=contourz(1,i,j)
                  enddo         ! j
               enddo            ! i
            else
               if (draw_wah) then
c     Record the zone for the hedge
                  Nzone=Nzone+1
                  call record_zone_definition_file(dim,Nzone,
     &                 zone_height,Ncontourz,Nppcz,contourz,
     &                 zone_mat)
               endif            ! draw_wah
            endif
c     append contour to file, so that the final step of the code may be
c     to retrieve this information in order to generate trees on the terrain
            call append_contour_file(dim,contour_file,
     &           Ncontourz,Nppcz,contourz,ilevel)
c     compute area of the park and wah
            call contour_area(dim,Ncontourz,Nppcz,contourz,
     &           main_area,secondary_area,total_area)
            Apark=Apark+secondary_area
            if (draw_wah) then
               Awah=Awah+total_area
            endif               ! draw_wah
c     
         endif                  ! contour_type=3
c-----------------------------------------------------------------------------
         if (contour_type(icontour).eq.4) then ! contour_type=4: no man's land (unused zones)
            thickness=lawn_thickness
            Ncontour_tmp=1
            Nppc_tmp=Nppc(icontour)
            do i=1,Nppc(icontour)
               do j=1,dim-1
                  contour_tmp(1,i,j)=contour(icontour,i,j)
               enddo            ! j
            enddo               ! i
            zone_mat=trim(unused_zones_material)
            Nzone=Nzone+1
            call record_zone_definition_file(dim,Nzone,
     &           thickness,Ncontour_tmp,Nppc_tmp,contour_tmp,
     &           zone_mat)
         endif                  ! contour_type=4
c-----------------------------------------------------------------------------
      enddo                     ! icontour
c     compute areas of the district and streets
      call contour_area(dim,Ncontour,Nppc,contour,
     &     main_area,secondary_area,total_area)
      Adistrict=Adistrict+main_area
      Aterrain=Aterrain+secondary_area
      Astreet=Astreet+total_area
c     Debug
      if (debug) then
         stop
      endif
c     Debug

      
      return
      end



      subroutine append_contour_file(dim,contour_file,
     &     Ncontour,Nppc,contour,ilevel)
      implicit none
      include 'max.inc'
c     
c     Purpose: to append a group on contours to the dedicated file
c     
c     Input:
c       + dim: dimension of space
c       + contour_file: data file
c       + Ncontour: number of contours
c       + Nppc: number of points for each contour
c       + contour: contour definition (position 2D coordinates)
c       + ilevel: level index
c     
c     Output: updated "contour_file" 
c     
c     I/O
      integer dim
      character*(Nchar_mx) contour_file
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer ilevel
c     temp
      logical file_exists
      integer icontour,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine append_contour_file'

      inquire(file=trim(contour_file),exist=file_exists)
      if (file_exists) then      
         open(31,file=trim(contour_file),access='append')
      else
         open(31,file=trim(contour_file),status='new')
      endif
      write(31,*) Ncontour
      write(31,*) ilevel
      do icontour=1,Ncontour
         write(31,*) Nppc(icontour)
         do i=1,Nppc(icontour)
            write(31,*) (contour(icontour,i,j),j=1,dim-1)
         enddo                  ! i
      enddo                     ! icontour
      close(31)
      
      return
      end
      
      

      subroutine make_hedge_or_wall(dim,Nzone,
     &     zone_width,zone_height,zone_mat,bl,l1,l2,
     &     u12,u14,u21,u41,x1,x2,x3,x4,P1,P2,area)
      implicit none
      include 'max.inc'
c
c     Purpose: to create a hedge or a wall on the border of a footprint
c
c     Input:
c       + dim: dimension of space
c       + Nzone: number of zones
c       + zone_width: hedge or wall width
c       + zone_height: hedge or wall height
c       + zone_mat: hedge or wall material
c       + bl: building length
c       + l1, l2: various lengths
c       + u12, u14, u21, u41: directions
c       + x1, x2, x3, x4: some positions
c       + P1, P2: positions
c
c     Output:
c       + Nzone: updated
c       + definition files in ./results
c       + area: area of the hedge or wall [m²]
c
c     I/O
      integer dim
      integer Nzone
      double precision zone_width
      double precision zone_height
      character*(Nchar_mx) zone_mat
      double precision bl(1:Ndim_mx-1)
      double precision l1,l2
      double precision u12(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision area
c     temp
      integer i,j
      integer Ncontourz
      integer Nppcz(1:Ncontour_mx)
      double precision contourz(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer izone
      double precision P(1:Ndim_mx)
      double precision P0(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision main_area,secondary_area,total_area
c     label
      character*(Nchar_mx) label
      label='subroutine make_hedge_or_wall'
      
      Ncontourz=1
      Nppcz(1)=4
      do izone=1,7
         if (izone.eq.1) then
            if (bl(1).gt.3.0D+0+2.0D+0*zone_width) then
               call scalar_vector(dim,
     &              (bl(1)-3.0D+0-2.0D+0*zone_width)
     &              /2.0D+0,u21,tmp)
               call add_vectors(dim,P2,tmp,P)
            else
               call copy_vector(dim,P2,P)
            endif
            i=1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,
     &           zone_width,u21,tmp)
            call add_vectors(dim,P,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,
     &           l2,u41,tmp)
            call add_vectors(dim,P,tmp,P)
            call copy_vector(dim,P,P0)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,
     &           zone_width,u12,tmp)
            call add_vectors(dim,P,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
         else if (izone.eq.2) then
            call copy_vector(dim,P0,P)
            i=1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call copy_vector(dim,x2,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,
     &           zone_width,u14,tmp)
            call add_vectors(dim,P,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call add_vectors(dim,P0,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
         else if (izone.eq.3) then
            call copy_vector(dim,x2,P)
            i=1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call copy_vector(dim,x3,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,zone_width,u21,tmp)
            call add_vectors(dim,x3,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call add_vectors(dim,x2,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
         else if (izone.eq.4) then
            call copy_vector(dim,x3,P)
            i=1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call copy_vector(dim,x4,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,zone_width,u41,tmp)
            call add_vectors(dim,x4,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call add_vectors(dim,x3,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
         else if (izone.eq.5) then
            call copy_vector(dim,x4,P)
            i=1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call copy_vector(dim,x1,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,zone_width,u12,tmp)
            call add_vectors(dim,x1,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call add_vectors(dim,x4,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
         else if (izone.eq.6) then
            if (bl(1).gt.3.0D+0+2.0D+0*zone_width) then
               call scalar_vector(dim,
     &              (bl(1)-3.0D+0-2.0D+0*zone_width)
     &              /2.0D+0+zone_width,u12,tmp)
            else
               call scalar_vector(dim,zone_width,u12,tmp)
            endif
            call add_vectors(dim,P1,tmp,P0)
            call copy_vector(dim,x1,P)
            i=1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,l2,u41,tmp)
            call add_vectors(dim,P0,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,zone_width,u14,tmp)
            call add_vectors(dim,P,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call add_vectors(dim,x1,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j                        
         else if (izone.eq.7) then
            call copy_vector(dim,P0,P)
            i=1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,zone_width,u21,tmp)
            call add_vectors(dim,P,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call scalar_vector(dim,l2,u41,tmp)
            call add_vectors(dim,P,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
            call add_vectors(dim,P0,tmp,P)
            i=i+1
            do j=1,dim-1
               contourz(1,i,j)=P(j)
            enddo               ! j
         endif
         Nzone=Nzone+1
         call record_zone_definition_file(dim,Nzone,
     &        zone_height,Ncontourz,Nppcz,contourz,
     &        zone_mat)
      enddo                     ! izone
c     compute area
      call contour_area(dim,Ncontourz,Nppcz,contourz,
     &     main_area,secondary_area,total_area)
      if (main_area.ne.total_area) then
         call error(label)
         write(*,*) 'main_area=',main_area
         write(*,*) 'total_area=',total_area
         write(*,*) 'should be equal'
         stop
      else
         area=main_area
      endif

      return
      end
      

      
      subroutine fill_with_trees(dim,index,Ncontour,Nppc,contour,
     &     trees_density,ilevel)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'distribution_parameters.inc'
c     
c     Purpose: to fill a main contour with trees
c     
c     Input:
c       + dim: dimension of space
c       + index: indication for tree density [discarded]
c         - index=1: use a high density (park)
c         - index=2: use a low density (lawn)
c       + Nc: number of contours
c       + Np: number of points for each contours
c       + contour: contours; must hold:
c         - contour1: contour of the surrounding footprint
c         - contour2: inner contour for the hedge
c         - subsequent contours: building, swimming pool, etc.
c       + trees_density: surface density for trees [#/Ha]
c       + ilevel: district level index
c     
c     Ouput: results/zone***.in and results/vegetation.in files
c     
c     I/O
      integer dim
      integer index
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision trees_density
      integer ilevel
c     temp
      integer Niter,imat,i,j
      double precision xmin0,xmax0,ymin0,ymax0
      double precision xmin,xmax,ymin,ymax
      double precision S0,S,d,scale,angle
      integer Ntree,Ntree_required
      logical keep_going,keep_searching,inside,position_is_clear
      logical file_exist,inside2,inside_oc
      double precision x0(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      character*(Nchar_mx) vegetation_file,filename
      integer Nmat_tree
      character*(Nchar_mx) mat_tree(1:Nmat_mx)
      character*(Nchar_mx) obj_file_tree(1:Nmat_mx)
      character*(Nchar_mx) obj_file_trunk(1:Nmat_mx)
      character*(Nchar_mx) obj_file_foliage(1:Nmat_mx)
      double precision cdf_tree(0:Nproba_mx)
      double precision tree_position(1:Ntree_mx,1:2)
      double precision Sc
      integer ic
c     parameters
      double precision d2tree
      double precision S1ha
      parameter(d2tree=5.0D+0)  ! m
      parameter(S1ha=1.0D+4)    !  m²
c     label
      character*(Nchar_mx) label
      label='subroutine fill_with_trees'
      
      vegetation_file='./results/vegetation.in'
      if (ilevel.eq.1) then
         filename='./data/trees_level1.in'
      else if (ilevel.eq.2) then
         filename='./data/trees_level2.in'
      else
         call error(label)
         write(*,*) 'ilevel=',ilevel
         stop
      endif
      
      call read_tree_file(filename,
     &     Nmat_tree,mat_tree,obj_file_tree,cdf_tree)

      call analyze_contour(dim,
     &     Ncontour,Nppc,contour,1,
     &     xmin0,xmax0,ymin0,ymax0)
      S0=(xmax0-xmin0)*(ymax0-ymin0)
      Sc=0.0D+0
      if (Ncontour.gt.1) then
         do ic=2,Ncontour
            call analyze_contour(dim,
     &           Ncontour,Nppc,contour,ic,
     &           xmin,xmax,ymin,ymax)
            if (ic.eq.2) then
               xmin0=xmin
               xmax0=xmax
               ymin0=ymin
               ymax0=ymax
               S0=(xmax0-xmin0)*(ymax0-ymin0)
            else
               Sc=Sc+(xmax-xmin)*(ymax-ymin)
            endif
         enddo                  ! ic
      endif                     ! Ncontour > 1
      S=S0-Sc
      if (S.le.0.0D+0) then
         S=S0
      endif
c     
      Ntree_required=int(trees_density*S/S1ha)
c     
      Ntree=0
      keep_going=.true.
      do while (keep_going)
         Niter=0
         keep_searching=.true.
         do while (keep_searching)
            Niter=Niter+1
            call uniform(xmin0,xmax0,x0(1))
            call uniform(ymin0,ymax0,x0(2))
            x0(dim)=0.0D+0
            call is_inside_contour(.false.,dim,
     &           Ncontour,Nppc,contour,
     &           2,x0,inside)
            inside_oc=.false.
            do i=3,Ncontour
               call is_inside_contour(.false.,dim,
     &              Ncontour,Nppc,contour,
     &              i,x0,inside2)
               if (inside2) then
                  inside_oc=.true.
                  goto 111
               endif            ! inside2
            enddo               ! i
 111        continue               
            position_is_clear=.true.
            if ((inside).and.(.not.inside_oc)) then
               do i=1,Ntree
                  do j=1,dim-1
                     x1(j)=tree_position(i,j)
                  enddo         ! j
                  x1(dim)=0.0D+0
                  call substract_vectors(dim,x1,x0,tmp)
                  call vector_length(dim,tmp,d)
                  if (d.le.d2tree) then
                     position_is_clear=.false.
                  endif         ! d<d2tree
               enddo            ! i
            else
               position_is_clear=.false.
            endif               ! inside
            if (position_is_clear) then
               Ntree=Ntree+1
               if (Ntree.gt.Ntree_mx) then
                  call error(label)
                  write(*,*) 'Ntree=',Ntree
                  write(*,*) '> Ntree_mx=',Ntree_mx
                  stop
               endif
               do j=1,dim-1
                  tree_position(Ntree,j)=x0(j)
               enddo            ! j
               keep_searching=.false.
            endif               ! position_is_clear
            if (Niter.gt.Niter_mx) then
               keep_searching=.false.
               keep_going=.false.
            endif               ! Niter>Niter_mx
         enddo                  ! while (keep_searching)
         if (Ntree.gt.Ntree_required) then
            keep_going=.false.
         endif                  ! Ntree>Ntree_required
      enddo                     ! while (keep_going)
c     
      inquire(file=trim(vegetation_file),exist=file_exist)
      if (file_exist) then
         open(11,file=trim(vegetation_file),access='append')
      else
         open(11,file=trim(vegetation_file),status='new')
         write(11,10) 'Input data for program "city_generator"'
         write(11,*)
         write(11,10) 'Position X [m] / Position Y [m] / scale / '
     &        //'rotation angle [deg] / material / obj file'
      endif                     ! file_exist
      do i=1,Ntree
         call choose_integer(Nmat_tree,cdf_tree,imat)
c     
c     ----------- sample height according to species -----------
         if (trim(obj_file_tree(imat)).eq.'tree01') then
            if (type01_tree_Height_dist.eq.1) then
               scale=type01_tree_Height_H
            else if (type01_tree_Height_dist.eq.2) then
               call uniform(type01_tree_Height_Hmin,
     &              type01_tree_Height_Hmax,
     &              scale)
            else if (type01_tree_Height_dist.eq.3) then
               call sample_normal(type01_tree_Height_mu,
     &              type01_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_01_trunk.obj'
            obj_file_foliage(i)='Tree_01_foliage.obj'
         else if (trim(obj_file_tree(imat)).eq.'tree02') then
            if (type02_tree_Height_dist.eq.1) then
               scale=type02_tree_Height_H
            else if (type02_tree_Height_dist.eq.2) then
               call uniform(type02_tree_Height_Hmin,
     &              type02_tree_Height_Hmax,
     &              scale)
            else if (type02_tree_Height_dist.eq.3) then
               call sample_normal(type02_tree_Height_mu,
     &              type02_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_02_trunk.obj'
            obj_file_foliage(i)='Tree_02_foliage.obj'
         else if (trim(obj_file_tree(imat)).eq.'tree03') then
            if (type03_tree_Height_dist.eq.1) then
               scale=type03_tree_Height_H
            else if (type03_tree_Height_dist.eq.2) then
               call uniform(type03_tree_Height_Hmin,
     &              type03_tree_Height_Hmax,
     &              scale)
            else if (type03_tree_Height_dist.eq.3) then
               call sample_normal(type03_tree_Height_mu,
     &              type03_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_03_trunk.obj'
            obj_file_foliage(i)='Tree_03_foliage.obj'
         else if (trim(obj_file_tree(imat)).eq.'tree04') then
            if (type04_tree_Height_dist.eq.1) then
               scale=type04_tree_Height_H
            else if (type04_tree_Height_dist.eq.2) then
               call uniform(type04_tree_Height_Hmin,
     &              type04_tree_Height_Hmax,
     &              scale)
            else if (type04_tree_Height_dist.eq.3) then
               call sample_normal(type04_tree_Height_mu,
     &              type04_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_04_trunk.obj'
            obj_file_foliage(i)='Tree_04_foliage.obj'
         else if (trim(obj_file_tree(imat)).eq.'tree05') then
            if (type05_tree_Height_dist.eq.1) then
               scale=type05_tree_Height_H
            else if (type05_tree_Height_dist.eq.2) then
               call uniform(type05_tree_Height_Hmin,
     &              type05_tree_Height_Hmax,
     &              scale)
            else if (type05_tree_Height_dist.eq.3) then
               call sample_normal(type05_tree_Height_mu,
     &              type05_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_05_trunk.obj'
            obj_file_foliage(i)='Tree_05_foliage.obj'
         else if (trim(obj_file_tree(imat)).eq.'tree06') then
            if (type06_tree_Height_dist.eq.1) then
               scale=type06_tree_Height_H
            else if (type06_tree_Height_dist.eq.2) then
               call uniform(type06_tree_Height_Hmin,
     &              type06_tree_Height_Hmax,
     &              scale)
            else if (type06_tree_Height_dist.eq.3) then
               call sample_normal(type06_tree_Height_mu,
     &              type06_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_06_trunk.obj'
            obj_file_foliage(i)='Tree_06_foliage.obj'
         else if (trim(obj_file_tree(imat)).eq.'tree07') then
            if (type07_tree_Height_dist.eq.1) then
               scale=type07_tree_Height_H
            else if (type07_tree_Height_dist.eq.2) then
               call uniform(type07_tree_Height_Hmin,
     &              type07_tree_Height_Hmax,
     &              scale)
            else if (type07_tree_Height_dist.eq.3) then
               call sample_normal(type07_tree_Height_mu,
     &              type07_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_07_trunk.obj'
            obj_file_foliage(i)='Tree_07_foliage.obj'
         else if (trim(obj_file_tree(imat)).eq.'tree08') then
            if (type08_tree_Height_dist.eq.1) then
               scale=type08_tree_Height_H
            else if (type08_tree_Height_dist.eq.2) then
               call uniform(type08_tree_Height_Hmin,
     &              type08_tree_Height_Hmax,
     &              scale)
            else if (type08_tree_Height_dist.eq.3) then
               call sample_normal(type08_tree_Height_mu,
     &              type08_tree_Height_sigma,
     &              scale)
            endif
            obj_file_trunk(i)='Tree_08_trunk.obj'
            obj_file_foliage(i)='Tree_08_foliage.obj'
         else
            call error(label)
            write(*,*) 'Unidentified tree:'
            write(*,*) 'obj_file_tree=',trim(obj_file_tree(imat))
            stop
         endif
c     ----------- sample height according to species -----------
c     
c     ----------- sample rotation angle -----------
         call uniform(0.0D+0,360.0D+0,angle)
c     ----------- sample rotation angle -----------
         write(11,*) tree_position(i,1), ! position X
     &        tree_position(i,2), ! position Y
     &        scale,            ! scaling factor
     &        angle,            ! rotation angle [deg]
     &        'tree_bark',      ! material for the trunk
     &        ' ',
     &        trim(obj_file_trunk(i)), ! obj file for the trunk
     &        ' ',
     &        trim(mat_tree(imat)), ! material for the foliage
     &        ' ',
     &        trim(obj_file_foliage(i)) ! obj file for the foliage
      enddo                     ! i
            
      return
      end



      subroutine generate_central_square(dim,
     &     mapXsize,mapYsize,Nzone,Nsector,alpha0,xcenter,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     csquare_thickness,csquare_Ncontour,
     &     csquare_Nppc,csquare_contour)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to generate the central square
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nzone: number of zones
c       + Nsector: number of angular sectors
c       + alpha0: angle for generating ring road contour
c       + xcenter: position of the ring center
c       + Nzone: total number of zones
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c     
c     Output:
c       + Nzone: updated value
c       + csquare_thickness: central square thickness
c       + csquare_Ncontour: number of contours for the central square
c       + csquare_Nppc: number of points that define each contour
c       + csquare_contour: central square contour
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nzone
      integer Nsector
      double precision alpha0
      double precision xcenter(1:Ndim_mx)
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      double precision csquare_thickness
      integer csquare_Ncontour
      integer csquare_Nppc(1:Ncontour_mx)
      double precision csquare_contour(1:Ncontour_mx,1:Nppc_mx,
     &     1:Ndim_mx-1)
c     temp
      integer icontour,isector,i,j
      double precision dalpha,alpha,dtheta
      double precision max_radius,radius
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int,d
      integer elem_type
      integer elem_index
      character*(Nchar_mx) zone_mat
      logical file_exist
      double precision scale,angle
      character*(Nchar_mx) foliage_mat,trunk_file,foliage_file
      character*(Nchar_mx) vegetation_file
c     label
      character*(Nchar_mx) label
      label='subroutine generate_central_square'

      vegetation_file='./results/vegetation.in'
      csquare_thickness=5.0D-2  ! m
      csquare_Ncontour=0
c     contour #1: main contour
      csquare_Ncontour=csquare_Ncontour+1
      if (csquare_Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'csquare_Ncontour=',csquare_Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
      icontour=1
      csquare_Nppc(icontour)=0
      dalpha=2.0D+0*pi/dble(Nsector)
      do isector=1,Nsector
         call sample_normal(mu_csquare_dim,sigma_csquare_dim,
     &        max_radius)
         alpha=alpha0+dalpha*dble(isector-1)
         u(1)=dcos(alpha)
         u(2)=dsin(alpha)
         u(3)=0.0D+0
c     look for intersections between (xcenter,u) and elements of the scene
         call line_scene_intersection(dim,mapXsize,mapYsize,
     &        xcenter,u,generate_river,river_branch,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Nroad,road_width,road_Nppt,road_track,
     &        intersection_found,xint,d2int,elem_type,elem_index)
         if ((intersection_found).and.(d2int.ge.max_radius)) then
            radius=0.90D+0*max_radius
         else
            radius=max_radius
         endif
         call scalar_vector(dim,radius,u,tmp)
         call add_vectors(dim,xcenter,tmp,x)
         call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x)
         csquare_Nppc(icontour)=csquare_Nppc(icontour)+1
         if (csquare_Nppc(icontour).gt.Nppc_mx) then
            call error(label)
            write(*,*) 'csquare_Nppc(',icontour,')=',
     &           csquare_Nppc(icontour)
            write(*,*) '> Nppc_mx=',Nppc_mx
            
            stop
         else
            do j=1,dim-1
               csquare_contour(icontour,csquare_Nppc(icontour),j)=
     &              x(j)
            enddo               ! j
         endif                  ! csquare_Nppc(icontour)>Nppc_mx
      enddo                     ! isector

      inquire(file=trim(vegetation_file),exist=file_exist)
      if (file_exist) then
         open(11,file=trim(vegetation_file),access='append')
      else
         open(11,file=trim(vegetation_file),status='new')
         write(11,10) 'Input data for program "city_generator"'
         write(11,*)
         write(11,10) 'Position X [m] / Position Y [m] / scale / '
     &        //'rotation angle [deg] / material for trunk / '
     &        //'obj file for trunk / material for foliage / '
     &        //'obj file for foliage'
      endif                     ! file_exist
c     generate holes in the main contour (for trees)
      radius=1.0D+1             ! m
      d=1.0D+0                  ! m
      dtheta=d/radius           ! rad
      do isector=1,Nsector
         icontour=icontour+1
         csquare_Ncontour=csquare_Ncontour+1
         if (csquare_Ncontour.gt.Ncontour_mx) then
            call error(label)
            write(*,*) 'csquare_Ncontour=',csquare_Ncontour
            write(*,*) '> Ncontour_mx=',Ncontour_mx
            stop
         endif
         alpha=alpha0+dalpha*dble(isector-1)
         csquare_Nppc(icontour)=4
         csquare_contour(icontour,1,1)=xcenter(1)+
     &        (radius-d/2.0D+0)*dcos(alpha-dtheta/2.0D+0)
         csquare_contour(icontour,1,2)=xcenter(2)+
     &        (radius-d/2.0D+0)*dsin(alpha-dtheta/2.0D+0)
         csquare_contour(icontour,2,1)=xcenter(1)+
     &        (radius+d/2.0D+0)*dcos(alpha-dtheta/2.0D+0)
         csquare_contour(icontour,2,2)=xcenter(2)+
     &        (radius+d/2.0D+0)*dsin(alpha-dtheta/2.0D+0)
         csquare_contour(icontour,3,1)=xcenter(1)+
     &        (radius+d/2.0D+0)*dcos(alpha+dtheta/2.0D+0)
         csquare_contour(icontour,3,2)=xcenter(2)+
     &        (radius+d/2.0D+0)*dsin(alpha+dtheta/2.0D+0)
         csquare_contour(icontour,4,1)=xcenter(1)+
     &        (radius-d/2.0D+0)*dcos(alpha+dtheta/2.0D+0)
         csquare_contour(icontour,4,2)=xcenter(2)+
     &        (radius-d/2.0D+0)*dsin(alpha+dtheta/2.0D+0)
         call uniform(6.0D+0,12.0D+0,scale)
         call uniform(0.0D+0,360.0D+0,angle)
         foliage_mat='dark_green'
         trunk_file='Tree_03_trunk.obj'
         foliage_file='Tree_03_foliage.obj'
         write(11,*) xcenter(1)+radius*dcos(alpha), ! position X
     &        xcenter(2)+radius*dsin(alpha), ! position Y
     &        scale,            ! scaling factor
     &        angle,            ! rotation angle [deg]
     &        'tree_bark ',
     &        trim(trunk_file),
     &        ' ',
     &        trim(foliage_mat),
     &        ' ',
     &        trim(foliage_file)
      enddo                     ! isector
      close(11)
c     generate definition file for central square
      Nzone=Nzone+1
      zone_mat=trim(central_plazza_material)
      call record_zone_definition_file(dim,Nzone,
     &     csquare_thickness,csquare_Ncontour,
     &     csquare_Nppc,csquare_contour,
     &     zone_mat)
      
      return
      end
