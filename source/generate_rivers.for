c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine generate_rivers(dim,mapXsize,mapYsize,
     &     river_branch,
     &     Nriver,river_width,river_Nppt,river_track)
      implicit none
      include 'max.inc'
      include 'distribution_parameters.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to generate river(s) definition files
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + river_branch: set to true if a river branch has to be generated
c     
c     Output:
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + './results/river***.in' definition files
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer i,j
      double precision xmin,xmax,ymin,ymax
      double precision Lmin,Lmax,L1,L2,L3,L4
      double precision central_track(1:Nppt_mx,1:Ndim_mx-1)
      double precision central_track2(1:Nppt_mx,1:Ndim_mx-1)
      double precision xc,yc
      character*(Nchar_mx) river_mat
c     label
      character*(Nchar_mx) label
      label='subroutine generate_rivers'

      if (river_branch) then
c     A first 4-segment track, starting at the top of the map and ending at the bottom
c     then a 2-segment track, starting at the end of the second segment of the first track,
c     and ending on the right side of the map
         Nriver=2
         river_Nppt(1)=5
         river_Nppt(2)=3
c     sample widths
c         call sample_normal(mu_rw,sigma_rw,river_width(1))
c         call sample_normal(mu_rw,sigma_rw,river_width(2))
c     ------------  sample rivers width ------------------
         if (river_width_dist.eq.1) then
            river_width(1)=river_width_val
            river_width(2)=river_width_val
         else if (river_width_dist.eq.2) then
            call uniform(river_width_valmin,river_width_valmax,
     &           river_width(1))
            call uniform(river_width_valmin,river_width_valmax,
     &           river_width(2))
         else if (river_width_dist.eq.3) then
            call sample_normal(river_width_mu,river_width_sigma,
     &           river_width(1))
            call sample_normal(river_width_mu,river_width_sigma,
     &           river_width(2))
         endif
c     ------------  sample rivers width ------------------
c     sample position at the top of the map (track 1)
         xmin=0.50D+0*mapXsize+river_width(1)/2.0D+0
         xmax=0.90D+0*mapXsize-river_width(1)/2.0D+0
         call uniform(xmin,xmax,central_track(1,1))
         central_track(1,2)=mapYsize
c     sample position at the bottom of the map (track 1)
         xmax=0.80D+0*mapXsize-river_width(1)/2.0D+0
         call uniform(xmin,xmax,central_track(5,1))
         central_track(5,2)=0.0D+0
c     sample position on the right side of the map (track 2)
         ymin=0.10D+0*mapYsize-river_width(2)/2.0D+0
         ymax=0.50D+0*mapYsize+river_width(2)/2.0D+0
         call uniform(ymin,ymax,central_track2(3,2))
         central_track2(3,1)=mapXsize
c     sample length of the first segment (track 1)
         Lmin=0.1D+0*mapYsize
         Lmax=0.25D+0*mapYsize
         call uniform(Lmin,Lmax,L1)
c     sample length of the 4th segment (track 1)
         call uniform(Lmin,Lmax,L4)
c     sample length of the second segment (track 2)
         Lmin=0.05D+0*mapXsize
         Lmax=0.10D+0*mapXsize
         call uniform(Lmin,Lmax,L2)
c     sample position of the common point between the 2 tracks
         xmin=0.65*mapXsize
         xmax=0.80*mapXsize
         call uniform(xmin,xmax,xc)
         ymin=0.40*mapYsize
         ymax=0.60*mapYsize
         call uniform(ymin,ymax,yc)
c     complete track 1
         central_track(2,1)=central_track(1,1)
         central_track(2,2)=central_track(1,2)-L1
         central_track(3,1)=xc
         central_track(3,2)=yc
         central_track(4,1)=central_track(5,1)
         central_track(4,2)=central_track(5,2)+L4
c     and add it to 'river_track'
         do i=1,river_Nppt(1)
            do j=1,dim-1
               river_track(1,i,j)=central_track(i,j)
            enddo               ! j
         enddo                  ! i
c     generate definition file
         river_mat=trim(water_zones_material)
         call record_river_definition_file(dim,1,river_width(1),
     &        river_Nppt(1),central_track,
     &        river_mat)
c     complete track 2
         central_track2(1,1)=xc
         central_track2(1,2)=yc
         central_track2(2,1)=central_track2(3,1)-L2
         central_track2(2,2)=central_track2(3,2)
c     and add it to 'river_track'
         do i=1,river_Nppt(2)
            do j=1,dim-1
               river_track(2,i,j)=central_track2(i,j)
            enddo               ! j
         enddo                  ! i
c     generate definition file
         river_mat=trim(water_zones_material)
         call record_river_definition_file(dim,2,river_width(2),
     &        river_Nppt(2),central_track2,
     &        river_mat)
      else ! no branches
c     Basic geometry: the main branch of the river crosses the map vertically;
c     the position at the top and bottom of the map may be different, so that at least
c     a significant portion of this main branch is not vertical.
         Nriver=1
         river_Nppt(1)=4 ! number of points needed to describe the central track
c     sample width
c         call sample_normal(mu_rw,sigma_rw,river_width(1))
c     ------------  sample river width ------------------
         if (river_width_dist.eq.1) then
            river_width(1)=river_width_val
         else if (river_width_dist.eq.2) then
            call uniform(river_width_valmin,river_width_valmax,
     &           river_width(1))
         else if (river_width_dist.eq.3) then
            call sample_normal(river_width_mu,river_width_sigma,
     &           river_width(1))
         endif
c     ------------  sample rivers width ------------------
c     sample position at the top of the map
         xmin=0.50D+0*mapXsize+river_width(1)/2.0D+0
         xmax=0.90D+0*mapXsize-river_width(1)/2.0D+0
         call uniform(xmin,xmax,central_track(1,1))
         central_track(1,2)=mapYsize
c     sample length of the first segment
         Lmin=0.1D+0*mapYsize
         Lmax=0.25D+0*mapYsize
         call uniform(Lmin,Lmax,L1)
c     position at the end of the first segment
         central_track(2,1)=central_track(1,1)
         central_track(2,2)=central_track(1,2)-L1
c     sample position at the bottom of the map
         call uniform(xmin,xmax,central_track(4,1))
         central_track(4,2)=0.0D+0
c     sample length of the 3rd segment
         call uniform(Lmin,Lmax,L3)
c     position at the end of the second segment
         central_track(3,1)=central_track(4,1)
         central_track(3,2)=central_track(4,2)+L3
c     add 'central_track' to 'river_track'
         do i=1,river_Nppt(1)
            do j=1,dim-1
               river_track(1,i,j)=central_track(i,j)
            enddo               ! j
         enddo                  ! i
c     generate definition file
         river_mat=trim(water_zones_material)
         call record_river_definition_file(dim,1,river_width(1),
     &        river_Nppt(1),central_track,
     &        river_mat)
      endif                     ! river_branch

      return
      end


      subroutine extract_river_track(dim,Nriver,river_Nppt,river_track,
     &     itrack,Npoint,track)
      implicit none
      include 'max.inc'
c     
c     Purpose: to extract a track from the list of river tracks
c     This routine will actually consider there are 3 branches to the river when river_branch=T
c     and the "itrack" parameter can take values between 1 and 3.
c     
c     Input:
c       + dim: dimension of space
c       + Nriver: number of rivers that have been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + itrack: index of the track to extract
c         - itrack=1: upper part of the main river
c         - itrack=2: lower part of the main river
c         - itrack=3: branch
c     
c     Output:
c       + Npoint: number of points in the required track
c       + track: required track
c     
c     I/O
      integer dim
      integer Nriver
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      integer itrack
      integer Npoint
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer ipoint,j
c     label
      character*(Nchar_mx) label
      label='subroutine extract_river_track'

      if (Nriver.eq.1) then
         if (itrack.ne.1) then
            call error(label)
            write(*,*) 'Nriver=',Nriver
            write(*,*) 'itrack=',itrack
            write(*,*) 'should be =1'
            stop
         endif
         Npoint=river_Nppt(1)
         do ipoint=1,Npoint
            do j=1,dim-1
               track(ipoint,j)=river_track(1,ipoint,j)
            enddo               ! j
         enddo                  ! ipoint
      else
         if ((itrack.lt.1).or.(itrack.gt.3)) then
            call error(label)
            write(*,*) 'Nriver=',Nriver
            write(*,*) 'itrack=',itrack
            write(*,*) 'should be in [1,3] range'
            stop
         endif
         Npoint=3
         if (itrack.eq.1) then
            do ipoint=1,Npoint
               do j=1,dim-1
                  track(ipoint,j)=river_track(1,ipoint,j)
               enddo            ! j
            enddo               ! ipoint
         else if (itrack.eq.2) then
            do ipoint=1,Npoint
               do j=1,dim-1
                  track(ipoint,j)=river_track(1,ipoint+2,j)
               enddo            ! j
            enddo               ! ipoint
         else if (itrack.eq.3) then
            do ipoint=1,Npoint
               do j=1,dim-1
                  track(ipoint,j)=river_track(2,ipoint,j)
               enddo            ! j
            enddo               ! ipoint
         endif                  ! itrack
      endif                     ! Nriver
      
      return
      end
