c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine mask_cells(dim,mapXsize,mapYsize,use_MapArray,
     &     cell_length,Ncontour,Nppc,contour,NcellsX,NcellsY,MapArray)
      implicit none
      include 'max.inc'
c     
c     Purpose: to mask MapArray cells according to provided contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + use_MapArray: set to true if cells have to be masked in MapArray
c       + cell_length: length of a cell [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + NcellsX: size of first dimension of 'MapArray'
c       + NcellsY: size of second dimension of 'MapArray'
c       + MapArray: array of cells; a value of T means the cell is occupied
c     
c     Output:
c       + MapArray: array of cells (updated)
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      logical use_MapArray
      double precision cell_length
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer NcellsX
      integer NcellsY
      logical(1) :: MapArray(NcellsX,NcellsY)
c     temp
      integer i,j,Ncells
      double precision bounding_box(1:Ndim_mx-1,1:2)
      integer interval(1:Ndim_mx-1,1:2)
      integer icontour
      double precision Pnode(1:Ndim_mx)
      logical inside,Pnode_is_in_contour
c     Debug
      integer k
c     Debug
c     label
      character*(Nchar_mx) label
      label='subroutine mask_cells'

c     Find bounding box for provided contours
c      
c     "bounding_box(i,j)" is for dimension i, min (j=1) or max (j=2)
c      
c                                  |                        |
c     bounding_box(i=2,j=2) --------------------------------------------
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                                  |                        |     
c     bounding_box(i=2,j=1) --------------------------------------------
c                                  |                        |
c                                  |                        |
c                                  |                        |
c                        bounding_box(i=1,j=1)      bounding_box(i=1,j=2)
c
      if (.not.use_MapArray) then
         goto 666
      endif
c      
      write(*,*) 'Masking cells...'
      
      call contour_bounding_box(dim,Ncontour,Nppc,contour,
     &     bounding_box)

c     "interval(i,j)" is the interval where "bounding_box(i,j)" is located
      do i=1,dim-1
         do j=1,2
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,
     &           bounding_box(i,j))
            interval(i,j)=ceiling(bounding_box(i,j)/cell_length)
c     pick next interval in the case "bounding_box(i,j)" is located exactly
c     at the interface between two intervals, for the lower boundary (j=1).
c     For the higher boundary (j=2), the default choice is fine.
            if ((interval(i,j).eq.int(bounding_box(i,j)/cell_length))
     &           .and.(j.eq.1)) then
               interval(i,j)=interval(i,j)+1
            endif
         enddo                  ! j
      enddo                     ! i

c     Consistency check: interval(i,j) should always be in range
      do i=1,dim-1
         if (i.eq.1) then
            Ncells=NcellsX
         else
            Ncells=NcellsY
         endif
         do j=1,2
            if (interval(i,j).lt.1) then
               call error(label)
               write(*,*) 'interval(',i,',',j,')=',interval(i,j)
               write(*,*) 'should be > 0'
               stop
            endif
            if (interval(i,j).gt.Ncells) then
               call error(label)
               write(*,*) 'interval(',i,',',j,')=',interval(i,j)
               write(*,*) 'should be <= Ncells=',Ncells
               stop
            endif
         enddo                  ! j
      enddo                     ! i

c     Debug
c      do i=1,dim-1
c         do j=1,2
c            write(*,*) 'interval(',i,',',j,')=',interval(i,j)
c         enddo                  ! j
c      enddo                     ! i
c     Debug

c     check for nodes only within the limits of the bounding box
      do i=interval(1,1),interval(1,2)-1
         do j=interval(2,1),interval(2,2)-1
            Pnode(1)=i*cell_length
            Pnode(2)=j*cell_length
            Pnode(3)=0.0D+0
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,
     &           Pnode)
            Pnode_is_in_contour=.false.
            do icontour=1,Ncontour
               call is_inside_contour(.false.,
     &              dim,Ncontour,Nppc,contour,
     &              icontour,Pnode,inside)
c     stop if node (i,j) is inside at least one contour
               if (inside) then
                  Pnode_is_in_contour=.true.
                  goto 111
               endif
            enddo               ! icontour
 111        continue
c     if node (i,j) is inside a contour, the 4 cells that have this node in common
c     are labeled as occupied
            if (Pnode_is_in_contour) then
               MapArray(i,j)=.true.
               MapArray(i,j+1)=.true.
               MapArray(i+1,j)=.true.
               MapArray(i+1,j+1)=.true.
            endif
c     
         enddo                  ! j
      enddo                     ! i

 666  continue
      return
      end
