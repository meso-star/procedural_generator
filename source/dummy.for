c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine dummy(NcellsX,NcellsY,MapArray)
      implicit none
      include 'max.inc'
c     
c     Purpose: test the passage of a allocatable array in subroutine arguments
c     
c     Input:
c       + NcellsX: size of first dimension of 'MapArray'
c       + NcellsY: size of second dimension of 'MapArray'
c       + MapArray: allocatable array
c     
c     Output:
c     
c     I/O
      integer NcellsX
      integer NcellsY
      logical(1) :: MapArray(NcellsX,NcellsY)
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine dummy'

c     Debug
c      write(*,*) 'This is ',trim(label)
c     Debug
      MapArray(2,3)=.true.

      return
      end
