c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine tetris2(dim,mapXsize,mapYsize,
     &     Nsector,alpha1,xcenter,ilevel,isector,
     &     Ncontour_global,Nppc_global,contour_global,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to subdivide a level 1 or level 2 sector
c     into a number of building contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nsector: number of angular sectors
c       + alpha1: angle for generating level1 ring road contour
c       + xcenter: position of the ring center
c       + ilevel: level index [1,2]
c       + isector: index of the sector
c       + Ncontour_global: number of contours (only one at input time)
c       + Nppc_global: number of points that define each contour
c       + contour_global: list of coordinates for each contour
c     
c     Output:
c       + Ncontour_global: number of contours (updated)
c       + Nppc_global: number of points that define each contour (updated)
c       + contour_global: list of coordinates for each contour (updated)
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c       + contour_type: type of contour
c         - 0: surrounding contour (contour index 1)
c         - 1: building only
c         - 2: terrain + building
c         - 3: terrain only
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nsector
      double precision alpha1
      double precision xcenter(1:Ndim_mx)
      integer ilevel
      integer isector
      integer Ncontour_global
      integer Nppc_global(1:Ncontour_mx)
      double precision contour_global(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer contour_type(1:Ncontour_mx)
c     temp
      logical generate_sector_plot
      integer ifootprint
      double precision deltaX,deltaY
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      double precision M10(1:Ndim_mx,1:Ndim_mx)
      double precision M01(1:Ndim_mx,1:Ndim_mx)
      character*(Nchar_mx) filename
      integer Ncontour_sm
      integer Nppc_sm(1:Ncontour_mx)
      double precision contour_sm(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      logical keep_generating_footprints
      integer Niter_max
      logical success
      double precision x0_seed(1:Ndim_mx)
      integer Np_seed
      double precision ctr_seed(1:Nppc_mx,1:Ndim_mx-1)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision P2P3(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision P4P1(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u43(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      integer icontour,i,j
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision width
      logical inside
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
c     label
      character*(Nchar_mx) label
      label='subroutine tetris2'
      
      generate_sector_plot=.true.
      Niter_max=1000

      if (Ncontour_global.ne.1) then
         call error(label)
         write(*,*) 'Ncontour_global=',Ncontour_global
         write(*,*) 'that seems awkward'
         stop
      endif
      if (Nppc_global(1).ne.4) then
         call error(label)
         write(*,*) 'Npcc_global(1)=',Nppc_global(1)
         write(*,*) 'should be equal to 4'
         stop
      endif
c     convert input "contour_global" to local "contour"
      call global2local(dim,xcenter,
     &     Ncontour_global,Nppc_global,contour_global,
     &     Ncontour,Nppc,contour,
     &     M01,M10)
c     generate inner contour for contour index 1
      call get_contour(dim,Ncontour,Nppc,contour,1,
     &     Np,ctr)
      call generate_close_contour(dim,Np,ctr,pavement_width,.true.,
     &     Np2,ctr2)
c     then add it to the 'shadow mask' contour
      Ncontour_sm=0
      call add_ctr_to_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,
     &     Np2,ctr2)
c     Find all border directions
      call get_position_from_ctr(dim,Np2,ctr2,1,P1)
      call get_position_from_ctr(dim,Np2,ctr2,2,P2)
      call get_position_from_ctr(dim,Np2,ctr2,3,P3)
      call get_position_from_ctr(dim,Np2,ctr2,4,P4)
      call substract_vectors(dim,P2,P1,P1P2)
      call substract_vectors(dim,P3,P2,P2P3)
      call substract_vectors(dim,P4,P3,P3P4)
      call substract_vectors(dim,P1,P4,P4P1)
      call normalize_vector(dim,P1P2,u12)
      call normalize_vector(dim,P2P3,u23)
      call normalize_vector(dim,P3P4,u34)
      call normalize_vector(dim,P4P1,u41)
      call scalar_vector(dim,-1.0D+0,u12,u21)
      call scalar_vector(dim,-1.0D+0,u23,u32)
      call scalar_vector(dim,-1.0D+0,u34,u43)
      call scalar_vector(dim,-1.0D+0,u41,u14)
c     Main loop
      ifootprint=0
      keep_generating_footprints=.true.
      do while (keep_generating_footprints)
         ifootprint=ifootprint+1
c     seeding
         call seeding(dim,mapXsize,mapYsize,
     &        Ncontour_sm,Nppc_sm,contour_sm,
     &        Niter_max,success,x0_seed,Np_seed,ctr_seed)
c     Debug
         write(*,*) 'After seeding, success=',success
         deltaX=20.0D+0
         deltaY=20.0D+0
c     Debug
         if (success) then
c     Debug
c            write(*,*) 'ctr_seed:'
c            write(*,*) 'Np_seed=',Np_seed
c            do i=1,Np
c               write(*,*) (ctr_seed(i,j),j=1,dim-1)
c            enddo               ! i
c     Debug
            call growth(dim,mapXsize,mapYsize,
     &           Ncontour_sm,Nppc_sm,contour_sm,
     &           x0_seed,Np_seed,ctr_seed,
     &           deltaX,deltaY,ifootprint,
     &           P1,P2,P3,P4,
     &           u12,u23,u34,u41,u21,u32,u43,u14,
     &           Np,ctr,
     &           on_left_side,on_right_side,
     &           on_bottom_side,on_top_side)
c     Adding ctr to contour_global
            call add_ctr_to_contour(dim,
     &           Ncontour,Nppc,contour,
     &           Np,ctr)
         endif                  ! success
c     Debug
c         write(*,*) 'Ncontour=',Ncontour
c         do icontour=1,Ncontour
c            write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
c            do i=1,Nppc(icontour)
c               write(*,*) (contour(icontour,i,j),j=1,dim-1)
c            enddo               ! i
c         enddo                  ! icontour
c     Debug
c     Generate surrounding contour and add it to contour_sm

c     Debug
         keep_generating_footprints=.false.
c     Debug
      enddo                     ! while (keep_generating_footprints)

c     adding 1st sm contour to plot
c      call get_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,1,
c     &     Np,ctr)
c      call add_ctr_to_contour(dim,
c     &     Ncontour,Nppc,contour,
c     &     Np,ctr)

      
c     generate gnuplot contour files (of contour in local referential)
      if (generate_sector_plot) then
         filename='./gnuplot/gra_sector'
         call record_sector_contour(filename,dim,
     &        Ncontour,Nppc,contour)
      endif                     ! generate_sector_plot

c     test
c      call local2global(dim,xcenter,
c     &     Ncontour,Nppc,contour,
c     &     Ncontour_global,Nppc_global,contour_global,
c     &     M01,M10)
c      write(*,*) 'Ncontour_global=',Ncontour_global
c      do icontour=1,Ncontour_global
c         write(*,*) 'Nppc_global(',icontour,')=',Nppc_global(icontour)
c         do i=1,Nppc_global(icontour)
c            write(*,*) (contour_global(icontour,i,j),j=1,dim-1)
c         enddo                  ! i
c      enddo                     ! icontour
c     test

      return
      end



      subroutine growth(dim,mapXsize,mapYsize,
     &     Ncontour_sm,Nppc_sm,contour_sm,
     &     x0_seed,Np_seed,ctr_seed,
     &     deltaX,deltaY,ifootprint,
     &     P1,P2,P3,P4,
     &     u12,u23,u34,u41,u21,u32,u43,u14,
     &     Np,ctr,
     &     on_left_side,on_right_side,
     &     on_bottom_side,on_top_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: to grow a contour from a seed
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour_sm: number of contours in the shadow mask
c       + Nppc_sm: numher of points for each contour in the shadow mask
c       + contour_sm: shadow mask contours
c       + x0_seed: central position of seeding contour
c       + Np_seed: number of points for seeding contour
c       + ctr_seed: seeding contour
c       + deltaX: maximum possible extension in X direction(s)
c       + deltaY: maximum possible extension in Y direction(s)
c       + ifootprint: index of the growing footprint
c       + P1,P2,P3,P4: positions of the 4 corners of contour index 1 of "contour_sm"
c       + u12,u23,u34,u41,u21,u32,u43,u14: propagation directions along the borders
c     
c     Output:
c       + Np: number of points for the generated contour
c       + ctr: generated contour
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c     
c     I/O
      integer dim
      double precision mapXsize,mapYsize
      integer Ncontour_sm
      integer Nppc_sm(1:Ncontour_mx)
      double precision contour_sm(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      double precision x0_seed(1:Ndim_mx)
      integer Np_seed
      double precision ctr_seed(1:Nppc_mx,1:Ndim_mx-1)
      double precision deltaX
      double precision deltaY
      integer ifootprint
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u43(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
c     temp
      integer i,j
      double precision d
      integer Np1
      double precision ctr1(1:Nppc_mx,1:Ndim_mx-1)
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
      integer Np3
      double precision ctr3(1:Nppc_mx,1:Ndim_mx-1)
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      integer direction
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision n12(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      double precision tmp(1:Ndim_mx),norm
      integer idx(1:2)
      double precision dir(1:2,1:Ndim_mx)
      double precision df(1:2)
      double precision main(1:Ndim_mx)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      logical consistent
      logical intersect
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
      logical is_insidej
      integer Nc1
      integer idx1(1:Ncontour_mx)
      logical is_insidei
      integer Nc2
      integer idx2(1:Ncontour_mx)
      logical intersection_found
      logical intersection1_found
      logical intersection2_found
      logical intersection3_found
      logical intersection4_found
      double precision Pint(1:Ndim_mx)
      double precision xint(1:Ndim_mx)
      double precision d2int
      double precision xmid(1:Ndim_mx),length
      integer icontour
      integer isegment
      integer Np_min
      double precision ctr_min(1:Nppc_mx,1:Ndim_mx-1)
      integer Np_max
      double precision ctr_max(1:Nppc_mx,1:Ndim_mx-1)
      double precision dmin,dmax
      logical keep_looking,solution_found
      integer Niter
      double precision d2x1x2
      double precision A(1:Ndim_mx)
      double precision Axint(1:Ndim_mx),sp
      logical A_is_at_end
      integer end_side
c     parameters
      double precision epsilon_d
      parameter(epsilon_d=1.0D-2)
c     label
      character*(Nchar_mx) label
      label='subroutine growth'

      on_left_side(ifootprint)=.false.
      on_right_side(ifootprint)=.false.
      on_bottom_side(ifootprint)=.false.
      on_top_side(ifootprint)=.false.
c     "ctr1" is the current valid contour
c     "ctr2" is the temporary growing contour, used to test consistency
      call duplicate_ctr(dim,Np_seed,ctr_seed,Np1,ctr1)
c     dummy "contour"
      Ncontour=0
      do icontour=1,Ncontour_sm
         Nppc(icontour)=Nppc_sm(icontour)
         call get_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,icontour,
     &        Np_tmp,ctr_tmp)
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_tmp,ctr_tmp)
      enddo                     ! icontour

c     Trying to grow in the specified direction
      do direction=1,4
         call duplicate_ctr(dim,Np1,ctr1,Np2,ctr2)
         call moving_points(dim,ifootprint,
     &        on_left_side,on_right_side,
     &        on_bottom_side,on_top_side,
     &        direction,
     &        u12,u23,u34,u41,u21,u32,u43,u14,
     &        idx,dir,df,main)
c     Debug
         write(*,*) '******************************'
         write(*,*) 'direction=',direction
         write(*,*) 'idx=',(idx(i),i=1,2)
         write(*,*) 'dir='
         do i=1,2
            write(*,*) (dir(i,j),j=1,2)
         enddo                  ! i
         write(*,*) 'df=',(df(i),i=1,2)
         write(*,*) 'Using current ctr2:'
         write(*,*) 'Np2=',Np2
         do i=1,Np
            write(*,*) (ctr2(i,j),j=1,dim-1)
         enddo                  ! i
c     Debug
         if ((idx(1).eq.0).and.(idx(2).eq.0)) then
c     displacement is forbidden in that direction for the footprint variety
c     skip to next displacement direction
            goto 111
         endif                  ! idx(1)=idx(2)=0
c     
         if ((direction.eq.1).or.(direction.eq.2)) then
            d=deltaX
         else if ((direction.eq.3).or.(direction.eq.4)) then
            d=deltaY
         endif
         call get_position_from_ctr(dim,Np2,ctr2,idx(1),x1)
         call get_position_from_ctr(dim,Np2,ctr2,idx(2),x2)
         do j=1,dim
            u1(j)=dir(1,j)
            u2(j)=dir(2,j)
         enddo                  ! j
c     -------------------------------------------------------------------------------
c     lines (x1,u1) and (x2,u2) may intersect at a position
c     such that the distance between [x1,x2] and this intersection
c     position is smaller than the required displacement length
c     This case must be detected because such a displacement has no meaning
         call substract_vectors(dim,x2,x1,x1x2)
         norm=dsqrt(x1x2(1)**2.0D+0+x1x2(2)**2.0D+0)
         if (norm.gt.0.0D+0) then
            n12(1)=x1x2(2)/norm
            n12(2)=-x1x2(1)/norm
            n12(3)=0.0D+0
         endif
         call line1_line2_intersection(dim,x1,x2,u1,u2,
     &        intersection_found,xint)
         if (intersection_found) then
c     Debug
c         write(*,*) 'intersection_found=',intersection_found
c         if (intersection_found) then
c            write(*,*) 'xint=',xint
c         endif
c     Debug
            call distance_to_vector(dim,x1,x2,xint,d2x1x2,A,
     &           A_is_at_end,end_side)
            call substract_vectors(dim,xint,A,Axint)
            call normalize_vector(dim,Axint,u)
            call scalar_product(dim,n12,u,sp)
c     Debug
c            write(*,*) 'A=',A
c            write(*,*) 'd2x1x2=',d2x1x2
c            write(*,*) 'n12=',n12
c            write(*,*) 'u=',u
c            write(*,*) 'sp=',sp
c     Debug
            if (sp.gt.00D+0) then
               d2int=sp*d2x1x2
c     Debug
c               write(*,*) 'd2int=',d2int,' d=',d
c     Debug
               if (d2int.lt.d) then
                  d=0.99D+0*d2int
c     In this case, the required displacement distance "d"
c     is set just beyond the maximum displacement "d2int"
c     for which the intersection between (x1,u1) and (x2,u2) is reached
c     Debug
c                  write(*,*) 'Travel distance is set to:',d
c     Debug
               endif            ! d2int > d
            endif               ! sp > 0
         endif                  ! intersection_found
c     -------------------------------------------------------------------------------
c     Now a special "ctr3" has to be produced just to detect the intersection
c     with contour_sm(1)
         call duplicate_ctr(dim,Np1,ctr1,Np3,ctr3)
         call scalar_vector(dim,d,main,tmp)
         call add_vectors(dim,x1,tmp,x)
         call set_position_in_ctr(dim,Np3,ctr3,idx(1),x)
         call scalar_vector(dim,d,main,tmp)
         call add_vectors(dim,x2,tmp,x)
         call set_position_in_ctr(dim,Np3,ctr3,idx(2),x)
c     Debug
         write(*,*) 'After deformation ctr3:'
         write(*,*) 'Np3=',Np3
         do i=1,Np
            write(*,*) (ctr3(i,j),j=1,dim-1)
         enddo                  ! i
c     Debug
c     test consistency of "ctr3"
c         Ncontour=Ncontour_sm
c         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
c     &        Np3,ctr3)
c         call identify_inconsistent_contour(dim,
c     &        mapXsize,mapYsize,
c     &        Ncontour,Nppc,contour,Ncontour,
c     &        consistent,
c     &        intersect,Nintersected,intersected_ic,
c     &        is_insidej,Nc1,idx1,
c     &        is_insidei,Nc2,idx2)
         call vector_length(dim,x1x2,length)
         call normalize_vector(dim,x1x2,u)
         call scalar_vector(dim,length/2.0D+0,u,tmp)
         call add_vectors(dim,x1,tmp,xmid)
c     Debug
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'x1x2=',x1x2
         write(*,*) 'u=',u
         write(*,*) 'xmid=',xmid
         write(*,*) 'main=',main
c     Debug
         if (direction.eq.1) then
            call segment_line_intersection(dim,P3,P4,xmid,main,.true.,
     &           intersection_found,Pint,d2int)
         else if (direction.eq.2) then
            call segment_line_intersection(dim,P1,P2,xmid,main,.true.,
     &           intersection_found,Pint,d2int)
         else if (direction.eq.3) then
            call segment_line_intersection(dim,P4,P1,xmid,main,.true.,
     &           intersection_found,Pint,d2int)
         else if (direction.eq.4) then
            call segment_line_intersection(dim,P2,P3,xmid,main,.true.,
     &           intersection_found,Pint,d2int)
         endif
c     Debug
         write(*,*) 'd=',d
         write(*,*) 'intersection_found=',intersection_found,d2int
c         write(*,*) 'intersect=',intersect
c         if (intersect) then
c            write(*,*) 'Nintersected=',Nintersected
c            do i=1,Nintersected
c               write(*,*) 'intersected_ic(',i,')=',
c     &              intersected_ic(i)
c            enddo               ! i
c         endif                  ! intersect
c         write(*,*) 'is_insidej=',is_insidej
c         write(*,*) 'is_insidei=',is_insidei
c     Debug
c         if ((.not.consistent)
c     &        .and.(intersect).and.
c     &        (.not.is_insidej).and.
c     &        (.not.is_insidei).and.
c     &        (Nintersected.eq.1).and.
c     &        (intersected_ic(1).eq.1)) then
c     Debug
         if ((intersection_found).and.(d2int.le.d)) then
            write(*,*) 'contour 1 is intersected by ctr3, direction=',
     &           direction
c     Debug
            if (direction.eq.1) then
               on_left_side(ifootprint)=.true.
c     Debug
               write(*,*) 'on_left_side(',ifootprint,')=',
     &              on_left_side(ifootprint)
c     Debug
               if (on_top_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,4,P3)
c     Debug
                  write(*,*) 'direction=',direction,
     &                 ' Point 4 takes position of P3'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(1),x)
                  do j=1,dim
                     u(j)=dir(1,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the left'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(1),
     &                    xint)
                  endif         ! error
               endif            ! on_top_side(iffotprint)
               if (on_bottom_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,1,P4)
c     Debug
                  write(*,*) 'Point 1 takes position of P4'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(2),x)
                  do j=1,dim
                     u(j)=dir(2,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the left'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(2),
     &                    xint)
                  endif         ! error
               endif            ! on_bottom_side(ifootprint)
            else if (direction.eq.2) then
               on_right_side(ifootprint)=.true.
c     Debug
               write(*,*) 'on_right_side(',ifootprint,')=',
     &              on_right_side(ifootprint)
c     Debug
               if (on_bottom_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,2,P1)
c     Debug
                  write(*,*) 'Point 2 takes position of P1'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(1),x)
                  do j=1,dim
                     u(j)=dir(1,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the right'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(1),
     &                    xint)
                  endif         ! error
               endif            ! on_bottom_side(ifootprint)
               if (on_top_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,3,P2)
c     Debug
                  write(*,*) 'Point 3 takes position of P2'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(2),x)
                  do j=1,dim
                     u(j)=dir(2,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the right'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(2),
     &                    xint)
                  endif         ! error
               endif            ! on_top_side(ifootprint)
            else if (direction.eq.3) then
               on_bottom_side(ifootprint)=.true.
c     Debug
               write(*,*) 'on_bottom_side(',ifootprint,')=',
     &              on_bottom_side(ifootprint)
c     Debug

               if (on_left_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,1,P4)
c     Debug
                  write(*,*) 'Point 1 takes position of P4'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(1),x)
                  do j=1,dim
                     u(j)=dir(1,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the bottom'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                     stop
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(1),
     &                    xint)
                  endif         ! error
               endif            ! on_left_side(ifootprint)
               if (on_right_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,2,P1)
c     Debug
                  write(*,*) 'Point 2 takes position of P1'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(2),x)
                  do j=1,dim
                     u(j)=dir(2,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the bottom'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                     stop
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(2),
     &                    xint)
                  endif         ! error
               endif            ! on_right_side(ifootprint)
            else if (direction.eq.4) then
               on_top_side(ifootprint)=.true.
c     Debug
               write(*,*) 'on_top_side(',ifootprint,')=',
     &              on_top_side(ifootprint)
c     Debug
               if (on_right_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,3,P2)
c     Debug
                  write(*,*) 'Point 3 takes position of P2'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(1),x)
                  do j=1,dim
                     u(j)=dir(1,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the top'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(1),
     &                    xint)
                  endif         ! error
               endif            ! on_left_side(ifootprint)
               if (on_left_side(ifootprint)) then
                  call set_position_in_ctr(dim,Np2,ctr2,4,P3)
c     Debug
                  write(*,*) 'Point 4 takes position of P3'
c     Debug
               else
                  call get_position_from_ctr(dim,Np1,ctr1,idx(2),x)
                  do j=1,dim
                     u(j)=dir(2,j)
                  enddo         ! j
                  call intersection_in_sector(dim,
     &                 Ncontour_sm,Nppc_sm,contour_sm,x,u,
     &                 intersection_found,xint,d2int,
     &                 icontour,isegment)
                  if ((.not.intersection_found).or.
     &                 ((intersection_found).and.
     &                 (icontour.ne.1))) then
                     call error(label)
                     write(*,*) 'ctr2 intersected border while '
     &                    //'growing to the top'
                     write(*,*) 'but while testing intersection:'
                     write(*,*) 'intersection_found=',
     &                    intersection_found
                     if (intersection_found) then
                        write(*,*) 'icontour=',icontour
                     endif
                     write(*,*) 'x=',x
                     write(*,*) 'u=',u
                  else
                     call set_position_in_ctr(dim,Np2,ctr2,idx(2),
     &                    xint)
                  endif         ! error
               endif            ! on_left_side(ifootprint)
            endif               ! direction
c     
         else
c     ctr3 is not consistent, for any reason: produce ctr2 by required
c     displacement, then test its consistency         
            call scalar_vector(dim,df(1)*d,u1,tmp)
            call add_vectors(dim,x1,tmp,x)
            call set_position_in_ctr(dim,Np2,ctr2,idx(1),x)
            call scalar_vector(dim,df(2)*d,u2,tmp)
            call add_vectors(dim,x2,tmp,x)
            call set_position_in_ctr(dim,Np2,ctr2,idx(2),x)
c     Debug
            write(*,*) 'After deformation ctr2:'
            write(*,*) 'Np2=',Np2
            do i=1,Np
               write(*,*) (ctr2(i,j),j=1,dim-1)
            enddo               ! i
c     Debug
         endif                  ! ctr3 consistent
c     test contour consistency
         Ncontour=Ncontour_sm
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np2,ctr2)
c     Debug
         write(*,*) '-------------------------------------'
         write(*,*) 'Current contour passed to identify...'
         write(*,*) 'Ncontour=',Ncontour
         do icontour=1,Ncontour
            write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
            do i=1,Nppc(icontour)
               write(*,*) (contour(icontour,i,j),j=1,dim-1)
            enddo               ! i
         enddo                  ! icontour
         write(*,*) '-------------------------------------'
c     Debug
         call identify_inconsistent_contour(dim,
     &        mapXsize,mapYsize,
     &        Ncontour,Nppc,contour,Ncontour,
     &        consistent,
     &        intersect,Nintersected,intersected_ic,
     &        is_insidej,Nc1,idx1,
     &        is_insidei,Nc2,idx2)
c     Debug
         write(*,*) 'd=',d,' consistent=',consistent
         write(*,*) 'intersect=',intersect
         if (intersect) then
            write(*,*) 'Nintersected=',Nintersected
            do i=1,Nintersected
               write(*,*) 'intersected_ic(',i,')=',
     &              intersected_ic(i)
            enddo               ! i
         endif                  ! intersect
         write(*,*) 'is_insidej=',is_insidej
         write(*,*) 'is_insidei=',is_insidei
c     Debug
         if (consistent) then
c     growed ctr is consistent
            call duplicate_ctr(dim,Np2,ctr2,Np1,ctr1)
         else                   ! ctr2 consistent=F
c     otherwise, apply a simple dichotomy to find the widest
c     consistent contour in the required direction
c     Debug
            write(*,*) 'Starting dichotomy procedure'
c     Debug
            call duplicate_ctr(dim,Np1,ctr1,Np_min,ctr_min)
            call duplicate_ctr(dim,Np2,ctr2,Np_max,ctr_max)
            dmin=0.0D+0
            dmax=d
            solution_found=.false.
            Niter=0
            keep_looking=.true.
            do while (keep_looking)
               Niter=Niter+1
               d=(dmin+dmax)/2.0D+0
c     grow the contour
               do i=1,2
                  do j=1,dim
                     u(j)=dir(i,j)
                  enddo         ! j
                  call scalar_vector(dim,df(i)*d,u,tmp)
                  call get_position_from_ctr(dim,Np_min,ctr_min,
     &                 idx(i),x)
                  call add_vectors(dim,x,tmp,x)
                  call set_position_in_ctr(dim,Np_max,ctr_max,
     &                 idx(i),x)
               enddo            ! i
c     test contour consistency
               Ncontour=Ncontour_sm
               call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &              Np_max,ctr_max)
               call identify_inconsistent_contour(dim,
     &              mapXsize,mapYsize,
     &              Ncontour,Nppc,contour,Ncontour,
     &              consistent,
     &              intersect,Nintersected,intersected_ic,
     &              is_insidej,Nc1,idx1,
     &              is_insidei,Nc2,idx2)
               if (consistent) then
                  dmin=d
                  if (dmax-dmin.le.epsilon_d) then
                     solution_found=.true.
                     keep_looking=.false.
                  endif
               else             ! consistent=F
                  dmax=d
               endif            ! consistent
               if (Niter.gt.Niter_mx) then
                  keep_looking=.false.
               endif
            enddo               ! while (keep_looking)
            if (solution_found) then
               call error(label)
               write(*,*) 'Dichotomy found no solution'
               write(*,*) 'dmin=',dmin
               write(*,*) 'dmax=',dmax
               write(*,*) 'dmax-dmin=',dmax-dmin
               stop
            else                ! solution_found=F
c     Debug
               write(*,*) 'Dichotomy found a solution'
c     Debug
               call duplicate_ctr(dim,Np_max,ctr_max,Np2,ctr2)
            endif               ! solution_found
            call duplicate_ctr(dim,Np2,ctr2,Np1,ctr1)
c     
         endif                  ! ctr2 is consistent
 111     continue
c     Debug
         write(*,*) 'Now ctr1:'
         write(*,*) 'Np1=',Np1
         do i=1,Np
            write(*,*) (ctr1(i,j),j=1,dim-1)
         enddo                  ! i
         write(*,*) 'on_left_side=',on_left_side(ifootprint)
         write(*,*) 'on_right_side=',on_right_side(ifootprint)
         write(*,*) 'on_bottom_side=',on_bottom_side(ifootprint)
         write(*,*) 'on_top_side=',on_top_side(ifootprint)
c     Debug
      enddo                     ! direction
      
      call duplicate_ctr(dim,Np1,ctr1,Np,ctr)
c     Debug
c      write(*,*) 'Returning ctr:'
c      write(*,*) 'Np=',Np
c      do i=1,Np
c         write(*,*) (ctr(i,j),j=1,dim-1)
c      enddo                     ! i
c     Debug

      return
      end



      subroutine seeding(dim,mapXsize,mapYsize,
     &     Ncontour_sm,Nppc_sm,contour_sm,
     &     Niter_max,success,x0,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a position in the free space
c
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour_sm: number of contours in the shadow mask
c       + Nppc_sm: numher of points for each contour in the shadow mask
c       + contour_sm: shadow mask contours
c       + Niter_max: maximum number of iterations
c     
c     Output:
c       + success: true of a position was generated
c       + x0: central position
c       + Np: number of points for the generated contour
c       + ctr: generated contour
c     
c     I/O
      integer dim
      double precision mapXsize,mapYsize
      integer Ncontour_sm
      integer Nppc_sm(1:Ncontour_mx)
      double precision contour_sm(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      integer Niter_max
      logical success
      double precision x0(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer Niter,iter,i,icontour,j
      logical keep_trying
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      double precision xmin_sm1,xmax_sm1,ymin_sm1,ymax_sm1
      double precision x(1:Ndim_mx)
      logical inside
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical consistent
      logical intersect
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
      logical is_insidej
      integer Nc1
      integer idx1(1:Ncontour_mx)
      logical is_insidei
      integer Nc2
      integer idx2(1:Ncontour_mx)
c     parameters
      double precision epsilonx,epsilony
      parameter(epsilonx=1.0D-2)
      parameter(epsilony=1.0D-2)
c     label
      character*(Nchar_mx) label
      label='subroutine seeding'
      
c     bounding box for the first sm contour
      call analyze_contour(dim,
     &     Ncontour_sm,Nppc_sm,contour_sm,1,
     &     xmin_sm1,xmax_sm1,ymin_sm1,ymax_sm1)
c     create dummy contour
      Ncontour=0
      do icontour=1,Ncontour_sm
         Nppc(icontour)=Nppc_sm(icontour)
         call get_contour(dim,Ncontour_sm,Nppc_sm,contour_sm,icontour,
     &        Np_tmp,ctr_tmp)
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_tmp,ctr_tmp)
      enddo                     ! icontour
c     
      Np=4
      Niter=1
      keep_trying=.true.
      do while (keep_trying)
         call uniform(xmin_sm1,xmax_sm1,x(1))
         call uniform(ymin_sm1,ymax_sm1,x(2))
         x(3)=0.0D+0
c     Debug
         x(1)=10.0D+0
         x(2)=60.0D+0
         x(3)=0.0D+0
         write(*,*) 'In seeding, x is set to: x=',x
c     Debug
         call is_inside_contour(.false.,dim,
     &        Ncontour_sm,Nppc_sm,contour_sm,
     &        1,x,inside)
         if (inside) then
            call copy_vector(dim,x,x0)
c     generate seed contour
            ctr(1,1)=x0(1)-epsilonx
            ctr(1,2)=x0(2)-epsilony
            ctr(2,1)=x0(1)+epsilonx
            ctr(2,2)=x0(2)-epsilony
            ctr(3,1)=x0(1)+epsilonx
            ctr(3,2)=x0(2)+epsilony
            ctr(4,1)=x0(1)-epsilonx
            ctr(4,2)=x0(2)+epsilony
            Ncontour=Ncontour_sm
            call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &           Np,ctr)
c     test contour consistency
            call identify_inconsistent_contour(dim,
     &           mapXsize,mapYsize,
     &           Ncontour,Nppc,contour,Ncontour,
     &           consistent,
     &           intersect,Nintersected,intersected_ic,
     &           is_insidej,Nc1,idx1,
     &           is_insidei,Nc2,idx2)
c     Debug
c            write(*,*) 'This is: ',trim(label)
c            write(*,*) 'consistent=',consistent
c            if (consistent) then
c               write(*,*) 'x0=',x0
c               do i=1,Np
c                  write(*,*) 'ctr(',i,')=',(ctr(i,j),j=1,dim-1)
c               enddo            ! i
c            endif
c            if (.not.consistent) then
c               write(*,*) 'intersect=',intersect
c               if (intersect) then
c                  write(*,*) 'Nintersected=',Nintersected
c                  do i=1,Nintersected
c                     write(*,*) 'intersected_ic(',i,')=',
c     &                    intersected_ic(i)
c                  enddo         ! i
c               endif
c               write(*,*) 'pic=',pic
c            endif
c     Debug
            if (consistent) then
               keep_trying=.false.
               success=.true.
            endif               ! consistent
         endif                  ! inside
c     
         if (keep_trying) then
            Niter=Niter+1
            if (Niter.gt.Niter_max) then
               keep_trying=.false.
               success=.false.
            endif               ! Niter > Niter_max
         endif                  ! keep_trying
c     
      enddo                     ! while (keep_trying)
      

      return
      end


      
      subroutine moving_points(dim,ifootprint,
     &     on_left_side,on_right_side,
     &     on_bottom_side,on_top_side,
     &     direction,
     &     u12,u23,u34,u41,u21,u32,u43,u14,
     &     idx,dir,df,main)
      implicit none
      include 'max.inc'
c     
c     Purpose: to provide the indexes of the 2 points that
c     should move for the provided growth direction
c     
c     Input:
c       + dim: dimension of space
c       + ifootprint: index of the growing footprint
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c       + direction: growth direction
c       + u12,u23,u34,u41,u21,u32,u43,u14: propagation directions along the borders
c     
c     Output:
c       + idx: index of the first and second point that should move;
c         a null value indicates movement is forbidden for that variety, in that direction
c       + dir: direction of propagation, for each position
c       + df: multiplication factor to apply over the distance, for each point,
c         due to the fact that the direction of propagation is not aligned with the axis
c       + main: main propagation direction for the value of "direction"
c     
c     I/O
      integer dim
      integer ifootprint
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer direction
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u43(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      integer idx(1:2)
      double precision dir(1:2,1:Ndim_mx)
      double precision df(1:2)
      double precision main(1:Ndim_mx)
c     temp
      integer variety,i1,i2,j
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision ux(1:Ndim_mx)
      double precision uy(1:Ndim_mx)
      double precision mux(1:Ndim_mx)
      double precision muy(1:Ndim_mx)
      double precision sp
c     label
      character*(Nchar_mx) label
      label='subroutine moving_points'

      ux(1)=1.0D+0
      ux(2)=0.0D+0
      ux(3)=0.0D+0
      uy(1)=0.0D+0
      uy(2)=1.0D+0
      uy(3)=0.0D+0
      call scalar_vector(dim,-1.0D+0,ux,mux)
      call scalar_vector(dim,-1.0D+0,uy,muy)
      
      call identify_contour_variety(dim,
     &     ifootprint,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     variety)
      if (variety.eq.1) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,ux,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else  if (variety.eq.2) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.3) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u21,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else if (variety.eq.4) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u14,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u41,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.5) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else if (variety.eq.6) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,mux,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,muy,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.7) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u14,u1)
            call copy_vector(dim,mux,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,uy,u2)
         endif ! direction
      else if (variety.eq.8) then
         if (direction.eq.1) then
            i1=1
            i2=4
            call copy_vector(dim,u14,u1)
            call copy_vector(dim,u23,u2)
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.9) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,ux,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,u41,u2)
         endif ! direction
      else if (variety.eq.10) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,ux,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,muy,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.11) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u41,u1)
            call copy_vector(dim,ux,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,uy,u1)
            call copy_vector(dim,u43,u2)
         endif ! direction
      else if (variety.eq.12) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=2
            i2=3
            call copy_vector(dim,u41,u1)
            call copy_vector(dim,u32,u2)
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.13) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,u43,u2)
         endif ! direction
      else if (variety.eq.14) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=1
            i2=2
            call copy_vector(dim,u34,u1)
            call copy_vector(dim,u21,u2)
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif ! direction
      else if (variety.eq.15) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=3
            i2=4
            call copy_vector(dim,u12,u1)
            call copy_vector(dim,u43,u2)
         endif ! direction
      else if (variety.eq.16) then
         if (direction.eq.1) then
            i1=0
            i2=0
         else if (direction.eq.2) then
            i1=0
            i2=0
         else if (direction.eq.3) then
            i1=0
            i2=0
         else if (direction.eq.4) then
            i1=0
            i2=0
         endif                  ! direction
      endif
      idx(1)=i1
      idx(2)=i2
      do j=1,dim
         dir(1,j)=u1(j)
         dir(2,j)=u2(j)
      enddo                     ! j
      if (direction.eq.1) then
         call copy_vector(dim,mux,main)
      else if (direction.eq.2) then
         call copy_vector(dim,ux,main)
      else if (direction.eq.3) then
         call copy_vector(dim,muy,main)
      else if (direction.eq.4) then
         call copy_vector(dim,uy,main)
      endif                     ! direction
      call scalar_product(dim,u1,main,sp)
      if (sp.eq.0.0D+0) then
         call error(label)
         write(*,*) 'sp=',sp
         write(*,*) 'u1=',u1
         write(*,*) 'main=',main
         stop
      else
         df(1)=1.0D+0/dabs(sp)
      endif                     ! sp=0
      call scalar_product(dim,u2,main,sp)
      if (sp.eq.0.0D+0) then
         call error(label)
         write(*,*) 'sp=',sp
         write(*,*) 'u2=',u2
         write(*,*) 'main=',main
         stop
      else
         df(2)=1.0D+0/dabs(sp)
      endif                     ! sp=0

      return
      end
