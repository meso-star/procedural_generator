c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine reshape_contours(dim,debug,mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,
     &     P1,P2,P3,P4,
     &     u12,u23,u34,u41,u21,u32,u43,u14,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: to reshape contours that do not fit in the first one
c     
c     Input:
c       + dim: dimension of space
c       + debug: true in debug mode
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points for each contour
c       + contour: definition of contours
c       + P1,P2,P3,P4: positions of the 4 corners of of "contour(1)"
c       + u12,u23,u34,u41,u21,u32,u43,u14: propagation directions along the borders
c     
c     Output:
c       + Ncontour, Nppc, contour: updated
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c     
c     I/O
      integer dim
      logical debug
      double precision mapXsize,mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision u21(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u43(1:Ndim_mx)
      double precision u14(1:Ndim_mx)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
c     temp
      integer icontour,i,j,iseg,jseg,Nseg,Nseg0
      integer i11,i12,i21,i22,ip1,ip2
      integer is_min,is_max,n1,n2
      integer jseg1,jseg2
      integer Np0
      double precision ctr0(1:Nppc_mx,1:Ndim_mx-1)
      integer Np,Nppt
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
      integer Ncontour_tmp
      integer Nppc_tmp(1:Ncontour_mx)
      double precision contour_tmp(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical consistent
      logical intersect
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
      logical is_insidej
      integer Nc1
      integer idx1(1:Ncontour_mx)
      logical is_insidei
      integer Nc2
      integer idx2(1:Ncontour_mx)
      double precision x0(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      integer isegment1,isegment2
      logical intersection1_found
      double precision xint1(1:Ndim_mx)
      double precision d2int1,d2int2
      double precision tmp(1:Ndim_mx)
      logical intersection2_found
      double precision xint2(1:Ndim_mx)
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
      logical identical,retry
      character*(Nchar_mx) filename
      character*3 str3
c     label
      character*(Nchar_mx) label
      label='subroutine reshape_contours'

c     Debug
      if (debug) then
         write(*,*) 'Ncontour=',Ncontour
      endif
c     Debug

      do icontour=1,Ncontour
         on_left_side(icontour)=.false.
         on_right_side(icontour)=.false.
         on_bottom_side(icontour)=.false.
         on_top_side(icontour)=.false.
      enddo                     ! icontour

      call get_contour(dim,Ncontour,Nppc,contour,1,
     &     Np0,ctr0)
      Nseg0=Np0
      do iseg=1,Nseg0
 111     continue
c     Debug
         if (debug) then
            write(*,*) '+++++++++++++++++++++++++++'
            write(*,*) 'iseg=',iseg
            write(*,*) '+++++++++++++++++++++++++++'
         endif
c     Debug
         call get_position_from_ctr(dim,Np0,ctr0,iseg,x0)
         if (iseg.eq.1) then
            call copy_vector(dim,u12,u0)
         else if (iseg.eq.2) then
            call copy_vector(dim,u23,u0)
         else if (iseg.eq.3) then
            call copy_vector(dim,u34,u0)
         else if (iseg.eq.4) then
            call copy_vector(dim,u41,u0)
         endif
         do icontour=2,Ncontour
            call copy_vector(dim,x0,x)
            call copy_vector(dim,u0,u)
            call get_contour(dim,Ncontour,Nppc,contour,icontour,
     &           Np,ctr)
c     Debug
            if (debug) then
               write(*,*) 'icontour=',icontour
               do i=1,Np
                  write(*,*) (ctr(i,j),j=1,dim-1)
               enddo            ! i
            endif
c     Debug

            call ctr2track(dim,Np,ctr,Nppt,track)
            call track_line_intersection(dim,Nppt,track,x,u,
     &           intersection1_found,xint1,d2int1,isegment1)
            if (intersection1_found) then
c     Debug
               if (debug) then
                  write(*,*) '-----------------------------'
                  write(*,*) 'intersection found from:'
                  write(*,*) 'x=',x
                  write(*,*) 'u=',u
                  write(*,*) 'iseg=',iseg
c                  write(*,*) 'u0=',u0
c                  write(*,*) 'u12=',u12
c                  write(*,*) 'u23=',u23
c                  write(*,*) 'u34=',u34
c                  write(*,*) 'u41=',u41
                  write(*,*) 'icontour=',icontour
                  write(*,*) 'xint1=',xint1
                  write(*,*) 'isegment1=',isegment1
c                  stop
               endif
c     Debug
c     looking for a second intersection
               call scalar_vector(dim,0.10D+0,u,tmp)
               call add_vectors(dim,xint1,tmp,x)
               call track_line_intersection(dim,Nppt,track,x,u,
     &              intersection2_found,xint2,d2int2,isegment2)
c     set "on_left_side", "on_right_side", "on_bottom_side" and "on_top_side"
               if (iseg.eq.1) then
                  on_right_side(icontour)=.true.
               else if (iseg.eq.2) then
                  on_top_side(icontour)=.true.
               else if (iseg.eq.3) then
                  on_left_side(icontour)=.true.
               else if (iseg.eq.4) then
                  on_bottom_side(icontour)=.true.
               endif
c     Debug
               if (debug) then
                  write(*,*) on_left_side(icontour),
     &                 on_right_side(icontour),
     &                 on_bottom_side(icontour),
     &                 on_top_side(icontour)
               endif
               if ((debug).and.(on_left_side(icontour)).and.
     &              (on_bottom_side(icontour))) then
                  write(*,*) 'icontour=',icontour
                  stop
               endif
c     Debug
               
c     Debug
c               if ((debug).and.(on_right_side(icontour)).and.
c     &              (on_top_side(icontour))) then
c                  write(*,*) 'icontour=',icontour
c                  write(*,*) 'iseg=',iseg
c                  call num2str3(icontour,str3)
c                  filename='./gnuplot/footprint'//trim(str3)//'.dat'
c                  Ncontour_tmp=0
c                  call add_ctr_to_contour(dim,
c     &                 Ncontour_tmp,Nppc_tmp,contour_tmp,
c     &                 Np,ctr)
c                  call record_gnuplot_contour(filename,dim,
c     &                 Ncontour_tmp,Nppc_tmp,contour_tmp)
c                  stop
c               endif
c     Debug               
               if (intersection2_found) then
c     Debug
                  if (debug) then
                     write(*,*) 'intersection found from:'
                     write(*,*) 'x0=',x0
                     write(*,*) 'u0=',u0
                     write(*,*) 'icontour=',icontour
                     write(*,*) 'xint2=',xint2
                     write(*,*) 'isegment2=',isegment2
                  endif
c     Debug
c     replace points in contour
                  if (isegment1.lt.isegment2) then
                     n1=isegment1
                     n2=Np-isegment2
                     Np2=n1+n2+2
                     do i=1,n1
                        do j=1,dim-1
                           ctr2(i,j)=ctr(i,j)
                        enddo   ! j
                     enddo      ! i
                     do j=1,dim-1
                        ctr2(n1+1,j)=xint1(j)
                        ctr2(n1+2,j)=xint2(j)
                     enddo      ! j
                     if (n2.gt.0) then
                        do i=1,n2
                           do j=1,dim-1
                              ctr2(Np2-i+1,j)=ctr(Np-i+1,j)
                           enddo ! j
                        enddo   ! i
                     endif      ! n2 > 0
                  endif         ! isegment1 < isegment2
                  if ((isegment1.gt.isegment2).or.
     &                 (isegment2.eq.1)) then
                     n1=isegment1-isegment2
                     Np2=n1+2
                     do j=1,dim-1
                        ctr2(1,j)=xint2(j)
                     enddo      !j
                     do i=1,n1
                        do j=1,dim-1
                           ctr2(i+1,j)=ctr(isegment2+i,j)
                        enddo   ! j
                     enddo      ! i
                     do j=1,dim-1
                        ctr2(Np2,j)=xint1(j)
                     enddo      ! j
                  endif
c     new contour
c     copy back "ctr" into "contour"
                  Nppc(icontour)=Np2
                  do i=1,Nppc(icontour)
                     do j=1,dim-1
                        contour(icontour,i,j)=ctr2(i,j)
                     enddo      ! j
                  enddo         ! i
               else             ! intersection2_found=.false.
                  call remove_contour(dim,
     &                 mapXsize,mapYsize,
     &                 Ncontour,Nppc,contour,icontour,
     &                 on_left_side,on_right_side,
     &                 on_bottom_side,on_top_side)
                  goto 111
               endif            ! intersection2_found
            endif               ! intersection1_found
         enddo                  ! icontour
      enddo                     ! iseg

c     check for identical positions
      do icontour=2,Ncontour
         call get_contour(dim,Ncontour,Nppc,contour,icontour,
     &        Np,ctr)
         retry=.true.
         do while (retry)
            Nseg=Np
            if (Nseg.le.0) then
               call error(label)
               write(*,*) 'Empty contour ?'
               stop
            endif
            retry=.false.
            do iseg=1,Nseg
               call segment_indexes(dim,Np,ctr,iseg,i01,i02,x01,x02)
               call identical_positions(dim,x01,x02,identical)
               if (identical) then
c     remove 2nd point
                  Np=Np-1
                  do i=i02,Np
                     do j=1,dim-1
                        ctr(i,j)=ctr(i+1,j)
                     enddo      ! j
                  enddo         ! i
                  retry=.true.
                  goto 123
               endif            ! identical
            enddo               ! iseg
 123        continue
         enddo                  ! while (retry)
c     put contour back in place
         Nppc(icontour)=Np
         do i=1,Nppc(icontour)
            do j=1,dim-1
               contour(icontour,i,j)=ctr(i,j)
            enddo               ! j
         enddo                  ! i
      enddo                     ! icontour
c
 666  continue
      return
      end



      subroutine is_contour_clockwise(dim,Np,ctr,clockwise)
      implicit none
      include 'max.inc'
c     
c     Purpose: find whether or not the provided contour
c     is clockwise (which is really bad)
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c     
c     Output:
c       + clockwise: true if contour is clockwise
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical clockwise
c     temp
      integer i,j,i1,i2,i3,Nd,Ni
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x1x2(1:Ndim_mx)
      double precision x1x3(1:Ndim_mx)
      double precision v12(1:Ndim_mx)
      double precision v13(1:Ndim_mx)
      double precision w(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_clockwise'

      Nd=0
      Ni=0
      do i=1,Np
         i1=i
         if (i.eq.1) then
            i3=Np
         else
            i3=i1-1
         endif
         if (i.eq.Np) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
            x3(j)=ctr(i3,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         x3(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,x1x2)
         call substract_vectors(dim,x3,x1,x1x3)
         call normalize_vector(dim,x1x2,v12)
         call normalize_vector(dim,x1x3,v13)
         call vectorial_product(dim,v12,v13,w)
         if (w(3).gt.0.0D+0) then
            Nd=Nd+1
         else
            Ni=Ni+1
         endif
      enddo                     ! i
c
      if (Nd.ge.Ni) then
         clockwise=.false.
      else
         clockwise=.true.
      endif
c     
      return
      end


      
      subroutine identify_contour_variety(dim,
     &     icontour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     variety)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: identify contour variety
c     
c     Input:
c       + dim: dimension of space
c       + icontour: examined contour
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c     
c     Output:
c       + variety: index between 1 and 16
c     
c     I/O
      integer dim
      integer icontour
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer variety
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine identify_contour_variety'

      variety=0
      if ((.not.on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=1
      else if ((.not.on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=2
      else if ((.not.on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=3
      else if ((.not.on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=4
      else if ((.not.on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=5
      else if ((.not.on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=6
      else if ((.not.on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=7
      else if ((.not.on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=8
      else if ((on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=9
      else if ((on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=10
      else if ((on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=11
      else if ((on_left_side(icontour)).and.
     &     (.not.on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=12
      else if ((on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=13
      else if ((on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (.not.on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=14
      else if ((on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (.not.on_top_side(icontour))) then
         variety=15
      else if ((on_left_side(icontour)).and.
     &     (on_right_side(icontour)).and.
     &     (on_bottom_side(icontour)).and.
     &     (on_top_side(icontour))) then
         variety=16
      endif
      if ((variety.lt.1).or.(variety.gt.16)) then
         call error(label)
         write(*,*) 'variety=',variety
         write(*,*) 'should be in the [1-16] range'
         write(*,*) 'icontour=',icontour
         write(*,*) 'on_left_side(',icontour,')=',
     &        on_left_side(icontour)
         write(*,*) 'on_right_side(',icontour,')=',
     &        on_right_side(icontour)
         write(*,*) 'on_bottom_side(',icontour,')=',
     &        on_bottom_side(icontour)
         write(*,*) 'on_top_side(',icontour,')=',
     &        on_top_side(icontour)
         stop
      endif

      return
      end


      
      subroutine identify_contour_type(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: identify contour type (for building generation)
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c     
c     Output:
c       + contour_type: type of contour
c         - 0: surrounding contour (contour index 1)
c         - 1: building only
c         - 2: terrain + building
c         - 3: terrain only (park)
c         - 4: terrain only (dirt, no hedge or wall)
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer contour_type(1:Ncontour_mx)
c     temp
      integer icontour
      double precision xmin,xmax,ymin,ymax
      double precision deltaX,deltaY
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision dmin,dop
      integer iseg_dmin,i01,i02
c     label
      character*(Nchar_mx) label
      label='subroutine identify_contour_type'

      do icontour=1,Ncontour
         if (icontour.eq.1) then
            contour_type(icontour)=0
         else
            call analyze_contour(dim,
     &           Ncontour,Nppc,contour,icontour,
     &           xmin,xmax,ymin,ymax)
            if (Nppc(icontour).ge.4) then
               deltaX=xmax-xmin
               deltaY=ymax-ymin
               if ((deltaX.le.3.0D+0*small_street_width).or.
     &              (deltaY.le.3.0D+0*small_street_width)) then
                  contour_type(icontour)=3
               else
                  if ((on_left_side(icontour)).or.
     &                 (on_right_side(icontour)).or.
     &                 (on_bottom_side(icontour)).or.
     &                 (on_top_side(icontour))) then
                     contour_type(icontour)=1
                  else
                     contour_type(icontour)=2
                  endif         ! on a border
               endif            ! deltaX or deltaY too small
            else
               contour_type(icontour)=3
            endif               ! Nppc(icontour)=4
c     very thin footprints
c            call get_contour(dim,Ncontour,Nppc,contour,icontour,
c     &           Np,ctr)
c            call identify_smallest_segment(dim,Np,ctr,
c     &           dmin,iseg_dmin,i01,i02,dop)
c            if ((dmin.le.1.0D+0*small_street_width).and.
c     &              (dop.le.1.0D+0*small_street_width)) then
c               contour_type(icontour)=4
c            endif
            if ((dabs(xmax-xmin).le.1.0D+0*small_street_width).or.
     &           (dabs(ymax-ymin).le.1.0D+0*small_street_width)) then
               contour_type(icontour)=4
            endif
         endif                  ! icontour=1
      enddo                     ! icontour

      return
      end
      


      subroutine eliminate_inconsistent_contours(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     Nremoved)
      implicit none
      include 'max.inc'
c     
c     Purpose: identify and remove inconsistent contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c     
c     Output:
c       + Nremoved: number of removed contours
c       + updated values of "Ncontour", "Nppc", "contour"
c     "on_left_side", "on_right_side", "on_bottom_side" and "on_top_side"
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer Nremoved
c     temp
      logical debug
      logical keep_going
      integer icontour,idx,Niter
      logical inconsistent
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
      character*3 str3
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine eliminate_inconsistent_contours'
c
      debug=.false.
      Nremoved=0
c
      Niter=0
      keep_going=.true.
      do while (keep_going)
         Niter=Niter+1
         do icontour=2,Ncontour
            inconsistent=.false.
c     1- self or foreign intersection
            call contour_intersects_contour(dim,
     &           mapXsize,mapYsize,
     &           Ncontour,Nppc,contour,icontour,
     &           inconsistent,Nintersected,intersected_ic)
            if (inconsistent) then
               call remove_contour(dim,
     &              mapXsize,mapYsize,
     &              Ncontour,Nppc,contour,icontour,
     &              on_left_side,on_right_side,
     &              on_bottom_side,on_top_side)
               Nremoved=Nremoved+1
               goto 111
            endif               ! inconsistent
         enddo                  ! icontour
         keep_going=.false.
 111     continue
      enddo                     ! while keep_going
c     2- at least one point of the contour is included in another contour
      keep_going=.true.
      do while (keep_going)
         Niter=Niter+1
         do icontour=2,Ncontour
            inconsistent=.false.
            call point_is_in_contourj(dim,
     &           mapXsize,mapYsize,
     &           Ncontour,Nppc,contour,icontour,
     &           inconsistent)
c     Debug
c            if (inconsistent) then
c               write(*,*) 'PIC contour:',icontour
c            endif
c     Debug
            if (inconsistent) then
               call remove_contour(dim,
     &              mapXsize,mapYsize,
     &              Ncontour,Nppc,contour,icontour,
     &              on_left_side,on_right_side,
     &              on_bottom_side,on_top_side)
               Nremoved=Nremoved+1
               goto 112
            endif               ! inconsistent
         enddo                  ! icontour
         keep_going=.false.
 112     continue
      enddo                     ! while (keep_going)

      return
      end
      


      subroutine identify_inconsistent_contour(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     consistent,
     &     intersect,Nintersected,intersected_ic,
     &     is_insidej,Nc1,idx1,
     &     is_insidei,Nc2,idx2)
      implicit none
      include 'max.inc'
c     
c     Purpose: identify whether or not a given contour is consistent
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the contour to analyze
c     
c     Output:
c       + consistent: true if contour index "icontour" is consistent
c       + intersect: true if contour "icontour" intersects another contour (when "consistent"=F)
c       + Nintersected: number of contours that are intersected
c       + intersected_ic: index of the intersected contours (when "intersect"=T)
c       + is_insidej: true if contour "icontour" is inside other contours
c       + Nc1: number of contours that surround "icontour"
c       + idx1: indexes of the contours that surround "icontour"
c       + is_insidei: true if any other contour is included inside contour "icontour"
c       + Nc2: number of contours that are inside "icontour"
c       + idx2: indexes of the contours that are inside "icontour"
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical consistent
      logical intersect
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
      logical is_insidej
      integer Nc1
      integer idx1(1:Ncontour_mx)
      logical is_insidei
      integer Nc2
      integer idx2(1:Ncontour_mx)
c     temp
      logical inconsistent
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine identify_inconsistent_contour'
c
c     Debug
c      write(*,*) 'This is: ',trim(label)
c      write(*,*) 'icontour=',icontour
c      do i=1,Nppc(icontour)
c         write(*,*) (contour(icontour,i,j),j=1,dim-1)
c      enddo                     ! i
c     Debug
      consistent=.true.
      intersect=.false.
      is_insidej=.false.
      is_insidei=.false.
      Nintersected=0
c     1- self or foreign intersection
      inconsistent=.false.
      call contour_intersects_contour(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     inconsistent,Nintersected,intersected_ic)
      if (inconsistent) then
         consistent=.false.
         intersect=.true.
c     Debug
c         write(*,*) 'This is: ',trim(label)
c         write(*,*) 'consistent=',consistent
c         write(*,*) 'Nintersected=',Nintersected
c         do i=1,Nintersected
c            write(*,*) 'intersected_ic(',i,')=',intersected_ic(i)
c         enddo                  ! i
c     Debug
      endif                     ! inconsistent
c      
c     2- contour may be inside other contours
      call is_contouri_inside_contourj(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     is_insidej,Nc1,idx1)
c      
c     3- contour may include other contours
      call is_contourj_inside_contouri(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     is_insidei,Nc2,idx2)

      return
      end

      

      subroutine remove_contour(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side)
      implicit none
      include 'max.inc'
c     
c     Purpose: remove a given contour from the list of contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the contour to remove
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c     
c     Output:
c       + updated values of "Ncontour", "Nppc", "contour",
c     "on_left_side", "on_right_side", "on_bottom_side" and "on_top_side"
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
c     temp
      integer jcontour,i,j
c     label
      character*(Nchar_mx) label
      label='subroutine remove_contour'

      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
c
      if (icontour.lt.Ncontour) then
         do jcontour=icontour,Ncontour-1
            Nppc(jcontour)=Nppc(jcontour+1)
            do i=1,Nppc(jcontour)
               do j=1,dim-1
                  contour(jcontour,i,j)=contour(jcontour+1,i,j)
               enddo            ! j
            enddo               ! i
            on_left_side(jcontour)=on_left_side(jcontour+1)
            on_right_side(jcontour)=on_right_side(jcontour+1)
            on_bottom_side(jcontour)=on_bottom_side(jcontour+1)
            on_top_side(jcontour)=on_top_side(jcontour+1)
         enddo                  ! jcontour
      endif                     ! icontour < Ncontour
c     Finally, update the total number of contours
      Ncontour=Ncontour-1
      if (Ncontour.lt.1) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be > 0'
         stop
      endif                     ! Ncontour < 1

      return
      end
      

      
      subroutine is_contouri_inside_contourj(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     inside,Nc,idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether a given contour index "icontour"
c     is inside other contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the examined contour
c     
c     Output:
c       + inside: true if contour "icontour" is inside at least one other contour (except the first one)
c       + Nc: number of contours that surround completely "icontour"
c       + idx: index of each contour that surround completely "icontour"
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical inside
      integer Nc
      integer idx(1:Ncontour_mx)
c     temp
      integer jcontour,i,j,i1
      integer Np
      double precision ctri(1:Nppc_mx,1:Ndim_mx-1)
      double precision x(1:Ndim_mx)
      logical insidej,in
c     label
      character*(Nchar_mx) label
      label='subroutine point_is_in_contourj'
c     
      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
c
c     extract contour "icontour"
      call get_contour(dim,Ncontour,Nppc,contour,icontour,
     &     Np,ctri)
c     
      inside=.false.
      Nc=0
      do jcontour=2,Ncontour
         if (jcontour.ne.icontour) then
            insidej=.true.
            do i=1,Np
               call get_position_from_ctr(dim,Np,ctri,i,x)
               call is_inside_contour(.false.,dim,
     &              Ncontour,Nppc,contour,jcontour,x,
     &              in)
               if (.not.in) then
                  insidej=.false.
                  goto 111
               endif
            enddo               ! i
 111        continue
            if (insidej) then
               inside=.true.
               Nc=Nc+1
               idx(Nc)=jcontour
            endif
         endif                  ! jcontour different from icontour
      enddo                     ! jcontour
c     
      return
      end


      
      subroutine is_contourj_inside_contouri(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     inside,Nc,idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether other contours are completely
c     included inside a given contour index "icontour"
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the examined contour
c     
c     Output:
c       + inside: true if other contours are completely included into contour "icontour"
c       + Nc: number of contours that are completely iside "icontour"
c       + idx: index of each contour that is completely inside "icontour"
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical inside
      integer Nc
      integer idx(1:Ncontour_mx)
c     temp
      logical debug
      integer jcontour,i,j,i1
      integer Np
      double precision ctrj(1:Nppc_mx,1:Ndim_mx-1)
      double precision x(1:Ndim_mx)
      logical insidei,in
c     label
      character*(Nchar_mx) label
      label='subroutine point_is_in_contourj'
c     
      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
c
c     Debug
c      write(*,*) 'This is: ',trim(label)
c      write(*,*) 'icontour:',icontour
c      do i=1,Nppc(icontour)
c         write(*,*) (contour(icontour,i,j),j=1,dim-1)
c      enddo                     ! i
c     Debug
      inside=.false.
      Nc=0
      do jcontour=1,Ncontour
         if (jcontour.ne.icontour) then
c     extract contour "jcontour"
            call get_contour(dim,Ncontour,Nppc,contour,jcontour,
     &           Np,ctrj)
c     Debug
c            write(*,*) 'jcontour:',jcontour
c            do i=1,Np
c               write(*,*) (ctrj(i,j),j=1,dim-1)
c            enddo               ! i
c     Debug
            insidei=.true.
            do i=1,Np
               call get_position_from_ctr(dim,Np,ctrj,i,x)
c     Debug
c               if ((icontour.eq.2).and.(jcontour.eq.1).and.
c     &              (i.eq.1)) then
c                  debug=.true.
c                  write(*,*) 'x=',x
c               else
c                  debug=.false.
c               endif
c     Debug
               debug=.false.
               call is_inside_contour(debug,dim,
     &              Ncontour,Nppc,contour,icontour,x,
     &              in)
c     Debug
c               if (debug) then
c                  write(*,*) 'From ',trim(label),' in=',in
c               endif
c     Debug
               if (.not.in) then
                  insidei=.false.
                  goto 111
               endif
            enddo               ! i
 111        continue
            if (insidei) then
               inside=.true.
               Nc=Nc+1
               idx(Nc)=jcontour
            endif               ! insidei
         endif                  ! jcontour different from icontour
      enddo                     ! jcontour
c     
      return
      end
      

      
      subroutine point_is_in_contourj(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     pic)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether or not a point of any other contour
c     is inside a given contour
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the examined contour
c     
c     Output:
c       + pic: true of any point that defines a contour "j"
c     is inside contour index "icontour"
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical pic
c     temp
      integer jcontour,i,j,i1
      double precision x(1:Ndim_mx)
      logical inside
c     label
      character*(Nchar_mx) label
      label='subroutine point_is_in_contourj'
c     
      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
c     
      pic=.false.
      do jcontour=1,Ncontour
         if (jcontour.ne.icontour) then
            do i=1,Nppc(jcontour)
               do j=1,dim-1
                  x(j)=contour(icontour,i,j)
               enddo            ! j
               x(dim)=0.0D+0
               call is_inside_contour(.false.,dim,
     &              Ncontour,Nppc,contour,icontour,x,
     &              inside)
               if (inside) then
                  pic=.true.
                  goto 666
               endif            ! inside
            enddo               ! i
         endif                  ! jcontour different from icontour
      enddo                     ! jcontour
c
 666  continue
      return
      end
      
      
      
      subroutine rightmost_segment(dim,
     &     Ncontour,Nppc,contour,icontour,
     &     isegment)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the segment of a given contour
c     that is the most on the right
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the contour to analyze
c
c     Output:
c       + isegment: index of the rightmost segment of contour index "icontour"
c
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer isegment
c     temp
      integer is,Ns,j,i
      integer i1,i2
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision xmax
      logical isegment_found
c     label
      character*(Nchar_mx) label
      label='subroutine rightmost_segment'

c     Debug
c      write(*,*) 'This is: ',trim(label)
c      write(*,*) 'icontour=',icontour
c      do i=1,Nppc(icontour)
c         write(*,*) (contour(icontour,i,j),j=1,dim-1)
c      enddo                     ! i
c     Debug

      Ns=Nppc(icontour)
      xmax=contour(icontour,1,1)
      isegment_found=.false.
      do is=1,Ns
         i1=is
         if (is.eq.Ns) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            p1(j)=contour(icontour,i1,j)
            p2(j)=contour(icontour,i2,j)
         enddo                  ! j
c     Debug
c         write(*,*) 'is=',is,' i1=',i1,' i2=',i2
c         write(*,*) 'p1=',(p1(j),j=1,dim-1)
c         write(*,*) 'p2=',(p2(j),j=1,dim-1)
c     Debug
         if ((p2(1).ge.p1(1)).and.(p2(1).ge.xmax)) then
            xmax=p2(1)
            isegment=is
            isegment_found=.true.
c     Debug
c            write(*,*) '-------------------'
c            write(*,*) 'p2(1)=',p2(1),' >= p1(1)=',p1(1)
c            write(*,*) 'p2(1)=',p2(1),' >= xmax=',xmax
c            write(*,*) 'isegment=',isegment
c            write(*,*) '-------------------'
c     Debug
         endif
      enddo                     ! is
      if (.not.isegment_found) then
         call error(label)
         write(*,*) 'could not find rightmost segment'
         stop
      endif
      
      return
      end
      

      
      subroutine last_footprint_onleft(dim,
     &     Ncontour,Nppc,contour,on_left_side,
     &     illf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the last footprint
c     that touches the left side on icontour=1
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + on_left_side: true if icontour>1 touches the left side of icontour=1
c
c     Output:
c       + illf: index of the last left footprint
c
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      integer illf
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine last_footprint_onleft'

      do i=1,Ncontour
         if (on_left_side(i)) then
            illf=i
         endif
      enddo                     ! i

      return
      end
      
      

      subroutine identify_bottom_footprint(debug,dim,
     &     Ncontour,Nppc,contour,
     &     on_left_side,x0,
     &     is_bf,ibf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify whether or not there is a footprint
c     below the "x0" position, supposed to represent the
c     left end of a new segment
c     
c     Input:
c       + debug: true in debug mode
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + on_left_side: true if icontour>1 touches the left side of icontour=1
c       + x0: position to test
c     
c     Output:
c       + is_bf: true if a footprint exists below "x0"
c       + ibf: index of the footprint below "x0"
c     
c     I/O
      logical debug
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      double precision x0(1:Ndim_mx)
      logical is_bf
      integer ibf
c     temp
      integer icontour,i,j,iter
      logical keep_searching,inside
      double precision d,dmin
      double precision xmin,xmax,ymin,ymax,ymaxx
      double precision dx_left
      logical dx_left_found
      double precision x(1:Ndim_mx)
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer ic
      integer is
      double precision xint0(1:Ndim_mx)
      double precision d2int0
      integer ic0
      integer is0
      integer Nintc,Nhit
      integer cidx(1:100)
      integer nidx(1:100)
      integer idx,nidx_max,idx_max,cidx_max,cidx_ymax
      logical idx_found
c     label
      character*(Nchar_mx) label
      label='subroutine identify_bottom_footprint'

c     Debug
c      write(*,*) trim(label),' debug=',debug
c     Debug
      
c     Debug
      if (debug) then
         write(*,*) '============================================'
         write(*,*) 'Entering ',trim(label)
         write(*,*) 'Ncontour=',Ncontour
      endif
c     Debug

      is_bf=.false.
      dmin=1.0D+2
      if (Ncontour.gt.1) then
         call copy_vector(dim,x0,x)
         Nintc=0
         iter=0
         keep_searching=.true.
         do while (keep_searching)
            iter=iter+1
            x(1)=x0(1)+1.0D-1*(iter-1) ! over Niter_mx=100 iterations, 10 m will be searched
            call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &           1,x,inside)
            if (.not.inside) then
c     if position x is outside contour 1, then subsequent positions will most probably be, too
               keep_searching=.false.
               goto 222
            endif
            do icontour=2,Ncontour
               call is_inside_contour(.false.,dim,
     &              Ncontour,Nppc,contour,icontour,x,
     &              inside)
               if (inside) then
c     if position x enters a contour>1, then subsequent positions will most probably be inside it too
c     and there is no meaning to search for a bottom contour
                  keep_searching=.false.
                  goto 222
               endif
            enddo               ! icontour
               
            call iis_bottom(dim,
     &           Ncontour,Nppc,contour,x,
     &           xint,d2int,ic,is)
c     Debug
c            if (debug) then
c               write(*,*) iter,ic
c            endif
c     Debug
            idx_found=.false.
            do i=1,Nintc
               if (ic.eq.cidx(i)) then
                  idx_found=.true.
                  idx=i
                  goto 111
               endif
            enddo               ! i
 111        continue
            if (ic.gt.1) then
               if (idx_found) then
                  nidx(idx)=nidx(idx)+1
               else
                  Nintc=Nintc+1
                  if (Nintc.gt.100) then
                     call error(label)
                     write(*,*) 'Nintc=',Nintc
                     write(*,*) ' > 100'
                     stop
                  else
                     cidx(Nintc)=ic
                     nidx(Nintc)=1
                  endif
               endif
            endif               ! ic>1
            if (iter.ge.Niter_mx) then
               keep_searching=.false.
            endif
 222        continue
         enddo                  ! while (keep_searching)
c     Total number of hits
         Nhit=0
         do i=1,Nintc
            Nhit=Nhit+nidx(i)
         enddo                  ! i
c     if Nhit=0, probably only contour 1 was hit
         if (Nhit.eq.0) then
            is_bf=.false.
            goto 333
         endif
c     Identify the contour that has the highest number of hits
         nidx_max=0
         do i=1,Nintc
            if (nidx(i).gt.nidx_max) then
               nidx_max=nidx(i)
               idx_max=i
            endif
         enddo                  ! i
c     Identify the intersected contour that has the highest index
         cidx_max=0
         do i=1,Nintc
            if (cidx(i).gt.cidx_max) then
               cidx_max=cidx(i)
            endif
         enddo                  ! i
c     Identify the contour that has the highest ymax
         do i=1,Nintc
            call analyze_contour(dim,
     &           Ncontour,Nppc,contour,cidx(i),
     &           xmin,xmax,ymin,ymax)
c     Debug
            if (debug) then
               write(*,*) 'i=',i,' ymax=',ymax
            endif
c     Debug
            if (i.eq.1) then
               ymaxx=ymax
               cidx_ymax=cidx(i)
            else
               if (ymax.gt.ymaxx) then
                  ymaxx=ymax
                  cidx_ymax=cidx(i)
c     Debug
                  if (debug) then
                     write(*,*) 'ymaxx=',ymaxx,' cidx_ymax=',cidx_ymax
                  endif
c     Debug
               endif            ! ymax>ymaxx
            endif               ! i=1
         enddo                  ! i
c     Debug
         if (debug) then
            write(*,*) 'highest number of hits:'
            write(*,*) 'idx_max=',idx_max
            write(*,*) 'nidx_max=',nidx_max
            write(*,*) 'full detail: Nintc=',Nintc
            do i=1,Nintc
               write(*,*) i,cidx(i),nidx(i)
            enddo               ! i
            write(*,*) 'Nhit=',Nhit
            write(*,*) 'cidx_max=',cidx_max
            write(*,*) 'cidx_ymax=',cidx_ymax
         endif
c     Debug
c     condition to attribute the "ibf" index to "idx_max"
c     condition based on the maximum number of hits
c$$$         if ((nidx_max.gt.Nhit/2).and.(cidx(idx_max).gt.1)) then
c$$$            is_bf=.true.
c$$$            ibf=cidx(idx_max)
c$$$            goto 666
c$$$         endif
c     condition based on the highest rank
c$$$         if (cidx_max.gt.1) then
c$$$            is_bf=.true.
c$$$            ibf=cidx_max
c$$$            goto 666
c$$$         endif
c     condition based on the highest ymax
c     Debug
         if (debug) then
            write(*,*) 'cidx_ymax=',cidx_ymax
            do i=1,Nppc(cidx_ymax)
               write(*,*) (contour(cidx_ymax,i,j),j=1,dim-1)
            enddo               ! i
         endif
c     Debug
         call is_inside_contour(debug,dim,
     &        Ncontour,Nppc,contour,cidx_ymax,x0,inside)
         if ((cidx_ymax.gt.1).and.(.not.inside)) then
            is_bf=.true.
            ibf=cidx_ymax
            goto 666
         endif
c$$$         x(1)=x(1)+1.0D-2
c$$$         call iis_bottom(dim,
c$$$     &        Ncontour,Nppc,contour,x,
c$$$     &        xint,d2int,ic,is)
c$$$         if ((ic.gt.1).and.(is.gt.1)) then
c$$$c     Debug
c$$$            if (debug) then
c$$$               write(*,*) 'Found intersection to bottom'
c$$$               write(*,*) 'from x0(1)+1.0D-2=',x(1)
c$$$               write(*,*) 'contour index:',ic
c$$$            endif
c$$$c     Debug
c$$$            is_bf=.true.
c$$$            ibf=ic
c$$$            goto 666
c$$$         endif
c     Debug
 333     continue
         if (debug) then
            write(*,*) 'Found NO intersection to bottom'
            write(*,*) 'from x0(1)+1.0D-2=',x(1)
            write(*,*) 'keep searching...'
         endif
c     Debug
c     otherwise, find the value of dx_left
         dx_left_found=.false.
         do icontour=2,Ncontour
            call analyze_contour(dim,
     &           Ncontour,Nppc,contour,icontour,
     &           xmin,xmax,ymin,ymax)
            if ((on_left_side(icontour)).and.(.not.dx_left_found)) then
               x(1)=contour(icontour,1,1)-1.0D-5
               x(2)=contour(icontour,1,2)
               x(3)=0.0D+0
c     Debug
               if (debug) then
                  write(*,*) 'on_left_side(',icontour,')=',
     &                 on_left_side(icontour)
                  write(*,*) 'dx_left_found=',dx_left_found
                  write(*,*) 'Using iis_left from x=',x
               endif
c     Debug
               call iis_left(dim,
     &              Ncontour,Nppc,contour,x,
     &              xint,d2int,ic,is)
               dx_left=d2int+1.0D-5
               if (dx_left.le.0.0D+0) then
                  call error(label)
                  write(*,*) 'dx_left=',dx_left
                  write(*,*) 'should be > 0'
                  stop
               endif
               dx_left_found=.true.
c     Debug
               if (debug) then
                  write(*,*) 'identified dx_left=',dx_left
               endif
c     Debug
            endif               ! on_left_side(icontour).AND.dx_left_found=F
c     shunt verifications
            goto 123
            if ((on_left_side(icontour)).and.(dx_left_found)) then
c     check 1st point of contour(icontour)
               x(1)=contour(icontour,1,1)-1.0D-5
               x(2)=contour(icontour,1,2)
               x(dim)=0.0D+0
c     Debug
               if (debug) then
                  write(*,*) 'on_left_side(',icontour,')=',
     &                 on_left_side(icontour)
                  write(*,*) 'dx_left_found=',dx_left_found
                  write(*,*) 'Using iis_left from x=',x
               endif
c     Debug
               call iis_left(dim,
     &              Ncontour,Nppc,contour,x,
     &              xint,d2int,ic,is)
c               if (dabs(d2int-dx_left).gt.1.0D-4) then
c                  call error(label)
c                  write(*,*) 'contour(',icontour,',1,1)=',x(1)
c                  write(*,*) 'd2int=',d2int
c                  write(*,*) 'should be equal to dx_left=',dx_left
c                  write(*,*) 'ic=',ic,' is=',is
c                  stop
c               endif
c     Debug
               if (debug) then
                  write(*,*) 'checked dx_left for icontour:',icontour
                  write(*,*) 'point index:',1
               endif
c     Debug
c     check last point of contour(icontour)
               x(1)=contour(icontour,Nppc(icontour),1)-1.0D-5
               x(2)=contour(icontour,Nppc(icontour),2)
               x(dim)=0.0D+0
c     Debug
               if (debug) then
                  write(*,*) 'on_left_side(',icontour,')=',
     &                 on_left_side(icontour)
                  write(*,*) 'dx_left_found=',dx_left_found
                  write(*,*) 'Using iis_left from x=',x
               endif
c     Debug
               call iis_left(dim,
     &              Ncontour,Nppc,contour,x,
     &              xint,d2int,ic,is)
c     Debug
               if (debug) then
                  write(*,*) 'checked dx_left for icontour:',icontour
                  write(*,*) 'point index:',Nppc(icontour)
               endif
c     Debug
            endif               ! on_left_side(icontour).AND.dx_left_found=T
 123        continue
         enddo                  ! icontour
c
         if (dx_left_found) then
c     Debug
            if (debug) then
               write(*,*) 'Looking for intersection to the '
     &              //'left from x0=',x0
            endif
c     Debug
            call iis_left(dim,
     &           Ncontour,Nppc,contour,x0,
     &           xint0,d2int0,ic0,is0)
c     Debug
            if (debug) then
               write(*,*) 'intersection to the left from x0=',x0
               write(*,*) 'xint0=',xint0
               write(*,*) 'd2int0=',d2int0
               write(*,*) 'ic0=',ic0,' is0=',is0
            endif
c     Debug
            if (dabs(d2int0-dx_left).le.1.0D-4) then
c     Debug
               if (debug) then
                  write(*,*) 'x0 sticks to the left'
               endif
c     Debug
               call copy_vector(dim,x0,x)
               x(1)=x(1)+1.0D+0
               call iis_bottom(dim,
     &              Ncontour,Nppc,contour,x,
     &              xint,d2int,ic,is)
               call is_inside_contour(debug,dim,
     &              Ncontour,Nppc,contour,ic,x,inside)
c     Debug
               if (debug) then
                  write(*,*) 'intersection to bottom using x=',x
                  write(*,*) 'xint=',xint
                  write(*,*) 'd2int=',d2int
                  write(*,*) 'ic=',ic,' is=',is
                  write(*,*) 'inside contour ',ic,':',inside
               endif
c     Debug
               if ((ic.gt.1).and.(.not.inside)) then
                  is_bf=.true.
                  ibf=ic
                  goto 666
               endif
            endif
         endif                  ! on_left_side(icontour).AND.dx_left_found=F
      endif                     ! Ncontour>1
 666  continue

c     Debug
      if (debug) then
         write(*,*) 'is_bf=',is_bf
         if (is_bf) then
            write(*,*) 'ibf=',ibf
         endif
      endif
c     Debug
c      
c     Debug
      if (debug) then
         write(*,*) 'Now exiting ',trim(label)
         write(*,*) '============================================'
      endif
c     Debug
      return
      end
      

      
      subroutine add_point_to_ctr(dim,x,Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to add a position to a contour definition
c     
c     Input:
c       + dim: dimension of space
c       + x: position to add
c       + Np: number of points in the "ctr" contour
c       + ctr: definition of the contour
c     
c     Output:
c       + updated values for Np and ctr
c     
c     I/O
      integer dim
      double precision x(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer j
      double precision x0(1:Ndim_mx)
      logical check,identical
c     label
      character*(Nchar_mx) label
      label='subroutine add_point_to_ctr'

      if (Np.eq.0) then
         check=.false.
      else
         check=.true.
         do j=1,dim-1
            x0(j)=ctr(Np,j)
         enddo                  ! j
         x0(dim)=0.0D+0
      endif                     ! Np>0
      if (check) then
         call identical_positions(dim,x0,x,identical)
      endif
      if ((.not.check).or.((check).and.(.not.identical))) then
         Np=Np+1
         if (Np.gt.Nppc_mx) then
            call error(label)
            write(*,*) 'Np=',Np
            write(*,*) '> Nppc_mx=',Nppc_mx
            stop
         else
            do j=1,dim-1
               ctr(Np,j)=x(j)
            enddo               ! j
         endif                  ! Np>Nppc_mx
      endif                     ! .not.identical

      return
      end



      subroutine analyze_contour_segment(dim,
     &     Ncontour,Nppc,contour,icontour,isegment,
     &     xmin,xmax,ymin,ymax)
      implicit none
      include 'max.inc'
c     
c     Purpose: provide various data about a given contour's segment
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the contour to analyze
c       + isegment: index of the segment to analyze
c     
c     Output:
c       + xmin: minimum value for X
c       + xmax: maximum value for X
c       + ymin: minimum value for Y
c       + ymax: maximum value for Y
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer isegment
      double precision xmin
      double precision xmax
      double precision ymin
      double precision ymax
c     temp
      integer i,j,i1,i2
      double precision p1(1:2)
      double precision p2(1:2)
c     label
      character*(Nchar_mx) label
      label='subroutine analyze_contour_segment'

      if (icontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
      i1=isegment
      if (isegment.eq.Nppc(icontour)) then
         i2=1
      else
         i2=i1+1
      endif
      do j=1,dim-1
         p1(j)=contour(icontour,i1,j)
         p2(j)=contour(icontour,i2,j)
      enddo                     ! j
      
      xmin=dmin1(p1(1),p2(1))
      xmax=dmax1(p1(1),p2(1))
      ymin=dmin1(p1(2),p2(2))
      ymax=dmax1(p1(2),p2(2))
      
      return
      end



      subroutine analyze_contour(dim,
     &     Ncontour,Nppc,contour,icontour,
     &     xmin,xmax,ymin,ymax)
      implicit none
      include 'max.inc'
c     
c     Purpose: provide various data about a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the contour to analyze
c     
c     Output:
c       + xmin: minimum value for X
c       + xmax: maximum value for X
c       + ymin: minimum value for Y
c       + ymax: maximum value for Y
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      double precision xmin
      double precision xmax
      double precision ymin
      double precision ymax
c     temp
      integer i
c     label
      character*(Nchar_mx) label
      label='subroutine analyze_contour'

      xmin=contour(icontour,1,1)
      xmax=contour(icontour,1,1)
      ymin=contour(icontour,1,2)
      ymax=contour(icontour,1,2)
      do i=2,Nppc(icontour)
         if (contour(icontour,i,1).lt.xmin) then
            xmin=contour(icontour,i,1)
         endif
         if (contour(icontour,i,1).gt.xmax) then
            xmax=contour(icontour,i,1)
         endif
         if (contour(icontour,i,2).lt.ymin) then
            ymin=contour(icontour,i,2)
         endif
         if (contour(icontour,i,2).gt.ymax) then
            ymax=contour(icontour,i,2)
         endif
      enddo                     ! i 

      return
      end



      
      subroutine check_courtyard_contour(dim,
     &     Ncontour,Nppc,contour)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: provide various data about a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     Output:
c       + Ncontour: updated
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer isegment,i1,i2
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer Nsegment
      double precision segment_length(1:Nppt_mx-1)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      integer seg_idx(1:Nppt_mx-1)
      logical keep_sorting
      double precision d,dmax
      integer iseg,Nsorted
      logical sorted(1:Nppt_mx-1),one_found
      double precision aspect_ratio
c     label
      character*(Nchar_mx) label
      label='subroutine check_courtyard_contour'

      if (Ncontour.ne.2) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be equal to 2'
         stop
      endif

c     extract courtyard contour and compute the length of each segment
      call get_contour(dim,Ncontour,Nppc,contour,2,Np,ctr)
      Nsegment=Nppc(2)
      do isegment=1,Nsegment
         i1=isegment
         if (isegment.lt.Nsegment) then
            i2=isegment+1
         else
            i2=1
         endif
         call get_position_from_ctr(dim,Np,ctr,i1,x1)
         call get_position_from_ctr(dim,Np,ctr,i2,x2)
         call distance_between_two_points(dim,x1,x2,
     &        segment_length(isegment))
      enddo                     ! isegment
c     Debug
c      do isegment=1,Nsegment
c         write(*,*) 'segment_length(',isegment,')=',
c     &        segment_length(isegment)
c      enddo                     ! isegment
c     Debug
      
c     find maximum length
      dmax=0.0D+0
      do isegment=1,Nsegment
         if (segment_length(isegment).gt.dmax) then
            dmax=segment_length(isegment)
         endif
      enddo                     ! isegment
c     Debug
c      write(*,*) 'dmax=',dmax
c     Debug
      
c     sort by increasing segment length
      do isegment=1,Nsegment
         sorted(isegment)=.false.
      enddo                     ! isegment
      Nsorted=0
      keep_sorting=.true.
      do while (keep_sorting)
         d=dmax
         one_found=.false.
         do isegment=1,Nsegment
            if ((segment_length(isegment).le.d).and.
     &           (.not.sorted(isegment))) then
               d=segment_length(isegment)
               iseg=isegment
               one_found=.true.
            endif
         enddo                  ! isegment
         if (one_found) then
            Nsorted=Nsorted+1
            if (Nsorted.gt.Nsegment) then
               call error(label)
               write(*,*) 'Nsorted=',Nsorted
               write(*,*) '> Nsegment=',Nsegment
               stop
            endif
            seg_idx(Nsorted)=iseg
            sorted(iseg)=.true.
         else
            keep_sorting=.false.
         endif
      enddo                     ! while (keep_sorting)
      if (Nsorted.ne.Nsegment) then
         call error(label)
         write(*,*) 'Nsorted=',Nsorted
         write(*,*) 'should be equal to Nsegment=',Nsegment
         stop
      endif
c     Debug
c      write(*,*) 'After sorting:'
c      do isegment=1,Nsegment
c         write(*,*) seg_idx(isegment),
c     &        segment_length(seg_idx(isegment))
c      enddo                     ! isegment
c      stop
c     Debug

c     remove coutryard contour based on size of the segments
c     1- whenever the smallest segment length < minimum_segment_length
      if (segment_length(seg_idx(1)).lt.minimum_segment_length) then
         Ncontour=1
         goto 666
      endif
c     2- whenever the aspect ratio > maximum_aspect_ratio
      aspect_ratio=segment_length(seg_idx(Nsegment))
     &     /segment_length(seg_idx(1))
      if (aspect_ratio.gt.maximum_aspect_ratio) then
         Ncontour=1
         goto 666
      endif
      
 666  continue
      return
      end
      


      subroutine contour_area(dim,Ncontour,Nppc,contour,
     &     main_area,secondary_area,total_area)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the area within a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     Ouput:
c       + main_area: area within the first contour
c       + secondary_area: area of contours 2-Ncontour
c       + total_area: main_area-secondary_area
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision main_area
      double precision secondary_area
      double precision total_area
c     temp
      integer icontour
      integer isegment,Nsegment
      double precision tri_area
      double precision c_area
      double precision xmin
      double precision xmax
      double precision ymin
      double precision ymax
      integer Np,i1,i2
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x0x1(1:Ndim_mx)
      double precision x0x2(1:Ndim_mx)
      double precision w(1:Ndim_mx)
      double precision length
c     label
      character*(Nchar_mx) label
      label='subroutine contour_area'

      main_area=0.0D+0
      secondary_area=0.0D+0
      do icontour=1,Ncontour
c     Debug
c         write(*,*) 'icontour=',icontour
c     Debug
         call analyze_contour(dim,
     &        Ncontour,Nppc,contour,icontour,
     &        xmin,xmax,ymin,ymax)
c     x0: position from which a triangle is drawn
c     with each segment of the contour
         x0(1)=(xmin+xmax)/2.0D+0
         x0(2)=(ymin+ymax)/2.0D+0
         x0(3)=0.0D+0
c     compute the area of the contour
         c_area=0.0D+0
         call get_contour(dim,Ncontour,Nppc,contour,icontour,Np,ctr)
         Nsegment=Nppc(icontour)
         do isegment=1,Nsegment
c     Debug
c            write(*,*) 'isegment=',isegment,' /',Nsegment
c     Debug
c     i1 & i2: indexes of the 2 points of the segment
            i1=isegment
            if (isegment.eq.Nsegment) then
               i2=1
            else
               i2=isegment+1
            endif
            call get_position_from_ctr(dim,Np,ctr,i1,x1)
            call get_position_from_ctr(dim,Np,ctr,i2,x2)
c     Debug
c            write(*,*) 'x0=',x0
c            write(*,*) 'x1=',x1
c            write(*,*) 'x2=',x2
c     Debug
c     compute area of the triangle
            call triangle_area(dim,x0,x1,x2,tri_area)
c     Debug
c            write(*,*) 'tri_area=',tri_area
c     Debug
            c_area=c_area+tri_area
         enddo                  ! isegment
c     Debug
c         write(*,*) 'c_area=',c_area
c     Debug
         if (icontour.eq.1) then
c     Area of contour 1
            main_area=main_area+c_area
         else
c     Area of contours 2-Ncontour
           secondary_area=secondary_area+c_area
         endif
      enddo                     ! icontour
c     The final area is computed as:
      total_area=main_area-secondary_area
c     Since secondary_area is supposed to represent the total
c     area of "holes" in the first contour, that should
c     therefore be substracted
      
      return
      end
      
      

      subroutine sector_contour(dim,mapXsize,mapYsize,
     &     Nsector,alpha1,xcenter,generate_river,ilevel,isector,
     &     Nroad,road_width,road_Nppt,road_track,
     &     Ncontour,Nppc,contour)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the contour for a given level 1 sector
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nsector: number of angular sectors
c       + alpha1: angle for generating level1 ring road contour
c       + xcenter: position of the ring center
c       + generate_river: set to true if a river has to be generated
c       + ilevel: level index [1,2]
c       + isector: index of the sector
c       + Nroad: number of existing roads
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c     
c     Output:
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nsector
      double precision alpha1
      double precision xcenter(1:Ndim_mx)
      logical generate_river
      integer ilevel
      integer isector
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer ring_road_index,ring_road_index2,i0,i1,i2
      integer iroad(1:4),isegment(1:4)
      integer i,j,iroad1,iroad2
      double precision Pa1(1:Ndim_mx)
      double precision Pb1(1:Ndim_mx)
      double precision normal1(1:Ndim_mx)
      double precision Pa2(1:Ndim_mx)
      double precision Pb2(1:Ndim_mx)
      double precision normal2(1:Ndim_mx)
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision width
      integer Nppt
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine sector_contour'

      if ((isector.lt.1).or.(isector.gt.Nsector)) then
         call error(label)
         write(*,*) 'Bad input argument: isector=',isector
         write(*,*) 'should be in [1-',Nsector,'] range'
         stop
      endif

c     index of the ring road
      call level1_ring_road_index(generate_river,ring_road_index)
      if (ilevel.eq.1) then
         i0=ring_road_index
         i1=i0+1
         i2=i0+2
      else if (ilevel.eq.2) then
         call level2_ring_road_index(generate_river,Nsector,
     &        ring_road_index2)
         i0=ring_road_index2
         i1=ring_road_index
         i2=i0+1
      else
         call error(label)
         write(*,*) 'Bad input argument:'
         write(*,*) 'ilevel=',ilevel
         write(*,*) 'should be in the [1,2] range'
         stop
      endif
c     Debug
c      write(*,*) 'i0=',i0
c      write(*,*) 'i1=',i1
c      write(*,*) 'i2=',i2
c     Debug
      
c     Index of each one of the 4 roads that surround the sector,
c     and the corresponding segment
      iroad(1)=i0
      iroad(2)=i1
      iroad(3)=i2+isector-1
      if (isector.lt.Nsector) then
         iroad(4)=iroad(3)+1
      else
         iroad(4)=i2
      endif
      isegment(1)=isector
      isegment(2)=isector
      isegment(3)=1
      isegment(4)=1

      Ncontour=1
      Nppc(Ncontour)=4
      do i=1,Nppc(Ncontour)
         if (i.eq.1) then
            iroad1=2
            iroad2=3
         else if (i.eq.2) then
            iroad1=3
            iroad2=1
         else if (i.eq.3) then
            iroad1=1
            iroad2=4
         else if (i.eq.4) then
            iroad1=4
            iroad2=2
         else
            call error(label)
            write(*,*) 'i=',i
            stop
         endif                  ! i

c     Debug
c         write(*,*) 'i=',i
c         write(*,*) 'iroad1=',iroad1
c         write(*,*) 'iroad2=',iroad2
c         write(*,*) 'iroad(iroad1)=',iroad(iroad1),
c     &        ' segment=',isegment(iroad1)
c         write(*,*) 'iroad(iroad2)=',iroad(iroad2),
c     &        ' segment=',isegment(iroad2)
c     Debug
c         
         call extract_road_track(dim,
     &        Nroad,road_width,road_Nppt,road_track,
     &        iroad(iroad1),width,Nppt,track)
         do j=1,dim-1
            Pa1(j)=track(isegment(iroad1),j)
            Pb1(j)=track(isegment(iroad1)+1,j)
         enddo                  ! j
         Pa1(dim)=0.0D+0
         Pb1(dim)=0.0D+0
         call normal_for_segment(dim,Pa1,Pb1,normal1)
         if ((iroad1.eq.2).or.(iroad1.eq.3)) then
            call scalar_vector(dim,-1.0D+0,normal1,normal1)
         endif
         call scalar_vector(dim,width/2.0D+0,normal1,tmp)
         call add_vectors(dim,Pa1,tmp,P11)
         call add_vectors(dim,Pb1,tmp,P12)
c     
         call extract_road_track(dim,
     &        Nroad,road_width,road_Nppt,road_track,
     &        iroad(iroad2),width,Nppt,track)
         do j=1,dim-1
            Pa2(j)=track(isegment(iroad2),j)
            Pb2(j)=track(isegment(iroad2)+1,j)
         enddo                  ! j
         Pa2(dim)=0.0D+0
         Pb2(dim)=0.0D+0
         call normal_for_segment(dim,Pa2,Pb2,normal2)
         if ((iroad2.eq.2).or.(iroad2.eq.3)) then
            call scalar_vector(dim,-1.0D+0,normal2,normal2)
         endif
         call scalar_vector(dim,width/2.0D+0,normal2,tmp)
         call add_vectors(dim,Pa2,tmp,P21)
         call add_vectors(dim,Pb2,tmp,P22)
c
c     Debug
c         write(*,*) 'i=',i,' Intersection between:'
c         write(*,*) 'P11=',P11
c         write(*,*) 'P12=',P12
c         write(*,*) 'P21=',P21
c         write(*,*) 'P22=',P22
c     Debug
         call line_line_intersection(dim,P11,P12,P21,P22,.false.,
     &        intersection_found,Pint)
         if (intersection_found) then
            do j=1,dim-1
               contour(Ncontour,i,j)=Pint(j)
            enddo               ! j
c     Debug
c            write(*,*) 'i=',i,' Pint=',Pint
c     Debug
         else
            call error(label)
            write(*,*) 'Intersection not found for i=',i
            write(*,*) 'iroad1=',iroad1
            write(*,*) 'Pa1=',Pa1
            write(*,*) 'Pb1=',Pb1
            write(*,*) 'normal1=',normal1
            write(*,*) 'iroad2=',iroad2
            write(*,*) 'Pa2=',Pa2
            write(*,*) 'Pb2=',Pb2
            write(*,*) 'normal2=',normal2
            stop
         endif                  ! intersection_found
      enddo                     ! i

      return
      end
      


      subroutine ground_contour(dim,mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,Aground)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate rivers contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c     
c     Output:
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + Aground: area of the ground
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision Aground
c     temp
      integer j
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision tri_area
c     label
      character*(Nchar_mx) label
      label='subroutine ground_contour'

      Ncontour=1
      if ((Ncontour.lt.1).or.(Ncontour.gt.Ncontour_mx)) then
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be in the [1-',Ncontour_mx,'] range'
         stop
      endif
      Nppc(1)=4
      if ((Nppc(1).lt.1).or.(Nppc(1).gt.Nppc_mx)) then
         call error(label)
         write(*,*) 'Nppc(1)=',Nppc(1)
         write(*,*) 'should be in the [1-',Nppc_mx,'] range'
         stop
      endif
c     contour
      contour(1,1,1)=0.0D+0
      contour(1,1,2)=0.0D+0
      contour(1,2,1)=mapXsize
      contour(1,2,2)=0.0D+0
      contour(1,3,1)=mapXsize
      contour(1,3,2)=mapYsize
      contour(1,4,1)=0.0D+0
      contour(1,4,2)=mapYsize
c     area
      Aground=0.0D+0
      do j=1,dim-1
         x1(j)=contour(1,1,j)
         x2(j)=contour(1,2,j)
         x3(j)=contour(1,3,j)
         x4(j)=contour(1,4,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      x3(dim)=0.0D+0
      x4(dim)=0.0D+0
      call triangle_area(dim,x1,x2,x3,tri_area)
      Aground=Aground+tri_area
      call triangle_area(dim,x1,x3,x4,tri_area)
      Aground=Aground+tri_area

      return
      end
      


      subroutine river_contour(dim,mapXsize,mapYsize,
     &     Nriver,river_width,river_Nppt,river_track,
     &     Ncontour,Nppc,contour,Ariver)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate rivers contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nriver: number of rivers that has been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c     
c     Output:
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + Ariver: area of river(s)
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision Ariver
c     temp
      integer iriver,icontour,Nseg,iseg,j
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n_tmp(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision d(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision tri_area
c     label
      character*(Nchar_mx) label
      label='subroutine river_contour'

c     Check for inconsistencies
      if ((Nriver.lt.1).or.(Nriver.gt.2)) then
         call error(label)
         write(*,*) 'Nriver=',Nriver
         write(*,*) 'should be between 1 and 2'
         stop
      endif
      Ncontour=Nriver
      if ((Ncontour.lt.1).or.(Ncontour.gt.Ncontour_mx)) then
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be in the [1-',Ncontour_mx,'] range'
         stop
      endif
      do iriver=1,Nriver
         if ((river_Nppt(iriver).lt.1).or.
     &        (river_Nppt(iriver).gt.Nppt_mx)) then
            call error(label)
            write(*,*) 'river_Nppt(',iriver,')=',river_Nppt(iriver)
            write(*,*) 'should be in the [1-',Nppt_mx,'] range'
            stop
         endif
      enddo                     ! iriver

c     Generate contours
      Ariver=0.0D+0
      do iriver=1,Nriver
         icontour=iriver
         Nseg=river_Nppt(iriver)-1
         if (Nseg.lt.1) then
            call error(label)
            write(*,*) 'Nseg=',Nseg
            write(*,*) 'should be > 0'
            stop
         endif
         Nppc(icontour)=2*(Nseg+1)
         if ((Nppc(icontour).lt.1).or.(Nppc(icontour).gt.Nppc_mx)) then
            call error(label)
            write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
            write(*,*) 'should be in the [1-',Nppc_mx,'] range'
            stop
         endif
c     loop over segments
         do iseg=1,Nseg
            p1(1)=river_track(iriver,iseg,1)
            p1(2)=river_track(iriver,iseg,2)
            p1(3)=0.0D+0
            p2(1)=river_track(iriver,iseg+1,1)
            p2(2)=river_track(iriver,iseg+1,2)
            p2(3)=0.0D+0
            call normal_for_segment(dim,p1,p2,n)
c     n is the normal for the current segment
            if (iseg.eq.1) then
               call copy_vector(dim,n,n1)
            else                ! iseg>1
               p0(1)=river_track(iriver,iseg-1,1)
               p0(2)=river_track(iriver,iseg-1,2)
               p0(3)=0.0D+0
               call normal_for_segment(dim,p0,p1,n_tmp)
               call add_vectors(dim,n_tmp,n,u)
               call normalize_vector(dim,u,n1)
            endif               ! iseg=1
c     n1 is the normal to use for P1
            if (iseg.eq.Nseg) then
               call copy_vector(dim,n,n2)
            else                ! iseg>1
               p0(1)=river_track(iriver,iseg+2,1)
               p0(2)=river_track(iriver,iseg+2,2)
               p0(3)=0.0D+0
               call normal_for_segment(dim,p2,p0,n_tmp)
               call add_vectors(dim,n_tmp,n,u)
               call normalize_vector(dim,u,n2)
            endif               ! iseg=Nseg
c     n2 is the normal to use for P2
c     generating points for the segment
            call scalar_vector(dim,river_width(iriver)/2.0D+0,n1,d)
            call substract_vectors(dim,p1,d,x1)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x1)
            call add_vectors(dim,p1,d,x4)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x4)
            call scalar_vector(dim,river_width(iriver)/2.0D+0,n2,d)
            call substract_vectors(dim,p2,d,x2)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x2)
            call add_vectors(dim,p2,d,x3)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x3)
c     adding points to contour
            if (iseg.eq.1) then
               do j=1,dim-1
                  contour(icontour,1,j)=x1(j)
                  contour(icontour,Nppc(icontour),j)=x4(j)
               enddo            ! j
            endif
            do j=1,dim-1
               contour(icontour,iseg+1,j)=x2(j)
               contour(icontour,Nppc(icontour)-iseg,j)=x3(j)
            enddo               ! j
c     compute area
            call triangle_area(dim,x1,x2,x3,tri_area)
            Ariver=Ariver+tri_area
            call triangle_area(dim,x1,x3,x4,tri_area)
            Ariver=Ariver+tri_area
         enddo                  ! iseg
      enddo                     ! iriver

      return
      end
      


      subroutine bridge_contour(dim,mapXsize,mapYsize,
     &     Nbridge,bridge_width,bridge_track,
     &     Ncontour,Nppc,contour,Abridge)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to generate rivers contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nbridge: number of briges that have been generated
c       + bridge_width: width for each bridge [m]
c       + bridge_track: track that define the position of each bridge
c     
c     Output:
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + Abridge: area of bridges
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nbridge
      double precision bridge_width(1:Nbridge_mx)
      double precision bridge_track(1:Nbridge_mx,1:2,1:Ndim_mx-1)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision Abridge
c     temp
      integer ibridge,icontour,j
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision d,tri_area
c     label
      character*(Nchar_mx) label
      label='subroutine bridge_contour'
      
c     Check for inconsistencies
      if (Nbridge.gt.Nbridge_mx) then
         call error(label)
         write(*,*) 'Nbridge=',Nbridge
         write(*,*) '> Nbridge_mx=',Nbridge_mx
         stop
      endif
      Ncontour=Nbridge
      if ((Ncontour.lt.1).or.(Ncontour.gt.Ncontour_mx)) then
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be in the [1-',Ncontour_mx,'] range'
         stop
      endif
c     Generate contours
      Abridge=0.0D+0
      do ibridge=1,Nbridge
         icontour=ibridge
         d=bridge_width(ibridge)/2.0D+0
         do j=1,dim-1
            p1(j)=bridge_track(ibridge,1,j)
            p2(j)=bridge_track(ibridge,2,j)
         enddo                  ! j
         p1(dim)=0.0D+0
         p2(dim)=0.0D+0
         call normal_for_segment(dim,p1,p2,normal)
         call scalar_vector(dim,d,normal,u)
         call substract_vectors(dim,p1,u,x1)
         call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x1)
         call substract_vectors(dim,p2,u,x2)
         call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x2)
         call add_vectors(dim,p2,u,x3)
         call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x3)
         call add_vectors(dim,p1,u,x4)
         call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,x4)
         Nppc(icontour)=4
         do j=1,dim-1
            contour(icontour,1,j)=x1(j)
            contour(icontour,2,j)=x2(j)
            contour(icontour,3,j)=x3(j)
            contour(icontour,4,j)=x4(j)
         enddo                  ! j
         call triangle_area(dim,x1,x2,x3,tri_area)
         Abridge=Abridge+tri_area
         call triangle_area(dim,x1,x3,x4,tri_area)
         Abridge=Abridge+tri_area
      enddo                     ! ibridge

      return
      end



      subroutine road_contour(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     Ncontour,Nppc,contour,Aroad)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate roads contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nroad: number of roads that have been generated
c       + road_width: width of each road that has been generated
c       + road_Nppt: number of points for the description of the central track for each road
c       + road_track: list of positions that describe the central track of each road
c     
c     Output:
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + Aroad: area of roads
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision Aroad
c     temp
      integer iroad,icontour,Nseg,iseg,j
      double precision p0(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision n(1:Ndim_mx)
      double precision n_tmp(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision d(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision x4(1:Ndim_mx)
      double precision tri_area
c     label
      character*(Nchar_mx) label
      label='subroutine road_contour'

c     Check for inconsistencies
      if ((Nroad.lt.1).or.(Nroad.gt.Nroad_mx)) then
         call error(label)
         write(*,*) 'Nroad=',Nroad
         write(*,*) 'should be in the [1-',Nroad_mx,'] range'
         stop
      endif
      Ncontour=Nroad
      if ((Ncontour.lt.1).or.(Ncontour.gt.Ncontour_mx)) then
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be in the [1-',Ncontour_mx,'] range'
         stop
      endif
      do iroad=1,Nroad
         if ((road_Nppt(iroad).lt.1).or.
     &        (road_Nppt(iroad).gt.Nppt_mx)) then
            call error(label)
            write(*,*) 'road_Nppt(',iroad,')=',
     &           road_Nppt(iroad)
            write(*,*) 'should be in the [1-',Nppt_mx,'] range'
            stop
         endif
      enddo                     ! iroad

c     Generate contours
      Aroad=0.0D+0
      do iroad=1,Nroad
         icontour=iroad
         Nseg=road_Nppt(iroad)-1
         if (Nseg.lt.1) then
            call error(label)
            write(*,*) 'Nseg=',Nseg
            write(*,*) 'should be > 0'
            stop
         endif
         Nppc(icontour)=2*(Nseg+1)
         if ((Nppc(icontour).lt.1).or.(Nppc(icontour).gt.Nppc_mx)) then
            call error(label)
            write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
            write(*,*) 'should be in the [1-',Nppc_mx,'] range'
            stop
         endif
c     loop over segments
         do iseg=1,Nseg
            do j=1,dim-1
               p1(j)=road_track(iroad,iseg,j)
               p2(j)=road_track(iroad,iseg+1,j)
            enddo               ! j
            p1(dim)=0.0D+0
            p2(dim)=0.0D+0
            call normal_for_segment(dim,p1,p2,n)
c     n is the normal for the current segment
            if (iseg.eq.1) then
               call copy_vector(dim,n,n1)
            else                ! iseg>1
               do j=1,dim-1
                  p0(j)=road_track(iroad,iseg-1,j)
               enddo            ! j
               p0(dim)=0.0D+0
               call normal_for_segment(dim,p0,p1,n_tmp)
               call add_vectors(dim,n_tmp,n,u)
               call normalize_vector(dim,u,n1)
            endif               ! iseg=1
c     n1 is the normal to use for P1
            if (iseg.eq.Nseg) then
               call copy_vector(dim,n,n2)
            else                ! iseg>1
               do j=1,dim-1
                  p0(j)=road_track(iroad,iseg+2,j)
               enddo            ! j
               p0(dim)=0.0D+0
               call normal_for_segment(dim,p2,p0,n_tmp)
               call add_vectors(dim,n_tmp,n,u)
               call normalize_vector(dim,u,n2)
            endif               ! iseg=Nseg
c     n2 is the normal to use for P2
c     generating points for the segment
            call scalar_vector(dim,road_width(iroad)/2.0D+0,n1,d)
            call substract_vectors(dim,p1,d,x1)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,
     &           x1)
            call add_vectors(dim,p1,d,x4)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,
     &           x4)
            call scalar_vector(dim,road_width(iroad)/2.0D+0,n2,d)
            call substract_vectors(dim,p2,d,x2)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,
     &           x2)
            call add_vectors(dim,p2,d,x3)
            call clamp_on_ground(dim,0.0D+0,mapXsize,0.0D+0,mapYsize,
     &           x3)
c     adding points to contour
            if (iseg.eq.1) then
               do j=1,dim-1
                  contour(icontour,1,j)=x1(j)
                  contour(icontour,Nppc(icontour),j)=x4(j)
               enddo            ! j
            endif
            do j=1,dim-1
               contour(icontour,iseg+1,j)=x2(j)
               contour(icontour,Nppc(icontour)-iseg,j)=x3(j)
            enddo               ! j
            call triangle_area(dim,x1,x2,x3,tri_area)
            Aroad=Aroad+tri_area
            call triangle_area(dim,x1,x3,x4,tri_area)
            Aroad=Aroad+tri_area
         enddo                  ! iseg
      enddo                     ! iroad

      return
      end

      

      subroutine is_inside_contour(debug,dim,Ncontour,Nppc,contour,
     &     icontour,P,inside)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find whether or not a given position is inside a contour
c     
c     Input:
c       + debug: true for debug mode
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: contour for the existing list of contours
c       + icontour: index of the contour
c       + P: position to test
c     
c     Output:
c       + inside: true if "P" is inside contour index "icontour"
c     
c     I/O
      logical debug
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      double precision P(1:Ndim_mx)
      logical inside
c     temp
      integer Nsegments,isegment,Npoints,i1,i2
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision d,dmin
      integer isegment_dmin,iseg2
      double precision A(1:Ndim_mx)
      double precision AP(1:Ndim_mx)
      double precision nAP(1:Ndim_mx)
      double precision A_dmin(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision sp
      logical A_is_at_end,A_is_at_end_dmin
      integer end_side,end_side_dmin
c     label
      character*(Nchar_mx) label
      label='subroutine is_inside_contour'

c     Debug
      if (debug) then
         write(*,*) trim(label),' :in'
         write(*,*) 'icontour=',icontour
         write(*,*) 'P=',P
      endif                     ! debug
c     Debug
      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be <= Ncontour=',Ncontour
         stop
      endif

      call get_contour(dim,Ncontour,Nppc,contour,icontour,
     &     Npoints,ctr)
c     find segment "P" is the closest from
      Nsegments=Npoints
c     Debug
      if (debug) then
         write(*,*) 'Nsegments=',Nsegments
      endif                     ! debug
c     Debug
      do isegment=1,Nsegments
         i1=isegment
         call get_position_from_ctr(dim,Npoints,ctr,i1,x1)
         if (isegment.eq.Nsegments) then
            i2=1
         else
            i2=isegment+1
         endif
         call get_position_from_ctr(dim,Npoints,ctr,i2,x2)
         call distance_to_vector(dim,x1,x2,P,d,A,A_is_at_end,end_side)
c     Debug
      if (debug) then
         write(*,*) 'isegment=',isegment
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'd=',d
      endif                     ! debug
c     Debug
         if (isegment.eq.1) then
            dmin=d
            isegment_dmin=1
            call copy_vector(dim,A,A_dmin)
            A_is_at_end_dmin=A_is_at_end
            end_side_dmin=end_side
         endif
         if (d.le.dmin) then
            dmin=d
            isegment_dmin=isegment
            call copy_vector(dim,A,A_dmin)
            A_is_at_end_dmin=A_is_at_end
            end_side_dmin=end_side
         endif
         if (d.le.0.0D+0) then  ! point P is on contour: inside=T
            inside=.true.
            goto 666
         endif
      enddo                     ! i
c     Debug
      if (debug) then
         write(*,*) 'isegment_dmin=',isegment_dmin
         write(*,*) 'dmin=',dmin
         write(*,*) 'A_dmin=',A_dmin
      endif                     ! debug
c     Debug
c     
      call normal_to_segment(dim,Ncontour,Nppc,contour,
     &     icontour,isegment_dmin,normal)
c     vector AP
      call substract_vectors(dim,P,A_dmin,AP)
c     
c     Debug
      if (debug) then
         write(*,*) 'A_is_at_end_dmin=',A_is_at_end_dmin
      endif
c     Debug
      if (A_is_at_end_dmin) then
         if (end_side_dmin.eq.1) then
            if (isegment_dmin.eq.1) then
               iseg2=Nsegments
            else
               iseg2=isegment_dmin-1
            endif
         else                   ! end_side_dmin=2
            if (isegment_dmin.eq.Nsegments) then
               iseg2=1
            else
               iseg2=isegment_dmin+1
            endif
         endif                  ! end_side_dmin=1 or 2
         call normal_to_segment(dim,Ncontour,Nppc,contour,
     &        icontour,iseg2,n2)
         call add_vectors(dim,normal,n2,tmp)
c     Debug
         if (debug) then
            write(*,*) 'iseg2=',iseg2
            write(*,*) 'n2=',n2
            write(*,*) 'normal=',normal
            write(*,*) 'tmp=',tmp
         endif
c     Debug
         call scalar_product(dim,AP,tmp,sp)
      else
c     then check "P" is on the side of the normal for that segment
c     normal to the current segment
c     scalar product between the normal and vector AP
         call normalize_vector(dim,AP,nAP)
         call scalar_product(dim,normal,nAP,sp)
c     Debug
         if (debug) then
            write(*,*) 'normal=',normal
            write(*,*) 'AP=',AP
            write(*,*) 'nAP=',nAP
         endif
c     Debug
      endif
c     Debug
      if (debug) then
         write(*,*) 'sp=',sp
      endif
c     Debug
      if (sp.ge.0.0D+0) then
         inside=.true.
      else
         inside=.false.
      endif

 666  continue
c     Debug
      if (debug) then
         write(*,*) 'inside=',inside
         write(*,*) trim(label),' :out'
      endif                     ! debug
c     Debug

      return
      end


      
      subroutine normal_to_segment(dim,Ncontour,Nppc,contour,
     &     icontour,isegment,normal)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the normal to a given segment of a given contour
c     that is directed toward the inside of the contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: list of contours
c       + icontour: index of the contour
c       + isegment: index of the segment
c     
c     Output:
c       + normal: normal to segment index "isegment" of contour index "icontour"
c     and directed toward the inside of the contour
c     
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer isegment
      double precision normal(1:Ndim_mx)
c     temp
      integer Npoints,Nsegments
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i1,i2,j
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision norm
c     label
      character*(Nchar_mx) label
      label='subroutine normal_to_segment'

      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be <= Ncontour=',Ncontour
         stop
      endif

      call get_contour(dim,Ncontour,Nppc,contour,icontour,
     &     Npoints,ctr)
      Nsegments=Npoints
      if (isegment.lt.1) then
         call error(label)
         write(*,*) 'isegment=',isegment
         write(*,*) 'should be > 0'
         stop
      endif
      if (isegment.gt.Nsegments) then
         call error(label)
         write(*,*) 'isegment=',isegment
         write(*,*) 'should be <= Nsegments=',Nsegments
         stop
      endif
      i1=isegment
      call get_position_from_ctr(dim,Npoints,ctr,i1,x1)
      if (isegment.eq.Nsegments) then
         i2=1
      else
         i2=isegment+1
      endif
      call get_position_from_ctr(dim,Npoints,ctr,i2,x2)
      call substract_vectors(dim,x2,x1,u)
      norm=dsqrt(u(1)**2.0D+0+u(2)**2.0D+0)
      if (norm.le.0.0D+0) then
         call error(label)
         write(*,*) 'norm=',norm
         write(*,*) 'u=',(u(j),j=1,dim-1)
         stop
      else
         normal(1)=-u(2)/norm
         normal(2)=u(1)/norm
         normal(3)=0.0D+0
      endif

      return
      end


      
      subroutine add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &     Npoints,ctr)
      implicit none
      include 'max.inc'
c
c     Purpose: to add a given contour to a list of contours
c
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + Npoints: number of points for the contour to add
c       + ctr: contour to add
c
c     Output:
c     updated values of "Ncontour", "Np" and "contour"
c
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer Npoints
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine add_ctr_to_contour'

      Ncontour=Ncontour+1
      if (Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
c      
      if (Npoints.gt.Nppc_mx) then
         call error(label)
         write(*,*) 'Npoints=',Npoints
         write(*,*) '> Nppc_mx=',Nppc_mx
         stop
      else
         Nppc(Ncontour)=Npoints
      endif
c      
      do i=1,Nppc(Ncontour)
         do j=1,dim-1
            contour(Ncontour,i,j)=ctr(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end



      subroutine get_contour(dim,Ncontour,Nppc,contour,icontour,
     &     Np,ctr)
      implicit none
      include 'max.inc'
c
c     Purpose: to extract a given contour from a list of contours
c
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours in the existing list of contours
c       + Nppc: number of points for each contour in the existing list of contours
c       + contour: contour tracks for the existing list of contours
c       + icontour: index of the contour to extract
c     
c     Output:
c       + Np: number of points for the extracted contour
c       + ctr: extracted contour
c
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine get_contour'

      if (icontour.lt.1) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be > 0'
         stop
      endif
      if (icontour.gt.Ncontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'while Ncontour=',Ncontour
         stop
      endif
c
      if (Nppc(icontour).gt.Nppt_mx) then
         call error(label)
         write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      else
         Np=Nppc(icontour)
      endif
c      
      do i=1,Np
         do j=1,dim-1
            ctr(i,j)=contour(icontour,i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end


      
      subroutine duplicate_ctr(dim,Np,ctr,Np2,ctr2)
      implicit none
      include 'max.inc'
c
c     Purpose: to duplicate a contour
c
c     Input:
c       + dim: dimension of space
c       + Np: number of points for the contour du duplicate
c       + ctr: contour tp duplicate
c     
c     Output:
c       + Np2: number of points for the duplicated contour
c       + ctr2: duplicated contour
c
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine duplicate_ctr'

      Np2=Np
      do i=1,Np2
         do j=1,dim-1
            ctr2(i,j)=ctr(i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end
      


      subroutine get_position_from_ctr(dim,Npoints,ctr,i,position)
      implicit none
      include 'max.inc'
c     
c     Purpose: to extract a position from a contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the contour
c       + ctr: contour
c       + i: index of the position to extract
c     
c     Output:
c       + position: required position
c     
c     I/O
      integer dim
      integer Npoints
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i
      double precision position(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine get_position_from_ctr'

      if (i.lt.1) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be > 0'
         stop
      endif
      if (i.gt.Npoints) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be <= Npoints=',Npoints
         stop
      endif

      do j=1,dim-1
         position(j)=ctr(i,j)
      enddo                     ! j
      position(dim)=0.0D+0
      
      return
      end
      


      subroutine set_position_in_ctr(dim,Npoints,ctr,i,position)
      implicit none
      include 'max.inc'
c     
c     Purpose: to set a position in a contour
c     
c     Input:
c       + dim: dimension of space
c       + Npoints: number of points for the contour
c       + ctr: contour
c       + i: index of the position to set
c     
c     Output:
c       + position: position to set
c     
c     I/O
      integer dim
      integer Npoints
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer i
      double precision position(1:Ndim_mx)
c     temp
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine get_position_from_ctr'

      if (i.lt.1) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be > 0'
         stop
      endif
      if (i.gt.Npoints) then
         call error(label)
         write(*,*) 'Bad input argument: i=',i
         write(*,*) 'should be <= Npoints=',Npoints
         stop
      endif

      do j=1,dim-1
         ctr(i,j)=position(j)
      enddo                     ! j
      
      return
      end



      subroutine generate_inner_contour(dim,debug,
     &     Ncontour,Nppc,contour,width)
      implicit none
      include 'max.inc'
c     
c     Purpose: to analyze whether or not a inner contour should be
c     generated for a type 4 building, and if yes,
c     generate the "best looking" inner contour from its outer contour
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours (updated)
c       + Nppc: number of points that define each contour (updated)
c       + contour: list of coordinates for each contour (updated)
c       + width: required width
c     
c     Output:
c       + possibly updated values for "Ncontour", "Nppc" and "contour"
c     
c     I/O
      integer dim
      logical debug
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision width
c     temp
      integer Nppc_out
      double precision ctr_out(1:Nppc_mx,1:2)
      integer Nppc_in
      double precision ctr_in(1:Nppc_mx,1:2)
      integer i,j
      integer i01,i02,i11,i12,i21,i22
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision T1(1:Ndim_mx)
      double precision T2(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision n0(1:Ndim_mx)
      double precision d,dmin,sp,dop
      logical gen_inner_contour
      integer variant
      integer iseg_dmin,iseg1,iseg2
      logical intersection_found,parallel
      double precision dvp
      double precision A(1:Ndim_mx)
      logical A_is_at_end
      integer end_side
      integer Ncontour_tmp
      integer Nppc_tmp(1:Ncontour_mx)
      double precision contour_tmp(1:Ncontour_mx,
     &     1:Nppc_mx,1:Ndim_mx-1)
      logical intersection,self_intersection
c     label
      character*(Nchar_mx) label
      label='subroutine generate_inner_contour'

c     Debug
      if (debug) then
         write(*,*) 'In: ',trim(label)
      endif
c     Debug
      
c     Get the outer contour
      Nppc_out=Nppc(1)
      do i=1,Nppc(1)
         do j=1,dim-1
            ctr_out(i,j)=contour(1,i,j)
         enddo                  ! j
      enddo                     ! i
c     Debug
      if (debug) then
         write(*,*) 'Nppc_out=',Nppc_out
         do i=1,Nppc_out
            write(*,*) (ctr_out(i,j),j=1,dim-1)
         enddo                  ! i
      endif
c     Debug
c     Identify smallest segment: iseg_dmin
      call identify_smallest_segment(dim,Nppc_out,ctr_out,
     &     dmin,iseg_dmin,i01,i02,dop)
c     + previous segment
      call identify_close_segments(dim,Nppc_out,ctr_out,iseg_dmin,
     &     iseg1,iseg2,i11,i12,i21,i22)
c     Debug
      if (debug) then
         write(*,*) 'iseg_dmin=',iseg_dmin
         write(*,*) 'iseg1=',iseg1,' iseg2=',iseg2
      endif
c     Debug
      do j=1,dim-1
         x1(j)=ctr_out(i11,j)
         x2(j)=ctr_out(i12,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call normalize_vector(dim,tmp,u1) ! u1: direction for segment "iseg1"
      call normal_for_segment(dim,x1,x2,n1)
      call scalar_vector(dim,width,n1,tmp)
      call add_vectors(dim,x1,tmp,P1)
      call add_vectors(dim,x2,tmp,P2)
c     Debug
      if (debug) then
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'u1=',u1
         write(*,*) 'n1=',n1
         write(*,*) 'P1=',P1
         write(*,*) 'P2=',P2
      endif
c     Debug
c     + next segment
      do j=1,dim-1
         x1(j)=ctr_out(i21,j)
         x2(j)=ctr_out(i22,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call normalize_vector(dim,tmp,u2) ! u2: direction for segment "iseg2"
      call normal_for_segment(dim,x1,x2,n2)
      call scalar_vector(dim,width,n2,tmp)
      call add_vectors(dim,x1,tmp,P3)
      call add_vectors(dim,x2,tmp,P4)
c     Debug
      if (debug) then
         write(*,*) 'x1=',x1
         write(*,*) 'x2=',x2
         write(*,*) 'u2=',u2
         write(*,*) 'n2=',n2
         write(*,*) 'P3=',P3
         write(*,*) 'P4=',P4
      endif
c     Debug
c     + detect whether or not previous and next segments are parallel
      call scalar_product(dim,u1,u2,sp)
      if (dabs(sp).gt.0.98D+0) then
         parallel=.true.
      else
         parallel=.false.
      endif
c     Debug
      if (debug) then
         write(*,*) 'parallel=',parallel
      endif
c     Debug
c     Find whether or not a inner contour should be generated
      gen_inner_contour=.false. ! default
      if (parallel) then
c     If those segments are parallel, generating a inner contour is worth it
c     only when "dmin" is large enough compared to "width"
         if (dmin.ge.3.0D+0*width) then
            gen_inner_contour=.true.
            variant=1
         endif
      else                      ! parallel=F
c     If previous and next segments are not parallel, a inner contour has
c     to be generated no matter what. Only the method changes with the
c     value of "dmin"
c         if ((dmin.le.3.0D+0*width).and.(dop.ge.1.0D+0*width)) then
c            gen_inner_contour=.true.
c            variant=2
c         endif
         if ((dmin.ge.3.0D+0*width).and.(dop.ge.3.0D+0*width)) then
            gen_inner_contour=.true.
            variant=1
         endif
      endif
c     Debug
      if (debug) then
         write(*,*) 'gen_inner_contour=',gen_inner_contour
         write(*,*) 'variant=',variant
      endif
c     Debug
      if (.not.gen_inner_contour) then
c     Debug
c         write(*,*) '--------------------------------------'
c         write(*,*) 'gen_inner_contour=',gen_inner_contour
c         if (parallel) then
c            write(*,*) 'parallel, but dmin=',dmin,
c     &           ' <3*width=',3.0D+0*width
c         else
c            write(*,*) 'NOT parallel, and: dmin=',dmin
c            write(*,*) 'dop=',dop
c            write(*,*) '1.5*width=',1.50D+0*width
c            write(*,*) '3*width=',3.0D+0*width
c         endif
c     Debug
         goto 666
      endif

c     basic inner ctr (if variant=1, that is all)
      call generate_close_contour(dim,
     &     Nppc_out,ctr_out,width,.true.,
     &     Nppc_in,ctr_in)
c     Debug
      if (debug) then
         write(*,*) 'After generate_close_contour:'
         write(*,*) 'Nppc_in=',Nppc_in
         do i=1,Nppc_in
            write(*,*) (ctr_in(i,j),j=1,dim-1)
         enddo                  ! i
      endif
c     Debug
      
c     
      if (variant.eq.2) then
c     Intersection between the two lines that are parallel
c     to segments "iseg1" and "iseg2", at a distance "width"
c     Debug
         if (debug) then
            write(*,*) 'calling lli 1'
         endif
c     Debug
         call line_line_intersection(dim,P1,P2,P3,P4,.false.,
     &        intersection_found,Pint)
c     Debug
         if (debug) then
            write(*,*) 'intersection_found=',intersection_found
            if (intersection_found) then
               write(*,*) 'Pint=',Pint
            endif
         endif
c     Debug
         if (.not.intersection_found) then
            goto 666
         endif
c     Distance to "iseg_dmin" segment
         do j=1,dim-1
            x1(j)=ctr_out(i01,j)
            x2(j)=ctr_out(i02,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
c     Debug
         if (debug) then
            write(*,*) 'x1=',x1
            write(*,*) 'x2=',x2
         endif
c     Debug
         call normal_for_segment(dim,x1,x2,n0)
         call distance_to_vector(dim,x1,x2,Pint,dvp,A,
     &        A_is_at_end,end_side)
c     Debug
         if (debug) then
            write(*,*) 'dvp=',dvp,' width=',width
         endif
c     Debug
c         if (dvp.gt.width) then
            call substract_vectors(dim,Pint,A,tmp)
            call normalize_vector(dim,tmp,u)
            call scalar_product(dim,n0,u,sp)
c     Debug
            if (debug) then
               write(*,*) 'sp=',sp
            endif
c     Debug
            if (sp.le.0.0D+0) then
c     negative scalar_product: the intersection occurs on the outer side
c     of the external contour
               goto 666
            else
               d=1.50D+0*dvp
            endif               ! sp < 0
c         else
c            d=dvp
c         endif                  ! dvp > width
         call scalar_vector(dim,d,u,tmp)
         call add_vectors(dim,x1,tmp,T1)
         call add_vectors(dim,x2,tmp,T2)
         T1(dim)=0.0D+0
         T2(dim)=0.0D+0
c     new position at the beginning of "iseg_dmin" segment's inner ctr
c     Debug
         if (debug) then
            write(*,*) 'd=',d
            write(*,*) 'u=',u
            write(*,*) 'tmp=',tmp
            write(*,*) 'calling lli 2'
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
            write(*,*) 'T1=',T1
            write(*,*) 'T2=',T2
         endif
c     Debug
         call line_line_intersection(dim,P1,P2,T1,T2,.false.,
     &        intersection_found,Pint)
c     Debug
         if (debug) then
            write(*,*) 'intersection_found=',intersection_found
            if (intersection_found) then
               write(*,*) 'Pint=',Pint
            endif
         endif
c     Debug
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found'
            write(*,*) 'between (P1P2) and (T1T2)'
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
            write(*,*) 'T1=',T1
            write(*,*) 'T2=',T2
            stop
         else
            do j=1,dim-1
               ctr_in(i01,j)=Pint(j)
            enddo               ! j
         endif
c     new position at the end of "iseg_dmin" segment's inner ctr
c     Debug
         if (debug) then
            write(*,*) 'calling lli 3'
         endif
c     Debug
         call line_line_intersection(dim,P3,P4,T1,T2,.false.,
     &        intersection_found,Pint)
c     Debug
         if (debug) then
            write(*,*) 'intersection_found=',intersection_found
            if (intersection_found) then
               write(*,*) 'Pint=',Pint
            endif
         endif
c     Debug
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found'
            write(*,*) 'between (P3P4) and (T1T2)'
            stop
         else
            do j=1,dim-1
               ctr_in(i02,j)=Pint(j)
            enddo               ! j
         endif
      endif                     ! variant=2
c     Debug
      if (debug) then
         write(*,*) 'Final inner contour:'
         write(*,*) 'Nppc_in=',Nppc_in
         do i=1,Nppc_in
            write(*,*) (ctr_in(i,j),j=1,dim-1)
         enddo                  ! i
      endif
c     Debug

      Ncontour_tmp=0
      call add_ctr_to_contour(dim,Ncontour_tmp,Nppc_tmp,contour_tmp,
     &     Nppc_out,ctr_out)
      call add_ctr_to_contour(dim,Ncontour_tmp,Nppc_tmp,contour_tmp,
     &     Nppc_in,ctr_in)
c     check contour for self_intersection
c     Debug
c      write(*,*) 'ok1: ',trim(label)
c     Debug
      call contour_self_intersects(dim,
     &     0.0D+0,0.0D+0,
     &     Ncontour_tmp,Nppc_tmp,contour_tmp,2,
     &     self_intersection)
c     Debug
c      write(*,*) 'ok2: ',trim(label)
c     Debug
c     check for intersection with outer contour
      call contours_intersect(dim,
     &     0.0D+0,0.0D+0,
     &     Ncontour_tmp,Nppc_tmp,contour_tmp,2,1,
     &     intersection)
      if ((.not.intersection).and.(.not.self_intersection)) then
c     Update contours
         Ncontour=Ncontour+1
         Nppc(Ncontour)=Nppc_in
         do i=1,Nppc_in
            do j=1,dim-1
               contour(Ncontour,i,j)=ctr_in(i,j)
            enddo               ! j
         enddo                  ! i
      endif
      
 666  continue
c     Debug
      if (debug) then
         write(*,*) 'Out: ',trim(label)
      endif
c     Debug
      return
      end



      subroutine identify_close_segments(dim,Nppc,ctr,iseg,
     &     iseg1,iseg2,i11,i12,i21,i22)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two surrounding segments for
c     a given segment of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c       + iseg: required segment index
c     
c     Output:
c       + iseg1: index of the segment just before "iseg"
c       + iseg2: index of the segment just after "iseg"
c       + i11 & i12: indexes of the first and second point for segment "iseg1"
c       + i21 & i22: indexes of the first and second point for segment "iseg2"
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppc_mx,1:2)
      integer iseg
      integer iseg1
      integer iseg2
      integer i11,i12
      integer i21,i22
c     temp
      integer Nseg,i1,i2
c     label
      character*(Nchar_mx) label
      label='subroutine identify_close_segments'

      Nseg=Nppc
c     + previous segment
      if (iseg.eq.1) then
         iseg1=Nseg
      else
         iseg1=iseg-1
      endif
      i11=iseg1
      if (iseg1.eq.Nseg) then
         i12=1
      else
         i12=i11+1
      endif
      if (iseg.eq.Nseg) then
         iseg2=1
      else
         iseg2=iseg+1
      endif
      i21=iseg2
      if (iseg2.eq.Nseg) then
         i22=1
      else
         i22=i21+1
      endif
      
      return
      end



      subroutine identify_smallest_segment(dim,Nppc,ctr,
     &     dmin,iseg_dmin,i01,i02,dop)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the index of the smallest segment
c     of a closed contour
c     
c     Input:
c       + dim: dimension of space
c       + Nppc: number of points for the trak that defines the contour
c       + ctr: ctr
c     
c     Output:
c       + dmin: length of the smallest segment
c       + iseg_dmin: index of the smallest segment
c       + i01 & i02: indexes of the two points for segment index "iseg_dmin"
c       + dop: length of the segment opposite to the smallest segment
c     
c     I/O
      integer dim
      integer Nppc
      double precision ctr(1:Nppc_mx,1:2)
      double precision dmin
      integer iseg_dmin
      integer i01,i02
      double precision dop
c     temp
      integer Nseg,iseg,j,iseg_op,i1,i2
      double precision d
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine identify_smallest_segment'

      Nseg=Nppc
      do iseg=1,Nseg
         i1=iseg
         if (iseg.eq.Nseg) then
            i2=1
         else
            i2=i1+1
         endif
         do j=1,dim-1
            x1(j)=ctr(i1,j)
            x2(j)=ctr(i2,j)
         enddo                  ! j
         x1(dim)=0.0D+0
         x2(dim)=0.0D+0
         call substract_vectors(dim,x2,x1,tmp)
         call vector_length(dim,tmp,d)
c     Debug
         if (d.le.0.0D+0) then
            call error(label)
            write(*,*) 'd=',d
            write(*,*) 'Nseg=',Nseg
            write(*,*) 'iseg=',iseg
            write(*,*) 'i1=',i1,' i2=',i2
            write(*,*) 'x1=',x1
            write(*,*) 'x2=',x2
            stop
         endif
c     Debug
         if (iseg.eq.1) then
            dmin=d
            iseg_dmin=iseg
         else
            if (d.lt.dmin) then
               dmin=d
               iseg_dmin=iseg
            endif               ! d < dmin
         endif                  ! iseg=1
      enddo                     ! iseg
      i01=iseg_dmin
      if (iseg_dmin.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif

      iseg_op=iseg_dmin+Nseg/2
      if (iseg_op.gt.Nseg) then
         iseg_op=iseg_op-Nseg
      endif
      i1=iseg_op
      if (iseg_op.eq.Nseg) then
         i2=1
      else
         i2=i1+1
      endif
      do j=1,dim-1
         x1(j)=ctr(i1,j)
         x2(j)=ctr(i2,j)
      enddo                     ! j
      x1(dim)=0.0D+0
      x2(dim)=0.0D+0
      call substract_vectors(dim,x2,x1,tmp)
      call vector_length(dim,tmp,dop)
      
      return
      end
      

      
      subroutine generate_close_contour(dim,Np,ctr,width,inside,
     &     Np2,ctr2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a new contour at a given distance from a existing contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points for the input contour
c       + ctr: input contour
c       + width: distance between old and new ctrs
c       + inside: true if the new ctr has to be generated inside the closed input contour
c     
c     Output:
c       + Np2: number of points for the generated contour
c       + ctr2: ctr for the generated contour
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision width
      logical inside
      integer Np2
      double precision ctr2(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer Nseg,iseg,i,j
      integer i11,i12,i21,i22
      double precision x11(1:Ndim_mx)
      double precision x12(1:Ndim_mx)
      double precision x21(1:Ndim_mx)
      double precision x22(1:Ndim_mx)
      logical identical
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_close_contour'

      Np2=0
      Nseg=Np
      do iseg=1,Nseg
c     extract coordinates of x11 and x12, the 2 points of segment "iseg"
         call segment_indexes(dim,Np,ctr,iseg,i11,i12,x11,x12)
c     extract coordinates of x21 and x22, the 2 points of segment "iseg-1"
         call previous_segment_indexes(dim,Np,ctr,iseg,i21,i21,x21,x22)
c     check
         call identical_vectors(dim,x11,x22,identical)
         if (.not.identical) then
            call error(label)
            write(*,*) 'x11=',x11
            write(*,*) 'x22=',x22
            write(*,*) 'should be identical'
            write(*,*) 'Np=',Np
            write(*,*) 'ctr:'
            do i=1,Np
               write(*,*) (ctr(i,j),j=1,dim-1)
            enddo               ! i
            write(*,*) 'iseg=',iseg
            write(*,*) 'i11=',i11,' i12=',i12
            write(*,*) 'segment previous to iseg:'
            write(*,*) 'i21=',i21,' i22=',i22
            stop
         endif
c     inner normals
         call normal_for_segment(dim,x11,x12,n1)
         call normal_for_segment(dim,x21,x22,n2)
c     if outside contour required: invert normals
         if (.not.inside) then
            call scalar_vector(dim,-1.0D+0,n1,n1)
            call scalar_vector(dim,-1.0D+0,n2,n2)
         endif
c     generate new points that define the two intersecting lines
         call scalar_vector(dim,width,n1,tmp)
         call add_vectors(dim,x11,tmp,P1)
         call add_vectors(dim,x12,tmp,P2)
         call scalar_vector(dim,width,n2,tmp)
         call add_vectors(dim,x21,tmp,P3)
         call add_vectors(dim,x22,tmp,P4)
c     intersect the two lines
         call line_line_intersection(dim,P1,P2,P3,P4,.false.,
     &        intersection_found,Pint)
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found between:'
            write(*,*) 'line 1 defined by:'
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
            call substract_vectors(dim,P2,P1,P1P2)
            call normalize_vector(dim,P1P2,u12)
            write(*,*) 'u12=',u12
            write(*,*) 'x11=',x11
            write(*,*) 'x12=',x12
            write(*,*) 'n1=',n1
            call scalar_vector(dim,width,n1,tmp)
            write(*,*) 'width*n1=',tmp
            write(*,*) 'line 2 defined by:'
            write(*,*) 'P3=',P3
            write(*,*) 'P4=',P4
            write(*,*) 'x21=',x21
            write(*,*) 'x22=',x22
            write(*,*) 'n2=',n2
            call scalar_vector(dim,width,n2,tmp)
            write(*,*) 'width*n2=',tmp
            call substract_vectors(dim,P4,P3,P3P4)
            call normalize_vector(dim,P3P4,u34)
            write(*,*) 'u34=',u34
            stop
         endif
c     add point to contour
         Np2=Np2+1
         do j=1,dim-1
            ctr2(Np2,j)=Pint(j)
         enddo                  ! j
      enddo                     ! i

      return
      end


      
      subroutine generate_close_contour2(dim,debug,Np_ref,ctr_ref,
     &     width_min,width_max,inward,try_best_fit,success,
     &     Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a new contour at a given distance from a reference contour
c     
c     Input:
c       + dim: dimension of space
c       + Np_ref: number of points for the reference contour
c       + ctr_ref: reference contour
c       + width_min: minimum distance between default contour and generated contour
c       + width_max: maximum distance between default contour and generated contour;
c                    This is also the value of width used when try_best_fit=F
c       + inward: true if the new contour has to be generated inside the default contour
c                 when false, the new contour is generated outward
c       + try_best_fit: when a valid contour could not be generated at a distance "width_max",
c                       the routine can try to find a valid contour between "width_min"
c                       and "width_max" when this switch is set to T. Otherwise it
c                       will not even try.
c     
c     Output:
c       + success: true when a contour could be generated at the specified "width_max"
c                  distance, or at a distance between "width_min" and "width_max" if
c                  "try_best_fit" was set to T.
c       + best_fit: when success=T, this switch specifies whether the generated contour
c                   was generated for the default "width_max" distance (best_fit=F)
c                   or is a best fit between "width_min" and "width_max"
c       + width: value of the width the best-fit contour was generated for, when
c                success=T and best_fit=T.
c       + Np: number of points for the generated contour
c       + ctr: ctr for the generated contour
c     
c     I/O
      integer dim
      logical debug
      integer Np_ref
      double precision ctr_ref(1:Nppc_mx,1:Ndim_mx-1)
      double precision width_min
      double precision width_max
      logical inward
      logical try_best_fit
      logical success
      logical best_fit
      double precision width
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,j
      integer Np_tmp
      double precision ctr_tmp(1:Nppc_mx,1:Ndim_mx-1)
      logical print_err
      logical test_inside_positions
      logical test_self_intersection
      logical test_intersection
      logical test_crossed_intersection
      logical test_clockwise
      logical valid,solution_found,keep_trying
      double precision dmin,dmax,d
      double precision Lmin,Lop
      integer iseg_Lmin,i01,i02
      integer Niter
c     parameters
      double precision epsilon_d
      parameter(epsilon_d=1.0D-2)
c     label
      character*(Nchar_mx) label
      label='subroutine generate_close_contour2'

      success=.false.           ! default
c     check consistency of the reference contour
      print_err=debug
      test_inside_positions=.false.
      test_self_intersection=.true.
      test_intersection=.false.
      test_crossed_intersection=.false.
      test_clockwise=.true.
      call is_contour_valid(dim,Np_ref,ctr_ref,Np_ref,ctr_ref,
     &     print_err,test_inside_positions,test_self_intersection,
     &     test_intersection,test_crossed_intersection,test_clockwise,
     &     valid)
      if (.not.valid) then
         call error(label)
         write(*,*) 'reference contour is not valid:'
         write(*,*) 'Np_ref=',Np_ref
         do i=1,Np_ref
            write(*,*) (ctr_ref(i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif                     ! .not.valid
c     Debug
      if (debug) then
         write(*,*) 'reference contour is valid'
      endif
c     Debug

c     generate required contour at maximum possible distance
      call generate_contour(dim,Np_ref,ctr_ref,width_max,inward,
     &     Np_tmp,ctr_tmp)
c     from now on, generated contour will be tested with these settings
      test_inside_positions=inward ! only test positions are inside the reference contour when inward=T
      test_self_intersection=.true.
      test_intersection=.true.
      test_crossed_intersection=inward ! only test crossed intersection when inward=T
      test_clockwise=.true.
c     test the farther contour
      call is_contour_valid(dim,Np_ref,ctr_ref,Np_tmp,ctr_tmp,
     &     print_err,test_inside_positions,test_self_intersection,
     &     test_intersection,test_crossed_intersection,test_clockwise,
     &     valid)
      if (valid) then
c     if it is valid, this is what was required by default
         call duplicate_ctr(dim,Np_tmp,ctr_tmp,Np,ctr)
         success=.true.
c     Debug
         if (debug) then
            write(*,*) 'New contour at distance:'
            write(*,*) 'width_max=',width_max
            write(*,*) 'is valid'
         endif
c     Debug
         goto 666
      else
         success=.false.
c     Debug
         if (debug) then
            write(*,*) 'New contour at distance:'
            write(*,*) 'width_max=',width_max
            write(*,*) 'is invalid'
         endif
c     Debug
      endif                     ! valid
c     otherwise, a best fit can be looked for if required
      if (try_best_fit) then
c     Debug
         if (debug) then
            write(*,*) 'Trying best-fit'
         endif
c     Debug
c     produce minimum distance contour
         call generate_contour(dim,Np_ref,ctr_ref,width_min,inward,
     &        Np_tmp,ctr_tmp)
c     if invalid, there is no best fit in the specified width range
         call is_contour_valid(dim,Np_ref,ctr_ref,Np_tmp,ctr_tmp,
     &        print_err,test_inside_positions,test_self_intersection,
     &        test_intersection,.false.,test_clockwise,
     &        valid)
c     Debug
         if (debug) then
            write(*,*) 'New contour at distance:'
            write(*,*) 'width_min=',width_min
            if (valid) then
               write(*,*) 'is valid'
            else
               write(*,*) 'is invalid'
            endif
         endif
c     Debug
         if (.not.valid) then
            goto 666
         endif
c     otherwise there may be a possibility...
         Niter=0
         solution_found=.false.
         dmin=width_min
         dmax=width_max
         keep_trying=.true.
         do while (keep_trying)
            Niter=Niter+1
c     Debug
            if (debug) then
               write(*,*) 'Niter=',Niter
            endif
c     Debug
            d=(dmin+dmax)/2.0D+0
c     produce contour for current distance "d"
            call generate_contour(dim,Np_ref,ctr_ref,d,inward,
     &           Np_tmp,ctr_tmp)
c     if invalid, there is no best fit in the specified width range
            call is_contour_valid(dim,Np_ref,ctr_ref,Np_tmp,ctr_tmp,
     &           print_err,test_inside_positions,test_self_intersection,
     &           test_intersection,test_crossed_intersection,
     &           test_clockwise,
     &           valid)
c     Debug
            if (debug) then
               write(*,*) 'new contour at distance d=',d
               if (valid) then
                  write(*,*) 'is valid'
               else
                  write(*,*) 'is invalid'
               endif
            endif
c     Debug
            if (valid) then
               dmin=d
            else
               dmax=d
            endif
            if ((valid).and.(dabs(dmax-dmin).le.epsilon_d)) then
               solution_found=.true.
               keep_trying=.false.
c     Debug
               if (debug) then
                  write(*,*) 'exiting because solution was found'
               endif
c     Debug
            endif
            if (Niter.gt.Niter_mx) then
               keep_trying=.false.
c     Debug
               if (debug) then
                  write(*,*) 'exiting because Niter_mx was reached'
               endif
c     Debug
            endif
         enddo                  ! while (keep_trying)
         if (solution_found) then
            call duplicate_ctr(dim,Np_tmp,ctr_tmp,Np,ctr)
            width=d
            success=.true.
         endif                  ! solution_found
      endif                     ! try_best_fit
      
 666  continue
      return
      end



      subroutine generate_contour(dim,Np_ref,ctr_ref,width,inward,
     &     Np,ctr)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a contour (without any consistency check)
c     at a given distance from a reference contour
c     
c     Input:
c       + dim: dimension of space
c       + Np_ref: number of points for the reference contour
c       + ctr_ref: reference contour
c       + width: distance to generate a new contour
c       + inward: true if the new contour has to be generated inside the default contour
c                 when false, the new contour is generated outward
c     
c     Output:
c       + Np: number of points for the generated contour
c       + ctr: ctr for the generated contour
c     
c     I/O
      integer dim
      integer Np_ref
      double precision ctr_ref(1:Nppc_mx,1:Ndim_mx-1)
      double precision width
      logical inward
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer iseg,Nseg,i,j
      integer i11,i12,i21,i22
      double precision x11(1:Ndim_mx)
      double precision x12(1:Ndim_mx)
      double precision x21(1:Ndim_mx)
      double precision x22(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision n2(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision P3P4(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      logical identical,intersection_found
c     label
      character*(Nchar_mx) label
      label='subroutine generate_contour'

      if (width.eq.0.0D+0) then
         call duplicate_ctr(dim,Np_ref,ctr_ref,Np,ctr)
         goto 666
      endif
      
      Np=0
      Nseg=Np_ref
      do iseg=1,Nseg
c     extract coordinates of x11 and x12, the 2 points of segment "iseg"
         call segment_indexes(dim,Np_ref,ctr_ref,iseg,
     &        i11,i12,x11,x12)
c     extract coordinates of x21 and x22, the 2 points of segment "iseg-1"
         call previous_segment_indexes(dim,Np_ref,ctr_ref,iseg,
     &        i21,i21,x21,x22)
c     check
         call identical_vectors(dim,x11,x22,identical)
         if (.not.identical) then
            call error(label)
            write(*,*) 'x11=',x11
            write(*,*) 'x22=',x22
            write(*,*) 'should be identical'
            write(*,*) 'Np_ref=',Np_ref
            write(*,*) 'ctr_ref:'
            do i=1,Np_ref
               write(*,*) (ctr_ref(i,j),j=1,dim-1)
            enddo               ! i
            write(*,*) 'iseg=',iseg
            write(*,*) 'i11=',i11,' i12=',i12
            write(*,*) 'segment previous to iseg:'
            write(*,*) 'i21=',i21,' i22=',i22
            stop
         endif
c     inner normals
         call normal_for_segment(dim,x11,x12,n1)
         call normal_for_segment(dim,x21,x22,n2)
c     if outside contour required: invert normals
         if (.not.inward) then
            call scalar_vector(dim,-1.0D+0,n1,n1)
            call scalar_vector(dim,-1.0D+0,n2,n2)
         endif
c     generate new points that define the two intersecting lines
         call scalar_vector(dim,width,n1,tmp)
         call add_vectors(dim,x11,tmp,P1)
         call add_vectors(dim,x12,tmp,P2)
         call scalar_vector(dim,width,n2,tmp)
         call add_vectors(dim,x21,tmp,P3)
         call add_vectors(dim,x22,tmp,P4)
c     intersect the two lines
         call line_line_intersection(dim,P1,P2,P3,P4,.false.,
     &        intersection_found,Pint)
         if (.not.intersection_found) then
            call error(label)
            write(*,*) 'Intersection not found between:'
            write(*,*) 'line 1 defined by:'
            write(*,*) 'P1=',P1
            write(*,*) 'P2=',P2
            call substract_vectors(dim,P2,P1,P1P2)
            call normalize_vector(dim,P1P2,u12)
            write(*,*) 'u12=',u12
            write(*,*) 'x11=',x11
            write(*,*) 'x12=',x12
            write(*,*) 'n1=',n1
            call scalar_vector(dim,width,n1,tmp)
            write(*,*) 'width*n1=',tmp
            write(*,*) 'line 2 defined by:'
            write(*,*) 'P3=',P3
            write(*,*) 'P4=',P4
            write(*,*) 'x21=',x21
            write(*,*) 'x22=',x22
            write(*,*) 'n2=',n2
            call scalar_vector(dim,width,n2,tmp)
            write(*,*) 'width*n2=',tmp
            call substract_vectors(dim,P4,P3,P3P4)
            call normalize_vector(dim,P3P4,u34)
            write(*,*) 'u34=',u34
            stop
         endif
c     add point to contour
         call add_point_to_ctr(dim,Pint,Np,ctr)
      enddo                     ! i

 666  continue
      return
      end
      

      
      subroutine is_contour_valid(dim,Np_ref,ctr_ref,Np,ctr,
     &     print_err,test_inside_positions,test_self_intersection,
     &     test_intersection,test_crossed_intersection,test_clockwise,
     &     valid)
      implicit none
      include 'max.inc'
c     
c     Purpose: to find whether or not a given contour is valid, relatively
c     to a reference contour.
c     
c     Input:
c       + dim: dimension of space
c       + Np_ref: number of points that define the reference contour
c       + ctr_ref: reference contour
c       + Np: number of points that define the contour to test
c       + ctr: contour to test
c       + print_err: true if origin of failure has to be displayed
c       + test_inside_positions: T when each position of "ctr" has to be tested as inside "ctr_ref"
c       + test_self_intersection: T when "ctr" has to be tested for self-intersection
c       + test_intersection: T when "ctr" has to be tested for intersecting "ctr_ref"
c       + test_crossed_intersection: T when each segment constituted by a point of "ctr_ref"
c     and its image in "ctr" has to NOT intersect "ctr"
c       + test_clockwise: T when "ctr" has to be tested for rotation direction
c     
c     Output:
c       + valid: T when "ctr" is valid
c     
c     I/O
      integer dim
      integer Np_ref
      double precision ctr_ref(1:Nppc_mx,1:Ndim_mx-1)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical print_err
      logical test_inside_positions
      logical test_self_intersection
      logical test_intersection
      logical test_crossed_intersection
      logical test_clockwise
      logical valid
c     temp
      integer i,j
      double precision P(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical inside,intersection_found,clockwise
c     label
      character*(Nchar_mx) label
      label='subroutine is_contour_valid'

      valid=.true.
c
c     prerequisite: number of points should be identical
      if (Np_ref.ne.Np) then
         valid=.false.
         goto 666
      endif
c     
      if (test_inside_positions) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_ref,ctr_ref)
         do i=1,Np
            call get_position_from_ctr(dim,Np,ctr,i,P)
            call is_inside_contour(.false.,dim,Ncontour,Nppc,contour,
     &           1,P,inside)
            if (.not.inside) then
               valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed inside_positions'
               endif
               goto 666
            endif               ! inside=F
         enddo                  ! i
      endif                     ! test_inside_positions
c
      if (test_self_intersection) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np,ctr)
         call contour_self_intersects(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour,Nppc,contour,1,
     &        intersection_found)
         if (intersection_found) then
            valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed self_intersects'
               endif
            goto 666
         endif                  ! intersection_found
      endif                     ! test_self_intersection
c
      if (test_intersection) then
         Ncontour=0
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np_ref,ctr_ref)
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour,
     &        Np,ctr)
         call contours_intersect(dim,
     &        0.0D+0,0.0D+0,
     &        Ncontour,Nppc,contour,1,2,
     &        intersection_found)
         if (intersection_found) then
            valid=.false.
            if (print_err) then
               call error(label)
               write(*,*) 'contour failed intersection'
            endif
            goto 666
         endif                  ! intersection_found
      endif                     ! test_intersection
c
      if (test_crossed_intersection) then
         do i=1,Np_ref
            call get_position_from_ctr(dim,Np_ref,ctr_ref,i,P1)
            call get_position_from_ctr(dim,Np,ctr,i,P2)
            call segment_intersects_contour(dim,P1,P2,Np,ctr,
     &           intersection_found,Pint)
            if (intersection_found) then
               valid=.false.
               if (print_err) then
                  call error(label)
                  write(*,*) 'contour failed crossed_intersection'
               endif
               goto 666
            endif               ! intersection_found
         enddo                  ! i
      endif                     ! test_crossed_intersection
c
      if (test_clockwise) then
         call is_contour_clockwise(dim,Np,ctr,clockwise)
         if (clockwise) then
            valid=.false.
            if (print_err) then
               call error(label)
               write(*,*) 'contour failed clockwise'
            endif
            goto 666
         endif                  ! clockwise
      endif                     ! test_clockwise
c     
 666  continue
      return
      end
      


      subroutine segments_intersection(dim,P0,P1,P2,inside,d,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Purpose: to get the intersection position between the two lines
c     parallel to 2 segments of a closed contour, [P0,P1] and [P1,P2], at a distance d.
c     
c     Input:
c       + dim: dimension of space
c       + P0: point that defines the beginning of the first segment
c       + P1: point that defines the end of the first segment / beginning of the second segment
c       + P2: point that defines the end of the second segment
c       + inside: true if the new ctrhas to be generated inside the closed input contour
c     
c     Output:
c       + intersection_found: true if intersection position was found
c       + Pint: intersection position between the line parallel to (P0P1) at a distance d (in direction n0)
c     and the line parallel to (P1P2) at a distance d (in direction n1), when intersection_found=.true.
c     
c     I/O
      integer dim
      double precision P0(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      logical inside
      double precision d
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      double precision n0(1:Ndim_mx)
      double precision n1(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision P0P1(1:Ndim_mx)
      double precision P1P2(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c      double precision n(1:Ndim_mx)
c      double precision u(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      double precision sp,alpha,beta,gamma
      double precision A(1:Ndim_mx)
      double precision B(1:Ndim_mx)
      logical identical
      integer j
c     label
      character*(Nchar_mx) label
      label='subroutine segments_intersection'
      
      call identical_positions(dim,P0,P1,identical)
      if (identical) then
         call error(label)
         write(*,*) 'P0=',P0
         write(*,*) 'P1=',P1
         write(*,*) 'should be different'
         stop
      endif
      call identical_positions(dim,P1,P2,identical)
      if (identical) then
         call error(label)
         write(*,*) 'P1=',P1
         write(*,*) 'P2=',P2
         write(*,*) 'should be different'
         stop
      endif

c     [P0P1]
      call substract_vectors(dim,P0,P1,P0P1)
      call normalize_vector(dim,P0P1,u0)
      call normal_for_segment(dim,P0,P1,n0)
      if (.not.inside) then
         call scalar_vector(dim,-1.0D+0,n0,n0)
      endif
c     [P1P2]
      call substract_vectors(dim,P2,P1,P1P2)
      call normalize_vector(dim,P1P2,u1)
      call normal_for_segment(dim,P1,P2,n1)
      if (.not.inside) then
         call scalar_vector(dim,-1.0D+0,n1,n1)
      endif
c     Intersection between parallel lines
      call scalar_product(dim,u0,u1,sp)
      if (sp.eq.-1.0D+0) then
         intersection_found=.false.
      else if (sp.eq.1.0D+0) then
         intersection_found=.true.
         call scalar_vector(dim,d,n1,t)
         call add_vectors(dim,p1,t,Pint)
      else
         intersection_found=.true.
         call scalar_vector(dim,d,n0,t)
         call add_vectors(dim,P0,t,A)
         call scalar_vector(dim,d,n1,t)
         call add_vectors(dim,P1,t,B)
         gamma=u1(2)*u0(1)-u1(1)*u0(2)
         if (gamma.eq.0.0D+0) then
            call error(label)
            write(*,*) 'gamma=',gamma
            write(*,*) 'should not happen at this point'
            stop
         endif
         beta=((A(2)-B(2))*u0(1)+(B(1)-A(1))*u0(2))/gamma
         if (u0(1).ne.0.0D+0) then
            alpha=(B(1)-A(1)+beta*u1(1))/u0(1)
         else
            if (u0(2).ne.0.0D+0) then
               alpha=(B(2)-A(2)+beta*u1(2))/u0(2)
            else
               write(*,*) 'u0=',(u0(j),j=1,2)
               write(*,*) 'is not normalized'
               stop
            endif
         endif
         call scalar_vector(dim,alpha,u0,t)
         call add_vectors(dim,A,t,Pint1)
         call scalar_vector(dim,beta,u1,t)
         call add_vectors(dim,B,t,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (.not.identical) then
            call error(label)
            write(*,*) 'Pint1=',Pint1
            write(*,*) 'Pint2=',Pint2
            write(*,*) 'are not identical'
            write(*,*) 'A=',A
            write(*,*) 'alpha=',alpha
            write(*,*) 'u0=',u0
            write(*,*) 'B=',B
            write(*,*) 'u1=',u1
            write(*,*) 'beta=',beta
            stop
         else
            call copy_vector(dim,Pint1,Pint)
         endif
      endif

      return
      end


      
      subroutine segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make a segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of segment "iseg"
c       + i02: index of the second point of segment "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j
c     label
      character*(Nchar_mx) label
      label='subroutine segment_indexes'

      Nseg=Np
      i01=iseg
      if (iseg.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end
      

      
      subroutine previous_segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make the previous segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of the segment previous to "iseg"
c       + i02: index of the second point of the segment previous to "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j,iseg1
c     label
      character*(Nchar_mx) label
      label='subroutine previous_segment_indexes'

      Nseg=Np
      if (iseg.eq.1) then
         iseg1=Nseg
      else
         iseg1=iseg-1
      endif
      i01=iseg1
      if (iseg1.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end
      

      
      subroutine next_segment_indexes(dim,Np,ctr,iseg,
     &     i01,i02,x01,x02)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the two positions that make the next segment of a given contour
c     
c     Input:
c       + dim: dimension of space
c       + Np: number of points in the contour
c       + ctr: contour
c       + iseg: index of the required segment
c     
c     Output:
c       + i01: index of the first point of the segment next to "iseg"
c       + i02: index of the second point of the segment next to "iseg"
c       + x01: position of point "i01"
c       + x02: position of point "i02"
c     
c     I/O
      integer dim
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      integer iseg
      integer i01
      integer i02
      double precision x01(1:Ndim_mx)
      double precision x02(1:Ndim_mx)
c     temp
      integer Nseg,j,iseg1
c     label
      character*(Nchar_mx) label
      label='subroutine next_segment_indexes'

      Nseg=Np
      if (iseg.eq.Nseg) then
         iseg1=1
      else
         iseg1=iseg+1
      endif
      i01=iseg1
      if (iseg1.eq.Nseg) then
         i02=1
      else
         i02=i01+1
      endif
      do j=1,dim-1
         x01(j)=ctr(i01,j)
         x02(j)=ctr(i02,j)
      enddo                     ! j
      x01(dim)=0.0D+0
      x02(dim)=0.0D+0

      return
      end



      subroutine contour_area2(dim,Ncontour,Npoints,points,area)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the area of the surface defined by a
c     number of contours
c     This version is based on a Delaunay triangulation
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours (including holes)
c       + Npoints: number of points that define each contour
c       + points: list of points that define each contour
c     
c     Output:
c       + area: area of the surface
c     
c     I/O
      integer dim
      integer Ncontour
      integer Npoints(1:Ncontour_mx)
      double precision points(1:Ncontour_mx,1:Nppt_mx,1:Ndim_mx)
      double precision area
c     temp
      integer Nv,Nf
      double precision vertices(1:Nv_so_mx,1:Ndim_mx)
      integer faces(1:Nf_so_mx,1:3)
      integer iface,j
      double precision face_area
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine contour_area2'

c     triangulate the surface
      call delaunay_triangulation(dim,Ncontour,Npoints,points,
     &     Nv,Nf,vertices,faces)
c     compute the area
      area=0.0D+0
      do iface=1,Nf
         do j=1,dim
            x1(j)=vertices(faces(iface,1),j)
            x2(j)=vertices(faces(iface,2),j)
            x3(j)=vertices(faces(iface,3),j)
         enddo                  ! j
         call triangle_area(dim,x1,x2,x3,face_area)
         area=area+face_area
      enddo                     ! iface
      
      return
      end
