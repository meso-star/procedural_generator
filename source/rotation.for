c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine rotation_matrix(dim,alpha,axe,M)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the rotation matrix of rotation
c     by angle "alpha" around a specified axe
c
c     Inputs:
c       + dim: dimension of vectors
c       + alpha: rotation angle
c       + axe: (ax,ay,az) coordinates of the elementary vector definig the direction of the rotation axe
c     
c     Outputs:
c       + M: rotation matrix
c
c     I/O
      integer dim
      double precision alpha
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
c     temp
      double precision scalar
      double precision naxe(1:Ndim_mx)
      double precision I3(1:Ndim_mx,1:Ndim_mx)
      double precision Maxe1(1:Ndim_mx,1:Ndim_mx)
      double precision Maxe2(1:Ndim_mx,1:Ndim_mx)
      double precision Mt1(1:Ndim_mx,1:Ndim_mx)
      double precision Mt2(1:Ndim_mx,1:Ndim_mx)
      double precision Mt3(1:Ndim_mx,1:Ndim_mx)
      double precision Mt4(1:Ndim_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine rotation_matrix'

c     checking inputs are understood
      call normalize_vector(dim,axe,naxe)
      call axe_matrix(naxe,Maxe1,Maxe2)
c     computation of rotation matrix M
      call identity_matrix(dim,I3)
      scalar=dcos(alpha)
      call matrix_scalar(dim,I3,scalar,Mt1)
      scalar=1.0D+0-dcos(alpha)
      call matrix_scalar(dim,Maxe1,scalar,Mt2)
      scalar=dsin(alpha)
      call matrix_scalar(dim,Maxe2,scalar,Mt3)
      call add_matrix(dim,Mt1,Mt2,Mt4)
      call add_matrix(dim,Mt4,Mt3,M)

      return
      end



      subroutine axe_matrix(axe,Maxe1,Maxe2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute axe matrices "Maxe1" and "Maxe2"
c
c     Input:
c       + axe: coordinates of the rotation axe
c
c     Output:
c       + Maxe1, Maxe2: matrices used to compute the 3D rotation matrix
c
c     I/O
      double precision axe(1:Ndim_mx)
      double precision Maxe1(1:Ndim_mx,1:Ndim_mx)
      double precision Maxe2(1:Ndim_mx,1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine axe_matrix'

      Maxe1(1,1)=axe(1)**2.0D+0
      Maxe1(1,2)=axe(1)*axe(2)
      Maxe1(1,3)=axe(1)*axe(3)
      Maxe1(2,1)=axe(1)*axe(2)
      Maxe1(2,2)=axe(2)**2.0D+0
      Maxe1(2,3)=axe(2)*axe(3)
      Maxe1(3,1)=axe(1)*axe(3)
      Maxe1(3,2)=axe(2)*axe(3)
      Maxe1(3,3)=axe(3)**2.0D+0

      Maxe2(1,1)=0.0D+0
      Maxe2(1,2)=-axe(3)
      Maxe2(1,3)=axe(2)
      Maxe2(2,1)=axe(3)
      Maxe2(2,2)=0.0D+0
      Maxe2(2,3)=-axe(1)
      Maxe2(3,1)=-axe(2)
      Maxe2(3,2)=axe(1)
      Maxe2(3,3)=0.0D+0

      return
      end
      
