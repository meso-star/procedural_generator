c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine tetris1(dim,mapXsize,mapYsize,
     &     Nsector,alpha1,xcenter,ilevel,isector,
     &     Ncontour,Nppc,contour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'constants.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to subdivide a level 1 or level 2 sector
c     into a number of building contours
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Nsector: number of angular sectors
c       + alpha1: angle for generating level1 ring road contour
c       + xcenter: position of the ring center
c       + ilevel: level index [1,2]
c       + isector: index of the sector
c       + Ncontour: number of contours (only one at input time)
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     Output:
c       + Ncontour: number of contours (updated)
c       + Nppc: number of points that define each contour (updated)
c       + contour: list of coordinates for each contour (updated)
c       + on_left_side: true if the footprint sticks to the left side of contour 1
c       + on_right_side: true if the footprint sticks to the right side of contour 1
c       + on_bottom_side: true if the footprint sticks to the bottom side of contour 1
c       + on_top_side: true if the footprint sticks to the top side of contour 1
c       + contour_type: type of contour
c         - 0: surrounding contour (contour index 1)
c         - 1: building only
c         - 2: terrain + building
c         - 3: terrain only
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Nsector
      double precision alpha1
      double precision xcenter(1:Ndim_mx)
      integer ilevel
      integer isector
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer contour_type(1:Ncontour_mx)
c     temp
      logical debug,clockwise
      integer i,j,ifootprint
      double precision theta
      double precision ux(1:Ndim_mx)
      double precision uy(1:Ndim_mx)
      double precision uz(1:Ndim_mx)
      double precision M10(1:Ndim_mx,1:Ndim_mx)
      double precision M01(1:Ndim_mx,1:Ndim_mx)
      double precision x_R0(1:Ndim_mx)
      double precision x_R1(1:Ndim_mx)
      double precision contour_R1(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      double precision x(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P23(1:Ndim_mx)
      double precision P34(1:Ndim_mx)
      double precision P41(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision u23(1:Ndim_mx)
      double precision u32(1:Ndim_mx)
      double precision u34(1:Ndim_mx)
      double precision u41(1:Ndim_mx)
      double precision di(1:Ndim_mx)
      double precision dj(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision x3(1:Ndim_mx)
      double precision d23,d
      integer Np,Niter
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer icontour,isegment,irsegment
      integer icontour_below
      integer isegment_below
      integer icontour_right
      integer isegment_right
      logical keep_going,ok
      logical is_footprint_below
      double precision deltaX,deltaY,distX,distY
      double precision remaining_deltaX
      double precision remaining_deltaY
      double precision width
      double precision dx,dy
      double precision dx_bottom,dy_bottom
      double precision theta_top,dx_top,dy_top
      double precision theta_left,dx_left,dy_left
      double precision theta_right,dx_right,dy_right
      double precision xmin,xmax,ymin,ymax,segment_lastx
      logical is_bf,top_reached,inside,inside2
      integer ibf,illf
      character*3 str3
      character*(Nchar_mx) filename
      double precision sp,dlength
      integer imax
      logical keep_searching
      logical x0_found
      integer idx_c1,idx_c2
      logical try,accepted
      double precision r
      integer Nremoved,Ntry
      logical generate_sector_plot
      double precision initialx
c     functions
      double precision arctan
c     label
      character*(Nchar_mx) label
      label='subroutine tetris1'

      Ntry=0
 888  continue
      generate_sector_plot=.true.
c     ux
      ux(1)=1.0D+0
      ux(2)=0.0D+0
      ux(3)=0.0D+0
c     uy
      uy(1)=0.0D+0
      uy(2)=1.0D+0
      uy(3)=0.0D+0
c     uz
      uz(1)=0.0D+0
      uz(2)=0.0D+0
      uz(3)=1.0D+0

      if (Ncontour.ne.1) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'that seems awkward'
         stop
      endif
      if (Nppc(1).ne.4) then
         call error(label)
         write(*,*) 'Npcc(1)=',Nppc(1)
         write(*,*) 'should be equal to 5'
         stop
      endif
      if (contour(1,1,1)-contour(1,4,1).eq.0.0D+0) then
         if (contour(1,1,2).lt.contour(1,4,2)) then
            theta=pi/2.0D+0
         else
            theta=-pi/2.0D+0
         endif
      else
         dx=contour(1,1,1)-contour(1,4,1)
         dy=contour(1,4,2)-contour(1,1,2)
         theta=arctan(dx,dy)
      endif
      call rotation_matrix(dim,theta,uz,M10)
      call invert_3x3_matrix(dim,M10,M01)

c     rotate the contour of the sector in order to work in local referential
      do i=1,Nppc(1)
         do j=1,dim-1
            x_R0(j)=contour(1,i,j)
         enddo                  ! j
         x_R0(dim)=0.0D+0
         call matrix_vector(dim,M10,x_R0,x_R1)
         do j=1,dim-1
            contour_R1(1,i,j)=x_R1(j)
         enddo                  ! j
      enddo
      
c     contour points and directions
      do j=1,dim-1
         P1(j)=contour_R1(1,1,j)
         P2(j)=contour_R1(1,2,j)
         P3(j)=contour_R1(1,3,j)
         P4(j)=contour_R1(1,4,j)
      enddo                     ! j
      P1(dim)=0.0D+0
      P2(dim)=0.0D+0
      P3(dim)=0.0D+0
      P4(dim)=0.0D+0
      call substract_vectors(dim,P2,P1,P12)
      call substract_vectors(dim,P3,P2,P23)
      call substract_vectors(dim,P4,P3,P34)
      call substract_vectors(dim,P1,P4,P41)
      call normalize_vector(dim,P12,u12)
      call normalize_vector(dim,P23,u23)
      call normalize_vector(dim,P34,u34)
      call normalize_vector(dim,P41,u41)
      call scalar_vector(dim,-1.0D+0,u23,u32)
c     bottom borders
      dy_bottom=pavement_width
c     top borders
      theta_top=dabs(arctan(contour_R1(1,2,1)-contour_R1(1,3,1),
     &     contour_R1(1,2,2)-contour_R1(1,3,2)))
      if (theta_top.ne.0.0D+0) then
         dx_top=dabs(pavement_width/dsin(theta_top))
      endif
      dy_top=dabs(pavement_width/dcos(theta_top))
c     left borders
      theta_left=dabs(arctan(contour_R1(1,3,2)-contour_R1(1,4,2),
     &     contour_R1(1,4,1)-contour_R1(1,3,1)))
      dx_left=dabs(pavement_width/dcos(theta_left))
      if (theta_left.ne.0.0D+0) then
         dy_left=dabs(pavement_width/dsin(theta_left))
      endif
c     right borders
      theta_right=dabs(arctan(contour_R1(1,2,2)-contour_R1(1,1,2),
     &     contour_R1(1,2,1)-contour_R1(1,1,1)))
      dx_right=dabs(pavement_width/dcos(theta_right))
      if (theta_right.ne.0.0D+0) then
         dy_right=dabs(pavement_width/dsin(theta_right))
      endif
c     Set the initial position
      x0(1)=P4(1)+dx_left
      x0(2)=P4(2)+dy_bottom
      x0(3)=0.0D+0
c     Debug
c      write(*,*) 'P4=',P4
c      write(*,*) 'x0=',x0
c     Debug
      call iis_left(dim,
     &     Ncontour,Nppc,contour_R1,x0,
     &     xint,d2int,icontour,isegment)
      x0(1)=xint(1)+dx_left
      x0(2)=xint(2)
      x0(3)=0.0D+0
c     initialization of the main loop
      on_bottom_side(2)=.true.
      on_top_side(2)=.false.
      on_left_side(2)=.true.
      on_right_side(2)=.false.
      top_reached=.false.
      keep_going=.true.
c
      do while (keep_going)
c     Initialization of the footprint's contour
         ifootprint=Ncontour+1
         initialx=0.0D+0
c     Debug
         if (ifootprint.eq.12) then
            debug=.true.
         else
            debug=.false.
         endif
         write(*,*) 'ifootprint=',ifootprint
c     Debug

c     Debug
         if (debug) then
            write(*,*) 'Starting all over for ifootprint=',ifootprint
         endif
c     Debug
         Np=0
         if (ifootprint.gt.2) then
            call rightmost_segment(dim,
     &           Ncontour,Nppc,contour_R1,ifootprint-1,
     &           irsegment)
c     Debug
            if (debug) then
               write(*,*) 'dx_left=',dx_left
            endif
c     Debug
c     Debug
            if (debug) then
               write(*,*) '---------------------------------------'
               write(*,*) 'Index of the rightmost segment'
               write(*,*) 'for footprint index ',ifootprint-1
               write(*,*) 'irsegment=',irsegment
               write(*,*) '---------------------------------------'
            endif
c     Debug
            x(1)=contour_R1(ifootprint-1,irsegment,1)+1.0D-5
            x(2)=contour_R1(ifootprint-1,irsegment,2)
            x(3)=0.0D+0
            call iis_right(dim,
     &           Ncontour,Nppc,contour_R1,x,
     &           xint,d2int,icontour,isegment)
c     Debug
            if (debug) then
               write(*,*) '---------------------------------------'
               write(*,*) 'on_right_side(',ifootprint-1,')=',
     &              on_right_side(ifootprint-1)
               write(*,*) 'top_reached=',top_reached
               write(*,*) 'Intersection to the right from x=',x
               write(*,*) 'xint=',xint
               write(*,*) 'd2int=',d2int
               write(*,*) 'intersected contour:',icontour
               write(*,*) 'intersected segment:',isegment
               write(*,*) 'small_street_width+dx_right=',
     &              small_street_width+dx_right
               write(*,*) '---------------------------------------'
            endif
c     Debug
c     special case
            if ((.not.top_reached).and.((on_right_side(ifootprint-1))
     &           .or.(d2int.le.small_street_width+dx_right))) then
c     Debug
               if (debug) then
                  write(*,*) 'New footprint sticks to the left'
               endif
c     Debug
               on_left_side(ifootprint)=.true.
               call last_footprint_onleft(dim,
     &              Ncontour,Nppc,contour_R1,on_left_side,
     &              illf)
c     Debug
               if (debug) then
                  write(*,*) 'retrieving last footprint on left:'
                  write(*,*) 'illf=',illf
               endif
c     Debug
               x0(1)=contour_R1(illf,Nppc(illf),1)
               x0(2)=contour_R1(illf,Nppc(illf),2)+small_street_width
               x0(3)=0.0D+0
c     Debug
               if (debug) then
                  write(*,*) 'using x0=',x0
               endif
c     Debug
               call iis_left(dim,
     &              Ncontour,Nppc,contour_R1,x0,
     &              xint,d2int,icontour,isegment)
               if (icontour.ne.1) then
                  call error(label)
                  write(*,*) 'During footprint initialization'
                  write(*,*) 'intersected contour=',icontour
                  write(*,*) 'expected: 1'
                  stop
               endif
               x0(1)=xint(1)+dx_left
c     Debug
               if (debug) then
                  write(*,*) '---------------------------------------'
                  write(*,*) 'Position for identification of bottom'
                  write(*,*) 'footprint: using x0=',x0
                  write(*,*) '---------------------------------------'
               endif
c     Debug
            else                ! on_right_side(ifootprint-1)=F
c     Debug
               if (debug) then
                  write(*,*) 'New footprint does NOT stick to the left'
               endif
c     Debug
               on_left_side(ifootprint)=.false.
               x0(1)=contour_R1(ifootprint-1,irsegment,1)
     &              +small_street_width
               x0(2)=contour_R1(ifootprint-1,irsegment,2)
               x0(3)=0.0D+0
               call is_inside_contour(.false.,dim,
     &              Ncontour,Nppc,contour_R1,
     &              1,x0,inside)
c     Debug
               if (debug) then
                  write(*,*) '---------------------------------------'
                  write(*,*) 'Position for identification of bottom'
                  write(*,*) 'footprint: using'
                  write(*,*) 'previous footprint:',ifootprint-1
                  write(*,*) 'point index:',irsegment
                  write(*,*) 'position:',
     &                 contour_R1(ifootprint-1,irsegment,1)
                  write(*,*) 'x0=',x0
                  write(*,*) 'inside contour 1:',inside
                  write(*,*) '---------------------------------------'
               endif
c     Debug
c
 123           continue
               if (.not.inside) then
                  try=.false.
                  idx_c1=1
c     Debug
                  if (debug) then
                     write(*,*) '----------------------------------'
                     write(*,*) 'x0 is not inside contour 1'
                     write(*,*) 'trying new localization of initial'
                     write(*,*) 'position for using in'
                     write(*,*) 'identify_bottom_footprint'
                  endif
c     Debug
                  x0_found=.false.
                  d=10.0D+0
                  call vector_length(dim,P23,d23)
c     Debug
                  if (debug) then
                     write(*,*) '####################################'
                     write(*,*) 'd23=',d23
                     write(*,*) '####################################'
                  endif
c     Debug
                  keep_searching=.true.
                  do while (keep_searching)
c     Debug
                     if (debug) then
                        write(*,*) 'initialx=',initialx
                        write(*,*) 'initialx+d=',initialx+d
                     endif
c     Debug
                     call scalar_vector(dim,initialx+d,u32,tmp)
                     call add_vectors(dim,P3,tmp,x)
                     x(2)=x(2)-1.0D+0
c     Debug
                     if (debug) then
                        write(*,*) 'x=',x
                     endif
c     Debug
                     call is_inside_contour(.false.,dim,
     &                    Ncontour,Nppc,contour_R1,
     &                    1,x,inside)
                     if (.not.inside) then
                        call error(label)
                        write(*,*) 'x=',x
                        write(*,*) 'is not inside contour 1'
                        do i=1,Nppc(1)
                           write(*,*) (contour_R1(1,i,j),j=1,dim-1)
                        enddo   ! i
                        stop
                     endif
c     Debug
                     if (debug) then
                        write(*,*) 'x=',x,' is inside contour 1'
                     endif
c     Debug
                     call iis_bottom(dim,
     &                    Ncontour,Nppc,contour_R1,x,
     &                    xint,d2int,icontour,isegment)
c     Debug
                     if (debug) then
                        write(*,*) 'trying for d=',d,' d23=',d23
                        write(*,*) 'x=',x
                        write(*,*) 'icontour=',icontour
                        write(*,*) 'd2int=',d2int
                        if (d2int.lt.dy_top+2.0D+0*small_street_width)
     &                       then
                           write(*,*) 'too small'
                        else
                           write(*,*) 'ok, next step'
                        endif
                     endif
c     Debug
c     
                     idx_c2=icontour
c     Debug
                     if (debug) then
                        write(*,*) 'idx_c1=',idx_c1,' idx_c2=',idx_c2
                     endif
c     Debug
                     if ((idx_c1.eq.idx_c2).and.(idx_c1.gt.1)) then
                        try=.true.
                     else
                        idx_c1=icontour
                        try=.false.
                     endif
c     Debug
                     if (debug) then
                        write(*,*) 'try=',try
                        if (.not.try) then
                           write(*,*) 'position rejected'
                        endif
                     endif
c     Debug
c                    
                     if ((try).and.(d2int.gt.
     &                    dy_top+2.0D+0*small_street_width)) then
                        call copy_vector(dim,xint,x)
                        x(2)=x(2)+small_street_width+0.1D+0
                        call iis_left(dim,
     &                       Ncontour,Nppc,contour_R1,x,
     &                       xint,d2int,icontour,isegment)
c     Debug
                        if (debug) then
                           write(*,*) 'intersection to the right'
                           write(*,*) 'from x=',x
                           write(*,*) 'icontour=',icontour
                        endif
c     Debug
                        if (icontour.eq.1) then
                           goto 112
                        endif
                        call copy_vector(dim,xint,x0)
                        x0(1)=x0(1)+small_street_width
                        x0_found=.true.
                        call is_inside_contour(.false.,dim,
     &                       Ncontour,Nppc,contour_R1,
     &                       1,x0,inside)
                        if (.not.inside) then
                           call error(label)
                           write(*,*) 'candidate x0=',x0
                           write(*,*) 'is not inside contour 1'
                           stop
                        endif
c     Debug
                        if (debug) then
                           write(*,*) 'x0 was identified:'
                           write(*,*)'x0=',x0
                           write(*,*) 'x0_found=',x0_found
                        endif
c     Debug
                        keep_searching=.false.
                        goto 333
                     endif      ! try AND d2int big enough
 112                 continue
c     Debug
                     if (debug) then
                        write(*,*) 'd23=',d23
                        write(*,*) 'dx_top=',dx_top
                     endif
c     Debug
                     d=d+small_street_width+0.2D+0
                     if (initialx+d.ge.d23-small_street_width) then
                        keep_searching=.false.
                     endif
c     Debug
                     if (debug) then
                        write(*,*) 'd updated to: d=',d,
     &                       ' while d23-small_street_width=',
     &                       d23-small_street_width
                        write(*,*) 'keep_searching=',keep_searching
                     endif
c     Debug
 333                 continue
                  enddo         ! while (keep_searching)
                  if (.not.x0_found) then
c     Debug
                     if (debug) then
                        write(*,*) 'x0 not found, giving up'
                     endif
c     Debug
                     keep_going=.false.
                     goto 666
                  else
                     initialx=initialx+d
                  endif         ! x0_found=F
c     
               endif            ! inside=F
c     
            endif               ! on_right_side(ifootprint-1)
c            
c     Debug
            if (debug) then
               write(*,*) 'Now using identify_bottom_footprint'
               write(*,*) 'with x0=',x0
            endif
c     Debug
            call identify_bottom_footprint(debug,dim,
     &           Ncontour,Nppc,contour_R1,
     &           on_left_side,x0,
     &           is_bf,ibf)
c     Debug
            if (debug) then
               write(*,*) '---------------------------------------'
               write(*,*) 'is_bf=',is_bf
               if (is_bf) then
                  write(*,*) 'ibf=',ibf
               endif
               write(*,*) '---------------------------------------'
            endif
c     Debug
            if (is_bf) then
               on_bottom_side(ifootprint)=.false.
            else
               on_bottom_side(ifootprint)=.true.
            endif               ! is_bf
            on_right_side(ifootprint)=.false.
            on_top_side(ifootprint)=.false.
            if (is_bf) then
c     Debug
               if (debug) then
                  write(*,*) '---------------------------------------'
                  write(*,*) 'Trying to set x0(2):'
                  write(*,*) 'contour_R1(',ibf,',',Nppc(ibf),',2)=',
     &                 contour_R1(ibf,Nppc(ibf),2)
                  write(*,*) '---------------------------------------'
               endif
c     Debug
               call copy_vector(dim,x0,x)
               x(2)=contour_R1(ibf,Nppc(ibf),2)+small_street_width
               call is_inside_contour(.false.,dim,
     &              Ncontour,Nppc,contour_R1,1,x,inside)
               if (inside) then
                  x0(2)=contour_R1(ibf,Nppc(ibf),2)+small_street_width
               else
                  Niter=0
                  inside2=.false.
                  do while (.not.inside2)
                     x(2)=x(2)-small_street_width/10.0D+0
                     call is_inside_contour(.false.,dim,
     &                    Ncontour,Nppc,contour_R1,1,x,inside2)
                     Niter=Niter+1
                     if (Niter.gt.Niter_mx) then
                        call error(label)
                        write(*,*) 'Niter=',Niter
                        write(*,*) '> Niter_mx=',Niter_mx
                        write(*,*) 'x=',x
                        stop
                     endif
                  enddo         ! while (.not.inside2)
                  x0(2)=x(2)
               endif            ! inside
            endif               ! is_bf
            call iis_top(dim,
     &           Ncontour,Nppc,contour_R1,x0,
     &           xint,d2int,icontour,isegment)
c     Debug
            if (debug) then
               write(*,*) 'iis_top result:'
               write(*,*) 'x0=',x0
               write(*,*) 'd2int=',d2int
               write(*,*) 'icontour=',icontour,' isegment=',isegment
               do i=1,Nppc(icontour)
                  write(*,*) (contour_R1(icontour,i,j),j=1,dim-1)
               enddo            ! i
            endif
c     Debug
            if (d2int.lt.small_street_width) then
               inside=.false.
c     Debug
               if (debug) then
                  write(*,*) 'x0 is too close to the top boundary'
               endif
c     Debug
               goto 123
            endif
c     Debug
            if (debug) then
               write(*,*) '---------------------------------------'
               write(*,*) 'on_left_side=',on_left_side(ifootprint)
               write(*,*) 'on_right_side=',on_right_side(ifootprint)
               write(*,*) 'on_bottom_side=',on_bottom_side(ifootprint)
               write(*,*) 'on_top_side=',on_top_side(ifootprint)
               write(*,*) 'First position: x0=',x0
               write(*,*) '---------------------------------------'
            endif
c     Debug
         endif                  ! ifootprint > 2
c     initialize current contour
         call add_point_to_ctr(dim,x0,Np,ctr)
c     Debug
         if (debug) then
            write(*,*) '---------------------------------------'
            write(*,*) 'Adding to ctr:'
            write(*,*) 'ctr(',Np,')=',(ctr(Np,j),j=1,dim-1)
            write(*,*) '---------------------------------------'
         endif
c     Debug
c     x0 is the position of the first horizontal segment
c     identification of the end of this segment
         call iis_right(dim,
     &        Ncontour,Nppc,contour_R1,x0,
     &        xint,d2int,icontour,isegment)
c     Debug
         if (debug) then
            write(*,*) '---------------------------------------'
            write(*,*) 'Intersection to the right from x0:'
            write(*,*) 'd2int=',d2int
            write(*,*) 'intersected contour:',icontour
            write(*,*) 'intersected segment:',isegment
            write(*,*) '---------------------------------------'
         endif
c     Debug
c     randomly generate footprint dimensions
c     generate values of deltaX that are not too close to the intersection
         call sample_normal(mu_footprint_deltaX,
     &        sigma_footprint_deltaX,deltaX)
         if (deltaX+4.0D+0.gt.d2int) then
            deltaX=d2int+3.0D+0
         endif
c     Debug
         if (debug) then
            write(*,*) 'deltaX=',deltaX,' d2int=',d2int
         endif
c     Debug
c     sample deltaY
         call sample_normal(mu_footprint_deltaY,
     &        sigma_footprint_deltaY,deltaY)
c         
         if (icontour.eq.1) then
            width=dx_right
         else
            width=small_street_width
         endif
         if (on_bottom_side(ifootprint)) then
c     Debug
            if (debug) then
               write(*,*) '---------------------------------------'
               write(*,*) 'current footprint is sticking at the bottom'
            endif
c     Debug
            if (deltaX.lt.d2int-width) then
               x0(1)=x0(1)+deltaX
c     Debug
               if (debug) then
                  write(*,*) 'x0(1) was updated to x0(1)+deltaX'
                  write(*,*) 'x0(1)=',x0(1)
                  write(*,*) '---------------------------------------'
               endif
c     Debug
            else
               x0(1)=xint(1)-dx_right
               on_right_side(ifootprint)=.true.
c     Debug
               if (debug) then
                  write(*,*) 'x0(1) was updated to xint(1)-dx_right'
                  write(*,*) 'x0(1)=',x0(1)
                  write(*,*) 'and on_right_side(',ifootprint,')=',
     &                 on_right_side(ifootprint)
                  write(*,*) '---------------------------------------'
               endif
c     Debug
            endif
         else                   ! on_bottom_side(ifootprint)=F
c     Debug
            if (debug) then
               write(*,*) 'Beginning of the loop for identifying'
               write(*,*) 'the end of horizontal travel'
            endif
c     Debug
            remaining_deltaX=deltaX
            ok=.true.
            do while (ok)
c     Debug
               if (debug) then
                  write(*,*) 'remaining_deltaX=',remaining_deltaX
                  write(*,*) 'is_bf=',is_bf
                  if (.not.is_bf) then
                     write(*,*) 'this should not happen'
                  endif
               endif
c     Debug
               if (.not.is_bf) then
                  call error(label)
                  write(*,*) 'is_bf=',is_bf
                  stop
               endif
               call iis_right(dim,
     &              Ncontour,Nppc,contour_R1,x0,
     &              xint,d2int,icontour,isegment)
               segment_lastx=contour_R1(ibf,Nppc(ibf)-1,1)
c     Debug
               if (debug) then
                  write(*,*) '---------------------------------------'
                  write(*,*) 'intersection to the right from x0=',x0
                  write(*,*) 'xint=',xint
                  write(*,*) 'intersected contour=',icontour
                  write(*,*) 'intersected segment=',isegment
                  write(*,*) 'end of segment right below:'
                  write(*,*) 'contour_R1(',ibf,',',Nppc(ibf)-1,',1)=',
     &                 segment_lastx
                  write(*,*) '---------------------------------------'
               endif
c     Debug
c               
               if (x0(1)+remaining_deltaX.lt.segment_lastx) then
c     Debug
                  if (debug) then
                     write(*,*) '-------------------------------------'
                     write(*,*) 'x0(1) is updated because'
                     write(*,*) 'remaining_deltaX is insufficient'
                     write(*,*) 'x0=',x0
                     write(*,*) '-------------------------------------'
                  endif
c     Debug
                  x0(1)=x0(1)+remaining_deltaX
                  remaining_deltaX=0.0D+0
                  ok=.false.
                  goto 111
               else
                  remaining_deltaX=remaining_deltaX-
     &                 (segment_lastx-x0(1))
                  x0(1)=segment_lastx
c     Debug
                  if (debug) then
                     write(*,*) '-------------------------------------'
                     write(*,*) 'x0(1) is set at the end of'
                     write(*,*) 'below segment, x0=',x0
                     write(*,*) '-------------------------------------'
                  endif
c     Debug
               endif
               call iis_right(dim,
     &              Ncontour,Nppc,contour_R1,x0,
     &              xint,d2int,icontour,isegment)
               if (icontour.eq.1) then
                  width=dx_right
               else
                  width=small_street_width
               endif
c     Debug
               if (debug) then
                  write(*,*) 'Intersection to the right from x0=',x0
                  write(*,*) 'xint=',xint,' icontour=',icontour
                  write(*,*) 'd2int=',d2int,' ssw+dw_right=',
     &                 small_street_width+dx_right
               endif
c     Debug
c     special case when current x0 arrives close to the right side
               if ((icontour.eq.1).and.
     &              (d2int.lt.small_street_width+dx_right)) then
                  x0(1)=x0(1)+d2int-dx_right
                  on_right_side(ifootprint)=.true.
c     Debug
                  if (debug) then
                     write(*,*) 'Insufficient d2int=',d2int
                     write(*,*) '+ intersected contour=',icontour
                     write(*,*) '=> sticking position to x0=',x0
                  endif
c     Debug
                  ok=.false.
                  goto 111
               endif
               
               if ((d2int.gt.small_street_width).and.
     &              (remaining_deltaX.gt.3.0D+0*small_street_width))
     &              then
                  x(1)=x0(1)+small_street_width
                  x(2)=x0(2)
                  x(3)=0.0D+0
c     Debug
                  if (debug) then
                     write(*,*) 'remaining_deltaX is sufficient'
                     write(*,*) 'x0(1)+=small_street_width=',x0(1)
                  endif
c     Debug
                  call identify_bottom_footprint(debug,dim,
     &                 Ncontour,Nppc,contour_R1,
     &                 on_left_side,x,
     &                 is_bf,ibf)
c     Debug
                  if (debug) then
                     write(*,*) '-------------------------------------'
                     write(*,*) 'Identification of bottom footprint:'
                     write(*,*) 'using x=',x
                     write(*,*) 'is_bf=',is_bf
                     if (is_bf) then
                        write(*,*) 'ibf=',ibf
                     endif
                     write(*,*) 'contour_R1(',ibf,',',
     &                    Nppc(ibf),',2)=',
     &                    contour_R1(ibf,Nppc(ibf),2)
                     write(*,*) '-------------------------------------'
                  endif
c     Debug
                  if ((.not.is_bf).or.((is_bf).and.
     &                 (contour_R1(ibf,Nppc(ibf),2).lt.
     &                 x(2)-small_street_width))) then
                     call copy_vector(dim,x,x0)
                     remaining_deltaX=remaining_deltaX-
     &                    small_street_width
                     call add_point_to_ctr(dim,x0,Np,ctr)
c     Debug
                     if (debug) then
                        write(*,*) 'adding to ctr:'
                        write(*,*) 'ctr(',Np,')=',(ctr(Np,j),j=1,dim-1)
                     endif
c     Debug
                     x(1)=x(1)+1.0D-1
                     call iis_bottom(dim,
     &                    Ncontour,Nppc,contour_R1,x,
     &                    xint,d2int,icontour,isegment)
c     Debug
                     if (debug) then
                        write(*,*) 'Intersection below from x=',x
                        write(*,*) 'intersected contour:',icontour
                        write(*,*) 'intersected segment:',isegment
                     endif
c     Debug
                     if ((.not.is_bf).and.(icontour.ne.1)) then
                        call error(label)
                        write(*,*) 'is_bf=',is_bf
                        write(*,*) 'while icontour=',icontour
                        stop
                     endif
                     if ((is_bf).and.(ibf.ne.icontour)) then
                        call error(label)
                        write(*,*) 'is_bf=',is_bf
                        write(*,*) 'while icontour=',icontour
                        stop
                     endif
                     if (icontour.eq.1) then
                        x0(2)=xint(2)+dy_bottom
                        ok=.false.
                        on_right_side(ifootprint)=.true.
c     Debug
                        if (debug) then
                           write(*,*) 'x0(2) updated above contour 1'
                           write(*,*) 'x0=',x0
                        endif
c     Debug
                     else
                        x0(2)=contour_R1(icontour,Nppc(icontour),2)
     &                       +small_street_width
c     Debug
                        if (debug) then
                           write(*,*) 'x0(2) updated above contour:',
     c                          icontour
                           write(*,*) 'x0=',x0
                        endif
c     Debug
                     endif      ! icontour
                     call add_point_to_ctr(dim,x0,Np,ctr)
c     Debug
                     if (debug) then
                        write(*,*) 'adding to ctr:'
                        write(*,*) 'ctr(',Np,')=',(ctr(Np,j),j=1,dim-1)
                     endif
c     Debug
                  endif
               else             ! d2int<=small_street_width OR remaining_deltaX<3*small_street_width
                  ok=.false.
               endif            ! (d2int>small_street_width).AND.(remaining_deltaX>3*small_street_width)
 111           continue
            enddo               ! while (ok)
c     Debug
            if (debug) then
               write(*,*) '---------------------------------------'
               write(*,*) 'End of loop for horizontal travel'
               write(*,*) '---------------------------------------'
            endif
c     Debug
         endif                  ! on_bottom_side
         call add_point_to_ctr(dim,x0,Np,ctr) ! end of the first segment
c     Debug
         if (debug) then
            write(*,*) 'adding to ctr:'
            write(*,*) 'ctr(',Np,')=',(ctr(Np,j),j=1,dim-1)
            write(*,*) 'on_right_side(',ifootprint,')=',
     &           on_right_side(ifootprint)
         endif
c     Debug
c     Adding to deltaY the Y-differential from the horizontal walk
         if (ctr(Np,2).lt.ctr(1,2)) then
            deltaY=deltaY+dabs(ctr(1,2)-ctr(Np,2))
c     Debug
            if (debug) then
               write(*,*) 'deltaY was increased by: ',
     &              dabs(ctr(1,2)-ctr(Np,2))
               write(*,*) 'deltaY=',deltaY
            endif
c     Debug
         endif

         
         if (on_right_side(ifootprint)) then
            call segment_line_intersection(dim,P3,P2,x0,u12,
     &           .true.,intersection_found,xint,d2int)
            if (.not.intersection_found) then
               call error(label)
               write(*,*) 'No intersection found'
               stop
            endif
            call scalar_product(dim,u12,u23,sp)
            dlength=pavement_width/dcos(pi/2.0D+0-dacos(dabs(sp)))
c     Debug
            if (debug) then
               write(*,*) 'footprint is on the right'
               write(*,*) 'flight distance:'
            endif
c     Debug
            if (deltaY/dcos(theta_right).gt.d2int-dlength) then
               call scalar_vector(dim,d2int-dlength,u12,tmp)
               on_top_side(ifootprint)=.true.
c     Debug
               if (debug) then
                  write(*,*) d2int-dlength
                  write(*,*) 'and footprint sticks to the top'
               endif
c     Debug
            else
c     Debug
               if (debug) then
                  write(*,*) deltaY/dcos(theta_right)
               endif
c     Debug
               call scalar_vector(dim,deltaY/dcos(theta_right),u12,tmp)
            endif
            call add_vectors(dim,x0,tmp,x0)
         else                   ! on_right_side(ifootprint)=F
c     Debug
            if (debug) then
               write(*,*) 'footprint is NOT on the right'
            endif
c     Debug
            call iis_top(dim,
     &           Ncontour,Nppc,contour_R1,x0,
     &           xint,d2int,icontour,isegment)
c     Debug
            if (debug) then
               write(*,*) 'intersection to the top:'
               write(*,*) 'xint=',xint
               write(*,*) 'd2int=',d2int
               write(*,*) 'deltaY=',deltaY
            endif
c     Debug
            if (icontour.eq.1) then
               width=dy_top
            else
               width=small_street_width
            endif
            if (deltaY.lt.d2int-width) then
               x0(2)=x0(2)+deltaY
c     Debug
               if (debug) then
                  write(*,*) 'traveling deltaY=',deltaY
                  write(*,*) 'x0=',x0
               endif
c     Debug
            else
               x0(2)=xint(2)-dy_top
               on_top_side(ifootprint)=.true.
c     Debug
               if (debug) then
                  write(*,*) 'traveling to the top:'
                  write(*,*) 'x0=',x0
                  write(*,*) 'and footprint sticks to the top'
               endif
c     Debug
            endif
         endif                  ! on_right_side(ifootprint)
         call add_point_to_ctr(dim,x0,Np,ctr)
c     Debug
         if (debug) then
            write(*,*) 'adding to ctr:'
            write(*,*) 'ctr(',Np,')=',(ctr(Np,j),j=1,dim-1)
            write(*,*) 'on_top_side(',ifootprint,')=',
     &           on_top_side(ifootprint)
         endif
c     Debug
         if (on_top_side(ifootprint)) then
c     Debug
            if (debug) then
               write(*,*) 'footprint sticks at the top'
            endif
c     Debug
            if (on_left_side(ifootprint)) then
c     Debug
               if (debug) then
                  write(*,*) 'footprint sticks at the left'
               endif
c     Debug
               call segment_line_intersection(dim,P3,P4,x0,u23,
     &              .true.,intersection_found,xint,d2int)
               if (.not.intersection_found) then
                  call error(label)
                  write(*,*) 'No intersection found'
                  stop
               endif
c     Debug
               if (debug) then
                  write(*,*) 'intersection_found=',intersection_found
                  write(*,*) 'xint=',xint
                  write(*,*) 'd2int=',d2int
               endif
c     Debug
               call scalar_product(dim,u23,u34,sp)
               dlength=pavement_width/dcos(pi/2.0D+0-dacos(dabs(sp)))
               call scalar_vector(dim,d2int-dlength,u23,tmp)
               call add_vectors(dim,x0,tmp,x0)
c     Debug
               if (debug) then
                  write(*,*) 'travel length:',d2int-dlength
                  write(*,*) 'along u23=',u23
                  write(*,*) 'x0=',x0
               endif
c     Debug
               call add_point_to_ctr(dim,x0,Np,ctr)
c     Debug
               if (debug) then
                  write(*,*) 'adding to ctr:'
                  write(*,*) 'ctr(',Np,')=',(ctr(Np,j),j=1,dim-1)
               endif
c     Debug
c$$$               dlength=dabs(x0(2)-ctr(1,2))/dcos(theta_left)
c$$$               call scalar_vector(dim,dlength,u34,tmp)
c$$$               call add_vectors(dim,x0,tmp,x0)
c$$$c     Debug
c$$$               if (debug) then
c$$$                  write(*,*) 'travel length:',dlength
c$$$                  write(*,*) 'along u34=',u34
c$$$                  write(*,*) 'x0=',x0
c$$$               endif
c$$$c     Debug
            else                ! on_left_side(ifootprint)=F
c     Debug
               if (debug) then
                  write(*,*) 'footprint does NOT stick at the left'
                  write(*,*) 'next position sticks to the top'
               endif
c     Debug
               x(1)=ctr(1,1)
               x(2)=ctr(1,2)
               x(3)=0.0D+0
               call iis_top(dim,
     &              Ncontour,Nppc,contour_R1,x,
     &              xint,d2int,icontour,isegment)
               x0(1)=xint(1)
               x0(2)=xint(2)-dy_top
               x0(3)=0.0D+0
            endif               ! on_left_side(ifootprint)
         else                   ! on_top_side(ifootprint)=F
c     Debug
            if (debug) then
               write(*,*) 'footprint does NOT stick at the top'
            endif
c     Debug
            call iis_left(dim,
     &           Ncontour,Nppc,contour_R1,x0,
     &           xint,d2int,icontour,isegment)
            if (on_left_side(ifootprint)) then
c     Debug
               if (debug) then
                  write(*,*) 'footprint sticks at the left'
                  write(*,*) 'intersection to the left from x0=',x0
                  write(*,*) 'intersected contour:',icontour
                  write(*,*) 'intersected segment:',isegment
               endif
c     Debug
               if ((icontour.eq.1).and.(isegment.eq.2)) then
                  on_top_side(ifootprint)=.true.
                  x0(1)=xint(1)-dx_top
                  x0(2)=xint(2)-dy_top
                  call add_point_to_ctr(dim,x0,Np,ctr)
                  call segment_line_intersection(dim,P3,P4,x0,u23,
     &                 .true.,intersection_found,xint,d2int)
                  if (.not.intersection_found) then
                     call error(label)
                     write(*,*) 'No intersection found'
                     stop
                  endif
                  call scalar_product(dim,u23,u34,sp)
                  dlength=pavement_width/dcos(pi/2.0D+0-dacos(dabs(sp)))
                  call scalar_vector(dim,d2int-dlength,u23,tmp)
                  call add_vectors(dim,x0,tmp,x0)
               else if ((icontour.eq.1).and.(isegment.eq.3)) then
                  x0(1)=xint(1)+dx_left
               else
                  call error(label)
                  write(*,*) 'icontour=',icontour
                  write(*,*) 'isegment=',isegment
                  stop
               endif
            else                ! on_left_side(ifootprint)=F
c     Debug
               if (debug) then
                  write(*,*) 'footprint does NOT stick at the left'
               endif
c     Debug
               x0(1)=ctr(1,1)
            endif               ! on_left_side(ifootprint)
         endif                  ! on_top_side(ifootprint)
         call add_point_to_ctr(dim,x0,Np,ctr) ! last point of the footprint
c     Debug
         if (debug) then
            write(*,*) 'adding to ctr:'
            write(*,*) 'ctr(',Np,')=',(ctr(Np,j),j=1,dim-1)
         endif
c     Debug
c
c     The footprint is finished
         call is_contour_clockwise(dim,Np,ctr,clockwise)
         if (clockwise) then
            Ntry=Ntry+1
            Ncontour=1
            if (Ntry.gt.10) then
               write(*,*) 'Failing after 10 attempts'
            else
               write(*,*) 'Bad contour, retrying'
               goto 888
            endif
         endif
c         
         call add_ctr_to_contour(dim,Ncontour,Nppc,contour_R1,
     &        Np,ctr)
c         
         if ((on_left_side(ifootprint)).and.
     &        (on_top_side(ifootprint))) then
c     The first footprint that sticks both to the left and the top
c     triggers the "last line"
            top_reached=.true.
c     Debug
c            write(*,*) '**************************************'
c            write(*,*) 'top_reached=',top_reached
c            write(*,*) 'for ifootprint=',ifootprint
c            write(*,*) '**************************************'
c     Debug
         endif
 666     continue
      enddo                     ! while (keep_going)

c     Debug
c      write(*,*) '**************************************'
c      write(*,*) 'Total number of footprints generated:'
c      write(*,*) Ncontour-1
c      write(*,*) '**************************************'
c     Debug

c     identify and remove inconsistent contours
      call eliminate_inconsistent_contours(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour_R1,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     Nremoved)
c     Debug
      write(*,*) '**************************************'
      write(*,*) 'Number of removed contours:',Nremoved
      write(*,*) '**************************************'
c     Debug

      if (generate_sector_plot) then
         filename='./gnuplot/gra_sector'
         call record_sector_contour(filename,dim,
     &        Ncontour,Nppc,contour_R1)
      endif                     ! generate_sector_plot

      call identify_contour_type(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,
     &     on_left_side,on_right_side,on_bottom_side,on_top_side,
     &     contour_type)

c     Revert back to global referential
      do icontour=1,Ncontour
         do i=1,Nppc(icontour)
            do j=1,dim-1
               x_R1(j)=contour_R1(icontour,i,j)
            enddo               ! j
            x_R1(dim)=0.0D+0
            call matrix_vector(dim,M01,x_R1,x_R0)
            do j=1,dim-1
               contour(icontour,i,j)=x_R0(j)
            enddo               ! j
         enddo                  ! i
      enddo                     ! icontour
      
      return
      end
