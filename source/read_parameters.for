c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_parameters(datafile,mu_rw,sigma_rw,mu_db,sigma_db)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read the various parameters used by the generator
c     
c     Input:
c       + datafile: file to read
c     
c     Output:
c       + mu_rw: average of rivers width normal distribution [m]
c       + sigma_rw: standard deviation of rivers width normal distribution [m]
c       + mu_db: average distance between bridges [m]
c       + sigma_db: standard deviation of distance between bridges [m]
c     
c     I/O
      character*(Nchar_mx) datafile
      double precision mu_rw
      double precision sigma_rw
      double precision mu_db
      double precision sigma_db
c     temp
      integer i,ios
c     label
      character*(Nchar_mx) label
      label='subroutine read_parameters'

      open(12,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         do i=1,4
            read(12,*)
         enddo                  ! i
         read(12,*) mu_rw
         read(12,*)
         read(12,*) sigma_rw
         do i=1,2
            read(12,*)
         enddo                  ! i
         read(12,*) mu_db
         read(12,*)
         read(12,*) sigma_db
         close(12)
      endif

c     Check for inconsistencies
      if (mu_rw.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'mu_rw=',mu_rw
         stop
      endif
      if (sigma_rw.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'sigma_rw=',sigma_rw
         stop
      endif
      if (mu_db.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'mu_db=',mu_db
         stop
      endif
      if (sigma_db.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input parameter:'
         write(*,*) 'sigma_db=',sigma_db
         stop
      endif

      return
      end
