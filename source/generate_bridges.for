c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine generate_bridges(dim,mapXsize,mapYsize,
     &     river_branch,Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nbridge,bridge_width,bridge_track,river_track_index)
      implicit none
      include 'max.inc'
      include 'distribution_parameters.inc'
      include 'city_parameters.inc'
      include 'constants.inc'
c     
c     Purpose: to generate bridges
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + draw_white_bands: true if white bands have to be drawn
c     
c     Output:
c       + Nbridge: number of bridges that have been generated
c       + bridge_width: width for each bridge [m]
c       + bridge_track: track that define the position of each bridge
c       + river_track_index: index of the river track each bridge is located on
c        -river_track_index=1: when river_brach=F, or upper part of the main river when river_branch=T
c        -river_track_index=2: lower part of the main river when river_branch=T
c        -river_track_index=3: secondary river when river_branch=T
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      logical draw_white_bands
      integer Nbridge
      double precision bridge_width(1:Nbridge_mx)
      double precision bridge_track(1:Nbridge_mx,1:2,1:Ndim_mx-1)
      integer river_track_index(1:Nbridge_mx)
c     temp
      integer Ntrack,itrack,Nsegment,isegment,Npoint,ipoint,i,j
      double precision d,L(1:3),d0,d2nb,dist,ds2b,osl,rw
      double precision bridge_length
      double precision deck_width
      double precision angle
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      double precision segment_length(1:Nppt_mx-1)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      integer isegment0
      logical keep_going,isegment0_found
      double precision u(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      double precision normal(1:Ndim_mx)
      double precision xc(1:Ndim_mx)
      double precision xref(1:Ndim_mx)
      double precision p1(1:Ndim_mx)
      double precision p2(1:Ndim_mx)
      character*(Nchar_mx) deck_mat
      character*(Nchar_mx) side_mat
      character*(Nchar_mx) wbands_mat
c     label
      character*(Nchar_mx) label
      label='subroutine generate_bridges'

c     river tracks lengths
      if (river_branch) then
         Ntrack=3
      else
         Ntrack=1
      endif                     ! river_branch
c     
      Nbridge=0
      do itrack=1,Ntrack
c     rw: river width
         if ((itrack.eq.1).or.(itrack.eq.2)) then
            rw=river_width(1)
         else
            rw=river_width(2)
         endif
         bridge_length=rw+2.0D+0*bridge_overlapping_distance
         deck_width=normal_road_width ! decks unite with major roads
c     extract the current river track
         call extract_river_track(dim,Nriver,river_Nppt,river_track,
     &        itrack,Npoint,track)
         Nsegment=Npoint-1
         L(itrack)=0.0D+0
         do isegment=1,Nsegment
            call get_position_from_track(dim,Npoint,track,isegment,
     &           x1)
            call get_position_from_track(dim,Npoint,track,isegment+1,
     &           x2)
            call distance_between_two_points(dim,x1,x2,d)
            segment_length(isegment)=d
            L(itrack)=L(itrack)+d
         enddo                  ! isegment
         if (itrack.eq.1) then
            d0=L(itrack)/1.0D+1
         else
            d0=L(itrack)/5.0D+0
         endif                  ! itrack
         keep_going=.true.
         do while (keep_going)
c     --------------- sample interbridge distance ------------------
c            call sample_normal(mu_db,sigma_db,d2nb)
            if (interbridge_distance_dist.eq.1) then
               d2nb=interbridge_distance_val
            else if (interbridge_distance_dist.eq.2) then
               call uniform(interbridge_distance_valmin,
     &              interbridge_distance_valmax,d2nb)
            else if (interbridge_distance_dist.eq.3) then
               call sample_normal(interbridge_distance_mu,
     &              interbridge_distance_sigma,d2nb)
            endif
c     --------------- sample interbridge distance ------------------

            if (d0+d2nb.ge.0.90D+0*L(itrack)) then
               keep_going=.false.
            else
               Nbridge=Nbridge+1
               if (Nbridge.gt.Nbridge_mx) then
                  call error(label)
                  write(*,*) 'Nbridge=',Nbridge
                  write(*,*) '> Nbridge_mx=',Nbridge_mx
                  stop
               endif
               river_track_index(Nbridge)=itrack
               bridge_width(Nbridge)=deck_width+2*bridge_side_width
               ds2b=d0+d2nb
               d0=ds2b
c     locate segment where bridge is located
               isegment0_found=.false.
               dist=0.0D+0
               do isegment=1,Nsegment
                  if ((ds2b.ge.dist).and.
     &                 (ds2b.le.dist+segment_length(isegment))) then
                     isegment0=isegment
                     isegment0_found=.true.
                     osl=ds2b-dist
                     goto 111
                  else
                     dist=dist+segment_length(isegment)
                  endif
               enddo            ! isegment
 111           continue
               if (.not.isegment0_found) then
                  call error(label)
                  write(*,*) 'isegment0 could not be found'
                  write(*,*) 'ds2b=',ds2b
                  write(*,*) 'L(',itrack,')=',L(itrack)
                  stop
               endif            ! isegment0_found=.false.
               if (osl.lt.0.0D+0) then
                  call error(label)
                  write(*,*) 'osl=',osl
                  write(*,*) 'should be positive'
                  stop
               endif
c     compute bridge position
               call get_position_from_track(dim,Npoint,track,
     &              isegment0,x1)
               call get_position_from_track(dim,Npoint,track,
     &              isegment0+1,x2)
               call substract_vectors(dim,x2,x1,u)
               call normalize_vector(dim,u,u)
               call scalar_vector(dim,osl,u,v)
               call add_vectors(dim,x1,v,xc)
               call normal_for_segment(dim,x1,x2,normal)
               call scalar_vector(dim,bridge_length/2.0D+0,normal,v)
               call substract_vectors(dim,xc,v,p1)
               call add_vectors(dim,xc,v,p2)
               do j=1,dim-1
                  bridge_track(Nbridge,1,j)=p1(j)
                  bridge_track(Nbridge,2,j)=p2(j)
               enddo            ! j
c     reference position
               call scalar_vector(dim,deck_width/2.0D+0,u,v)
               call add_vectors(dim,p1,v,xref)
c     angle
               if (p2(1)-p1(1).ne.0.0D+0) then
                  angle=datan((p2(2)-p1(2))/(p2(1)-p1(1)))*180.0D+0/pi ! deg
               else
                  angle=90.0D+0 ! deg
               endif
c     generate definition file
               deck_mat=trim(bridge_deck_material)
               side_mat=trim(bridge_side_material)
               wbands_mat=trim(white_bands_material)
               call record_bridge_definition_file(dim,Nbridge,
     &              xref,deck_width,bridge_length,angle,
     &              draw_white_bands,
     &              deck_mat,side_mat,wbands_mat)
c     
            endif               ! d0+d2nb>=0.9D*L(itrack)
         enddo                  ! while (keep_going)
      enddo                     ! itrack

      return
      end


      
      subroutine extract_bridge_track(dim,
     &     Nbridge,bridge_width,bridge_track,
     &     itrack,width,track)
      implicit none
      include 'max.inc'
c     
c     Purpose: extract a track from the list of bridge tracks
c     
c     Input:
c       + dim: dimension of space
c       + Nbridge: number of bridges
c       + bridge_width: width of each bridge [m]
c       + bridge_track: track of each bridge
c       + itrack: index of the track to extract
c     
c     Output:
c       + width: width of the extracted track [m]
c       + track: extracted track
c     
c     I/O
      integer dim
      double precision bridge_width(1:Nbridge_mx)
      integer Nbridge
      double precision bridge_track(1:Nbridge_mx,
     &     1:2,1:Ndim_mx-1)
      integer itrack
      double precision width
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine extract_bridge_track'

      if (itrack.lt.1) then
         call error(label)
         write(*,*) 'itrack=',itrack
         write(*,*) 'should be > 0'
         stop
      endif
      if (itrack.gt.Nbridge) then
         call error(label)
         write(*,*) 'itrack=',itrack
         write(*,*) 'should be < Nbridge=',Nbridge
         stop
      endif

      if (bridge_width(itrack).le.0.0D+0) then
         call error(label)
         write(*,*) 'bridge_width(',itrack,')=',bridge_width(itrack)
         write(*,*) 'should be positive'
         stop
      else
         width=bridge_width(itrack)
      endif                     ! bridge_width(itrack)<0
      do i=1,2
         do j=1,dim-1
            track(i,j)=bridge_track(itrack,i,j)
         enddo                  ! j
      enddo                     ! i

      return
      end
