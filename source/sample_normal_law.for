c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine sample_normal(mu,sigma,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to sample a random number according to the normal distribution of parameters (mu,sigma):
c     p(x)=1/(sigma*sqrt(2*pi))*exp[-(x-mu)^2/(2*sigma^2)]
c     
c     Input:
c       + mu: mean of the Normal distribution
c       + sigma: standard deviation of the Normal distribution
c     
c     Output:
c       + x: random number sampled according to the normal distribution of parameters (mu,sigma)
c     
c     I/O
      double precision mu
      double precision sigma
      double precision x
c     temp
      double precision r
      double precision x_inv
      double precision eps
      logical keep_sampling
c     label
      character*(Nchar_mx) label
      label='subroutine sample_normal'

      keep_sampling=.true.
      do while (keep_sampling)
         call random_gen(r)
         eps=1.0D-8
         call inverf(2.0D+0*r-1.0D+0,eps,x_inv)
         x=mu+dsqrt(2.0D+0)*sigma*x_inv
c     generate only positive values
         if (x.gt.0.0D+0) then
            keep_sampling=.false.
         endif
      enddo ! while (keep_sampling)

      return
      end
      


      subroutine inverf(y,eps,x)
      implicit none
      include 'max.inc'
c     
c     Purpose: to invert the y=erf(x) function
c     
c     Input:
c       + y: value of y=erf(x) in the [-1,1] range
c       + eps: uncertainty over x
c     
c     Output:
c       + x: value of x in the [-inf,+inf] range
c     
c     I/O
      double precision x
      double precision y
      double precision eps
c     temp
      double precision errx
      double precision x0,coeff,xup,xtmp,ytmp
      double precision x1,x2,y1,y2
      double precision x_mid,y_mid
      logical xup_found,x_found
      integer niter
c     label
      character*(Nchar_mx) label
      label='subroutine inverf'

      x0=0.0D+0
c     Identify second boundary of the interval
      xtmp=1.0D+0
      if (y.ge.0.0D+0) then
         coeff=2.0D+0
         niter=0
         xup_found=.false.
         do while (.not.xup_found)
            niter=niter+1
            xtmp=xtmp*coeff
            ytmp=derf(xtmp)
            if (ytmp.gt.y) then
               xup=xtmp
               xup_found=.true.
            endif
            if (niter.gt.Niter_mx) then
               call error(label)
               write(*,*) 'Max. number of iterations reached'
               write(*,*) 'while looking for xup'
               write(*,*) 'y=',y
               write(*,*) 'xtmp=',xtmp,' ytmp=',ytmp
               stop
            endif
         enddo
         x1=x0
         x2=xup
      else                      ! y<0
         coeff=-2.0D+0
         niter=0
         xup_found=.false.
         do while (.not.xup_found)
            niter=niter+1
            xtmp=xtmp*coeff
            ytmp=derf(xtmp)
            if (ytmp.lt.y) then
               xup=xtmp
               xup_found=.true.
            endif
            if (niter.gt.Niter_mx) then
               call error(label)
               write(*,*) 'Max. number of iterations reached'
               write(*,*) 'while looking for xup'
               write(*,*) 'y=',y
               write(*,*) 'xtmp=',xtmp,' ytmp=',ytmp
               stop
            endif
         enddo
         x1=xup
         x2=x0
      endif
      y1=derf(x1)
      y2=derf(x2)

c     Dichotomy to identify x
      niter=0
      x_found=.false.
      do while (.not.x_found)
         niter=niter+1
         x_mid=(x1+x2)/2.0D+0
         y_mid=derf(x_mid)
         if (y_mid.ge.y) then
            x2=x_mid
         else
            x1=x_mid
         endif
         if (dabs(x2-x1).le.eps) then
            x=x_mid
            x_found=.true.
         endif
         if (niter.gt.Niter_mx) then
            call error(label)
            write(*,*) 'Max. number of iterations reached'
            write(*,*) 'while looking for x'
            write(*,*) 'x1=',x1,' y1=',y1
            write(*,*) 'x2=',x2,' y2=',y2
            stop
         endif
      enddo

      return
      end
