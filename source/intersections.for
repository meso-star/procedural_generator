c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine contour_intersects_contour(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     intersection_found,Nintersected,intersected_ic)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether or not a given contour intersects another contour,
c     including itself
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the examined contour
c     
c     Output:
c       + intersection_found: true if contour index "icontour" intersects another contour
c       + Nintersected: number of contours that are intersected
c       + intersected_ic: index of the intersected contours (when "intersect"=T)
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical intersection_found
      integer Nintersected
      integer intersected_ic(1:Ncontour_mx)
c     temp
      integer jcontour,i,j
      logical intersection
c     label
      character*(Nchar_mx) label
      label='subroutine contour_intersects_contour'

      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
c     Debug
c      write(*,*) 'This is: ',trim(label)
c      write(*,*) 'icontour=',icontour
c      do i=1,Nppc(icontour)
c         write(*,*) (contour(icontour,i,j),j=1,dim-1)
c      enddo                     ! i
c     Debug
c     
      intersection_found=.false.
      Nintersected=0
      do jcontour=1,Ncontour
c     Debug
c         write(*,*) 'Trying intersection with jcontour=',jcontour
c         do i=1,Nppc(jcontour)
c            write(*,*) (contour(jcontour,i,j),j=1,dim-1)
c         enddo                  ! i
c     Debug
         intersection=.false.
         if (icontour.eq.jcontour) then
            call contour_self_intersects(dim,
     &           mapXsize,mapYsize,
     &           Ncontour,Nppc,contour,icontour,
     &           intersection)
c     Debug
c            if (intersection) then
c               write(*,*) 'Self_intersection, icontour=',icontour
c            endif
c     Debug
         else                   ! icontour and jcontour are different
c     Debug
c            write(*,*) 'This is: ',trim(label)
c            write(*,*) 'trying intersection between ',
c     &           icontour,' and ',jcontour
c     Debug
            call contours_intersect(dim,
     &           mapXsize,mapYsize,
     &           Ncontour,Nppc,contour,icontour,jcontour,
     &           intersection)
c     Debug
c             if (intersection) then
c                write(*,*) 'Contour ',icontour,' intersects ',jcontour
c             else
c                write(*,*) 'No intersection found'
c             endif
c     Debug
         endif                  ! icontour=jcontour
         if (intersection) then
            intersection_found=.true.
            Nintersected=Nintersected+1
            intersected_ic(Nintersected)=jcontour
c     Debug
c            write(*,*) 'Nintersected=',Nintersected,
c     &           (intersected_ic(i),i=1,Nintersected)
c     Debug
         endif                  ! intersection
      enddo                     ! jcontour
c     Debug
c      write(*,*) 'This is: ',trim(label)
c      write(*,*) 'intersection_found=',intersection_found
c      write(*,*) 'Nintersected=',Nintersected
c      do i=1,Nintersected
c         write(*,*) 'intersected_ic(',i,')=',intersected_ic(i)
c      enddo                     ! i
c     Debug
c     
      return
      end


      
      subroutine contour_self_intersects(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,
     &     intersection_found)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether or not a given contour intersects itself
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the examined contour
c     
c     Output:
c       + intersection_found: true if contour index "icontour" intersects itself
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      logical intersection_found
c     temp
      integer Nsegi,iseg,jseg,i1,i2,j1,j2,j
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      logical identical,intersection
      double precision alpha,beta,gamma
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision d1,d2
      double precision tmp(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine contour_self_intersects'

      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
c
      intersection_found=.false.
c     
      Nsegi=Nppc(icontour)
      do iseg=1,Nsegi
         i1=iseg
         if (iseg.eq.Nsegi) then
            i2=1
         else
            i2=iseg+1
         endif
         do j=1,dim-1
            P11(j)=contour(icontour,i1,j)
            P12(j)=contour(icontour,i2,j)
         enddo                  ! j
         P11(dim)=0.0D+0
         P12(dim)=0.0D+0
         call substract_vectors(dim,P12,P11,tmp)
         call vector_length(dim,tmp,d1)
         call identical_positions(dim,P11,P12,identical)
         if (identical) then
c            call error(label)
c            write(*,*) 'P11=',P11
c            write(*,*) 'P12=',P12
c            write(*,*) 'should be different'
c            stop
            goto 222
         endif
c     
         do jseg=1,Nsegi
            if (iseg.eq.jseg) then
               intersection_found=.false.
            else                ! iseg and jseg are different
               j1=jseg
               if (jseg.eq.Nsegi) then
                  j2=1
               else
                  j2=jseg+1
               endif
               do j=1,dim-1
                  P21(j)=contour(icontour,j1,j)
                  P22(j)=contour(icontour,j2,j)
               enddo            ! j
               P21(dim)=0.0D+0
               P22(dim)=0.0D+0
               call substract_vectors(dim,P22,P21,tmp)
               call vector_length(dim,tmp,d2)
               call identical_positions(dim,P21,P22,identical)
               if (identical) then
c                  call error(label)
c                  write(*,*) 'P21=',P21
c                  write(*,*) 'P22=',P22
c                  write(*,*) 'should be different'
c                  stop
                  goto 111
               endif            ! identical
c     
               call substract_vectors(dim,P12,P11,tmp)
               call normalize_vector(dim,tmp,u1)
               call substract_vectors(dim,P22,P21,tmp)
               call normalize_vector(dim,tmp,u2)
c     
               gamma=u1(2)*u2(1)-u1(1)*u2(2)
               if (dabs(gamma).lt.1.0D-10) then
c     This is the case when the 2 lines are parallel: no intersection
                  intersection=.false.
               else
                  intersection=.true.
                  alpha=((P11(1)-P21(1))*u2(2)-
     &                 (P11(2)-P21(2))*u2(1))/gamma
                  if (dabs(u2(1)).le.1.0D-10) then
                     beta=(P11(2)-P21(2)+alpha*u1(2))/u2(2)
                  else
                     beta=(P11(1)-P21(1)+alpha*u1(1))/u2(1)
                  endif
                  call scalar_vector(dim,alpha,u1,tmp)
                  call add_vectors(dim,P11,tmp,Pint1)
                  call scalar_vector(dim,beta,u2,tmp)
                  call add_vectors(dim,P21,tmp,Pint2)
                  call identical_positions(dim,Pint1,Pint2,identical)
                  if (identical) then
                     call copy_vector(dim,Pint1,Pint)
                  else
                     call error(label)
                     write(*,*) 'Pint1 and Pint2 are different'
                     write(*,*) 'P11=',P11
                     write(*,*) 'P12=',P12
                     write(*,*) 'u1=',u1
                     write(*,*) 'alpha=',alpha
                     write(*,*) 'Pint1=',Pint1
                     write(*,*) 'P21=',P21
                     write(*,*) 'P22=',P22
                     write(*,*) 'u2=',u2
                     write(*,*) 'beta=',beta
                     write(*,*) 'Pint2=',Pint2
                     stop
                  endif         ! identical
               endif            ! gamma=0
               if ((intersection).and.
     &              (alpha.gt.epsilon).and.
     &              (d1-alpha.gt.epsilon).and.
     &              (beta.gt.epsilon).and.
     &              (d2-beta.gt.epsilon)) then
c     Debug
c                  write(*,*) 'P11=',P11
c                  write(*,*) 'P12=',P12
c                  write(*,*) 'P21=',P21
c                  write(*,*) 'P22=',P22
c                  write(*,*) 'alpha=',alpha,' d1=',d1
c                  write(*,*) 'beta=',beta,' d2=',d2
c                  write(*,*) 'Pint1=',Pint1
c                  write(*,*) 'Pint2=',Pint2
c     Debug
                  intersection_found=.true.
                  goto 666
               endif            ! intersection
            endif               ! iseg=jseg
 111        continue
         enddo                  ! jseg
 222     continue
      enddo                     ! iseg
c     
 666  continue
      return
      end
      

      
      subroutine contours_intersect(dim,
     &     mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,icontour,jcontour,
     &     intersection_found)
      implicit none
      include 'max.inc'
c     
c     Purpose: detect whether or not two given contours intersect each other
c     DO NOT use when icontour=jcontour
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + icontour: index of the first contour
c       + jcontour: index of the second contour
c     
c     Output:
c       + intersection_found: true if contours index "icontour" and "jcontour" intersect
c
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer icontour
      integer jcontour
      logical intersection_found
c     temp
      integer i,j,i1,i2,j1,j2
      integer Nsegi,Nsegj,iseg,jseg
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      logical identical,intersection
      double precision alpha,beta,gamma
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision d1,d2
      double precision d3,d4
      double precision tmp(1:Ndim_mx)
      double precision x1(1:Ndim_mx)
      double precision x2(1:Ndim_mx)
      double precision uab(1:Ndim_mx)
      double precision uac(1:Ndim_mx)
      double precision uad(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
      double precision sp1,sp2
      logical id1,id2
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine contours_intersect'

      if ((icontour.lt.1).or.(icontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
      if ((jcontour.lt.1).or.(jcontour.gt.Ncontour)) then
         call error(label)
         write(*,*) 'jcontour=',jcontour
         write(*,*) 'should be in the [1-',Ncontour,'] range'
         stop
      endif
      if (icontour.eq.jcontour) then
         call error(label)
         write(*,*) 'icontour=',icontour
         write(*,*) 'jcontour=',jcontour
         write(*,*) 'should be different'
         stop
      endif
c     
      intersection_found=.false.
c
      Nsegi=Nppc(icontour)
      Nsegj=Nppc(jcontour)
      do iseg=1,Nsegi
         i1=iseg
         if (iseg.eq.Nsegi) then
            i2=1
         else
            i2=iseg+1
         endif
         do j=1,dim-1
            P11(j)=contour(icontour,i1,j)
            P12(j)=contour(icontour,i2,j)
         enddo                  ! j
         P11(dim)=0.0D+0
         P12(dim)=0.0D+0
         call substract_vectors(dim,P12,P11,tmp)
         call vector_length(dim,tmp,d1)
         call identical_positions(dim,P11,P12,identical)
         if (identical) then
c            call error(label)
c            write(*,*) 'P11=',P11
c            write(*,*) 'P12=',P12
c            write(*,*) 'should be different'
c            write(*,*) 'contour ',icontour
c            do i=1,Nppc(icontour)
c               write(*,*) (contour(icontour,i,j),j=1,dim-1)
c            enddo               ! i
c            stop
            goto 222
         endif
c     
         do jseg=1,Nsegj
c     Debug
c            write(*,*) 'iseg=',iseg,' and jseg=',jseg
c            write(*,*) 'P11=',P11
c            write(*,*) 'P12=',P12
c            write(*,*) 'P21=',P21
c            write(*,*) 'P22=',P22
c     Debug
            j1=jseg
            if (jseg.eq.Nsegj) then
               j2=1
            else
               j2=jseg+1
            endif
            do j=1,dim-1
               P21(j)=contour(jcontour,j1,j)
               P22(j)=contour(jcontour,j2,j)
            enddo               ! j
            P21(dim)=0.0D+0
            P22(dim)=0.0D+0
            call substract_vectors(dim,P22,P21,tmp)
            call vector_length(dim,tmp,d2)
            call identical_positions(dim,P21,P22,identical)
            if (identical) then
c               call error(label)
c               write(*,*) 'P21=',P21
c               write(*,*) 'P22=',P22
c               write(*,*) 'should be different'
c               write(*,*) 'contour ',jcontour
c               do i=1,Nppc(jcontour)
c                  write(*,*) (contour(jcontour,i,j),j=1,dim-1)
c               enddo            ! i
c               stop
               goto 111
            endif               ! identical
c     
            call substract_vectors(dim,P12,P11,tmp)
            call normalize_vector(dim,tmp,u1)
            call substract_vectors(dim,P22,P21,tmp)
            call normalize_vector(dim,tmp,u2)
c     
            gamma=u1(2)*u2(1)-u1(1)*u2(2)
c     Debug
c            write(*,*) 'gamma=',gamma
c     Debug
            if (dabs(gamma).lt.epsilon) then
               intersection_found=.false.
            else
               intersection=.true.
               alpha=((P11(1)-P21(1))*u2(2)-
     &              (P11(2)-P21(2))*u2(1))/gamma
               if (dabs(u2(1)).le.1.0D-10) then
                  beta=(P11(2)-P21(2)+alpha*u1(2))/u2(2)
               else
                  beta=(P11(1)-P21(1)+alpha*u1(1))/u2(1)
               endif
               call scalar_vector(dim,alpha,u1,tmp)
               call add_vectors(dim,P11,tmp,Pint1)
               call scalar_vector(dim,beta,u2,tmp)
               call add_vectors(dim,P21,tmp,Pint2)
               call identical_positions(dim,Pint1,Pint2,identical)
               if (identical) then
                  call copy_vector(dim,Pint1,Pint)
               else
                  call error(label)
                  write(*,*) 'Pint1 and Pint2 are different'
                  write(*,*) 'gamma=',gamma
                  write(*,*) 'P11=',P11
                  write(*,*) 'P12=',P12
                  write(*,*) 'u1=',u1
                  write(*,*) 'alpha=',alpha
                  write(*,*) 'Pint1=',Pint1
                  write(*,*) 'P21=',P21
                  write(*,*) 'P22=',P22
                  write(*,*) 'u2=',u2
                  write(*,*) 'beta=',beta
                  write(*,*) 'Pint2=',Pint2
                  stop
               endif            ! identical
               if ((intersection).and.
     &              (alpha.ge.epsilon).and.
     &              (d1-alpha.ge.epsilon).and.
     &              (beta.ge.epsilon).and.
     &              (d2-beta.ge.epsilon)) then
                  intersection_found=.true.
c     Debug
c                  write(*,*) 'This is: ',trim(label)
c                  write(*,*) 'Intersection found !'
c                  write(*,*) 'iseg=',iseg,' jseg=',jseg
c                  write(*,*) 'P11=',P11
c                  write(*,*) 'P12=',P12
c                  write(*,*) 'u1=',u1
c                  write(*,*) 'd1=',d1
c                  write(*,*) 'alpha=',alpha
c                  write(*,*) 'P21=',P21
c                  write(*,*) 'P22=',P22
c                  write(*,*) 'u2=',u2
c                  write(*,*) 'd2=',d2
c                  write(*,*) 'beta=',beta
c                  write(*,*) 'Pint1=',Pint1
c                  write(*,*) 'Pint2=',Pint2
c                  write(*,*) 'gamma=',gamma
c                  write(*,*) dabs(gamma).lt.epsilon
c     Debug
                  goto 666
               endif            ! intersection is valid
            endif               ! |gamma| < epsilon
 111        continue
         enddo                  ! jseg
 222     continue
      enddo                     ! iseg
c     
 666  continue
      return
      end

      

      subroutine segment_intersects_contour(dim,P1,P2,Np,ctr,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c
c     Purpose: to test the intersection between a segment and a contour
c
c     Input:
c       + dim: dimension of space
c       + P1: firt point of segment
c       + P2: second point of segment
c       + Np: number of points in contour
c       + ctr: contour
c
c     Output:
c       + intersection_found: true if a intersection between the segment
c     and a segment of the contour has been found
c       + Pint: intersection position
c
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      integer Np
      double precision ctr(1:Nppc_mx,1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      integer iseg,Nseg
      integer i1,i2,j
      double precision P3(1:Ndim_mx)
      double precision P4(1:Ndim_mx)
      double precision tmp(1:Ndim_mx)
      double precision d1,d2
      logical identical,intersection
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision alpha,beta,gamma
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine segment_intersects_contour'

      call substract_vectors(dim,P2,P1,tmp)
      call vector_length(dim,tmp,d1)
      
      Nseg=Np
      do iseg=1,Nseg
         i1=iseg
         if (iseg.eq.Nseg) then
            i2=1
         else
            i2=iseg+1
         endif
         do j=1,dim-1
            P3(j)=ctr(i1,j)
            P4(j)=ctr(i2,j)
         enddo                  ! j
         P3(dim)=0.0D+0
         P4(dim)=0.0D+0
         call substract_vectors(dim,P4,P3,tmp)
         call vector_length(dim,tmp,d2)
         call identical_positions(dim,P3,P4,identical)
         if (identical) then
            goto 111
         endif                  ! identical
c     
         call substract_vectors(dim,P2,P1,tmp)
         call normalize_vector(dim,tmp,u1)
         call substract_vectors(dim,P4,P3,tmp)
         call normalize_vector(dim,tmp,u2)
c     
         gamma=u1(2)*u2(1)-u1(1)*u2(2)
         if (dabs(gamma).lt.epsilon) then
            intersection_found=.false.
         else
            intersection=.true.
            alpha=((P1(1)-P3(1))*u2(2)-
     &           (P1(2)-P3(2))*u2(1))/gamma
            if (dabs(u2(1)).le.1.0D-10) then
               beta=(P1(2)-P3(2)+alpha*u1(2))/u2(2)
            else
               beta=(P1(1)-P3(1)+alpha*u1(1))/u2(1)
            endif
            call scalar_vector(dim,alpha,u1,tmp)
            call add_vectors(dim,P1,tmp,Pint1)
            call scalar_vector(dim,beta,u2,tmp)
            call add_vectors(dim,P3,tmp,Pint2)
            call identical_positions(dim,Pint1,Pint2,identical)
            if (identical) then
               call copy_vector(dim,Pint1,Pint)
            else
               call error(label)
               write(*,*) 'Pint1 and Pint2 are different'
               write(*,*) 'gamma=',gamma
               write(*,*) 'P1=',P1
               write(*,*) 'P2=',P2
               write(*,*) 'u1=',u1
               write(*,*) 'alpha=',alpha
               write(*,*) 'Pint1=',Pint1
               write(*,*) 'P3=',P3
               write(*,*) 'P4=',P4
               write(*,*) 'u2=',u2
               write(*,*) 'beta=',beta
               write(*,*) 'Pint2=',Pint2
               stop
            endif               ! identical
            if ((intersection).and.
     &           (alpha.ge.epsilon).and.
     &           (d1-alpha.ge.epsilon).and.
     &           (beta.ge.epsilon).and.
     &           (d2-beta.ge.epsilon)) then
               intersection_found=.true.
               goto 666
            endif               ! intersection is valid
         endif                  ! |gamma| < epsilon
 111     continue
      enddo                     ! iseg

 666  continue
      return
      end
      


      subroutine line_scene_intersection(dim,mapXsize,mapYsize,
     &     x0,u,generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     Nroad,road_width,road_Nppt,road_track,
     &     intersection_found,xint,d2int,elem_type,elem_index)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify a intersection between a (x0,u) line and
c     every element of the scene (at a given time)
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + x0: origin position of the line
c       + u: direction of the line
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + Nriver: number of rivers that have been generated
c       + river_width: width of each river that has been generated
c       + river_Nppt: number of points for the description of the central track for each river
c       + river_track: list of positions that describe the central track of each river
c       + Nroad: number of roads that have been generated
c       + road_width: width for each road [m]
c       + road_Nppt: number of points for each road track
c       + road_track: track that define the position of each road
c
c     Output
c       + intersection_found: true if intersection position was found
c       + xint: closest intersection position (if found)
c       + d2int: distance to closest intersection
c       + elem_type: type of element that was intersected
c         - elem_type=0: border of the map
c         - elem_type=1: river
c         - elem_type=2: road
c       + elem_index: index of the intersected element
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
      double precision x0(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      logical generate_river
      logical river_branch
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer elem_type
      integer elem_index
c     temp
      integer Nbranch,ibranch,iroad,isegment
      double precision width
      integer Nppt
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      logical intersection
      double precision Pint(1:Ndim_mx)
      double precision d
      integer duplicated_Nppt
      double precision duplicated_track(1:Nppt_mx,1:Ndim_mx-1)
c     label
      character*(Nchar_mx) label
      label='subroutine line_scene_intersection'

      intersection_found=.false.
c     looking for intersections with map border
      Nppt=5
      track(1,1)=0.0D+0
      track(1,2)=0.0D+0
      track(2,1)=mapXsize
      track(2,2)=0.0D+0
      track(3,1)=mapXsize
      track(3,2)=mapYsize
      track(4,1)=0.0D+0
      track(4,2)=mapYsize
      track(5,1)=0.0D+0
      track(5,2)=0.0D+0
      call track_line_intersection(dim,Nppt,track,
     &     x0,u,intersection,Pint,d,isegment)
c     Debug
c      write(*,*) 'intersection=',intersection
c      if (intersection) then
c         write(*,*) 'Pint=',Pint
c         write(*,*) 'd=',d
c      endif
c     Debug
      if ((intersection).and.((.not.intersection_found).or.
     &        ((intersection_found).and.(d.lt.d2int)))) then
         intersection_found=.true.
         call copy_vector(dim,Pint,xint)
         d2int=d
         elem_type=0            ! border of the map
         if (Pint(2).eq.0.0D+0) then
            elem_index=1
         else if (Pint(1).eq.mapXsize) then
            elem_index=2
         else if (Pint(2).eq.mapYsize) then
            elem_index=3
         else if (Pint(1).eq.0.0D+0) then
            elem_index=4
         else
            call error(label)
            write(*,*) 'cannot determine elem_index'
            write(*,*) 'Pint=',Pint
            stop
         endif
      endif      
c     looking for intersections with rivers, if any
      if (generate_river) then
         if (river_branch) then
            Nbranch=3
         else
            Nbranch=1
         endif                  ! river_branch
         do ibranch=1,Nbranch
            if ((ibranch.eq.1).or.(ibranch.eq.2)) then
               width=river_width(1)
            else
               width=river_width(2)
            endif
            call extract_river_track(dim,Nriver,river_Nppt,
     &           river_track,ibranch,Nppt,track)
            call duplicate_track(dim,mapXsize,mapYsize,
     &           Nppt,track,width/2.0D+0,-1,
     &           duplicated_Nppt,duplicated_track)
            call track_line_intersection(dim,
     &           duplicated_Nppt,duplicated_track,
     &           x0,u,intersection,Pint,d,isegment)
            if ((intersection).and.((.not.intersection_found).or.
     &           ((intersection_found).and.(d.lt.d2int)))) then
               intersection_found=.true.
               call copy_vector(dim,Pint,xint)
               d2int=d
               elem_type=1
               elem_index=ibranch
            endif
            call duplicate_track(dim,mapXsize,mapYsize,
     &           Nppt,track,width/2.0D+0,1,
     &           duplicated_Nppt,duplicated_track)
            call track_line_intersection(dim,
     &           duplicated_Nppt,duplicated_track,
     &           x0,u,intersection,Pint,d,isegment)
            if ((intersection).and.((.not.intersection_found).or.
     &           ((intersection_found).and.(d.lt.d2int)))) then
               intersection_found=.true.
               call copy_vector(dim,Pint,xint)
               d2int=d
               elem_type=1
               elem_index=ibranch
            endif
         enddo                  ! ibranch
      endif                     ! generate_river
c     looking for intersections with roads
      do iroad=1,Nroad
         width=road_width(iroad)
         call extract_road_track(dim,
     &        Nroad,road_width,road_Nppt,road_track,
     &        iroad,width,Nppt,track)
         call duplicate_track(dim,mapXsize,mapYsize,
     &        Nppt,track,width/2.0D+0,-1,
     &        duplicated_Nppt,duplicated_track)
         call track_line_intersection(dim,
     &        duplicated_Nppt,duplicated_track,
     &        x0,u,intersection,Pint,d,isegment)
         if ((intersection).and.((.not.intersection_found).or.
     &        ((intersection_found).and.(d.lt.d2int)))) then
            intersection_found=.true.
            call copy_vector(dim,Pint,xint)
            d2int=d
            elem_type=2
            elem_index=iroad
         endif
         call duplicate_track(dim,mapXsize,mapYsize,
     &        Nppt,track,width/2.0D+0,1,
     &        duplicated_Nppt,duplicated_track)
         call track_line_intersection(dim,
     &        duplicated_Nppt,duplicated_track,
     &        x0,u,intersection,Pint,d,isegment)
         if ((intersection).and.((.not.intersection_found).or.
     &        ((intersection_found).and.(d.lt.d2int)))) then
            intersection_found=.true.
            call copy_vector(dim,Pint,xint)
            d2int=d
            elem_type=2
            elem_index=iroad
         endif
      enddo                     ! iroad
      
      return
      end



      subroutine iis_right(dim,
     &     Ncontour,Nppc,contour,x0,
     &     xint,d2int,icontour,isegment)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the intersection between a ray (x0, +ix)
c     and all contours defined for a given sector
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + x0: starting position of the ray 
c       + u0: direction of the ray
c      
c     Output:
c       + intersection_found: true if a intersection was found
c       + xint: intersection position
c       + d2int: distance to instersection
c       + icontour: index of the intersected contour
c       + isegment: index of the intersected segment
c    
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer icontour
      integer isegment
c     temp
      integer i,j
      logical inside
      double precision u0(1:Ndim_mx)
      logical intersection_found
      logical debug
c     label
      character*(Nchar_mx) label
      label='subroutine iis_right'

      debug=.false.
      call is_inside_contour(debug,dim,Ncontour,Nppc,contour,
     &     1,x0,inside)
      if (.not.inside) then
         call error(label)
         write(*,*) 'Position x0=',x0
         write(*,*) 'should be inside contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif
c     
      u0(1)=1.0D+0
      u0(2)=0.0D+0
      u0(3)=0.0D+0
      call intersection_in_sector(dim,
     &     Ncontour,Nppc,contour,x0,u0,
     &     intersection_found,xint,d2int,icontour,isegment)
      if (.not.intersection_found) then
         call error(label)
         write(*,*) 'Intersection not found'
         write(*,*) 'x0=',x0
         write(*,*) 'u0=',u0
         write(*,*) 'contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif                     ! intersection_found=F

      return
      end



      subroutine iis_left(dim,
     &     Ncontour,Nppc,contour,x0,
     &     xint,d2int,icontour,isegment)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the intersection between a ray (x0, -ix)
c     and all contours defined for a given sector
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + x0: starting position of the ray 
c       + u0: direction of the ray
c      
c     Output:
c       + intersection_found: true if a intersection was found
c       + xint: intersection position
c       + d2int: distance to instersection
c       + icontour: index of the intersected contour
c       + isegment: index of the intersected segment
c    
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer icontour
      integer isegment
c     temp
      integer i,j
      logical inside
      double precision u0(1:Ndim_mx)
      logical intersection_found
      logical debug
c     label
      character*(Nchar_mx) label
      label='subroutine iis_left'

c     Debug
c      write(*,*) 'This is: ',trim(label)
c      write(*,*) 'contour1:'
c      do i=1,Nppc(1)
c         write(*,*) (contour(1,i,j),j=1,dim-1)
c      enddo                     ! i
c     Debug

      debug=.false.
      call is_inside_contour(debug,dim,Ncontour,Nppc,contour,
     &     1,x0,inside)
      if (.not.inside) then
         call error(label)
         write(*,*) 'Position x0=',x0
         write(*,*) 'should be inside contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif
c     
      u0(1)=-1.0D+0
      u0(2)=0.0D+0
      u0(3)=0.0D+0
      call intersection_in_sector(dim,
     &     Ncontour,Nppc,contour,x0,u0,
     &     intersection_found,xint,d2int,icontour,isegment)
      if (.not.intersection_found) then
         call error(label)
         write(*,*) 'Intersection not found'
         write(*,*) 'x0=',x0
         write(*,*) 'u0=',u0
         write(*,*) 'contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif                     ! intersection_found=F

      return
      end



      subroutine iis_top(dim,
     &     Ncontour,Nppc,contour,x0,
     &     xint,d2int,icontour,isegment)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the intersection between a ray (x0, +iy)
c     and all contours defined for a given sector
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + x0: starting position of the ray 
c       + u0: direction of the ray
c      
c     Output:
c       + intersection_found: true if a intersection was found
c       + xint: intersection position
c       + d2int: distance to instersection
c       + icontour: index of the intersected contour
c       + isegment: index of the intersected segment
c    
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer icontour
      integer isegment
c     temp
      integer i,j
      logical inside
      double precision u0(1:Ndim_mx)
      logical intersection_found
      logical debug
c     label
      character*(Nchar_mx) label
      label='subroutine iis_top'

      debug=.false.
      call is_inside_contour(debug,dim,Ncontour,Nppc,contour,
     &     1,x0,inside)
      if (.not.inside) then
         call error(label)
         write(*,*) 'Position x0=',x0
         write(*,*) 'should be inside contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif
c     
      u0(1)=0.0D+0
      u0(2)=1.0D+0
      u0(3)=0.0D+0
      call intersection_in_sector(dim,
     &     Ncontour,Nppc,contour,x0,u0,
     &     intersection_found,xint,d2int,icontour,isegment)
      if (.not.intersection_found) then
         call error(label)
         write(*,*) 'Intersection not found'
         write(*,*) 'x0=',x0
         write(*,*) 'u0=',u0
         write(*,*) 'contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif                     ! intersection_found=F

      return
      end



      subroutine iis_bottom(dim,
     &     Ncontour,Nppc,contour,x0,
     &     xint,d2int,icontour,isegment)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the intersection between a ray (x0, -iy)
c     and all contours defined for a given sector
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + x0: starting position of the ray 
c       + u0: direction of the ray
c      
c     Output:
c       + intersection_found: true if a intersection was found
c       + xint: intersection position
c       + d2int: distance to instersection
c       + icontour: index of the intersected contour
c       + isegment: index of the intersected segment
c    
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer icontour
      integer isegment
c     temp
      integer i,j
      logical inside
      double precision u0(1:Ndim_mx)
      logical intersection_found
      logical debug
c     label
      character*(Nchar_mx) label
      label='subroutine iis_bottom'

      debug=.false.
      call is_inside_contour(debug,dim,Ncontour,Nppc,contour,
     &     1,x0,inside)
      if (.not.inside) then
         call error(label)
         write(*,*) 'Position x0=',x0
         write(*,*) 'should be inside contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif
c     
      u0(1)=0.0D+0
      u0(2)=-1.0D+0
      u0(3)=0.0D+0
      call intersection_in_sector(dim,
     &     Ncontour,Nppc,contour,x0,u0,
     &     intersection_found,xint,d2int,icontour,isegment)
      if (.not.intersection_found) then
         call error(label)
         write(*,*) 'Intersection not found'
         write(*,*) 'x0=',x0
         write(*,*) 'u0=',u0
         write(*,*) 'contour:'
         write(*,*) 'Nppc=',Nppc(1)
         do i=1,Nppc(1)
            write(*,*) (contour(1,i,j),j=1,dim-1)
         enddo                  ! i
         stop
      endif                     ! intersection_found=F

      return
      end



      subroutine intersection_in_sector(dim,
     &     Ncontour,Nppc,contour,x0,u0,
     &     intersection_found,xint,d2int,icontour,isegment)
      implicit none
      include 'max.inc'
c     
c     Purpose: to identify the intersection between a given ray (x0, u0)
c     and all contours defined for a given sector
c     
c     Input:
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + x0: starting position of the ray 
c       + u0: direction of the ray
c      
c     Output:
c       + intersection_found: true if a intersection was found
c       + xint: intersection position
c       + d2int: distance to instersection
c       + icontour: index of the intersected contour
c       + isegment: index of the intersected segment
c    
c     I/O
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision u0(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer icontour
      integer isegment
c     temp
      integer i,j,ic,isegment0
      integer Npoints,Nppt
      double precision ctr(1:Nppc_mx,1:Ndim_mx-1)
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      logical int_found
      double precision Pint(1:Ndim_mx)
      double precision d
c     label
      character*(Nchar_mx) label
      label='subroutine intersection_in_sector'

      intersection_found=.false.
      do ic=1,Ncontour
         call get_contour(dim,Ncontour,Nppc,contour,ic,
     &        Npoints,ctr)
         call  ctr2track(dim,Nppc,ctr,Nppt,track)
         call track_line_intersection(dim,Nppt,track,x0,u0,
     &        int_found,Pint,d,isegment0)
         if ((int_found).and.((.not.intersection_found).or.
     &        ((intersection_found).and.(d.lt.d2int)))) then
            intersection_found=.true.
            call copy_vector(dim,Pint,xint)
            d2int=d
            icontour=ic
            isegment=isegment0
         endif
      enddo                     ! icontour

      return
      end

      
      
      subroutine track_line_intersection(dim,Nppt,track,x0,u,
     &     intersection_found,xint,d2int,isegment0)
      implicit none
      include 'max.inc'
c      
c      Purpose: to get the intersection between a track and a
c      line defined by its origin and its direction
c      
c      Input:
c       + dim: dimension of space
c       + Nppt: number of points in the track
c       + track: definition of the track
c       + x0: origin position of the line
c       + u: direction of the line
c      
c      Output:
c       + intersection_found: true if intersection position was found
c       + xint: closest intersection position (if found)
c       + d2int: distance to closest intersection
c       + isegment0: index of the intersected segment
c     
c     I/O
      integer dim
      integer Nppt
      double precision track(1:Nppt_mx,1:Ndim_mx-1)
      double precision x0(1:Ndim_mx)
      double precision u(1:Ndim_mx)
      logical intersection_found
      double precision xint(1:Ndim_mx)
      double precision d2int
      integer isegment0
c     temp
      logical constrain
      integer Nsegment,isegment,j
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision Pint(1:Ndim_mx)
      logical keep_looking
      logical intersection
      double precision d,dist2int
c     label
      character*(Nchar_mx) label
      label='subroutine track_line_intersection'

c     Debug
c      write(*,*) 'In: ',trim(label)
c      write(*,*) 'd2int=',d2int
c     Debug
      if (Nppt.lt.1) then
         call error(label)
         write(*,*) 'Nppt=',Nppt
         write(*,*) 'should be > 0'
         stop
      endif
      if (Nppt.gt.Nppt_mx) then
         call error(label)
         write(*,*) 'Nppt=',Nppt
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      endif
      Nsegment=Nppt-1
      intersection_found=.false.
      keep_looking=.true.
      isegment=1
      do while (keep_looking)
c     Debug
c         write(*,*) '1- isegment=',isegment,' d2int=',d2int
c     Debug
         do j=1,dim-1
            P1(j)=track(isegment,j)
            P2(j)=track(isegment+1,j)
         enddo                  ! j
         P1(dim)=0.0D+0
         P2(dim)=0.0D+0
c     Debug
c         write(*,*) '2- isegment=',isegment,' d2int=',d2int
c     Debug
         constrain=.true.
         call segment_line_intersection(dim,P1,P2,x0,u,constrain,
     &        intersection,Pint,dist2int)
c     Debug
c         write(*,*) '3- isegment=',isegment,' d2int=',d2int
c     Debug
c     Debug
c         write(*,*) 'isegment=',isegment
c         write(*,*) 'intersection=',intersection
c         if (intersection) then
c            write(*,*) 'd2int=',d2int
c         endif
c     Debug
         if (intersection) then
            if (.not.intersection_found) then
               intersection_found=.true.
               call copy_vector(dim,Pint,xint)
               call distance_between_two_points(dim,x0,Pint,d)
               d2int=d
               isegment0=isegment
            else                ! intersection_found was already T
               call distance_between_two_points(dim,x0,Pint,d)
               if (d.lt.d2int) then
                  call copy_vector(dim,Pint,xint)
                  d2int=d
                  isegment0=isegment
               endif            ! d<d2int
            endif               ! .not.intersection_found
         endif                  ! intersection
c     Debug
c         write(*,*) '4- isegment=',isegment,' d2int=',d2int
c     Debug
         if (isegment.ge.Nsegment) then
            keep_looking=.false.
         else
            isegment=isegment+1
         endif
      enddo                     ! while (keep_looking)

c     Debug
c      write(*,*) 'intersection_found=',intersection_found
c      if (intersection_found) then
c         write(*,*) 'd2int=',d2int,' isegment0=',isegment0
c      endif
c      write(*,*) 'Out: ',trim(label)
c      stop
c     Debug
      return
      end
      
      
      
      subroutine segment_line_intersection(dim,P1,P2,P3,v,constrain,
     &     intersection_found,Pint,d2int)
      implicit none
      include 'max.inc'
c     
c     Puropse: to get the intersection position between a [P1,P2] segment 
c     and a line defined by a position P3 and a vector v
c     
c     Input:
c       + dim: dimension of space
c       + P1: point that defines the beginning of the segment
c       + P2: point that defines the end of the segment
c       + P3: point that defines the beginning of the intersecting line
c       + v: direction vector of the intersecting line
c       + contrain: T if the intersection has to be on the [P1P2] segment and in +u direction
c     
c     Output:
c       + intersection_found: true if intersection position was found
c       + Pint: intersection position (if found)
c       + d2int: distance from P3 to Pint
c     
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision P3(1:Ndim_mx)
      double precision v(1:Ndim_mx)
      logical constrain
      logical intersection_found
      double precision Pint(1:Ndim_mx)
      double precision d2int
c     temp
      double precision alpha,beta,gamma
      double precision P1P2(1:Ndim_mx)
      double precision u12(1:Ndim_mx)
      double precision d12
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
      double precision t(1:Ndim_mx)
      logical identical
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine segment_line_intersection'

      intersection_found=.false.
c     [P1P2]
      call substract_vectors(dim,P2,P1,P1P2)
      call normalize_vector(dim,P1P2,u12)
      call vector_length(dim,P1P2,d12)
c     beta: coordinate of Pint on the intersecting line
c     alpha: coordinate of Pint on the [P1P2] segment
      gamma=u12(1)*v(2)-u12(2)*v(1)
c     Debug
c      write(*,*) 'gamma=',gamma
c     Debug
      if (dabs(gamma).lt.epsilon) then
         intersection_found=.false.
      else
         beta=((P3(1)-P1(1))*u12(2)-(P3(2)-P1(2))*u12(1))
     &        /gamma
         if (u12(1).ne.0.0D+0) then
            alpha=(P3(1)-P1(1)+beta*v(1))/u12(1)
         else
            if (u12(2).eq.0.0D+0) then
               call error(label)
               write(*,*) 'u12(1)=',u12(1)
               write(*,*) 'u12(2)=',u12(2)
               write(*,*) 'should not both be null'
               stop
            else
               alpha=(P3(2)-P1(2)+beta*v(2))/u12(2)
            endif
         endif
c     Debug
c         write(*,*) 'alpha=',alpha, 'd12=',d12
c         write(*,*) ' beta=',beta
c     Debug
c     check that intersection positions found from alpha and beta
c     are the same:
c     Pint=P1+alpha*u12
c     Pint=P3+beta*v
         call scalar_vector(dim,alpha,u12,t)
         call add_vectors(dim,P1,t,Pint1)
         call scalar_vector(dim,beta,v,t)
         call add_vectors(dim,P3,t,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
c     Debug
c         write(*,*) 'identical=',identical
c     Debug
         if (identical) then
            intersection_found=.true.
            call copy_vector(dim,Pint1,Pint)
            d2int=beta
c     Additionnally, alpha can be constrained so that Pint
c     is between P1 and P2 and beta>0
            if (constrain) then
               if ((alpha.lt.0.0D+0).or.
     &              (alpha.gt.d12).or.
     &              (beta.lt.0.0D+0)) then
                  intersection_found=.false.
               endif
            endif               ! constrain
         else                   ! .not.identical
            intersection_found=.false.
         endif                  ! identical or not
      endif

      return
      end



      subroutine line_line_intersection(dim,P11,P12,P21,P22,constrain,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the intersection position between 2 lines defined
c     by 2 positions, on the same z=cst plane (2D)
c     
c     Input:
c       + dim: dimension of space
c       + P11: first position that defines line 1
c       + P12: second position that defines line 1
c       + P21: first position that defines line 2
c       + P22: second position that defines line 2
c       + constrain: true if intersection has to be constrained within the 2 segments
c     
c     Output:
c       + intersection_found: true if a intersection was found
c       + Pint: intersection position if intersection_found=T
c     
c     I/O
      integer dim
      double precision P11(1:Ndim_mx)
      double precision P12(1:Ndim_mx)
      double precision P21(1:Ndim_mx)
      double precision P22(1:Ndim_mx)
      logical constrain
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      logical identical,intersection
      double precision alpha,beta,gamma
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      double precision d1,d2
      double precision tmp(1:Ndim_mx)
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c     parameters
      double precision epsilon
      parameter(epsilon=1.0D-10)
c     label
      character*(Nchar_mx) label
      label='subroutine line_line_intersection'

      call identical_positions(dim,P11,P12,identical)
      if (identical) then
         call error(label)
         write(*,*) 'P11=',P11
         write(*,*) 'P12=',P12
         write(*,*) 'should be different'
         stop
      endif
      call identical_positions(dim,P21,P22,identical)
      if (identical) then
         call error(label)
         write(*,*) 'P21=',P21
         write(*,*) 'P22=',P22
         write(*,*) 'should be different'
         stop
      endif

      call substract_vectors(dim,P12,P11,tmp)
      call vector_length(dim,tmp,d1)
      call normalize_vector(dim,tmp,u1)
      call substract_vectors(dim,P22,P21,tmp)
      call vector_length(dim,tmp,d2)
      call normalize_vector(dim,tmp,u2)

      gamma=u1(2)*u2(1)-u1(1)*u2(2)
      intersection=.false.
c     Debug
c      write(*,*) 'gamma=',gamma
c     Debug
      if (dabs(gamma).lt.epsilon) then
c     This is the case when the 2 lines are parallel: no intersection
         intersection_found=.false.
      else
         intersection=.true.
         alpha=((P11(1)-P21(1))*u2(2)-(P11(2)-P21(2))*u2(1))/gamma
         if (dabs(u2(1)).le.1.0D-10) then
            beta=(P11(2)-P21(2)+alpha*u1(2))/u2(2)
         else
            beta=(P11(1)-P21(1)+alpha*u1(1))/u2(1)
         endif
         call scalar_vector(dim,alpha,u1,tmp)
         call add_vectors(dim,P11,tmp,Pint1)
         call scalar_vector(dim,beta,u2,tmp)
         call add_vectors(dim,P21,tmp,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (identical) then
            call copy_vector(dim,Pint1,Pint)
         else
            call error(label)
            write(*,*) 'Pint1 and Pint2 are different'
            write(*,*) 'P11=',P11
            write(*,*) 'P12=',P12
            write(*,*) 'u1=',u1
            write(*,*) 'alpha=',alpha
            write(*,*) 'Pint1=',Pint1
            write(*,*) 'P21=',P21
            write(*,*) 'P22=',P22
            write(*,*) 'u2=',u2
            write(*,*) 'beta=',beta
            write(*,*) 'Pint2=',Pint2
            stop
         endif                  ! identical
         if ((constrain).and.(intersection).and.(identical)) then
            if ((alpha.gt.epsilon).and.
     &           (d1-alpha.gt.epsilon).and.
     &           (beta.gt.epsilon).and.
     &           (d2-beta.gt.epsilon)) then
               intersection_found=.true.
            else
               intersection_found=.false.
            endif               ! intersection is within each segment
         else                   ! no constrain
            intersection_found=.true.
         endif                  ! intersection AND Pint1=Pint2
      endif                     ! gamma=0

      return
      end



      subroutine line1_line2_intersection(dim,P1,P2,u1,u2,
     &     intersection_found,Pint)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the intersection position between 2 lines defined
c     by a position and a propagation direction, on the same z=cst plane (2D)
c     
c     Input:
c       + dim: dimension of space
c       + P1: first position that defines line 1
c       + P2: second position that defines line 2
c       + u1: propagation direction for line 1
c       + u2: propagation direction for line 2
c     
c     Output:
c       + intersection_found: true if a intersection was found
c       + Pint: intersection position if intersection_found=T
c     
c     I/O
      integer dim
      double precision P1(1:Ndim_mx)
      double precision P2(1:Ndim_mx)
      double precision u1(1:Ndim_mx)
      double precision u2(1:Ndim_mx)
      logical intersection_found
      double precision Pint(1:Ndim_mx)
c     temp
      logical identical
      double precision alpha,beta,gamma
      double precision tmp(1:Ndim_mx),sp
      double precision Pint1(1:Ndim_mx)
      double precision Pint2(1:Ndim_mx)
c     label
      character*(Nchar_mx) label
      label='subroutine line_line_intersection'

      call identical_positions(dim,P1,P2,identical)
      if (identical) then
         call error(label)
         write(*,*) 'P1=',P1
         write(*,*) 'P2=',P2
         write(*,*) 'should be different'
         stop
      endif
c     special case of parallel lines
      call scalar_product(dim,u1,u2,sp)
      if (dabs(sp).eq.1.0D+0) then
         intersection_found=.false.
         goto 666
      endif

      gamma=u1(2)*u2(1)-u1(1)*u2(2)
      if (dabs(gamma).lt.1.0D-10) then
c     This is the case when the 2 lines are parallel: no intersection
         intersection_found=.false.
      else
         intersection_found=.true.
         alpha=((P1(1)-P2(1))*u2(2)+(P2(2)-P1(2))*u2(1))/gamma
         if (dabs(u2(1)).le.1.0D-10) then
            beta=(P1(2)-P2(2)+alpha*u1(2))/u2(2)
         else
            beta=(P1(1)-P2(1)+alpha*u1(1))/u2(1)
         endif
         call scalar_vector(dim,alpha,u1,tmp)
         call add_vectors(dim,P1,tmp,Pint1)
         call scalar_vector(dim,beta,u2,tmp)
         call add_vectors(dim,P2,tmp,Pint2)
         call identical_positions(dim,Pint1,Pint2,identical)
         if (identical) then
            call copy_vector(dim,Pint1,Pint)
         else
            call error(label)
            write(*,*) 'Pint1 and Pint2 are different'
            write(*,*) 'P1=',P1
            write(*,*) 'u1=',u1
            write(*,*) 'alpha=',alpha
            write(*,*) 'Pint1=',Pint1
            write(*,*) 'P2=',P2
            write(*,*) 'u2=',u2
            write(*,*) 'beta=',beta
            write(*,*) 'Pint2=',Pint2
            stop
         endif                  ! identical
      endif                     ! gamma=0
      
 666  continue
      return
      end
