c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      program procedural_generator
      implicit none
      include 'max.inc'
c     
c     Purpose: to procedurally generate input data files for program "city_generator"
c     
c     Variables:
c     + dimension of space
      integer dim
c     + data
      character*(Nchar_mx) datafile
      double precision mapXsize
      double precision mapYsize
      double precision cell_length
      logical new_seed
      logical use_MapArray
      logical draw_white_bands
      logical generate_river
      logical river_branch
      logical enable_t4b
      logical draw_wah
      double precision trees_density
c     + map array
      logical(1), allocatable :: MapArray(:,:)
c     rivers
      integer Nriver
      double precision river_width(1:2)
      integer river_Nppt(1:2)
      double precision river_track(1:2,1:Nppt_mx,1:Ndim_mx-1)
c     + bridges
      integer Nbridge
      double precision bridge_width(1:Nbridge_mx)
      double precision bridge_track(1:Nbridge_mx,1:2,1:Ndim_mx-1)
      integer river_track_index(1:Nbridge_mx)
c     + roads
      integer side
      integer Nroad
      double precision road_width(1:Nroad_mx)
      integer road_Nppt(1:Nroad_mx)
      double precision road_track(1:Nroad_mx,
     &     1:Nppt_mx,1:Ndim_mx-1)
c     + road ring
      integer Nlevel,Nsector
      double precision alpha1,alpha2
      double precision xcenter(1:Ndim_mx)
c     + zones
      integer Nzone
c     + central square
      double precision csquare_thickness
      integer csquare_Ncontour
      integer csquare_Nppc(1:Ncontour_mx)
      double precision csquare_contour(1:Ncontour_mx,1:Nppc_mx,
     &     1:Ndim_mx-1)
c     + tetris data
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      logical on_left_side(1:Ncontour_mx)
      logical on_right_side(1:Ncontour_mx)
      logical on_bottom_side(1:Ncontour_mx)
      logical on_top_side(1:Ncontour_mx)
      integer contour_type(1:Ncontour_mx)
c     + temp
      character*(Nchar_mx) seed_file
      character*(Nchar_mx) filename
      character*3 lev_str3,sec_str3
      integer seed,ios,iostatus,Nbuilding,Nsp
      double precision r
      integer NcellsX,NcellsY
      integer DeAllocateStatus
      integer ix,iy,i,j
      integer ilevel,isector
      character*(Nchar_mx) command
      character*(Nchar_mx) contour_file
c     + areas
      double precision main_area,secondary_area,total_area
      double precision Acsquare
      double precision Atree
      double precision Aground
      double precision Ariver
      double precision Abridge
      double precision Aroad
      double precision Adistrict
      double precision Aterrain
      double precision Astreet
      double precision Abuilding
      double precision Acourtyard
      double precision Apark
      double precision Awah
c     + parameters
      integer SizeOfLogical,SizeOfInteger,SizeOfDouble
      parameter(SizeOfLogical=2) ! bytes
      parameter(SizeOfInteger=4) ! bytes
      parameter(SizeOfDouble=8) ! bytes
c     label
      character*(Nchar_mx) label
      label='program procedural_generator'

      dim=3
      
c     remove previous result files
      call init()

c     Reading 'data.in'
      datafile='./data.in'
      call read_data(datafile,mapXsize,mapYsize,
     &     cell_length,new_seed,use_MapArray,draw_white_bands,
     &     generate_river,river_branch,enable_t4b,
     &     draw_wah,trees_density)

      NcellsX=ceiling(mapXsize/cell_length)
      NcellsY=ceiling(mapYsize/cell_length)

c     Memory allocation
      if (use_MapArray) then
         allocate(MapArray(NcellsX,NcellsY))
         do ix=1,NcellsX
            do iy=1,NcellsY
               MapArray(ix,iy)=.false.
            enddo               ! iy
         enddo                  ! ix
      endif

c     Reading './data/distribution_options.in'
      datafile='./data/distribution_options.in'
      call read_distribution_options(datafile)

c     Random number initialization
      seed_file='./configuration/seed'
      call read_seed(seed_file,seed)
      if (new_seed) then
         call initialize_random_generator(seed)
         call random_gen(r)
         seed=int(r*10000)
         call record_seed(seed_file,seed)
      endif                     ! new_seed
      call initialize_random_generator(seed)

c     more initialization
      Nroad=0
      Nzone=0
c     ---------------------------------------------------------------
c     GROUND
c     generate definition file
      call record_ground_definition_file(dim,mapXsize,mapYsize)
c     generate contour
      call ground_contour(dim,mapXsize,mapYsize,
     &     Ncontour,Nppc,contour,Aground)
c     record contour in gnuplot data file
      filename='./gnuplot/ground_contour.dat'
      call record_gnuplot_contour(filename,dim,
     &     Ncontour,Nppc,contour)
c     ---------------------------------------------------------------
c     
c     ---------------------------------------------------------------
c     RIVERS and BRIDGES
c     Generate rivers
      if (generate_river) then         
         write(*,*) 'Generating rivers...'
c     generate definition files
         call generate_rivers(dim,mapXsize,mapYsize,
     &        river_branch,
     &        Nriver,river_width,river_Nppt,river_track)
c     generate contour
         call river_contour(dim,mapXsize,mapYsize,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Ncontour,Nppc,contour,Ariver)
c     record contour in gnuplot data file
         filename='./gnuplot/river_contour.dat'
         call record_gnuplot_contour(filename,dim,
     &        Ncontour,Nppc,contour)
c     mask corresponding cells
         call mask_cells(dim,mapXsize,mapYsize,use_MapArray,
     &        cell_length,Ncontour,Nppc,contour,NcellsX,NcellsY,
     &        MapArray)
c     BRIDGES: only when rivers are required
         write(*,*) 'Generating bridges...'
         call generate_bridges(dim,mapXsize,mapYsize,
     &        river_branch,Nriver,river_width,river_Nppt,river_track,
     &        draw_white_bands,
     &        Nbridge,bridge_width,bridge_track,river_track_index)
c     generate contour
         call bridge_contour(dim,mapXsize,mapYsize,
     &        Nbridge,bridge_width,bridge_track,
     &        Ncontour,Nppc,contour,Abridge)
c     record contour in gnuplot data file
         filename='./gnuplot/bridge_contour.dat'
         call record_gnuplot_contour(filename,dim,
     &        Ncontour,Nppc,contour)
c     mask corresponding cells
         call mask_cells(dim,mapXsize,mapYsize,use_MapArray,
     &        cell_length,Ncontour,Nppc,contour,NcellsX,NcellsY,
     &        MapArray)
      else
         Ariver=0.0D+0
         Abridge=0.0D+0
      endif
c     ---------------------------------------------------------------
c
c     ---------------------------------------------------------------
c     ROADS
      write(*,*) 'Generating major roads...'
c     There is at least one major road
      call generate_major_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,side)
c      
      write(*,*) 'Generating dirt roads...'
      call generate_dirt_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track)
c      
c      write(*,*) 'Generating level1 road ring...'
      call generate_level1_road_ring(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,alpha1,xcenter)

c     ---------------------------------------------------------------
c     CENTRAL SQUARE
      write(*,*) 'Generating central square...'
      call generate_central_square(dim,
     &     mapXsize,mapYsize,Nzone,Nsector,alpha1,xcenter,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     csquare_thickness,csquare_Ncontour,
     &     csquare_Nppc,csquare_contour)
c     compute area
      call contour_area(dim,
     &     csquare_Ncontour,csquare_Nppc,csquare_contour,
     &     main_area,secondary_area,total_area)
      Acsquare=total_area
      Atree=secondary_area
c     record contour in gnuplot data file
      filename='./gnuplot/central_square_contour.dat'
      call record_gnuplot_contour(filename,dim,
     &     csquare_Ncontour,csquare_Nppc,csquare_contour)
c     mask corresponding cells
      call mask_cells(dim,mapXsize,mapYsize,use_MapArray,cell_length,
     &     csquare_Ncontour,csquare_Nppc,csquare_contour,
     &     NcellsX,NcellsY,MapArray)
c     ---------------------------------------------------------------
c      

c     ---------------------------------------------------------------
c     LEVEL1 CENTRAL ROADS
      write(*,*) 'Generating central roads...'
      call generate_level1_central_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,xcenter,
     &     csquare_Ncontour,csquare_Nppc,csquare_contour)
c     ---------------------------------------------------------------
c

c     ---------------------------------------------------------------
c     LEVEL2 ROADS
      write(*,*) 'Generating level2 road ring...'
      call generate_level2_road_ring(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,Nsector,xcenter,alpha1,
     &     alpha2)
      call generate_level2_central_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,xcenter)
c     ---------------------------------------------------------------
c

c     ---------------------------------------------------------------
c     EXTERNAL ROADS
      write(*,*) 'Generating external roads...'
      call generate_external_roads(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     generate_river,river_branch,
     &     Nriver,river_width,river_Nppt,river_track,
     &     draw_white_bands,
     &     Nsector,xcenter)
c     ---------------------------------------------------------------
c

      
c     ---------------------------------------------------------------
c     CONNECT BRIDGES
      if (generate_river) then
         write(*,*) 'Connecting bridges to road network...'
         call connect_bridges(dim,mapXsize,mapYsize,
     &        Nroad,road_width,road_Nppt,road_track,
     &        generate_river,river_branch,
     &        Nriver,river_width,river_Nppt,river_track,
     &        Nbridge,bridge_width,bridge_track,
     &        draw_white_bands,side,river_track_index,
     &        Nsector,xcenter)
      endif                     ! generate_river
c     ---------------------------------------------------------------
c
      
c     ---------------------------------------------------------------
c     generate roads contour
      call road_contour(dim,mapXsize,mapYsize,
     &     Nroad,road_width,road_Nppt,road_track,
     &     Ncontour,Nppc,contour,Aroad)
c     Aroad should be increased with Acsquare
      Aroad=Aroad+Acsquare
c     record contour in gnuplot data file
      filename='./gnuplot/road_contour.dat'
      call record_gnuplot_contour(filename,dim,
     &     Ncontour,Nppc,contour)
c     mask corresponding cells
      call mask_cells(dim,mapXsize,mapYsize,use_MapArray,cell_length,
     &     Ncontour,Nppc,contour,NcellsX,NcellsY,MapArray)
c     ---------------------------------------------------------------
c
      Nbuilding=0
      Nsp=0
      Nlevel=2
      contour_file='./results/contours_temp.dat'

      Adistrict=0.0D+0
      Aterrain=0.0D+0
      Astreet=0.0D+0
      Abuilding=0.0D+0
      Acourtyard=0.0D+0
      Apark=0.0D+0
      Awah=0.0D+0
      do ilevel=1,Nlevel
c     record contour in gnuplot data file
         call num2str3(ilevel,lev_str3)
         do isector=1,Nsector
            call sector_contour(dim,mapXsize,mapYsize,
     &           Nsector,alpha1,xcenter,generate_river,ilevel,isector,
     &           Nroad,road_width,road_Nppt,road_track,
     &           Ncontour,Nppc,contour)
            call tetris3(dim,mapXsize,mapYsize,
     &           Nsector,alpha1,xcenter,ilevel,isector,
     &           Ncontour,Nppc,contour,
     &           on_left_side,on_right_side,on_bottom_side,on_top_side,
     &           contour_type)
            call populate_sector(dim,
     &           mapXsize,mapYsize,
     &           Nzone,Nbuilding,Nsp,Nsector,alpha1,xcenter,
     &           contour_file,
     &           ilevel,isector,
     &           Ncontour,Nppc,contour,
     &           on_left_side,on_right_side,on_bottom_side,on_top_side,
     &           contour_type,enable_t4b,
     &           draw_wah,
     &           Adistrict,Aterrain,Astreet,Abuilding,
     &           Acourtyard,Apark,Awah)
c     record contour in gnuplot data file
            call num2str3(isector,sec_str3)
            filename='./gnuplot/sector_contour_level'
     &           //trim(lev_str3)
     &           //'_sector'
     &           //trim(sec_str3)
     &           //'.dat'
            call record_gnuplot_contour(filename,dim,
     &           Ncontour,Nppc,contour)
         enddo                  ! isector
      enddo                     ! ilevel

c     ---------------------------------------------------------------
c     Trees (placed after terrains and buildings have been computed so that
c     a change in the tree density does not affect the town layout)
      call trees(dim,contour_file,trees_density)
      
c     ---------------------------------------------------------------
      
c     ---------------------------------------------------------------
c     generate Gnuplot script for plotting contours
      filename='./gnuplot/gra_contours'
      call record_gnuplot_script(filename,
     &     dim,mapXsize,mapYsize,generate_river,
     &     Nlevel,Nsector)
c     ---------------------------------------------------------------
c
c     ---------------------------------------------------------------
c     record MapArray into Gnuplot data file
      if (use_MapArray) then
         filename='./gnuplot/MapArray.dat'
         call record_MapArray(filename,dim,
     &        NcellsX,NcellsY,MapArray)
      endif                     ! use_MapArray
c     ---------------------------------------------------------------

      if (use_MapArray) then
c     Memory de-allocation
         deallocate(MapArray,stat=DeAllocateStatus)
         if (DeAllocateStatus.ne.0) then
            call error(label)
            write(*,*) 'Could not deallocate MapArray'
            stop
         endif                  ! DeAllocateStatus.ne.0
      endif                     ! use_MapArray

c     Record areas
      filename='./results/areas.txt'
      call record_area_file(filename,dim,
     &     Aground,Ariver,Abridge,Aroad,Adistrict,Aterrain,Astreet,
     &     Abuilding,Acourtyard,Apark,Awah)
      
      end
