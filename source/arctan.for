c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      double precision function arctan(x,y)
      implicit none
      include 'max.inc'
      include 'constants.inc'
c     
c     Purpose: to compute the alpha=arctan of y/x
c     as a function of x and y, the cartesian coordinates of the point
c     whose coordinate alpha (polar coordinates) has to be found between
c     zero and 2*pi.
c
c     Input:
c       + x: value of x
c       + y: value of y
c
c     Ouput:
c       + arctan: value of alpha, between 0 and 2pi
c
c     I/O
      double precision x,y
c     temp
c     label
      character*(Nchar_mx) label
      label='function arctan'

      if (x.eq.0.0D+0) then
         if (y.gt.0.0D+0) then
            arctan=pi/2.0D+0
         else
            arctan=-pi/2.0D+0
         endif
      else if ((x.gt.0.0D+0).and.(y.ge.0.0D+0)) then
         arctan=atan(y/x)
      else if ((x.lt.0.0D+0).and.(y.ge.0.0D+0)) then
         arctan=pi+atan(y/x)
      else if ((x.lt.0.0D+0).and.(y.le.0.0D+0)) then
         arctan=pi+atan(y/x)
      else if ((x.gt.0.0D+0).and.(y.le.0.0D+0)) then
         arctan=2.0D+0*pi+atan(y/x)
      endif
      
      return
      end
