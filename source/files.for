c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine record_ground_definition_file(dim,mapXsize,mapYsize)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to record a ground definition file
c     
c     Input:
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c     
c     Output: the corresponding './results/ground.in' definition file
c     
c     I/O
      integer dim
      double precision mapXsize
      double precision mapYsize
c     temp
      character*(Nchar_mx) ground_mat
      character*(Nchar_mx) filename
c     label
      character*(Nchar_mx) label
      label='subroutine record_ground_definition_file'

      ground_mat=trim(default_map_material)
      filename='./results/ground.in'
      open(21,file=trim(filename))
      write(21,10) 'Input data for program "city_generator"'
      write(21,*)
      write(21,10) 'Min / max. map extension over X [m]'
      write(21,*) 0.0D+0
      write(21,*) mapXsize
      write(21,10) 'Min / max. map extension over Y [m]'
      write(21,*) 0.0D+0
      write(21,*) mapYsize
      write(21,10) 'Material for ground'
      write(21,10) trim(ground_mat)
      close(21)
      write(*,*) 'File was generated: ',trim(filename)
 
      return
      end



      subroutine record_river_definition_file(dim,iriver,width,
     &     Nppt,central_track,river_mat)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record a river definition file
c     
c     Input:
c       + dim: dimension of space
c       + iriver: index of the river
c       + width: width of the river [m]
c       + Nppt: number of points for the description of the central track for each river
c       + central_track: list of positions that describe the central track of each river
c       + river_mat: material for the river
c     
c     Output: the corresponding './results/river***.in' definition file
c     
c     I/O
      integer dim
      integer iriver
      double precision width
      integer Nppt
      double precision central_track(1:Nppt_mx,1:2)
      character*(Nchar_mx) river_mat
c     temp
      character*(Nchar_mx) filename
      character*3 str3
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine record_river_definition_file'

      if (Nppt.gt.Nppt_mx) then
         call error(label)
         write(*,*) 'Nppt=',Nppt
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      endif
      
      call num2str3(iriver,str3)
      filename='./results/river'//trim(str3)//'.in'
      open(21,file=trim(filename))
      write(21,10) 'Input data for program "city_generator"'
      write(21,*)
      write(21,10) 'Material for the river'
      write(21,10) trim(river_mat)
      write(21,10) 'Width [m]'
      write(21,*) width
      write(21,10) 'Central track [X/Y]'
      do i=1,Nppt
         write(21,*) (central_track(i,j),j=1,dim-1)
      enddo                     ! i
      close(21)
      write(*,*) 'File was generated: ',trim(filename)
 
      return
      end



      subroutine record_bridge_definition_file(dim,ibridge,
     &     xref,width,length,angle,draw_white_bands,
     &     deck_mat,side_mat,wbands_mat)
      implicit none
      include 'max.inc'
      include 'city_parameters.inc'
      include 'formats.inc'
c     
c     Purpose: to record a bridge definition file
c     
c     Input:
c       + dim: dimension of space
c       + ibridge: index of the bridge
c       + xref: reference position
c       + width: width of the deck [m]
c       + length: total length of the bridge [m]
c       + angle: orientation
c       + draw_white_bands: true if white bands have to be drawn
c       + deck_mat: material for the deck
c       + side_mat: material for the sides
c       + wbands_mat: material for white bands
c     
c     Output: the corresponding './results/bridge***.in' definition file
c     
c     I/O
      integer dim
      integer ibridge
      double precision xref(1:Ndim_mx)
      double precision width
      double precision length
      double precision angle
      logical draw_white_bands
      character*(Nchar_mx) deck_mat
      character*(Nchar_mx) side_mat
      character*(Nchar_mx) wbands_mat
c     temp
      character*(Nchar_mx) filename
      character*3 str3
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine record_bridge_definition_file'

c      
      call num2str3(ibridge,str3)
      filename='./results/bridge'//trim(str3)//'.in'
      open(21,file=trim(filename))
      write(21,10) 'Input data for program "city_generator"'
      write(21,*)
      write(21,10) 'X position (m)'
      write(21,*) xref(1)
      write(21,10) 'Y position (m)'
      write(21,*) xref(2)
      write(21,10) 'rotation angle (deg)'
      write(21,*) angle
      write(21,10) 'Total length [m]'
      write(21,*) length
      write(21,10) 'Deck width [m]'
      write(21,*) width
      write(21,10) 'Side width [m]'
      write(21,*) bridge_side_width
      write(21,10) 'Deck height [m]'
      write(21,*) bridge_deck_height_above_water
      write(21,10) 'Side height [m]'
      write(21,*) bridge_side_height_above_water
      write(21,10) 'Material for the deck'
      write(21,10) trim(deck_mat)
      write(21,10) 'Material for the sides'
      write(21,10) trim(side_mat)
      write(21,10) 'Draw white bands ? [T/F]'
      write(21,*) draw_white_bands
      write(21,10) 'Material for white bands'
      write(21,10) trim(wbands_mat)
      close(21)
      write(*,*) 'File was generated: ',trim(filename)
 
      return
      end



      subroutine record_road_definition_file(dim,iroad,width,
     &     Nppt,central_track,draw_white_bands,
     &     road_mat,wbands_mat)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record a road definition file
c     
c     Input:
c       + dim: dimension of space
c       + iroad: index of the road
c       + width: width of the road [m]
c       + Nppt: number of points for the description of the central track for each road
c       + central_track: list of positions that describe the central track of each road
c       + draw_white_bands: true if white bands have to be drawn
c       + road_mat: material for the road
c       + wbands_mat: material for white bands
c     
c     Output: the corresponding './results/road***.in' definition file
c     
c     I/O
      integer dim
      integer iroad
      double precision width
      integer Nppt
      double precision central_track(1:Nppt_mx,1:2)
      logical draw_white_bands
      character*(Nchar_mx) road_mat
      character*(Nchar_mx) wbands_mat
c     temp
      character*(Nchar_mx) filename
      character*3 str3
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine record_road_definition_file'

      if (Nppt.gt.Nppt_mx) then
         call error(label)
         write(*,*) 'Nppt=',Nppt
         write(*,*) '> Nppt_mx=',Nppt_mx
         stop
      endif
      
      call num2str3(iroad,str3)
      filename='./results/road'//trim(str3)//'.in'
      open(21,file=trim(filename))
      write(21,10) 'Input data for program "city_generator"'
      write(21,*)
      write(21,10) 'Material for the road'
      write(21,10) trim(road_mat)
      write(21,10) 'Draw white bands ? [T/F]'
      write(21,*) draw_white_bands
      write(21,10) 'Material for white bands'
      write(21,10) trim(wbands_mat)
      write(21,10) 'Width [m]'
      write(21,*) width
      write(21,10) 'Central track [X/Y]'
      do i=1,Nppt
         write(21,*) (central_track(i,j),j=1,dim-1)
      enddo                     ! i
      close(21)
      write(*,*) 'File was generated: ',trim(filename)
 
      return
      end



      subroutine record_zone_definition_file(dim,izone,
     &     thickness,Ncontour,Nppc,contour,
     &     zone_mat)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record a road definition file
c     
c     Input:
c       + dim: dimension of space
c       + izone: index of the zone
c       + thickness: thickness of the zone
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + zone_mat: material for the zone
c     
c     Output: the corresponding './results/zone***.in' definition file
c     
c     I/O
      integer dim
      integer izone
      double precision thickness
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      character*(Nchar_mx) zone_mat
c     temp
      character*(Nchar_mx) filename
      character*4 str4
      integer i,j,icontour
c     label
      character*(Nchar_mx) label
      label='subroutine record_zone_definition_file'

c     Debug
c      write(*,*) 'zone=',izone
c      write(*,*) 'Ncontour=',Ncontour
c     stop
c     Debug

      if (Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
      if (Ncontour.lt.1) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be positive'
         stop
      endif
      if (izone.lt.1) then
         call error(label)
         write(*,*) 'izone=',izone
         write(*,*) 'should be positive'
         stop
      endif
      do icontour=1,Ncontour
         if (Nppc(icontour).gt.Nppc_mx) then
            call error(label)
            write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
            write(*,*) '> Nppc_mx=',Nppc_mx
            stop
         endif
      enddo                     ! icontour
      
      call num2str4(izone,str4)
      filename='./results/zone'//trim(str4)//'.in'
      open(21,file=trim(filename))
      write(21,10) 'Input data for program "city_generator"'
      write(21,*)
      write(21,10) 'Material for the zone'
      write(21,10) trim(zone_mat)
      write(21,10) 'Thickness [m]'
      write(21,*) thickness
      do icontour=1,Ncontour
         if (icontour.eq.1) then
            write(21,10) 'Contour track [X/Y separated by one space]'
         else
            write(21,32) 'Hole ',icontour-1,
     &           ' track [X/Y separated by one space]'
         endif                  ! icontour=1
         do i=1,Nppc(icontour)
            write(21,*) (contour(icontour,i,j),j=1,dim-1)
         enddo                  ! i
      enddo                     ! icontour
      close(21)
      write(*,*) 'File was generated: ',trim(filename)
 
      return
      end



      subroutine record_sp_definition_file(dim,isp,
     &     edge_width,thickness,Ncontour,Nppc,contour,
     &     zone_mat)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record a swimming pool definition file
c     
c     Input:
c       + dim: dimension of space
c       + isp: index of the swimming pool
c       + edge_width: width of the edge
c       + thickness: thickness of the zone
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c       + zone_mat: material for the zone
c     
c     Output: the corresponding './results/swimmingpool***.in' definition file
c     
c     I/O
      integer dim
      integer isp
      double precision edge_width
      double precision thickness
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      character*(Nchar_mx) zone_mat
c     temp
      character*(Nchar_mx) filename
      character*3 str3
      integer i,j,icontour
c     label
      character*(Nchar_mx) label
      label='subroutine record_sp_definition_file'

      if (Ncontour.gt.Ncontour_mx) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) '> Ncontour_mx=',Ncontour_mx
         stop
      endif
      if (Ncontour.lt.1) then
         call error(label)
         write(*,*) 'Ncontour=',Ncontour
         write(*,*) 'should be positive'
         stop
      endif
      if (isp.lt.1) then
         call error(label)
         write(*,*) 'isp=',isp
         write(*,*) 'should be positive'
         stop
      endif
      do icontour=1,Ncontour
         if (Nppc(icontour).gt.Nppc_mx) then
            call error(label)
            write(*,*) 'Nppc(',icontour,')=',Nppc(icontour)
            write(*,*) '> Nppc_mx=',Nppc_mx
            stop
         endif
      enddo                     ! icontour
      
      call num2str3(isp,str3)
      filename='./results/swimmingpool'//trim(str3)//'.in'
      open(21,file=trim(filename))
      write(21,10) 'Input data for program "city_generator"'
      write(21,*)
      write(21,10) 'Material for the edge'
      write(21,10) trim(zone_mat)
      write(21,10) 'Width of the edge [m]'
      write(21,*) edge_width
      write(21,10) 'Thickness [m]'
      write(21,*) thickness
      do icontour=1,Ncontour
         if (icontour.eq.1) then
            write(21,10) 'Contour track [X/Y separated by one space]'
         else
            write(21,32) 'Hole ',icontour-1,
     &           ' track [X/Y separated by one space]'
         endif                  ! icontour=1
         do i=1,Nppc(icontour)
            write(21,*) (contour(icontour,i,j),j=1,dim-1)
         enddo                  ! i
      enddo                     ! icontour
      close(21)
      write(*,*) 'File was generated: ',trim(filename)
 
      return
      end



      subroutine record_type0_building_definition_file(dim,
     &     Ncubes,length,e_in,e_ext,position,
     &     angle,fwall,e_glass,
     &     internal_walls_mat,
     &     external_walls_mat,
     &     glass_panes_mat,
     &     ibuilding)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to record a type 4 building definition file
c     A type 0 building is a pile of cubes, with no roof
c     
c     Input:
c       + dim: dimension of space
c       + Ncubes: number of cubes in each direction of space
c       + length: length of a single cube, in each direction of space
c       + e_in: thickness of a internal wall [m]
c       + e_ext: thickness of a external wall [m]
c       + position: position [m/m]
c       + angle: rotation angle [deg]
c       + fwall: fraction of the wall used by windows [0-1]
c       + e_glass: glass thickness [mm]
c       + internal_walls_mat: material for internal walls
c       + external_walls_mat: material for external_walls
c       + glass_panes_mat: material for glass panes
c       + ibuilding: index of the building
c     
c     Output: the "./results/building***.in" definition file
c     
c     I/O
      integer dim
      integer Ncubes(1:Ndim_mx)
      double precision length(1:Ndim_mx)
      double precision e_in
      double precision e_ext
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision fwall
      double precision e_glass
      character*(Nchar_mx) internal_walls_mat
      character*(Nchar_mx) external_walls_mat
      character*(Nchar_mx) glass_panes_mat
      integer ibuilding
c     temp
      integer i,j
      character*(Nchar_mx) filename
      character*3 str3
c     label
      character*(Nchar_mx) label
      label='subroutine record_type0_building_definition_file'

c     check for errors
      if (e_in.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_in=',e_in,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type0'
         stop
      endif
      if (e_ext.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_ext=',e_ext,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type0'
         stop
      endif

      call num2str3(ibuilding,str3)
      filename='./results/building'//trim(str3)//'.in'
      open(22,file=trim(filename))
      write(22,10) 'Input data for program "city_generator"'
      write(22,*)
      write(22,10) 'Type'
      write(22,*) 0
      write(22,10) 'Number of cubes in X direction'
      write(22,*) Ncubes(1)
      write(22,10) 'Number of cubes in Y direction'
      write(22,*) Ncubes(2)
      write(22,10) 'Number of cubes in Z direction'
      write(22,*) Ncubes(3)
      write(22,10) 'Dimension of a single cubic cell along '
     &     //'(X,Y,Z) axis (m)'
      do j=1,dim
         write(22,*) length(j)
      enddo                     ! j
      write(22,10) 'Thickness of internal walls [m]'
      write(22,*) e_in
      write(22,10) 'Thickness of external walls [m]'
      write(22,*) e_ext
      write(22,10) 'X position [m]'
      write(22,*) position(1)
      write(22,10) 'Y position [m]'
      write(22,*) position(2)
      write(22,10) 'rotation angle [deg]'
      write(22,*) angle
      write(22,10) 'Fraction of the wall used for windows [0-1]'
      write(22,*) fwall
      write(22,10) 'Thickness of the glass [mm]'
      write(22,*) e_glass
      write(22,10) 'Material for internal walls'
      write(22,10) trim(internal_walls_mat)
      write(22,10) 'Material for external walls'
      write(22,10) trim(external_walls_mat)
      write(22,10) 'Material for glass panes'
      write(22,10) trim(glass_panes_mat)
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end



      subroutine record_type1_building_definition_file(dim,
     &     Ncubes,length,e_in,e_ext,position,
     &     angle,fwall,e_glass,roof_height,
     &     internal_walls_mat,
     &     external_walls_mat,
     &     glass_panes_mat,
     &     roof_mat,
     &     ibuilding)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to record a type 4 building definition file
c     A type 1 building is a pile of cubes, with a double-sided roof
c     
c     Input:
c       + dim: dimension of space
c       + Ncubes: number of cubes in each direction of space
c       + length: length of a single cube, in each direction of space
c       + e_in: thickness of a internal wall [m]
c       + e_ext: thickness of a external wall [m]
c       + position: position [m/m]
c       + angle: rotation angle [deg]
c       + fwall: fraction of the wall used by windows [0-1]
c       + e_glass: glass thickness [mm]
c       + roof_height: height of the roof [m]
c       + internal_walls_mat: material for internal walls
c       + external_walls_mat: material for external_walls
c       + glass_panes_mat: material for glass panes
c       + roof_mat: material for the roof
c       + ibuilding: index of the building
c     
c     Output: the "./results/building***.in" definition file
c     
c     I/O
      integer dim
      integer Ncubes(1:Ndim_mx)
      double precision length(1:Ndim_mx)
      double precision e_in
      double precision e_ext
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision fwall
      double precision e_glass
      double precision roof_height
      character*(Nchar_mx) internal_walls_mat
      character*(Nchar_mx) external_walls_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      integer ibuilding
c     temp
      integer i,j
      character*(Nchar_mx) filename
      character*3 str3
c     label
      character*(Nchar_mx) label
      label='subroutine record_type1_building_definition_file'

c     check for errors
      if (e_in.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_in=',e_in,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type1'
         stop
      endif
      if (e_ext.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_ext=',e_ext,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type1'
         stop
      endif
      
      call num2str3(ibuilding,str3)
      filename='./results/building'//trim(str3)//'.in'
      open(22,file=trim(filename))
      write(22,10) 'Input data for program "city_generator"'
      write(22,*) 
      write(22,10) 'Type'
      write(22,*) 1
      write(22,10) 'Number of cubes in X direction'
      write(22,*) Ncubes(1)
      write(22,10) 'Number of cubes in Y direction'
      write(22,*) Ncubes(2)
      write(22,10) 'Number of cubes in Z direction'
      write(22,*) Ncubes(3)
      write(22,10) 'Dimension of a single cubic cell along '
     &     //'(X,Y,Z) axis (m)'
      do j=1,dim
         write(22,*) length(j)
      enddo                     ! j
      write(22,10) 'Thickness of internal walls [m]'
      write(22,*) e_in
      write(22,10) 'Thickness of external walls [m]'
      write(22,*) e_ext
      write(22,10) 'X position [m]'
      write(22,*) position(1)
      write(22,10) 'Y position [m]'
      write(22,*) position(2)
      write(22,10) 'rotation angle [deg]'
      write(22,*) angle
      write(22,10) 'Fraction of the wall used for windows [0-1]'
      write(22,*) fwall
      write(22,10) 'Thickness of the glass [mm]'
      write(22,*) e_glass
      write(22,10) 'Height of the roof [m]'
      write(22,*) roof_height
      write(22,10) 'Thickness of the roof [m]'
      write(22,*) roof_thickness
      write(22,10) 'Material for internal walls'
      write(22,10) trim(internal_walls_mat)
      write(22,10) 'Material for external walls'
      write(22,10) trim(external_walls_mat)
      write(22,10) 'Material for glass panes'
      write(22,10) trim(glass_panes_mat)
      write(22,10) 'Material for the roof'
      write(22,10)  trim(roof_mat)
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end



      subroutine record_type2_building_definition_file(dim,
     &     Ncubes,length,e_in,e_ext,position,
     &     angle,fwall,e_glass,roof_height,
     &     internal_walls_mat,
     &     external_walls_mat,
     &     glass_panes_mat,
     &     roof_mat,
     &     ibuilding)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to record a type 4 building definition file
c     A type 2 building is a pile of cubes, with a four-sided roof
c     that unite along a central line
c     
c     Input:
c       + dim: dimension of space
c       + Ncubes: number of cubes in each direction of space
c       + length: length of a single cube, in each direction of space
c       + e_in: thickness of a internal wall [m]
c       + e_ext: thickness of a external wall [m]
c       + position: position [m/m]
c       + angle: rotation angle [deg]
c       + fwall: fraction of the wall used by windows [0-1]
c       + e_glass: glass thickness [mm]
c       + roof_height: height of the roof [m]
c       + internal_walls_mat: material for internal walls
c       + external_walls_mat: material for external_walls
c       + glass_panes_mat: material for glass panes
c       + roof_mat: material for the roof
c       + ibuilding: index of the building
c     
c     Output: the "./results/building***.in" definition file
c     
c     I/O
      integer dim
      integer Ncubes(1:Ndim_mx)
      double precision length(1:Ndim_mx)
      double precision e_in
      double precision e_ext
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision fwall
      double precision e_glass
      double precision roof_height
      character*(Nchar_mx) internal_walls_mat
      character*(Nchar_mx) external_walls_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      integer ibuilding
c     temp
      integer i,j
      character*(Nchar_mx) filename
      character*3 str3
c     label
      character*(Nchar_mx) label
      label='subroutine record_type2_building_definition_file'

c     check for errors
      if (e_in.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_in=',e_in,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type2'
         stop
      endif
      if (e_ext.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_ext=',e_ext,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type2'
         stop
      endif
      
      call num2str3(ibuilding,str3)
      filename='./results/building'//trim(str3)//'.in'
      open(22,file=trim(filename))
      write(22,10) 'Input data for program "city_generator"'
      write(22,*) 
      write(22,10) 'Type'
      write(22,*) 2
      write(22,10) 'Number of cubes in X direction'
      write(22,*) Ncubes(1)
      write(22,10) 'Number of cubes in Y direction'
      write(22,*) Ncubes(2)
      write(22,10) 'Number of cubes in Z direction'
      write(22,*) Ncubes(3)
      write(22,10) 'Dimension of a single cubic cell along '
     &     //'(X,Y,Z) axis (m)'
      do j=1,dim
         write(22,*) length(j)
      enddo                     ! j
      write(22,10) 'Thickness of internal walls [m]'
      write(22,*) e_in
      write(22,10) 'Thickness of external walls [m]'
      write(22,*) e_ext
      write(22,10) 'X position [m]'
      write(22,*) position(1)
      write(22,10) 'Y position [m]'
      write(22,*) position(2)
      write(22,10) 'rotation angle [deg]'
      write(22,*) angle
      write(22,10) 'Fraction of the wall used for windows [0-1]'
      write(22,*) fwall
      write(22,10) 'Thickness of the glass [mm]'
      write(22,*) e_glass
      write(22,10) 'Height of the roof [m]'
      write(22,*) roof_height
      write(22,10) 'Thickness of the roof [m]'
      write(22,*) roof_thickness
      write(22,10) 'Material for internal walls'
      write(22,10) trim(internal_walls_mat)
      write(22,10) 'Material for external walls'
      write(22,10) trim(external_walls_mat)
      write(22,10) 'Material for glass panes'
      write(22,10) trim(glass_panes_mat)
      write(22,10) 'Material for the roof'
      write(22,10)  trim(roof_mat)
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end



      subroutine record_type3_building_definition_file(dim,
     &     Ncubes,length,e_in,e_ext,position,
     &     angle,fwall,e_glass,roof_height,
     &     internal_walls_mat,
     &     external_walls_mat,
     &     glass_panes_mat,
     &     roof_mat,
     &     ibuilding)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to record a type 4 building definition file
c     A type 3 building is a pile of cubes, with a four-sided roof
c     that unite on a central position
c     
c     Input:
c       + dim: dimension of space
c       + Ncubes: number of cubes in each direction of space
c       + length: length of a single cube, in each direction of space
c       + e_in: thickness of a internal wall [m]
c       + e_ext: thickness of a external wall [m]
c       + position: position [m/m]
c       + angle: rotation angle [deg]
c       + fwall: fraction of the wall used by windows [0-1]
c       + e_glass: glass thickness [mm]
c       + roof_height: height of the roof [m]
c       + internal_walls_mat: material for internal walls
c       + external_walls_mat: material for external_walls
c       + glass_panes_mat: material for glass panes
c       + roof_mat: material for the roof
c       + ibuilding: index of the building
c     
c     Output: the "./results/building***.in" definition file
c     
c     I/O
      integer dim
      integer Ncubes(1:Ndim_mx)
      double precision length(1:Ndim_mx)
      double precision e_in
      double precision e_ext
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision fwall
      double precision e_glass
      double precision roof_height
      character*(Nchar_mx) internal_walls_mat
      character*(Nchar_mx) external_walls_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      integer ibuilding
c     temp
      integer i,j
      character*(Nchar_mx) filename
      character*3 str3
c     label
      character*(Nchar_mx) label
      label='subroutine record_type3_building_definition_file'

c     check for errors
      if (e_in.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_in=',e_in,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type3'
         stop
      endif
      if (e_ext.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_ext=',e_ext,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type3'
         stop
      endif
      
      call num2str3(ibuilding,str3)
      filename='./results/building'//trim(str3)//'.in'
      open(22,file=trim(filename))
      write(22,10) 'Input data for program "city_generator"'
      write(22,*) 
      write(22,10) 'Type'
      write(22,*) 3
      write(22,10) 'Number of cubes in X direction'
      write(22,*) Ncubes(1)
      write(22,10) 'Number of cubes in Y direction'
      write(22,*) Ncubes(2)
      write(22,10) 'Number of cubes in Z direction'
      write(22,*) Ncubes(3)
      write(22,10) 'Dimension of a single cubic cell along '
     &     //'(X,Y,Z) axis (m)'
      do j=1,dim
         write(22,*) length(j)
      enddo                     ! j
      write(22,10) 'Thickness of internal walls [m]'
      write(22,*) e_in
      write(22,10) 'Thickness of external walls [m]'
      write(22,*) e_ext
      write(22,10) 'X position [m]'
      write(22,*) position(1)
      write(22,10) 'Y position [m]'
      write(22,*) position(2)
      write(22,10) 'rotation angle [deg]'
      write(22,*) angle
      write(22,10) 'Fraction of the wall used for windows [0-1]'
      write(22,*) fwall
      write(22,10) 'Thickness of the glass [mm]'
      write(22,*) e_glass
      write(22,10) 'Height of the roof [m]'
      write(22,*) roof_height
      write(22,10) 'Thickness of the roof [m]'
      write(22,*) roof_thickness
      write(22,10) 'Material for internal walls'
      write(22,10) trim(internal_walls_mat)
      write(22,10) 'Material for external walls'
      write(22,10) trim(external_walls_mat)
      write(22,10) 'Material for glass panes'
      write(22,10) trim(glass_panes_mat)
      write(22,10) 'Material for the roof'
      write(22,10)  trim(roof_mat)
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end



      subroutine record_type4_building_definition_file(dim,
     &     Nfloor,floor_height,e_in,e_ext,position,
     &     angle,fwall,ffloor,e_glass,roof_height,
     &     internal_walls_mat,
     &     external_walls_mat,
     &     glass_panes_mat,
     &     roof_mat,
     &     Ncontour,Nppc,contour,
     &     ibuilding)
      implicit none
      include 'max.inc'
      include 'formats.inc'
      include 'city_parameters.inc'
c     
c     Purpose: to record a type 4 building definition file
c     A type 4 building is defined from is ground contour
c     and has a roof
c     
c     Input:
c       + dim: dimension of space
c       + Nfloor: number of floors
c       + floor_height: height of a floor [m]
c       + e_in: thickness of a internal wall [m]
c       + e_ext: thickness of a external wall [m]
c       + position: position [m/m]
c       + angle: rotation angle [deg]
c       + fwall: fraction of the wall used by windows [0-1]
c       + ffloor: fraction of floor height used by windows [0-1]
c       + e_glass: glass thickness [mm]
c       + roof_height: height of the roof [m]
c       + internal_walls_mat: material for internal walls
c       + external_walls_mat: material for external_walls
c       + roof_mat: material for the roof
c       + glass_panes_mat: material for glass panes
c       + Ncontour: number of contours (updated)
c       + Nppc: number of points that define each contour (updated)
c       + contour: list of coordinates for each contour (updated)
c       + ibuilding: index of the building
c     
c     Output: the "./results/building***.in" definition file
c     
c     I/O
      integer dim
      integer Nfloor
      double precision floor_height
      double precision e_in
      double precision e_ext
      double precision position(1:Ndim_mx-1)
      double precision angle
      double precision fwall
      double precision ffloor
      double precision e_glass
      double precision roof_height
      character*(Nchar_mx) internal_walls_mat
      character*(Nchar_mx) external_walls_mat
      character*(Nchar_mx) glass_panes_mat
      character*(Nchar_mx) roof_mat
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
      integer ibuilding
c     temp
      integer i,j,icontour
      character*(Nchar_mx) filename
      character*3 str3
c     label
      character*(Nchar_mx) label
      label='subroutine record_type4_building_definition_file'

c     check for errors
      if (e_in.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_in=',e_in,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type4'
         stop
      endif
      if (e_ext.lt.e_glass*1.0D-3) then
         call error(label)
         write(*,*) 'e_ext=',e_ext,' m'
         write(*,*) '< e_glass=',e_glass,' mm'
         write(*,*) 'ibuilding=',ibuilding
         write(*,*) 'type4'
         stop
      endif
      
      call num2str3(ibuilding,str3)
      filename='./results/building'//trim(str3)//'.in'
      open(22,file=trim(filename))
      write(22,10) 'Input data for program "city_generator"'
      write(22,*) 
      write(22,10) 'Type'
      write(22,*) 4
      write(22,10) 'Number of floors'
      write(22,*) Nfloor
      write(22,10) 'Height of a floor [m]'
      write(22,*) floor_height
      write(22,10) 'Thickness of internal walls [m]'
      write(22,*) e_in
      write(22,10) 'Thickness of external walls [m]'
      write(22,*) e_ext
c      write(22,10) 'Position (X/Y separated by a single space) [m]'
c      write(22,*) (position(j),j=1,dim-1)
      write(22,10) 'rotation angle [deg]'
      write(22,*) angle
      write(22,10) 'Fraction of the wall used for windows [0-1]'
      write(22,*) fwall
      write(22,10) 'Fraction of the floor height used by windows [0-1]'
      write(22,*) ffloor
      write(22,10) 'Thickness of the glass [mm]'
      write(22,*) e_glass
      write(22,10) 'Height of the roof [m]'
      write(22,*) roof_height
      write(22,10) 'Thickness of the roof [m]'
      write(22,*) roof_thickness
      write(22,10) 'Material for internal walls'
      write(22,10) trim(internal_walls_mat)
      write(22,10) 'Material for external walls'
      write(22,10) trim(external_walls_mat)
      write(22,10) 'Material for glass panes'
      write(22,10) trim(glass_panes_mat)
      write(22,10) 'Material for the roof'
      write(22,10)  trim(roof_mat)
      do icontour=1,Ncontour
         if (icontour.eq.1) then
            write(22,10) 'External contour track (X/Y separated by a '
     &           //'single space, anti-clockwise)'
         else
            write(22,32) 'Hole ',icontour-1,
     &           ' track [X/Y separated by a single '
     &           //'space, anti-clockwise)'
         endif                  ! icontour=1
         do i=1,Nppc(icontour)
            write(22,*) (contour(icontour,i,j),j=1,dim-1)
         enddo                  ! i
      enddo                     ! icontour
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end
      


      subroutine record_gnuplot_contour(filename,dim,
     &     Ncontour,Nppc,contour)
      implicit none
      include 'max.inc'
c     
c     Purpose: to generate a gnuplot file for visualizing contours
c     
c     Input:
c       + filename: name of the file to generate
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     Output: the requested file
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer i,icontour,j
c     label
      character*(Nchar_mx) label
      label='subroutine record_gnuplot_contour'

      open(22,file=trim(filename))
      do icontour=1,Ncontour
         do i=1,Nppc(icontour)
            write(22,*) (contour(icontour,i,j),j=1,dim-1)
         enddo                  ! i
         write(22,*) (contour(icontour,1,j),j=1,dim-1)
         write(22,*)
      enddo                     ! icontour
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end


      
      subroutine record_sector_contour(filename,dim,
     &     Ncontour,Nppc,contour)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to record the "gra_sector" file for plotting
c     a sector
c     
c     Input:
c       + filename: name of the file to record
c       + dim: dimension of space
c       + Ncontour: number of contours
c       + Nppc: number of points that define each contour
c       + contour: list of coordinates for each contour
c     
c     Output:
c       + the "gra_sector" file
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      integer Ncontour
      integer Nppc(1:Ncontour_mx)
      double precision contour(1:Ncontour_mx,1:Nppc_mx,1:Ndim_mx-1)
c     temp
      integer icontour,i,j
      character*(Nchar_mx) filetmp
      character*3 str3
c     label
      character*(Nchar_mx) label
      label='subroutine record_sector_contour'

      open(11,file=trim(filename))
      write(11,10) 'set term pdf enhanced color font '
     &     //'"Times-Roman,20" linewidth 1 size 10,10'
      write(11,10) 'set output "sector.pdf"'
      write(11,*) 
      write(11,10) 'set size ratio -1'
      write(11,10) 'set nokey'
      do icontour=1,Ncontour
         call num2str3(icontour,str3)
         if (icontour.eq.1) then
            write(11,10) 'plot "./contour'
     &           //trim(str3)
     &           //'.dat" u 1:2 notitle w lines '
     &           //'lt rgb "black" lw 1 \'
         else
            write(11,10) '    ,"./contour'
     &           //trim(str3)
     &           //'.dat" u 1:2 notitle w lines '
     &           //'lt rgb "black" lw 1 \'
         endif
         filetmp='./gnuplot/contour'
     &        //trim(str3)
     &        //'.dat'
         open(12,file=trim(filetmp))
         do i=1,Nppc(icontour)
            write(12,*) (contour(icontour,i,j),j=1,dim-1)
         enddo                  ! i
         write(12,*) (contour(icontour,1,j),j=1,dim-1)
         close(12)
      enddo                     ! icontour
      write(11,*) 
      write(11,10) 'quit'
      close(11)

      return
      end



      subroutine record_gnuplot_script(filename,
     &     dim,mapXsize,mapYsize,generate_river,
     &     Nlevel,Nsector)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Purpose: to generate the gnuplot script for plotting contours
c     
c     Input:
c       + filename: name of the file to generate
c       + dim: dimension of space
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + generate_river: set to true if a river has to be generated
c       + Nlevel: number of sector levels
c       + Nsector: number of sectors per level
c     
c     Output: the requested file
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      double precision mapXsize
      double precision mapYsize
      logical generate_river
      integer Nlevel,Nsector
c     temp
      integer ilevel,isector
      character*3 lev_str3,sec_str3
      character*(Nchar_mx) datafile,line
c     label
      character*(Nchar_mx) label
      label='subroutine record_gnuplot_script'

      open(22,file=trim(filename))
      write(22,10) 'set term pdf enhanced color font "Times-Roman,20"'
     &     //' linewidth 1 size 10,10'
      write(22,10) 'set output "contours.pdf"'
      write(22,10) 
      write(22,10) 'set size ratio -1'
      write(22,10) 'set nokey'
      write(22,41) 'set xrange [',0.0D+0,':',mapXsize,']'
      write(22,41) 'set yrange [',0.0D+0,':',mapYsize,']'
      write(22,10) 
      write(22,10) 'plot \'
      write(22,10) '     "./ground_contour.dat" u 1:2 notitle w lines'
     &     //' lt rgb "black" lw 3 \'
      if (generate_river) then
         write(22,10) '    ,"./river_contour.dat" u 1:2 notitle '
     &        //'w lines lt rgb "blue" lw 1 \'
         write(22,10) '    ,"./bridge_contour.dat" u 1:2 notitle '
     &        //'w lines lt rgb "red" lw 1 \'
      endif                     ! generate_river
      write(22,10) '    ,"./road_contour.dat" u 1:2 notitle '
     &     //'w lines lt rgb "black" lw 1 \'
      write(22,10) '    ,"./central_square_contour.dat" '
     &     //'u 1:2 notitle w lines lt rgb "yellow" lw 1 \'
      do ilevel=1,Nlevel
         call num2str3(ilevel,lev_str3)
         do isector=1,Nsector
            call num2str3(isector,sec_str3)
            datafile='./sector_contour_level'
     &           //trim(lev_str3)
     &           //'_sector'
     &           //trim(sec_str3)
     &           //'.dat'
            line='    ,"'
     &           //trim(datafile)
     &           //'" '
     &           //'u 1:2 notitle w lines lt rgb "black" lw 1 '
            call add_backslash_if_absent(line)
            write(22,10) trim(line)
         enddo                  ! isector
      enddo                     ! ilevel
      write(22,10) 
      write(22,10) 'quit'
      close(22)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end



      subroutine record_MapArray(filename,dim,
     &     NcellsX,NcellsY,MapArray)
      implicit none
      include 'max.inc'
c     
c     Purpose: to record the MapArray into a ascii file
c     that will be used as input data by Gnuplot
c     
c     Input:
c       + filename: name of the file to generate
c       + dim: dimension of space
c       + NcellsX: size of first dimension of 'MapArray'
c       + NcellsY: size of second dimension of 'MapArray'
c       + MapArray: array of cells; a value of T means the cell is occupied
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      integer NcellsX
      integer NcellsY
      logical(1) :: MapArray(NcellsX,NcellsY)
c     temp
      integer i,j
c     label
      character*(Nchar_mx) label
      label='subroutine record_MapArray'

      open(23,file=trim(filename))
      do i=1,NcellsX
         do j=1,NcellsY
            if (MapArray(i,j)) then
               write(23,*) i,j,1.0D+0
            else
               write(23,*) i,j,0.0D+0
            endif
         enddo                  ! j
      enddo                     ! i
      close(23)
      write(*,*) 'File was generated: ',trim(filename)
      
      return
      end



      subroutine record_area_file(filename,dim,
     &     Aground,Ariver,Abridge,Aroad,Adistrict,Aterrain,Astreet,
     &     Abuilding,Acourtyard,Apark,Awah)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c     
c     Pourpose: to record the result file for areas
c     
c     Input:
c       + filemane: name of the file to generate
c       + dim: dimension of space
c       + Aground: area of the ground [m²]
c       + Ariver: area of river(s) [m²]
c       + Abridge: area of bridges [m²]
c       + Aroad: area of roads [m²]
c       + Adistrict: total area of districts [m²]
c       + Aterrain: total area of terrains [m²]
c       + Astreet: total area of streets [m²]
c       + Abuilding: total area of buildings [m²]
c       + Acourtyard: total area of courtyards [m²]
c       + Apark: total area of parks [m²]
c       + Awah: total area of walls and hedges [m²]
c     
c     Output: the "filename" file
c     
c     I/O
      character*(Nchar_mx) filename
      integer dim
      double precision Aground
      double precision Ariver
      double precision Abridge
      double precision Aroad
      double precision Adistrict
      double precision Aterrain
      double precision Astreet
      double precision Abuilding
      double precision Acourtyard
      double precision Apark
      double precision Awah
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine record_area_file'

      open(15,file=trim(filename))
      write(15,10) '# Unit of area: square meters'
      write(15,10) '# Area of ground (includes everything):'
      write(15,*) Aground
      write(15,10) '# Area of rivers:'
      write(15,*) Ariver
      write(15,10) '# Area of bridges:'
      write(15,*) Abridge
      write(15,10) '# Area of roads (including central plazza):'
      write(15,*) Aroad
      write(15,10) '# Area of districts (includes terrains '
     &     //'and streets):'
      write(15,*) Adistrict
      write(15,10) '# Area of terrains (includes buildings, '
     &     //'walls & hedges, courtyards and parks):'
      write(15,*) Aterrain
      write(15,10) '# Area of streets:'
      write(15,*) Astreet
      write(15,10) '# Area of buildings:'
      write(15,*) Abuilding
      write(15,10) '# Area of courtyards:'
      write(15,*) Acourtyard
      write(15,10) '# Area of parks (terrain - walls & hedges):'
      write(15,*) Apark
      write(15,10) '# Area of walls & hedges:'
      write(15,*) Awah
      close(15)
      write(*,*) 'File was generated: ',trim(filename)

      return
      end
