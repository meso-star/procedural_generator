c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_data(datafile,mapXsize,mapYsize,
     &     cell_length,new_seed,use_MapArray,draw_white_bands,
     &     generate_river,river_branch,enable_t4b,
     &     draw_wah,trees_density)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read input data
c     
c     Input:
c       + datafile: name of the data file to read
c     
c     Output:
c       + mapXsize: size of the map along the X-coordinate [m]
c       + mapYsize: size of the map along the Y-coordinate [m]
c       + cell_length: length of a cell [m]
c       + new_seed: set to true if a new seed has to be used
c       + use_MapArray: set to true if cells have to be masked in MapArray
c       + draw_white_bands: true if white bands have to be drawn
c       + generate_river: set to true if a river has to be generated
c       + river_branch: set to true if a river branch has to be generated
c       + enable_t4b: set to true if type 4 buildings have to be enabled
c       + draw_wah: set to true if wall and hedges have to be drawn
c       + trees_density: surface density for trees [#/Ha]
c     
c     I/O
      character*(Nchar_mx) datafile
      double precision mapXsize
      double precision mapYsize
      double precision cell_length
      logical new_seed
      logical use_MapArray
      logical draw_white_bands
      logical generate_river
      logical river_branch
      logical enable_t4b
      logical draw_wah
      double precision trees_density
c     temp
      integer ios,i
c     label
      character*(Nchar_mx) label
      label='program procedural_generator'

      open(11,file=trim(datafile),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found: ',trim(datafile)
         stop
      else
         do i=1,4
            read(11,*)
         enddo                  ! i
         read(11,*) mapXsize
         read(11,*)
         read(11,*) mapYsize
         read(11,*)
         read(11,*) cell_length
         read(11,*)
         read(11,*) new_seed
         read(11,*)
         read(11,*) use_MapArray
         read(11,*)
         read(11,*) draw_white_bands
         do i=1,2
            read(11,*)
         enddo                  ! i
         read(11,*) generate_river
         read(11,*) 
         read(11,*) river_branch
         do i=1,2
            read(11,*)
         enddo                  ! i
         read(11,*) enable_t4b
         do i=1,2
            read(11,*)
         enddo                  ! i
         read(11,*) draw_wah
         do i=1,2
            read(11,*)
         enddo                  ! i
         read(11,*) trees_density
         close(11)
      endif

c     Check for inconsistencies
      if (mapXsize.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input data:'
         write(*,*) 'mapXsize=',mapXsize
         write(*,*) 'should be > 0'
         stop
      endif
      if (mapYsize.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input data:'
         write(*,*) 'mapYsize=',mapYsize
         write(*,*) 'should be > 0'
         stop
      endif
      if (cell_length.le.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input data:'
         write(*,*) 'cell_length=',cell_length
         write(*,*) 'should be > 0'
         stop
      endif
      if (cell_length.gt.mapXsize) then
         call error(label)
         write(*,*) 'Bad input data:'
         write(*,*) 'cell_length=',cell_length
         write(*,*) 'should be < mapXsize=',mapXsize
         stop
      endif
      if (cell_length.gt.mapYsize) then
         call error(label)
         write(*,*) 'Bad input data:'
         write(*,*) 'cell_length=',cell_length
         write(*,*) 'should be < mapYsize=',mapYsize
         stop
      endif
      if (trees_density.lt.0.0D+0) then
         call error(label)
         write(*,*) 'Bad input data:'
         write(*,*) 'trees_density=',trees_density
         write(*,*) 'should be > 0'
         stop
      endif

      return
      end
