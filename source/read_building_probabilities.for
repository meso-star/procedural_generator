c     Copyright (C) 2020 |Meso|Star> (contact@meso-star.com)
      subroutine read_building_probabilities(filename,enable_t4b,
     &     global_p,Nb,btype,cdf)
      implicit none
      include 'max.inc'
c     
c     Purpose: to read a file that defines the probability
c     to draw a building and the probability associated with each building type
c     
c     Input:
c       + filename: name of the file
c       + enable_t4b: set to true if type 4 buildings have to be enabled
c     
c     Output:
c       + global_p: global probability to draw a building
c       + Nb: number of building types
c       + btype: list of building types
c       + cdf: cumulated density function for choosing building type
c     
c     I/O
      character*(Nchar_mx) filename
      logical enable_t4b
      double precision global_p
      integer Nb
      character*(Nchar_mx) btype(1:Nmat_mx)
      double precision cdf(0:Nproba_mx)
c     temp
      integer ios,iostatus,i
      logical keep_looking
      double precision p,fcdf
      character*(Nchar_mx) str
      double precision proba(1:Nmat_mx)
c     label
      character*(Nchar_mx) label
      label='read_building_probabilities'

      open(11,file=trim(filename),status='old',iostat=ios)
      if (ios.ne.0) then
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(filename)
         stop
      else
         do i=1,3
            read(11,*)
         enddo                  ! i
         read(11,*) global_p
         if ((global_p.lt.0.0D+0).or.(global_p.gt.1.0D+0)) then
            call error(label)
            write(*,*) 'global_p=',global_p
            write(*,*) 'should be in the [0,1] range'
            stop
         endif
         do i=1,2
            read(11,*)
         enddo                  ! i
         Nb=0
         keep_looking=.true.
         do while (keep_looking)
            read(11,*,iostat=iostatus) p,str
            if (iostatus.eq.0) then
               Nb=Nb+1
               if (Nb.gt.Nmat_mx) then
                  call error(label)
                  write(*,*) 'Nb=',Nb
                  write(*,*) '> Nmat_mx=',Nmat_mx
                  stop
               endif
               if ((p.lt.0.0D+0).or.(p.gt.1.0D+0)) then
                  call error(label)
                  write(*,*) 'In file: ',trim(filename)
                  write(*,*) 'p=',p
                  write(*,*) 'should be in the [0,1] range'
                  stop
               endif
               if ((trim(str).eq.'type4').and.(.not.enable_t4b)) then
                  p=0.0D+0
               endif
               proba(Nb)=p
               btype(Nb)=trim(str)
            else
               keep_looking=.false.
            endif
         enddo                  ! while (keep_looking)
      endif
      close(11)

      if (Nb.gt.Nproba_mx) then
         call error(label)
         write(*,*) 'Nb=',Nb
         write(*,*) '> Nproba_mx=',Nproba_mx
         stop
      else
         cdf(0)=0.0D+0
         do i=1,Nb
            cdf(i)=cdf(i-1)+proba(i)
         enddo                  ! i
         fcdf=cdf(Nb)
         if (fcdf.ne.1.0D+0) then
            do i=1,Nb
               cdf(i)=cdf(i)/fcdf
            enddo               ! i
         endif                  ! fcdf>1
      endif                     ! Nmat>Nproba_mx
      
      return
      end
