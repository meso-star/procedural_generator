      program main
      implicit none
      include 'max.inc'
c     
c     Purpose: to test the correct execution of the
c     "read_distribution_options()" subroutine
c
c     I/O
      character*(Nchar_mx) datafile
c     label
      character*(Nchar_mx) label
      label='program main'

      datafile='./distribution_options.in'
      call read_distribution_options(datafile)

      end
