	integer Ndim_mx
	integer*8 Nnode_mx
	integer Nbuilding_mx
	integer Nwindow_per_building_mx
	integer Nobj_mx
	integer Np_mx
	integer Nnodepercell_mx
	integer Ncell_mx
	integer Nface_mx
	integer Nfacepercell_mx
	integer Ncellpernode_mx
	integer Ncellpervoxel_mx
	integer Nbfaces_mx
        integer Nv_so_mx
        integer Nf_so_mx
	integer Nv_mx
	integer Nf_mx
        integer Nvox
	integer Nvox_mx
	integer Nvox_in_cell_mx
	integer Nnodes_in_voxel_mx

	integer Nchar_mx

	parameter(Ndim_mx=4)
	parameter(Nnode_mx=1000000)
	parameter(Nbuilding_mx=1000)
	parameter(Nwindow_per_building_mx=1000)
	parameter(Nobj_mx=100000)
	parameter(Nnodepercell_mx=4)
	parameter(Ncell_mx=5000000)
	parameter(Nface_mx=10000000)
	parameter(Nfacepercell_mx=4)
	parameter(Ncellpernode_mx=100)
	parameter(Nbfaces_mx=200000)
        parameter(Nv_so_mx=100000)
        parameter(Nf_so_mx=150000)
        parameter(Nv_mx=1000000)
        parameter(Nf_mx=1000000)
	parameter(Np_mx=Ncell_mx*Nfacepercell_mx)
	parameter(Ncellpervoxel_mx=1000000)
        parameter(Nvox=150)
        parameter(Nvox_mx=Nvox**Ndim_mx)
	parameter(Nnodes_in_voxel_mx=100)
	parameter(Nvox_in_cell_mx=10000)

	parameter(Nchar_mx=1000)
