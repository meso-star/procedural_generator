      subroutine parse_obj(data_file,
     &     usemtl,mtl_file,Ngroup,group_name,mtl_name,group_lindex)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to parse a given obj file and extract only vertices and faces definitions
c     
c     Input:
c       + data_file: input data file
c
c     Output:
c       + usemtl: T if a material file is used
c       + mtl_file: name of the material file is one is used
c       + Ngroup: number of groups (0 if none)
c       + group_name: name of each group
c       + mtl_name: material used for each group (if usemtl=T)
c       + group_lindex: first and last line index for each group
c
c     I/O
      character*(Nchar_mx) data_file
      logical usemtl
      character*(Nchar_mx) mtl_file
      integer Ngroup
      character*(Nchar_mx) group_name(1:Ngroup_mx)
      character*(Nchar_mx) mtl_name(1:Ngroup_mx)
      integer group_lindex(1:Ngroup_mx,1:2)
c     temp
      integer ios,iline,Nlines,igroup,usemtl_lindex,j
      logical group_lindex_set(1:Ngroup_mx,1:2)
      character*(Nchar_mx) line
      logical file_exist,keep_looking,firstl_found,lastl_found
      character*(Nchar_mx) tmp_file
c     label
      character*(Nchar_mx) label
      label='subroutine parse_obj'

      call get_nlines(data_file,Nlines)
      
      open(11,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'file could not be found:'
         write(*,*) trim(data_file)
         stop
      else
         write(*,*) 'reading file: ',trim(data_file)
         usemtl=.false.
         do iline=1,Nlines
            read(11,10) line
            if (line(1:6).eq.'mtllib') then
               usemtl=.true.
               mtl_file=line(8:len_trim(line))
               usemtl_lindex=iline
               goto 111
            endif
         enddo                  ! iline
 111     continue
         if (usemtl) then
            write(*,*) 'Uses material file: ',trim(mtl_file)
            tmp_file='./data/'//trim(mtl_file)
            inquire(file=trim(tmp_file),exist=file_exist)
            if (file_exist) then
               write(*,*) 'That was found in the /data directory'
            else
               write(*,*) 'That could not be found'
            endif
         else
            write(*,*) 'Does not use materials'
         endif
      endif
      Ngroup=0
      do igroup=1,Ngroup_mx
         do j=1,2
            group_lindex_set(igroup,j)=.false.
         enddo                  ! j
      enddo                     ! igroup
      rewind(11)
c     
      if (usemtl) then
         igroup=0
         do iline=1,Nlines
            read(11,10) line
            if (line(1:2).eq.'g ') then
               if (igroup.gt.0) then
                  if (.not.group_lindex_set(igroup,2)) then
                     group_lindex(igroup,2)=iline-1
                     group_lindex_set(igroup,2)=.true.
                  endif         ! group_lindex_set(igroup,2)=F
               endif            ! igroup>0
               if (igroup+1.le.Ngroup_mx) then
                  group_name(igroup+1)=line(3:len_trim(line))
               endif
            endif
            if (line(1:6).eq.'usemtl') then
               if (igroup.gt.0) then
                  if (.not.group_lindex_set(igroup,2)) then
                     group_lindex(igroup,2)=iline-1
                     group_lindex_set(igroup,2)=.true.
                  endif         ! group_lindex_set(igroup,2)=F
               endif            ! igroup>0
               igroup=igroup+1
               if (igroup.gt.Ngroup_mx) then
                  call error(label)
                  write(*,*) 'Ngroup_mx was reached'
                  stop
               endif
               if (.not.group_lindex_set(igroup,1)) then
                  group_lindex(igroup,1)=iline+1
                  group_lindex_set(igroup,1)=.true.
               endif            ! group_lindex_set(igroup,1)=F
               mtl_name(igroup)=line(8:len_trim(line))
            endif
         enddo                  ! iline
         if (igroup.gt.0) then
            if (.not.group_lindex_set(igroup,2)) then
               group_lindex(igroup,2)=Nlines
               group_lindex_set(igroup,2)=.true.
            endif               ! group_lindex_set(igroup,2)=F
         endif                  ! igroup>0
         Ngroup=igroup
      else                      ! usemtl=F
         Ngroup=1
         keep_looking=.true.
         iline=0
         do while (keep_looking)
            iline=iline+1
            read(11,10) line
            if ((line(1:2).eq.'v ').or.(line(1:2).eq.'f ')) then
               if (.not.group_lindex_set(Ngroup,1)) then
                  group_lindex(Ngroup,1)=iline
                  group_lindex_set(Ngroup,1)=.true.
               endif
            else
               if ((group_lindex_set(Ngroup,1)).and.
     &              (.not.group_lindex_set(Ngroup,2))) then
                  group_lindex(Ngroup,2)=iline-1
                  group_lindex_set(Ngroup,2)=.true.
               endif
            endif
            if (iline.ge.Nlines) then
               keep_looking=.false.
               if ((group_lindex_set(Ngroup,1)).and.
     &              (.not.group_lindex_set(Ngroup,2))) then
                  group_lindex(Ngroup,2)=iline
                  group_lindex_set(Ngroup,2)=.true.
               endif
            endif               ! iline >= Nlines
         enddo                  ! while (keep_looking)
      endif
      close(11)
c     Debug
      write(*,*) 'Ngroup=',Ngroup
      do igroup=1,Ngroup
         write(*,*) 'group index:',igroup
         write(*,*) 'group name: ',trim(group_name(igroup))
         write(*,*) 'material name: ',trim(mtl_name(igroup))
         write(*,*) 'group_lindex=',(group_lindex(igroup,j),j=1,2)
      enddo                     ! igroup
c     Debug

      return
      end
