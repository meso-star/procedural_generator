      subroutine error(label)
      implicit none
      include 'max.inc'
c
c     Purpose: to display the name of the subroutine the error originates from
c
c     Input:
c       + label: name of the subroutine that generates the error
c

c     I/O
      character*(Nchar_mx) label
c     temp
      integer strlen

      write(*,*) 'Error from ',label(1:strlen(label)),' :'

      return
      end



      subroutine warning(label)
      implicit none
      include 'max.inc'
c
c     Purpose: to display the name of the subroutine the warning originates from
c
c     Input:
c       + label: name of the subroutine that generates the error
c

c     I/O
      character*(Nchar_mx) label
c     temp
      integer strlen

      write(*,*) 'Warning from ',label(1:strlen(label)),' :'

      return
      end
