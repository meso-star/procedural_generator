      subroutine dble2str(num,nap,str,err)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert a double precision to a character string;
c     the output character string uses a "+/-0.XXXXE+/-N" convention
c
c     Input:
c       + num: double precision
c       + nap: number of digits after the "." that are converted to character
c     
c     Output:
c       + str: character string
c       + err: false means conversion is OK; a value of 1 means "num"
c                was not recognized.
c

c     I/O
      double precision num
      integer nap
      character*(Nchar_mx) str
      logical err
c     temp
      integer sign
      integer i,j,idx,t
      character*1 str1
      character*3 str3
      integer n
      double precision absnum,tmp,a
      character*(Nchar_mx) nstr
      integer strlen
      character*(Nchar_mx) label
      label='subroutine dble2str'

      if (num.eq.0.0D+0) then
         str='0.00'
         err=.false.
         goto 666
      endif

      if (num.lt.0.0D+0) then
         sign=-1
      else
         sign=1
      endif
      absnum=dabs(num)
      
      err=.false.
      n=0
      tmp=absnum
      if (absnum.gt.1.0D+0) then
         do while (tmp.gt.1.0D+0)
            n=n+1
            tmp=tmp/1.0D+1
c     Debug
c            write(*,*) tmp,n
c     Debug
         enddo
      else if (absnum.lt.1.0D+0) then
         do while (tmp.lt.1.0D+0)
            n=n-1
            tmp=tmp*1.0D+1
         enddo
      else if (absnum.eq.1.0D+0) then
      else
         call error(label)
         write(*,*) 'absnum=',absnum
         stop
      endif
      a=tmp
c     at this point, absnum=a*10^n
c     Debug
c      write(*,*) 'a=',a
c      write(*,*) 'n=',n
c     Debug

      str=''
      idx=int(a)
      write(str1,11) idx
      t=idx*10
      str=str(1:strlen(str))
     &     //str1(1:strlen(str1))
      str=str(1:strlen(str))
     &     //'.'
      do i=1,nap
         idx=int(a*10**i-t)
c     Debug
c         write(*,*) 'i=',i,' idx=',idx
c     Debug
         t=10*(t+idx)
         write(str1,11) idx
         str=str(1:strlen(str))
     &        //str1(1:strlen(str1))
      enddo ! i
c     Debug
c      write(*,*) 'str=',str(1:strlen(str))
c     Debug

      if (n.ne.0) then
         call num2str3(abs(n),str3)
         if (n.lt.0) then
            nstr='-'//str3(1:strlen(str3))
         else
            nstr=str3(1:strlen(str3))
         endif
         str=str(1:strlen(str))
     &        //'E'
     &        //nstr(1:strlen(nstr))
c     &        //'}'
      endif

      if (sign.eq.-1) then
         str="-"//str(1:strlen(str))
      endif
      
 666  continue
      return
      end



      subroutine dble2str_noexp(num,nap,str,err)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to convert a double precision to a character string
c     using a natural "+/-XXXX.XXXXX" convention
c
c     Input:
c       + num: double precision
c       + nap: number of digits after the "." that are converted to character
c     
c     Output:
c       + str: character string
c       + err: false means conversion is OK; a value of 1 means "num"
c                was not recognized.
c

c     I/O
      double precision num
      integer nap
      character*(Nchar_mx) str
      logical err
c     temp
      integer sign
      integer i,j,idx,t
      character*1 str1,zero_ch
      character*3 str3
      integer n
      double precision absnum,tmp,a
      character*(Nchar_mx) nstr
      integer strlen
      character*(Nchar_mx) label
      label='subroutine dble2str_noexp'

      if (num.eq.0.0D+0) then
         str='0.00'
         err=.false.
         goto 666
      endif

      if (num.lt.0.0D+0) then
         sign=-1
      else
         sign=1
      endif
      absnum=dabs(num)
      
      err=.false.
      n=0
      tmp=absnum
      write(zero_ch,11) 0
      if (absnum.gt.1.0D+0) then
         do while (tmp.ge.1.0D+0)
            n=n+1
            tmp=tmp/1.0D+1
c     Debug
c            write(*,*) tmp,n
c     Debug
         enddo
         n=n-1
      else if (absnum.lt.1.0D+0) then
         do while (tmp.lt.1.0D+0)
            n=n-1
            tmp=tmp*1.0D+1
         enddo
      else if (absnum.eq.1.0D+0) then
      else
         call error(label)
         write(*,*) 'absnum=',absnum
         stop
      endif
      a=tmp
c     at this point, absnum=a*10^n
c     Debug
c      write(*,*) 'a=',a
c      write(*,*) 'n=',n
c     Debug

      str=''
      if (n.ge.0) then
         tmp=absnum
         do i=n,0,-1
            idx=tmp/(10**i)
            tmp=tmp-idx*10**i
            write(str1,11) idx
            str=str(1:strlen(str))
     &           //str1(1:strlen(str1))
         enddo                  ! i
         str=str(1:strlen(str))
     &        //'.'
         do i=1,nap
            tmp=tmp*10
            idx=int(tmp)
            tmp=tmp-dble(idx)
            write(str1,11) idx
            str=str(1:strlen(str))
     &           //str1(1:strlen(str1))
         enddo ! i
      else
         str=zero_ch(1:strlen(zero_ch))//'.'
         do i=1,abs(n+1)
            str=str(1:strlen(str))
     &           //zero_ch(1:strlen(zero_ch))
         enddo ! i
         tmp=absnum*10**abs(n+1)
         do i=1,nap
            tmp=tmp*10
            idx=int(tmp)
            tmp=tmp-dble(idx)
            write(str1,11) idx
            str=str(1:strlen(str))
     &           //str1(1:strlen(str1))
         enddo ! i
      endif

      if (sign.eq.-1) then
         str="-"//str(1:strlen(str))
      endif
      
 666  continue
      return
      end
