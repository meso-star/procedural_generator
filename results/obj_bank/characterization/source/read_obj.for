      subroutine read_obj(data_file,dim,
     &     Nv_so,Nf_so,v_so,f_so)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to read a given obj file
c
c     Input:
c       + data_file: obj file to read
c       + dim: dimension of space
c
c     Output:
c       + Nv_so: number of vertices
c       + Nf_so: number of faces
c       + v_so: vertices coordinates
c       + f_so: faces node indexes
c
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
c     temp
      character*(Nchar_mx) ch_line
      integer Nlines,iline
      integer ios
      logical first_v_found,last_v_found
      integer iline_first_v,iline_last_v
      logical first_f_found,last_f_found
      integer iline_first_f,iline_last_f
      logical keep_looking
      character*1 str1
      integer iv,iface,j
c     label
      character*(Nchar_mx) label
      label='subroutine read_obj'

      call get_nlines(data_file,Nlines)
      
      open(11,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'file could not be found:'
         write(*,*) trim(data_file)
         stop
      else
c     Looking for lines that define vertices
         first_v_found=.false.
         last_v_found=.false.
         iline=1
         keep_looking=.true.
         do while (keep_looking)
            read(11,10) ch_line
            if (ch_line(1:1).eq.'v') then ! first character is the definition of a vertex
               if (.not.first_v_found) then
                  first_v_found=.true.
                  iline_first_v=iline
               endif
            else ! not the definition of a vertex
               if ((first_v_found).and.(.not.last_v_found)) then
                  last_v_found=.true.
                  iline_last_v=iline-1
               endif
            endif
            iline=iline+1
            if (iline.gt.Nlines) then
               keep_looking=.false.
            endif
            if ((first_v_found).and.(last_v_found)) then
               keep_looking=.false.
            endif
         enddo                  ! while (keep_looking)
         if (.not.first_v_found) then
            call error(label)
            write(*,*) 'could not identify first vertex line in file:'
            write(*,*) trim(data_file)
            stop
         endif
         if (.not.last_v_found) then
            call error(label)
            write(*,*) 'could not identify last vertex line in file:'
            write(*,*) trim(data_file)
            stop
         endif
c     Debug
c         if (first_v_found) then
c            write(*,*) 'iline_first_v=',iline_first_v
c         endif
c         if (last_v_found) then
c            write(*,*) 'iline_last_v=',iline_last_v
c         endif
c     Debug
c     Looking for lines that define faces
         rewind(11)
         first_f_found=.false.
         last_f_found=.false.
         iline=1
         keep_looking=.true.
         do while (keep_looking)
            read(11,10) ch_line
            if (ch_line(1:1).eq.'f') then ! first character is the definition of a face
               if (.not.first_f_found) then
                  first_f_found=.true.
                  iline_first_f=iline
               endif
            else ! not the definition of a vertex
               if ((first_f_found).and.(.not.last_f_found)) then
                  last_f_found=.true.
                  iline_last_f=iline-1
               endif
            endif
            iline=iline+1
            if (iline.gt.Nlines) then
               if (.not.last_f_found) then
                  last_f_found=.true.
                  iline_last_f=iline-1
               endif
               keep_looking=.false.
            endif
            if ((first_f_found).and.(last_f_found)) then
               keep_looking=.false.
            endif
         enddo                  ! while (keep_looking)
         if (.not.first_f_found) then
            call error(label)
            write(*,*) 'could not identify first face line in file:'
            write(*,*) trim(data_file)
            stop
         endif
         if (.not.last_f_found) then
            call error(label)
            write(*,*) 'could not identify last face line in file:'
            write(*,*) trim(data_file)
            stop
         endif
c     Debug
c         if (first_f_found) then
c            write(*,*) 'iline_first_f=',iline_first_f
c         endif
c         if (last_f_found) then
c            write(*,*) 'iline_last_f=',iline_last_f
c         endif
c     Debug
c
         rewind(11)
c     read preamble
         do iline=1,iline_first_v-1
            read(11,*)
         enddo                  ! iline
c     read vertices
         Nv_so=iline_last_v-iline_first_v+1
         if (Nv_so.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so=',Nv_so
            write(*,*) '> Nv_so_mx=',Nv_so_mx
            stop
         endif
         iv=0
         do iline=1,Nv_so
            iv=iv+1
            read(11,*) str1,(v_so(iv,j),j=1,dim)
         enddo                  ! iline
         do iline=1,iline_first_f-iline_last_v-1
            read(11,*)
         enddo                  ! iline
c     read faces
         Nf_so=iline_last_f-iline_first_f+1
         if (Nf_so.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nf_so=',Nf_so
            write(*,*) '> Nf_so_mx=',Nf_so_mx
            stop
         endif
         iface=0
         do iline=1,Nf_so
            iface=iface+1
c     Debug
c            write(*,*) 'iline=',iline,' iface=',iface
c     Debug
            read(11,*) str1,(f_so(iface,j),j=1,dim)
         enddo                  ! iline
c     
      endif
      close(11)
      
      return
      end
      
