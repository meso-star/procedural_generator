      subroutine read_obj(data_file,dim,Ngroup,group_lindex,igroup,
     &     Nv_so,Nf_so,v_so,f_so)
      implicit none
      include 'max.inc'
      include 'formats.inc'
c
c     Purpose: to read a given obj file
c
c     Input:
c       + data_file: obj file to read
c       + dim: dimension of space
c       + Ngroup: number of groups (0 if none)
c       + group_lindex: first and last line index for each group
c       + igroup: index of the group to read
c
c     Output:
c       + Nv_so: number of vertices
c       + Nf_so: number of faces
c       + v_so: vertices coordinates
c       + f_so: faces node indexes
c
c     I/O
      character*(Nchar_mx) data_file
      integer dim
      integer Ngroup
      integer group_lindex(1:Ngroup_mx,1:2)
      integer igroup
      integer Nv_so,Nf_so
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
c     temp
      character*(Nchar_mx) line
      integer Nlines,iline,Nlf
      integer ios
      logical first_v_found,last_v_found
      integer iline_first_v,iline_last_v
      logical first_f_found,last_f_found
      integer iline_first_f,iline_last_f
      logical keep_looking
      character*1 str1
      integer iv,iface,i,j
      integer ntriangle
      integer fidx1(1:3)
      integer fidx2(1:3)
c     label
      character*(Nchar_mx) label
      label='subroutine read_obj'
c
      call get_nlines(data_file,Nlines)
c      
      open(11,file=trim(data_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'file could not be found:'
         write(*,*) trim(data_file)
         stop
      else
c     Check input data
         if (Ngroup.le.0) then
            call error(label)
            write(*,*) 'Ngroup=',Ngroup
            write(*,*) 'should be > 0'
            stop
         endif
         if (Ngroup.gt.Ngroup_mx) then
            call error(label)
            write(*,*) 'Ngroup=',Ngroup
            write(*,*) 'should be <= Ngroup_mx=',Ngroup_mx
            stop
         endif
         if (igroup.le.0) then
            write(*,*) 'igroup=',igroup
            write(*,*) 'should be > 0'
            stop
         endif
         if (igroup.gt.Ngroup) then
            write(*,*) 'igroup=',igroup
            write(*,*) 'should be <= Ngroup=',Ngroup
            stop
         endif
         do j=1,2
            if (group_lindex(igroup,j).lt.1) then
               call error(label)
               write(*,*) 'group_lindex(',igroup,',',j,')=',
     &              group_lindex(igroup,j)
               write(*,*) 'should be > 0'
               stop
            endif
            if (group_lindex(igroup,j).gt.Nlines) then
               call error(label)
               write(*,*) 'group_lindex(',igroup,',',j,')=',
     &              group_lindex(igroup,j)
               write(*,*) 'should be <= Nlines=',Nlines
               stop
            endif
         enddo                  ! j
         if (group_lindex(igroup,2).le.group_lindex(igroup,1)) then
            call error(label)
            write(*,*) 'group_lindex(',igroup,',2)=',
     &           group_lindex(igroup,2)
            write(*,*) 'should be >'
            write(*,*) 'group_lindex(',igroup,',1)=',
     &           group_lindex(igroup,1)
            stop
         endif
c     Looking for lines that define vertices
         first_v_found=.false.
         last_v_found=.false.
         iline=1
         do i=1,group_lindex(igroup,1)-1
            read(11,*)
            iline=iline+1
         enddo                  ! i
         keep_looking=.true.
         do while (keep_looking)
            read(11,10) line
            if (line(1:2).eq.'v ') then ! first character is the definition of a vertex
               if (.not.first_v_found) then
                  first_v_found=.true.
                  iline_first_v=iline
               endif
            else ! not the definition of a vertex
               if ((first_v_found).and.(.not.last_v_found)) then
                  last_v_found=.true.
                  iline_last_v=iline-1
               endif
            endif
            iline=iline+1
            if (iline.gt.group_lindex(igroup,2)) then
               keep_looking=.false.
            endif
            if ((first_v_found).and.(last_v_found)) then
               keep_looking=.false.
            endif
         enddo                  ! while (keep_looking)
         if (.not.first_v_found) then
            call error(label)
            write(*,*) 'could not identify first vertex line in file:'
            write(*,*) trim(data_file)
            stop
         endif
         if (.not.last_v_found) then
            call error(label)
            write(*,*) 'could not identify last vertex line in file:'
            write(*,*) trim(data_file)
            stop
         endif
c     Debug
c         if (first_v_found) then
c            write(*,*) 'iline_first_v=',iline_first_v
c         endif
c         if (last_v_found) then
c            write(*,*) 'iline_last_v=',iline_last_v
c         endif
c     Debug
c     Looking for lines that define faces
         rewind(11)
         first_f_found=.false.
         last_f_found=.false.
         iline=1
         do i=1,group_lindex(igroup,1)-1
            read(11,*)
            iline=iline+1
         enddo                  ! i
         keep_looking=.true.
         do while (keep_looking)
            read(11,10) line
            if (line(1:2).eq.'f ') then ! first character is the definition of a face
               if (.not.first_f_found) then
                  first_f_found=.true.
                  iline_first_f=iline
               endif
            else ! not the definition of a vertex
               if ((first_f_found).and.(.not.last_f_found)) then
                  last_f_found=.true.
                  iline_last_f=iline-1
               endif
            endif
            iline=iline+1
            if (iline.gt.group_lindex(igroup,2)) then
               if (.not.last_f_found) then
                  last_f_found=.true.
                  iline_last_f=iline-1
               endif
               keep_looking=.false.
            endif
            if ((first_f_found).and.(last_f_found)) then
               keep_looking=.false.
            endif
         enddo                  ! while (keep_looking)
         if (.not.first_f_found) then
            call error(label)
            write(*,*) 'could not identify first face line in file:'
            write(*,*) trim(data_file)
            stop
         endif
         if (.not.last_f_found) then
            call error(label)
            write(*,*) 'could not identify last face line in file:'
            write(*,*) trim(data_file)
            stop
         endif
c     Debug
c         if (first_f_found) then
c            write(*,*) 'iline_first_f=',iline_first_f
c         endif
c         if (last_f_found) then
c            write(*,*) 'iline_last_f=',iline_last_f
c         endif
c     Debug
c
         rewind(11)
c     read preamble
         do iline=1,iline_first_v-1
            read(11,*)
         enddo                  ! iline
c     read vertices
         Nv_so=iline_last_v-iline_first_v+1
         if (Nv_so.gt.Nv_so_mx) then
            call error(label)
            write(*,*) 'Nv_so=',Nv_so
            write(*,*) '> Nv_so_mx=',Nv_so_mx
            stop
         endif
         iv=0
         do iline=1,Nv_so
            iv=iv+1
            read(11,*) str1,(v_so(iv,j),j=1,dim)
         enddo                  ! iline
         do iline=1,iline_first_f-iline_last_v-1
            read(11,*)
         enddo                  ! iline
c     read faces
         Nlf=iline_last_f-iline_first_f+1
         iface=0
         do iline=1,Nlf
            read(11,10) line
            call retrieve_face_indexes(line,ntriangle,fidx1,fidx2)
            if ((ntriangle.eq.1).or.(ntriangle.eq.2)) then
               iface=iface+1
               do j=1,3
                  f_so(iface,j)=fidx1(j)
               enddo            ! j
            endif
            if (ntriangle.eq.2) then
               iface=iface+1
               do j=1,3
                  f_so(iface,j)=fidx2(j)
               enddo            ! j
            endif
         enddo                  ! iline
         Nf_so=iface
         if (Nf_so.gt.Nf_so_mx) then
            call error(label)
            write(*,*) 'Nf_so=',Nf_so
            write(*,*) '> Nf_so_mx=',Nf_so_mx
            stop
         endif            
c     
      endif
      close(11)
      
      return
      end
      


      subroutine retrieve_face_indexes(line,ntriangle,fidx1,fidx2)
      implicit none
      include 'max.inc'
c     
c     Purpose: to analyze a line that contains the definition of a face
c     in order to extract the indexes of the vertex that compose this face
c     
c     Input:
c       + line: character string
c     
c     Output:
c       + ntriangle: number of output triangles (1 or 2)
c       + fidx1: indexes of the 3 vertex for the triangle 1
c       + fidx2: indexes of the 3 vertex for the triangle 2
c     
c     I/O
      character*(Nchar_mx) line
      integer ntriangle
      integer fidx1(1:3)
      integer fidx2(1:3)
c     temp
      integer i,j
      logical cslash
      integer nslash,nspv,slash_index(1:Ncs_mx)
      integer nspace,space_index(1:Ncs_mx)
      character*(Nchar_mx) str
      integer num,err
c     label
      character*(Nchar_mx) label
      label='subroutine retrieve_face_indexes'

c     looking for spaces
      nspace=0
      do i=1,len_trim(line)
         if (line(i:i).eq.' ') then
            nspace=nspace+1
            if (nspace.gt.Ncs_mx) then
               call error(label)
               write(*,*) 'Ncs_mx was reached'
               write(*,*) 'for line:'
               write(*,*) trim(line)
               stop
            else
               space_index(nspace)=i
            endif
         endif                  ! space found
      enddo                     ! i
      if (nspace.eq.3) then
         ntriangle=1
      else if (nspace.eq.4) then
         ntriangle=2
      else
         call error(label)
         write(*,*) 'Number of space found:',nspace
         write(*,*) 'in line:'
         write(*,*) trim(line)
         stop
      endif
c
c     Looking for slashes
      cslash=.false.
      do i=1,len_trim(line)
         if (line(i:i).eq.'/') then
            cslash=.true.
            goto 111
         endif
      enddo                     ! i
 111  continue
      if (cslash) then
         nslash=0
         do i=1,len_trim(line)
            if (line(i:i).eq.'/') then
               nslash=nslash+1
               if (nslash.gt.Ncs_mx) then
                  call error(label)
                  write(*,*) 'Ncs_mx was reached'
                  write(*,*) 'for line:'
                  write(*,*) trim(line)
                  stop
               else
                  slash_index(nslash)=i
               endif
            endif               ! slash found
         enddo                  ! i
         if (ntriangle.eq.1) then
            if (nslash.eq.3) then
               nspv=1
            else if (nslash.eq.6) then
               nspv=2
            else
               call error(label)
               write(*,*) 'ntriangle=',ntriangle
               write(*,*) 'Number of slash found:',nslash
               write(*,*) 'in line:'
               write(*,*) trim(line)
               stop
            endif
         endif                  ! ntriangle=1
         if (ntriangle.eq.2) then
            if (nslash.eq.4) then
               nspv=1
            else if (nslash.eq.8) then
               nspv=2
            else
               call error(label)
               write(*,*) 'ntriangle=',ntriangle
               write(*,*) 'Number of slash found:',nslash
               write(*,*) 'in line:'
               write(*,*) trim(line)
               stop
            endif
         endif                  ! ntriangle=2
      endif                     ! cslash

c     Identification of vertexes
      if (cslash) then
c     Triangle 1
         if ((ntriangle.eq.1).or.(ntriangle.eq.2)) then
            str=line(space_index(1)+1:slash_index(1)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx1(1)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(2)+1:slash_index(1+nspv)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx1(2)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(3)+1:slash_index(1+2*nspv)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx1(3)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
         endif ! ntriangle= 1 or 2
c     Triangle 2
         if (ntriangle.eq.2) then
            str=line(space_index(1)+1:slash_index(1)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx2(1)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(3)+1:slash_index(1+2*nspv)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx2(2)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(4)+1:slash_index(1+3*nspv)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx2(3)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
         endif                  ! ntriangle=2
c     
      else                      ! cslash=F
         if ((ntriangle.eq.1).or.(ntriangle.eq.2)) then
            str=line(space_index(1)+1:space_index(2)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx1(1)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(2)+1:space_index(3)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx1(2)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(3)+1:len_trim(line))
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx1(3)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
         endif                  ! ntriangle= 1 or 2
         if (ntriangle.eq.2) then
            str=line(space_index(1)+1:space_index(2)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx2(1)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(3)+1:space_index(4)-1)
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx2(2)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
            str=line(space_index(4)+1:len_trim(line))
            call str2num(str,num,err)
            if (err.eq.0) then
               fidx2(3)=num
            else
               call error(label)
               write(*,*) 'could not convert to numeric: str=',trim(str)
               stop
            endif
         endif                  ! ntriangle=2
c     
      endif
      
      return
      end
