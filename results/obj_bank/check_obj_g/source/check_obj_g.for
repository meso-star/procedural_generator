      program check_obj_g
      implicit none
      include 'max.inc'
      include 'param.inc'
c      
c     Purpose: general purpose program to check various aspects
c     on a given obj mesh
c     This version works with .obj files that contain groups
c      
c     Variables
      integer dim
      character*(Nchar_mx) datafile
      character*(Nchar_mx) obj_file
      double precision displacement(1:Ndim_mx)
      double precision rotation(1:Ndim_mx)
      character*(Nchar_mx) data_file,temp_file,output_file
      logical usemtl
      character*(Nchar_mx) mtl_file
      integer Ngroup
      character*(Nchar_mx) group_name(1:Ngroup_mx)
      character*(Nchar_mx) mtl_name(1:Ngroup_mx)
      integer group_lindex(1:Ngroup_mx,1:2)
      integer Nv,Nf
      double precision vertices(1:Nv_mx,1:Ndim_mx)
      integer faces(1:Nf_mx,1:3)
      integer Nv_so,Nf_so,Nv1
      double precision v_so(1:Nv_so_mx,1:Ndim_mx)
      integer f_so(1:Nf_so_mx,1:3)
      integer iv,i,j,iface,igroup
      double precision extrema(1:Ndim_mx,1:2)
      double precision alpha
      double precision axe(1:Ndim_mx)
      double precision M(1:Ndim_mx,1:Ndim_mx)
      logical invert_normal
      character*(Nchar_mx) mtl_file_in,mtl_file_out,command
      logical mtl_file_found
      double precision lmax,scale
c     label
      character*(Nchar_mx) label
      label='program check_obj'

      dim=3
      datafile='./data.in'
      call read_data(dim,datafile,
     &     obj_file,displacement,rotation)
      
      Nv=0
      Nf=0
      data_file='./data/'//trim(obj_file)
      temp_file='./data/temp.obj'
c     Parsing: find number of groups if any, materials file name, group names, etc
      call parse_obj(data_file,
     &     usemtl,mtl_file,Ngroup,group_name,mtl_name,group_lindex)
c     
c     ---------------------------------------------------------------
c     First pass: concatenate all final groups (after rotation/move)
c     so that the vertical extension can be known
c     ---------------------------------------------------------------
      do igroup=1,Ngroup
         call read_obj(data_file,dim,Ngroup,group_lindex,igroup,
     &        Nv_so,Nf_so,v_so,f_so)
c     move and rotate object
         do j=1,dim
c     Rotation first
            if (rotation(j).ne.0.0D+0) then
               do i=1,dim
                  axe(i)=0.0D+0
               enddo            ! i
               axe(j)=1.0D+0
               alpha=rotation(j)*pi/180.0D+0
               call rotation_matrix(dim,alpha,axe,M)
               call rotate_obj(dim,Nv_so,v_so,M)
            endif               ! rotation(j).ne.0
         enddo                  ! j
c     Then displacement
         call move_obj(dim,Nv_so,v_so,displacement)
c     add to global object
         call add2obj(dim,
     &        Nv,Nf,vertices,faces,
     &        invert_normal,
     &        Nv_so,Nf_so,v_so,f_so)
      enddo                     ! igroup
c     ---------------------------------------------------------------
c     Identify Z-extension
c     ---------------------------------------------------------------
      do i=1,2
         extrema(3,i)=vertices(1,3)
      enddo                     ! i
      do iv=1,Nv
         if (vertices(iv,3).lt.extrema(3,1)) then
            extrema(3,1)=vertices(iv,3)
         endif
         if (vertices(iv,3).gt.extrema(3,2)) then
            extrema(3,2)=vertices(iv,3)
         endif
      enddo                     ! iv
      lmax=extrema(3,2)-extrema(3,1)
c     normalization scale
      if (lmax.le.0.0D+0) then
         scale=1.0D+0
      else
         scale=1.0D+0/lmax
      endif
c     ---------------------------------------------------------------
c     Second pass: do all over again, but now the vertical scaling
c     constant is known
c     ---------------------------------------------------------------
c     Output file
      output_file='./results/'//trim(obj_file)
      invert_normal=.false.
      if (usemtl) then
c     Record header of file
         call record_group(dim,output_file,invert_normal,
     &        usemtl,mtl_file,Ngroup,group_name,mtl_name,0,0,
     &        Nv,Nf,vertices,faces)
      endif
      do igroup=1,Ngroup
         call read_obj(data_file,dim,Ngroup,group_lindex,igroup,
     &        Nv_so,Nf_so,v_so,f_so)
         if (igroup.eq.1) then
            Nv1=Nv_so
         endif
c     move and rotate object
         do j=1,dim
c     Rotation first
            if (rotation(j).ne.0.0D+0) then
               do i=1,dim
                  axe(i)=0.0D+0
               enddo            ! i
               axe(j)=1.0D+0
               alpha=rotation(j)*pi/180.0D+0
               call rotation_matrix(dim,alpha,axe,M)
               call rotate_obj(dim,Nv_so,v_so,M)
            endif               ! rotation(j).ne.0
         enddo                  ! j
c     Then displacement
         call move_obj(dim,Nv_so,v_so,displacement)
c     Then Z-scaling
         call scale_obj(dim,Nv_so,v_so,scale)
c     record group
         call record_group(dim,output_file,invert_normal,
     &        usemtl,mtl_file,Ngroup,group_name,mtl_name,igroup,Nv1,
     &        Nv_so,Nf_so,v_so,f_so)
      enddo                     ! igroup
c     Copy mtl file if found
      mtl_file_in='./data/'//trim(mtl_file)
      inquire(file=trim(mtl_file_in),exist=mtl_file_found)
      if (mtl_file_found) then
         mtl_file_out='./results/'//trim(mtl_file)
         command='cp '//trim(mtl_file_in)//' '//trim(mtl_file_out)
         call exec(command)
      endif
c     Final message
      write(*,*) 'Output file was generated:'
      write(*,*) trim(output_file)
c     
      end
      
